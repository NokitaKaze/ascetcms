<?php
    $sad_options = (object)array(
        'database'                           => (object)array(
            'host'     => '127.0.0.1',
            'db'       => 'test',
            'login'    => 'root',
            'password' => '',
            'prefix'   => 'sad_',
            'dirty_transactions_methods' => true,
            'table_prefix_change'        => true
        ),
        'database_debug'                     => (object)array(
            'host'     => '127.0.0.1',
            'db'       => 'test',
            'login'    => 'root',
            'password' => '',
            'prefix'   => 'sad_',
            'insert_query_strategy' => AscetPDO::DEBUG_INSERT_QUERY_AFTER,
            'dirty_transactions_methods' => false
        ),

        'database_connection_strategy'       => AscetCMSEngine::DATABASE_CONNECTION_STRATEGY_BEFORE,       // Как инициализировать SQL
        'database_connection_error_strategy' => AscetCMSEngine::DATABASE_CONNECTION_ERROR_STRATEGY_MESSAGE,// Как реагировать на ошибки подключения sql
        'database_error_strategy'            => AscetCMSEngine::DATABASE_ERROR_STRATEGY_EXCEPTION,         // Как реагировать на ошибки исполнения sql
        'database_transaction_start'         => true,            // Запускать ли транзакции автоматом
        'database_transaction_level'         => 'read committed',// Уровень изоляции транзакций

        'hidewtime'                          => true, // hide run  time in the of page; May be set in config.php
        'hidetimestamp'                      => true, // hide timestamp in the of page; May be set in config.php
        'etag_cache'                         => true, // E-tag/Last-modified; May be set in config.php
        'hide_cms'                           => false,// hide all CMS behavior
        'modules_list'                       => array(
            'ascetcmsadminpanel.php'
        ),//
        'error_reporting_level'              => E_ALL ^ E_NOTICE,
        'error_display'                      => false,
        'execution_error_strategy'           => AscetCMSEngine::EXECUTION_ERROR_STRATEGY_MESSAGE,// Как реагировать на ошибки исполнения

        'keyvalue_storage_engine'            => 'File',
        'keyvalue_storage_settings'          => (object)array(
            'storage_type' => iAscetKeyValueStorage::StoragePersistent,
            'region_type'  => iAscetKeyValueStorage::RegionDomain
        ),

        'additional'                         => (object)array(),// Дополнительные опции
        'need_show_profiling_stamps'         => true,// Нужно рисовать Profiling stamps
    );
?>
