/*
   Copyright 2011 BiSe Trojanov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

function hsv2rgb(h,s,v){
 s=s/100;v=v/100;
 var hi=Math.floor(h/60) % 6;
 var f =h/60-Math.floor(h/60);
 var p =v*(1-s);
 var q =v*(1-f*s);
 var t =v*(1-(1-f)*s);

 var r=0;var g=0;var b=0;
 switch (hi){
  case 0:r=v;g=t;b=p;break;
  case 1:r=q;g=v;b=p;break;
  case 2:r=p;g=v;b=t;break;
  case 3:r=p;g=q;b=v;break;
  case 4:r=t;g=p;b=v;break;
  case 5:r=v;g=p;b=q;break;
 }
 return [Math.round(r*255), Math.round(g*255), Math.round(b*255)];
}

function rgb2hsv(r,g,b){
 r=r/255;g=g/255;b=b/255;
 var max=Math.max(r,g,b);
 var min=Math.min(r,g,b);
 var ar =[
  (g-b)/(max-min)*60+0,
  (g-b)/(max-min)*60+360,
  (b-r)/(max-min)*60+120,
  (r-g)/(max-min)*60+240
 ];
 if (max==r && g>=b){var h=ar[0];}
 if (max==r && g< b){var h=ar[1];}
 if (max==g){var h=ar[2];}
 if (max==b){var h=ar[3];}
 if (max==min){var h=0;}

 if (max==0){var s=0;}else{var s=1-(min/max);}
 return [Math.round(h), Math.round(s*100), Math.round(max*100)];
}

function parseHC(text){
 var r =new RegExp('([0-9a-f]{2,2})([0-9a-f]{2,2})([0-9a-f]{2,2})','i');
 var hx=text.match(r);
 if (hx==null){
  var r =new RegExp('([0-9a-f])([0-9a-f])([0-9a-f])','i');
  var hx2=text.match(r);
  if (hx2==null){return null;}
  var hx=[0,hx2[1]+hx2[1], hx2[2]+hx2[2], hx2[3]+hx2[3]];
 }
 return rgb=[parseInt(hx[1],16)*1, parseInt(hx[2],16)*1, parseInt(hx[3],16)*1];
}

function createHC(r,g,b){
 var c=(r << 16) | (g << 8) | b;
 s=c.toString(16);
 while (s.length<6){s='0'+s;}
 return s;
}