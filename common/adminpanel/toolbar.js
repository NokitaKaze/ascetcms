/*
   Copyright 2012 BiSe Trojanov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
function toolbarover(obj){
 if (!hidtag[obj.tag].over){
  $('#hid_'+obj.tag).css({'display':'block', 'height': 3});
  $('#hid_'+obj.tag).animate({'height':$('#hid_'+obj.tag)[0].scrollHeight},300);
 }
 hidtag[obj.tag]={'over':true, 'out':false};
}

function toolbarout(obj){
 if ( hidtag[obj.tag].out){ return;}
 if (!hidtag[obj.tag].over){return;}
 
 setTimeout('toolbarbye("'+obj.tag+'")',1000);
 hidtag[obj.tag].out=true;
}

function toolbarbye(tag){
 if (!hidtag[tag].out){return;}
 hidtag[tag]={'over':false, 'out':false};
 $('#hid_'+tag).fadeOut(300);
}

var hidtag={};
$('ul.toolbar a').each(function (){
 $(this)[0].tag=$(this)[0].id;
 $('#hid_'+$(this)[0].id)[0].tag=$(this)[0].id;
 hidtag[$(this)[0].id]={'out':false, 'over':false};
 
 var w=8;var h=$(this).height()-3;var p=$(this)[0];
 while (p!==null){w+=p.offsetLeft; h+=p.offsetTop; p =p.offsetParent;}
 $('#hid_'+$(this)[0].id).css({'left':w, 'top':h});
});