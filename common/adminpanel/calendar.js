/*
   Copyright 2011 BiSe Trojanov

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
function getMaxDays(year, month){
 if ((month==1)||(month==3)||(month==5)||(month==7)||(month==8)||(month==10)||(month==12)){return 31;}
 if ((month==4)||(month==6)||(month==9)||(month==11)){return 30;}
 if (year % 100==0){
  if ((year/100) % 4==0){return 29;}else{return 28;}
 }else{
  if (year % 4==0){return 29;}else{return 28;}
 }
}

function Calendar_choose(id, name, y, m, d){
 var date=new Date($('#hid'+id)[0].value*1000);
 date.setFullYear(y);
 date.setMonth(m-1);
 date.setDate(d);
 $('#hid'+id)[0].value=Math.floor(date.getTime()/1000);
 $('#divcal'+id).html(DrawCalendar_sub(id, name, Math.floor(date.getTime()/1000)));
 RedrawTime(id);
}

function Calendar_Close(){
 $('.calendar .hidden').fadeOut(400);
}

function Calendar_Open(id){
 $('#divhid'+id).fadeIn(400);
}

function Time_choose(id, name){
 var date= new Date($('#hid'+id)[0].value*1000);
 var re  = new RegExp('^([0-9]+).([0-9]+).([0-9]+)$');
 var m   = re.exec($('#his'+id)[0].value);
 if (m!==null){
  date.setHours(m[1]);
  date.setMinutes(m[2]);
  date.setSeconds(m[3]);
  var r=(date.getTime()/1000);
  if (r!==Math.round($('#hid'+id)[0].value)){
   $('#hid'+id)[0].value=r;
   RedrawTime(id);
  }
 }
 setTimeout('Time_choose("'+id+'", "'+name+'");',100);
}

function RedrawTime(id){
 var date= new Date($('#hid'+id)[0].value*1000);
 var s=date.getFullYear()+'-';
 var m =date.getMonth()+1;
 if (m<10){s+='0';}s+=m+'-';
 if (date.getDate()<10){s+='0';}s+=date.getDate()+' '+date.getHours()+':';
 if (date.getMinutes()<10){s+='0';}s+=date.getMinutes()+':';
 if (date.getSeconds()<10){s+='0';}s+=date.getSeconds();

 $('#el'+id)[0].value=s;
}

function DrawCalendar_sub(id, name, time, y2, m2){
 var date=new Date(time*1000);
 var y =date.getFullYear();
 var m =date.getMonth()+1;
 var d =date.getDate();

 if (y2==undefined){
  y2=y;m2=m;
  var dow=(date.getDay()+35+1-d) % 7;
 }else{
  if (m2==0){m2=12;y2--;}else{
   if (m2==13){m2=1;y2++;}
  }
  var dow=new Date(y2,m2-1,1,12).getDay();
 }
 if (dow==0){dow=7;}

 var gmt=-1*date.getTimezoneOffset();
 var buf='<input type="hidden" value="'+time+'" name="'+name+'" id="hid'+id+
         '"><input id="el'+id+'" onclick="Calendar_Open(\''+id+
         '\');" title="Date UTC';
 if (gmt<0){buf+='-';}else{buf+='+';}
 t_h=Math.floor(gmt/60);if (t_h<10){buf+='0';}buf+=t_h;
 t_h=gmt % 60;if (t_h<10){buf+='0';}
 buf+=t_h+'" readonly><div class="hidden" id="divhid'+id+'"><input class="his" id="his'+
     id+'" value="'+date.getHours()+':';
 if (date.getMinutes()<10){buf+='0';}buf+=date.getMinutes()+':';
 if (date.getSeconds()<10){buf+='0';}buf+=date.getSeconds()+'"><table><tr class="header"><td colspan="7"><div style="position: relative;"><ul style="left: 5px; "><li style="margin-right: 5px;"><a href="javascript:" onclick="$(\'#divcal'+id+'\').html(DrawCalendar_sub(\''+id+'\', \''+name+'\', '+time+', '+(y2-1)+', '+m2+')); RedrawTime(\''+id+'\');" title="A year ago">&lt;&lt;</a></li><li><a href="javascript:" onclick="$(\'#divcal'+id+'\').html(DrawCalendar_sub(\''+id+'\', \''+name+'\', '+time+', '+y2+', '+(m2-1)+')); RedrawTime(\''+id+'\');"  title="A month ago">&lt;</a></li></ul><span>'+m2+' '+y2+'</span><ul style="right: 5px;"><li style="margin-right: 5px;"><a href="javascript:" onclick="$(\'#divcal'+id+'\').html(DrawCalendar_sub(\''+id+'\', \''+name+'\', '+time+', '+y2+', '+(m2+1)+')); RedrawTime(\''+id+'\');" title="A month later">&gt;</a></li><li><a href="javascript:" onclick="$(\'#divcal'+id+'\').html(DrawCalendar_sub(\''+id+'\', \''+name+'\', '+time+', '+(y2+1)+', '+m2+')); RedrawTime(\''+id+'\');" title="A year later">&gt;&gt;</a></li></ul></div></td></tr><tr class="line">';
 if (dow>1){buf+='<td colspan="'+(dow-1)+'"></td>';}
 for (var t_d=1;t_d<=getMaxDays(y2, m2);t_d++){
  buf+='<td';
  if ((t_d==d)&&(y2==y)&&(m2==m)){buf+=' class="current"';}
  buf+='><a href="javascript:" onclick="Calendar_choose(\''+id+'\', \''+name+
       '\', '+y2+', '+m2+', '+t_d+');">'+t_d+'</a></td>';dow++;
  if (dow==8){dow=1;buf+='</tr><tr class="line">';}
 }

 buf+='</tr></table><a href="javascript:" onclick="Calendar_Close();" class="close_button">Close</a></div>';

 return buf;
}

function DrawCalendar(id, name, time){
 document.write('<div class="calendar" id="divcal'+id+'"></div>');
 $('#divcal'+id).html(DrawCalendar_sub(id, name,time));
 RedrawTime(id);
 setTimeout('Time_choose("'+id+'", "'+name+'");',1000);
}
