<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', $sad_domain.' / Visits');

    ob_start();
    $q = sql_query('SELECT *, INET_NTOA(`ip`) AS `ip1` FROM `____visits`
WHERE `time`>=UNIX_TIMESTAMP()-10*24*3600 ORDER BY `time` DESC');
    echo '<table><tr class="head"><td>time</td><td>ip</td><td>uri</td><td>referer</td><td>useragent</td></tr>';
    while ($q_c = sql_fetch($q)) {
        echo '<tr><td><nobr>'.date('Y-m-d H:i:sO', $q_c['time']).'</nobr></td><td><span ';
        echo '>'.$q_c['ip1'].'</span></td>'.
             '<td>'.sad_safe_html($q_c['requesturi']).'</td><td><a href="'.
             sad_safe_html($q_c['referer']).'" target="_blank">'.
             sad_safe_html(preg_replace('|^https?://((www\\.)?[a-z0-9.-]+).*$|i',
                 '\1', $q_c['referer'])).'</a></td>'.
             '<td style="';
        if ((int) $q_c['is_bot'] > 0) {
            echo 'color: orange;';
        }
        echo '"><nobr>'.sad_safe_html($q_c['useragent']).'</nobr></td></tr>';
    }
    echo '</table>';
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);
?>