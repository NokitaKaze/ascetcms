<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if (!class_exists('AscetCMS\\Helper\\Image')) {
        echo 'Class AscetCMS\\Helper\\Image not found</body></html>';

        return;
    }
    if (!function_exists('imagecreatetruecolor')) {
        echo 'Module GD not found</body></html>';

        return;
    }

    AscetCMSEngine::set_template('adminpanel_pictures.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['pictures']['title'].' / '.$sad_domain);
    echo '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['pictures']['infobox'].'</div>';

    $html2 = '';

    $t1 = explode('/', $_POST['destdir']);
    $t2 = $sad_root.'/storage/';
    foreach ($t1 as $t3) {
        if (($t3 === '.') or ($t3 === '..') or ($t3 === '')) {
            continue;
        }
        $t2 .= $t3.'/';
        if (!file_exists($t2)) {
            mkdir($t2);
            $html2 .= 'Folder <strong>'.$t3.'</strong> created<br/>';
        }
    }

    foreach ($_POST as $key => $value) {
        if (!preg_match('|^file_[0-9]+$|', $key)) {
            continue;
        }

        $old = $sad_root.'/storage/'.sad_safe_path($_POST['sourcedir']).'/'.$value;
        $sha512 = hash_file('sha512', $old);
        $ext = preg_replace('|^.*\\.([a-z]+)$|', '$1', $value);
        $new = $t2.$sha512.'.'.$ext;
        $new2 = $t2.'.thumbs/1_'.$sha512.'.'.$ext;

        if ($old !== $new) {
            copy($old, $new);
        }
        $t = AscetCMS\Helper\Image::image_resize($new, $new2, $_POST['width'], $_POST['height']);
        $html2 .= '<br /><a href="'.$sad_baseurl.'/storage/'.$_POST['destdir'].'/.thumbs/1_'.
                  $sha512.'.'.$ext.'" target="_blank">'.$new2.'</a> <b>'.filesize($new2).
                  '</b> bytes';
    }
    unset($t, $t1, $t2, $t3);
    AscetCMSEngine::set_prepare('html2', $html2);
?>