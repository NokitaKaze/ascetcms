<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['delete']['title'].' / '.$sad_domain);

    ob_start();
    if ((int) $_GET['id'] > 0 and strlen($_GET['table']) > 0) {
        if (preg_match('|^_users$|i', $_GET['table']) and !AscetCMSEngine::iamd()) {
            AscetCMSEngine::$http_status = 403;
            echo 'No, you can not delete records from admin table</body></html>';

            return;
        }
        if ((int) $_GET['confirm'] > 0) {
            sql_query('DELETE FROM `'.sad_safe_mysql($sad_prefix.$_GET['table']).
                      '` WHERE `id`='.(int) $_GET['id']);
            echo 'Deleted <script>window.close();</script></body></html>';

            return;
        }

        echo preg_replace('|{#id#}|', (int) $_GET['id'], AscetCMSEngine::$options->additional->LANGS['delete']['text']);

        ?>
        <br><a href="?" onclick="window.close();" class="nicebutton"
               style="color: #ccffcc; font-weight: bold;"><?php echo AscetCMSEngine::$options->additional->LANGS['delete']['no']; ?></a>
        <a href="?table=<?php echo urlencode($_GET['table']); ?>&amp;id=<?php echo (int) $_GET['id']; ?>&amp;confirm=1"
           class="nicebutton"
           style="width: 100px; color: #ffcccc;"><?php echo AscetCMSEngine::$options->additional->LANGS['delete']['yes']; ?></a><?php
    } else {
        echo '<script>window.close();</script>';
    }
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);

?>