<?php
    $url = '';
    foreach ($_GET as $key => &$value) {
        if (preg_match('|data_([a-z0-9]+)|i', $key, $a)) {
            $url .= '&data_'.urlencode($a[1]).'='.urlencode($value);
        }
    }
    AscetCMSEngine::$http_status = 302;
    header('location: '.AscetCMSEngine::$sad_baseurl.'/i/edit.php?table='.urlencode($_GET['table']).
           '&id=new&edit='.urlencode($_GET['edit']).'&parent='.urlencode($_GET['parent']).$url);
?>