<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', 'History / '.$sad_domain);

    ob_start();
    echo '<table>
        <tr class="head">
            <td>Title</td>
            <td>Text</td>
            <td>Time</td>
        <tr>';
    $q = sql_query('SELECT * FROM `____history` ORDER BY `time` DESC LIMIT 100');

    while ($q_c = sql_fetch($q)) {
        echo '<tr>
  <td>'.sad_safe_html($q_c['title']).'</td>
  <td>'.sad_safe_html($q_c['text']).'</td>
  <td>'.gmdate('Y-m-d\TH:i:sO', $q_c['time']).'</td>
 <tr>';
    }
    echo '</table>';
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);
?>