<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if (!AscetCMSEngine::iamd()) {
        AscetCMSEngine::$http_status = 403;
        echo 'Sorry. For developer only.</body></html>';

        return;
    }

    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', 'Edit Code / '.$sad_domain);
    AscetCMSEngine::set_prepare('head1',
        '<link rel="stylesheet" href="'.$sad_baseurl.'/common/adminpanel/edit.css" type="text/css" media="screen">');

    ob_start();
    $f1 = sad_safe_path($_POST['file']);
    $fullpath = $sad_root.'/templates';
    foreach (explode('/', preg_replace('|/[^/]*$|', '', $f1)) as $s) {
        $fullpath .= '/'.$s;
        if (!file_exists($fullpath)) {
            mkdir($fullpath);
            chmod($fullpath, 0700);
        }
    }
    unset($fullpath, $s);
    if (strlen($f1) > 0) {
        $file = $sad_root.'/templates/'.$f1;

        $file_put = file_put_contents($file, $_POST['buf'], LOCK_EX);
        if (($file_put === null) or ($file_put === false)) {
            echo '<span style="color: red">Can not save file</span><br/>';
        } else {
            echo '<span style="color: green">File saved</span><br/>';
        }
    } else {
        $f1 = sad_safe_path($_GET['folder'].'/'.$_GET['file']);
        $file = $sad_root.'/templates/'.$f1;
    }
    echo $file;
    AscetCMSEngine::error_reporting(~E_ALL);

    $special = false;
    echo '<script>document.title="'.sad_safe_mysql(preg_replace('_^.+?/([^/]+)$_', '$1', $f1)).
         '"+" / "+document.title;</script>';
?>
    <form method="post" action="?file=<?php echo urlencode($f1); ?>">
        <?php
            if (preg_match('|^buttons/|', $f1)) {
                $special = true;
                ?>

                <span style="float: right">
  <a href="javascript:" onclick="but_add();"><img src="<?php echo $sad_baseurl; ?>/common/adminpanel/plus.png"
                                                  style="border: none;"></a><br/>
  <a href="javascript:" onclick="but_sort();"><img src="<?php echo $sad_baseurl; ?>/common/adminpanel/arrow-switch.png"
                                                   style="border: none;"></a><br/>
  <a href="javascript:" onclick="but_delete();"><img src="<?php echo $sad_baseurl; ?>/common/adminpanel/cross.png"
                                                     style="border: none;"></a><br/>
  <a href="javascript:" onclick="but_edit();"><img src="<?php echo $sad_baseurl; ?>/common/adminpanel/receipt--pencil.png"
                                                   style="border: none;"></a><br/>
  <a href="javascript:" onclick="but_editcode();"><img
          src="<?php echo $sad_baseurl; ?>/common/adminpanel/blue-document-attribute-p.png" style="border: none;"></a><br/>
  <a href="javascript:" onclick="but_options();"><img src="<?php echo $sad_baseurl; ?>/common/adminpanel/wand.png"
                                                      style="border: none;"></a><br/>
 </span>

                <script>
                    function but_add() {
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo '<a href="/i/add.php?table={#table#}&amp;edit=textandtitle&amp;parent=parent" target="_blank" title="Add"><img src="/common/adminpanel/plus.png" style="border: none;" alt=" add "></a>';
                                ?>';
                    }

                    function but_sort() {
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo('<a href="/i/sort.php?table={#table#}&amp;title=title&amp;parent=parent&amp;edit=textandtitle" target="_blank" title="Sort"><img src="/common/adminpanel/arrow-switch.png" style="border: none;" alt=" sort "></a>');
                                ?>';
                    }

                    function but_delete() {
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo('<a href="/i/delete.php?table={#table#}&amp;id={#id#}" target="_blank" title="DELETE"><img src="/common/adminpanel/cross.png" style="border: none;" alt=" delete "></a>');
                                ?>';
                    }

                    function but_edit() {
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo('<a href="/i/edit.php?table={#table#}&amp;id={#id#}&amp;edit=textandtitle" target="_blank" title="Edit"><img src="/common/adminpanel/receipt--pencil.png" style="border: none;" alt=" edit "></a>');
                                ?>';
                    }

                    function but_options() {
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo('<a href="/i/options.php" target="_blank" title="Edit main options"><img src="/common/adminpanel/wand.png" style="border: none;" alt=" Edit main options "></a>');
                                ?>';
                    }

                    function but_editcode() {
                        // @todo Переписать
                        document.getElementById('buf').value += "\r\n" + '<?php
                                echo('<a href="/i/editcode.php?file=<'.'?php echo urlencode($sad_script); ?'.
                                     '>" target="_blank" title="Edit code of this page"><img src="/common/adminpanel/blue-document-attribute-p.png" style="border: none;" alt=" Edit code of this page "></a>');
                                ?>';
                    }
                </script>

            <?php }
            if (preg_match('|^edit/|', $f1)) {
                $special = true;
                ?>
                <span style="float: right; width: 165px;">
  <abbr title="Textbox">fieldname label</abbr><br/><br/>
  <abbr title="Textarea">fieldname [simpletext] label</abbr><br/><br/>
  <abbr title="TinyMCE">fieldname [text] label</abbr><br/><br/>
  <abbr title="Calendar">fieldname [date] label</abbr><br/><br/>
  <abbr title="Checkbox">fieldname [checkbox] label</abbr><br/><br/>
  <abbr title="Image">fieldname [image] {<span style="color: gray">max_width max_height max_size_in_bytes</span> "folder/for/file"
      <span style="color: gray">nohash</span> [<span style="color: gray">"folder/for/thumb" "prefix_for_file" "filetype_png/jpg/gif"</span>
      width_or_auto height_or_auto]} label</abbr><br/><br/>
 </span>

            <?php } ?>

        <textarea style="width: <?php if ($special) {
            echo '700px';
        } else {
            echo '98%';
        } ?>; height: 500px; " id="buf" name="buf"><?php echo sad_safe_html(file_get_contents($file)); ?></textarea>
        <input type="hidden" name="file" value="<?php echo sad_safe_html($f1); ?>"><br>
        <input type="submit" class="nicebutton mrgn" value="Write File">
    </form>
<?php
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);
?>