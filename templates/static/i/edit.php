<?php
    /**
     * Форма редактирования записи
     *
     * @todo Переписать под отдельный шаблон
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if (
        ((int) $_GET['id'] < 1 and $_GET['id'] !== 'new')
        or strlen($_GET['edit']) < 1
        or strlen($_GET['table']) < 1
    ) {
        echo '`edit` is ';
        if (strlen($_GET['edit']) > 0) {
            echo '<b>not</b> ';
        }
        echo 'null<br />';
        echo '`table` is ';
        if (strlen($_GET['table']) > 0) {
            echo '<b>not</b> ';
        }
        echo 'null<br /><script>window.close();</script>';

        return;
    }

    $edit = preg_replace('|[/\\\\]|', '', $_GET['edit']);

    if (!file_exists($sad_root.'/templates/edit/'.$edit)) {
        echo 'Template `'.sad_safe_html($edit).'` from edit does not exist. ';
        if (AscetCMSEngine::iamd()) {
            echo '<a href="'.$sad_baseurl.'/i/editcode.php?file=edit%2F'.urlencode($edit).'" target="_blank">Create</a> it';
        }

        return;
    }

    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['edit']['title'].' / '.$sad_domain);
    $head1 = '<link rel="stylesheet" href="'.$sad_baseurl.'/common/adminpanel/edit.css" type="text/css" media="screen">
 <link rel="stylesheet" href="'.$sad_baseurl.'/common/adminpanel/calendar.css" type="text/css" media="screen">
 <script type="text/javascript" src="'.$sad_baseurl.'/common/adminpanel/calendar.js"></script>';
    AscetCMSEngine::set_prepare('head1', $head1);

    if ((int) $_GET['id'] > 0) {
        $q_c = sql_query_fetch('SELECT * FROM `'.sad_safe_mysql($sad_prefix.$_GET['table']).
                               '` WHERE `id`=?;', array((int) $_GET['id']));
        if ((int) $q_c['id'] === 0) {
            $q_c = array();
            $q_c['sname'] = $_GET['sname'];
            $q_c['parent'] = $_GET['parent'];
        }
    } else {
        $q_c = array();
        $q_c['sname'] = $_GET['sname'];
        $q_c['parent'] = $_GET['parent'];
    }

    /**
     * Заполняем дефолтные значения
     */
    foreach ($_GET as $key => &$value) {
        if (preg_match('|^data_([a-z0-9_]+)|', $key, $a)) {
            $q_c[$a[1]] = $value;
        }
    }
    unset($key, $a, $value);

    ob_start();
    echo '<script src="'.$sad_baseurl.'/common/tinymce/tinymce.min.js" type="text/javascript"></script>';
?>
    <script>
        tinymce.init({
// General options
            mode: "specific_textareas",
            editor_selector: "tinyMCE",
// URL
            convert_urls: false,

            plugins: "code,hr,image,layer,link,lists,media,paste,save,print,preview,table",

            /*
             // Theme options
             theme_advanced_buttons1 : "save,newdocument,code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,styleprops",
             theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,forecolor,backcolor,|,del,ins",
             theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,images",
             theme_advanced_toolbar_location : "top",
             theme_advanced_toolbar_align : "left",
             theme_advanced_statusbar_location : "bottom",
             theme_advanced_resizing : true,
             */

// tags
            extended_valid_elements: 'noindex'
        });

        function show(tab) {
            $('div.form').hide();
            $('#tab' + tab).show();
            $('ul.tabs li').removeClass('active');
            $('#ctab' + tab).addClass('active');
        }

    </script><?php

    //scan for additional templates
    $sc = scandir($sad_root.'/templates/edit/');
    $tabs = array($edit);
    foreach ($sc as &$file) {
        if (preg_match('|^'.sad_safe_reg($edit).'_|i', $file)) {
            $tabs[] = $file;
        }
    }
    unset($sc, $file);

    //Caption
    echo "<ul class=\"tabs\">\r\n";
    foreach ($tabs as $tab) {
        $button = file_get_contents($sad_root.'/templates/edit/'.$tab);
        preg_match('_^<\\?(php)?\\s*(//|#)\\s*(.*?)(\\s*\\?>)?\\s*(\r|\n)_', $button, $a);
        if (strlen($a[3]) < 1) {
            $a[3] = $tab;
        }
        echo '<li id="ctab'.$tab.'" class="active"><a href="javascript:" onclick="show(\''.$tab.
             '\');">'.$a[3]."</a>";
        if (AscetCMSEngine::iamd()) {
            echo '<a href="'.$sad_baseurl.'/i/editcode.php?file=edit%2F'.urlencode($tab).
                 '" target="_blank" title="Edit Template"><img src="'.$sad_baseurl.
                 '/common/adminpanel/receipt--pencil.png" alt="edit template" style="border: none;"></a>';
        }
        echo "</li>\r\n";
    }
    /** @noinspection HtmlUnknownTarget */
    echo '</ul><form method="post" enctype="multipart/form-data" action="'.$sad_baseurl.'/i/edit_post.php">';
    unset($a);

    $used_sname = false;
    $used_parent = false;//for new record
    $image_count = -1;

    //parse templates
    $full_pattern = '';
    foreach ($tabs as $tab) {
        list($t, $image_count, $used_sname, $used_parent) =
            AscetCMSAdminPanel::admin_edit_parse_tab($tab, $q_c, $image_count, $used_sname, $used_parent);
        $full_pattern .= $t;
    }
    unset($t_id, $b_s, $button, $pattern, $a, $tab, $tabs, $t);
    echo $full_pattern;

    echo '<input type="submit" class="mrgn nicebutton" value="';
    if (strtolower($_GET['id']) == 'new') {
        echo AscetCMSEngine::$options->additional->LANGS['edit']['add'];
    } else {
        echo AscetCMSEngine::$options->additional->LANGS['edit']['edit'];
    }
    echo '">';

    if (strtolower($_GET['id']) == 'new' and !$used_sname) {
        echo '<input type="hidden" name="data_sname"  value="'.sad_safe_html($_GET['sname'])."\">\r\n";
    }
    if (strtolower($_GET['id']) == 'new' and !$used_parent) {
        echo '<input type="hidden" name="data_parent" value="'.sad_safe_html($_GET['parent'])."\">\r\n";
    }
?>
    <input type="hidden" name="opt_edit" value="<?php echo sad_safe_html($edit); ?>">
    <input type="hidden" name="opt_table" value="<?php echo sad_safe_html($_GET['table']); ?>">
    <input type="hidden" name="opt_id" value="<?php echo sad_safe_html(strtolower($_GET['id'])); ?>">
<?php
    echo '</form>';
    if (strlen($q_c['id']) > 0) {
        ?>
        <form action="./delete.php">
        <input type="hidden" name="table" value="<?php echo sad_safe_html($_GET['table']); ?>">
        <input type="hidden" name="id" value="<?php echo sad_safe_html($q_c['id']); ?>">
        <input type="submit" class="mrgn sbm nicebutton"
               value="<?php echo AscetCMSEngine::$options->additional->LANGS['edit']['delete']; ?>">
        </form><?php
    }
?>
    <script>
        show("<?php echo sad_safe_mysql($edit); ?>");
        $('.calendar .hidden').hide();
    </script>
<?php
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);

?>