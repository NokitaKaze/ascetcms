<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_uploadfiles.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['uploadfiles']['title'].' / '.$sad_domain);
    set_time_limit(2 * 3600);

    AscetCMSEngine::set_prepare('l_upload_max', AscetCMSEngine::$options->additional->LANGS['index']['l_upload_max']);
    AscetCMSEngine::set_prepare('l_post_max', AscetCMSEngine::$options->additional->LANGS['index']['l_post_max']);
    AscetCMSEngine::set_prepare('l_post_td2',
        AscetCMS\Helper\Sizes::draw_size(AscetCMS\Helper\Sizes::parse_size(ini_get('post_max_size')), '-.2'));
    AscetCMSEngine::set_prepare('l_upload_td2',
        AscetCMS\Helper\Sizes::draw_size(AscetCMS\Helper\Sizes::parse_size(ini_get('upload_max_filesize')), '-.2'));

    $t1 = explode('/', str_replace('\\', '/', $_POST['path']));
    if (!file_exists("$sad_root/storage")) {
        mkdir("$sad_root/storage");
    }
    $path = '';
    foreach ($t1 as $t2) {
        if ($t2 == '.' or $t2 == '..' or $t2 == '') {
            continue;
        }
        $path .= '/'.$t2;
        if (!file_exists("$sad_root/storage$path")) {
            mkdir("$sad_root/storage$path");
        }
    }
    unset($t1, $t2);


    if (isset($_FILES['file']['tmp_name'])) {
        ob_start();
        if (((int) $_POST['overwrite']) > 0) {
            echo 'Overwrite all existing files<br />';
        } else {
            echo 'Skip existing files<br />';
        }
        echo "Path: /storage$path<br />";

        foreach ($_FILES['file']['tmp_name'] as $key => $tmp) {
            if (strlen($tmp) < 1) {
                if (strlen($_FILES['file']['name'][$key]) < 1) {
                    continue;
                }
                echo '<span style="color: red"><strong>'.sad_safe_html($_FILES['file']['name'][$key]).'</strong> not uploaded';
                if ($_FILES['file']['error'][$key] == 1 or $_FILES['file']['error'][$key] == 2) {
                    echo ' (too large file, limit is '.ini_get('upload_max_filesize').' and '.ini_get('post_max_size').')';
                }
                echo '</span><br />';
                continue;
            }
            $new_name = preg_replace('|(.*/)?([^/]+)$|', '$2', $_FILES['file']['name'][$key]);
            echo '<a href="'.$sad_baseurl.'/storage'.$path.'/'.$new_name.
                 '" target="_blank">'.sad_safe_html($new_name).'</a> ';
            $u = true;
            if (file_exists($sad_root.'/storage'.$path.'/'.$new_name)) {
                echo 'exists...';
                if ((int) $_POST['overwrite'] > 0) {
                    echo ' overwriten';
                } else {
                    echo ' <strong>not uploaded</strong>';
                    $u = false;
                }
            }
            if ($u) {
                if (!copy($tmp, $sad_root.'/storage'.$path.'/'.$new_name)) {
                    echo '<span style="color: red">not copied</span>';
                }
            }
            echo ' (size: <strong>'.filesize($sad_root.'/storage'.$path.'/'.$new_name).'</strong> bytes)<br />';
        }

        $buf = ob_get_contents();
        AscetCMSEngine::set_prepare('file_uploaded_block', $buf);
    }//if isset

    foreach (array('infobox', 'drop_here', 'overwrite_title', 'overwrite', 'folder_title', 'folder',
                   'files', 'uploadbutton') as $key) {
        AscetCMSEngine::set_prepare($key, AscetCMSEngine::$options->additional->LANGS['uploadfiles'][$key]);
    }

    AscetCMSEngine::set_prepare('upload_max_filesize',
        '<script>var upload_max_filesize='.floor(AscetCMS\Helper\Sizes::parse_size(ini_get('upload_max_filesize')) / 1024 / 1024).
        ';</script>');
    AscetCMSEngine::set_prepare('folder_path', (strlen($_POST['path']) > 0) ? sad_safe_html($_POST['path']) : '/');

?>