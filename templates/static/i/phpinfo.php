<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', 'PHP Info / '.$sad_domain);

    ob_start();
    phpinfo();
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);
?>