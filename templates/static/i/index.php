<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::set_template('adminpanel_index.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['index']['title'].
                                         ' / '.AscetCMSEngine::$options->domain);
    AscetCMSEngine::set_prepare('badbrowser_html', AscetCMSEngine::$options->additional->LANGS['index']['badbrowser']);

    ob_start();
?>
    <table>
        <tr>
            <td title="sad_version"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_version']; ?></td>
            <td><a href="https://bitbucket.org/NokitaKaze/ascetcms" target="_blank">AscetCMS/<strong title="sad_version"><?php
                            echo AscetCMSEngine::version; ?></strong></a></td>
        <tr>
        <tr>
            <td><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_doc_root']; ?></td>
            <td><?php echo $_SERVER['DOCUMENT_ROOT']; ?></td>
        <tr>
        <tr>
            <td title="sad_root"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_cmsroot']; ?></td>
            <td><?php echo $sad_root; ?></td>
        <tr>
        <tr>
            <td title="sad_protocol"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_protocol']; ?></td>
            <td><?php echo AscetCMSEngine::$options->protocol; ?></td>
        <tr>
        <tr>
            <td title="sad_domain"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_domain']; ?></td>
            <td><?php echo $sad_domain; ?></td>
        <tr>
        <tr>
            <td title="sad_baseurl"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_cmsurl']; ?></td>
            <td><?php echo $sad_baseurl; ?></td>
        <tr>

    </table>
<?php
    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('table1', $buf);

    $html1 = '';
    $a = array_flip(get_loaded_extensions());
    if (!isset($a['dom'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_dom'].'</div>';
    }
    if (!isset($a['libxml'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_libxml'].'</div>';
    }
    if (!isset($a['gd'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_gd'].'</div>';
    }
    if (!isset($a['iconv'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_iconv'].'</div>';
    }
    if (!isset($a['curl'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_curl'].'</div>';
    }
    if (!isset($a['mbstring'])) {
        $html1 .= '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['mn_mbstring'].'</div>';
    }
    AscetCMSEngine::set_prepare('html1', $html1);

    ob_start();
?>
    <table>
    <tr>
        <td>PHP Version</td>
        <td title="<?php echo PHP_VERSION_ID; ?>"><?php echo phpversion(); ?></td>
    </tr>
    <tr>
        <td>OS</td>
        <td><?php echo php_uname(); ?></td>
    </tr>
    <tr>
        <td title="php_sapi_name"><?php echo AscetCMSEngine::$options->additional->LANGS['index']['l_handler']; ?></td>
        <td><?php echo php_sapi_name(); ?></td>
    </tr>

<?php
    echo '<tr><td title="post_max_size">'.AscetCMSEngine::$options->additional->LANGS['index']['l_post_max'].'</td><td>'.
         AscetCMS\Helper\Sizes::draw_size(AscetCMS\Helper\Sizes::parse_size(ini_get('post_max_size')), '-.2').'</td></tr>';
    echo '<tr><td title="upload_max_filesize">'.AscetCMSEngine::$options->additional->LANGS['index']['l_upload_max'].'</td><td>'.
         AscetCMS\Helper\Sizes::draw_size(AscetCMS\Helper\Sizes::parse_size(ini_get('upload_max_filesize')), '-.2').'</td></tr>';
    echo '<tr><td title="memory_limit">'.AscetCMSEngine::$options->additional->LANGS['index']['l_memory_lim'].'</td><td>'.
         AscetCMS\Helper\Sizes::draw_size(AscetCMS\Helper\Sizes::parse_size(ini_get('memory_limit')), '-.2').'</td></tr>';
    if (function_exists('mb_internal_encoding')) {
        echo '<tr><td title="mb_internal_encoding">'.AscetCMSEngine::$options->additional->LANGS['index']['l_int_enc'].
             '</td><td>'.mb_internal_encoding().'</td></tr>';
    }
    echo '<tr><td title="display_errors">'.AscetCMSEngine::$options->additional->LANGS['index']['l_disp_er'].'</td><td>';
    if (ini_get('display_errors')) {
        echo 'Yes';
    } else {
        echo 'No';
    }
    echo '</td></tr>';
    /* echo '<tr><td title="suhosin.sql.bailout_on_error">Hide SQL-errors via Suhosin patch</td><td>';
    if (ini_get('suhosin.sql.bailout_on_error')){echo 'Yes';}else{echo 'No';}
    echo '</td></tr>'; */
    echo '</table>';

    if (function_exists('apache_get_modules')) {
        $a = array_flip(apache_get_modules());
        if (!isset($a['mod_rewrite'])) {
            echo '<div class="infobox">'.AscetCMSEngine::$options->additional->LANGS['index']['ma_rewrite'].'</div>';
        }
    }

    if (AscetCMSEngine::iamd()) {
        if (isset($_GET['action']) and ($_GET['action'] == 'error_type')) {
            $t1 = $_SESSION['error_type'] ^ ($_SESSION['error_type'] & $_GET['id']);
            if ((int) ($_GET['value']) > 0) {
                $t1 = $t1 | $_GET['id'];
            }
            $_SESSION['error_type'] = $t1;
        }
        if ($_GET['action'] == 'error_disp') {
            $_SESSION['error_disp'] = $_GET['value'];
        }

        $sad_tmp_ers = array(
            array('name' => 'E_ERROR'),
            array('name' => 'E_WARNING'),
            array('name' => 'E_PARSE'),
            array('name' => 'E_NOTICE'),
            array('name' => 'E_CORE_ERROR'),
            array('name' => 'E_CORE_WARNING'),
            array('name' => 'E_COMPILE_ERROR'),
            array('name' => 'E_COMPILE_WARNING'),
            array('name' => 'E_USER_ERROR'),
            array('name' => 'E_USER_WARNING'),
            array('name' => 'E_USER_NOTICE'),
            array('name' => 'E_STRICT'),
            array('name' => 'E_RECOVERABLE_ERROR'),
            array('name' => 'E_DEPRECATED')
        );

        echo '<table>';
        $t2 = '';
        $t3 = '';
        echo '<tr><td></td><td><a href="?action=error_disp&value='.(1 - $_SESSION['error_disp']).
             '" class="nicebutton" style="width:220px; padding:5px;';
        if ($_SESSION['error_disp'] < 1) {
            echo 'color: #ccc;';
        } else {
            echo 'color: #ff3;font-weight: bold;';
        }
        echo '">display_errors</a></td><td>'.AscetCMSEngine::$options->additional->LANGS['error_reporting']['display_error'].
             '</td></tr>';
        foreach ($sad_tmp_ers as $sad_tmp_er) {
            $t1 = (int) (($_SESSION['error_type'] & $sad_tmp_er['name']) > 0);
            echo '<tr><td>'.$sad_tmp_er['name'].'</td><td><a href="?action=error_type&value='.(1 - $t1).
                 '&id='.$sad_tmp_er['name'].'" class="nicebutton" style="width:220px; padding:5px;';
            if ($t1 > 0) {
                $t2 .= ' | '.$sad_tmp_er['name'];
            } else {
                echo 'color: #ccc;';
                $t3 .= ' ^ '.$sad_tmp_er['name'];
            }
            echo '">'.$sad_tmp_er['name'].'</a></td><td>'.
                 AscetCMSEngine::$options->additional->LANGS['error_reporting'][$sad_tmp_er['name']].'</td></tr>';
        }
        echo '</table>';
        if (strlen($t2) - 3 > strlen($t3) + 5) {
            echo 'error_reporting(E_ALL'.$t3.');';
        } else {
            echo 'error_reporting('.substr($t2, 3).');';
        }
    }

    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html3', $buf);
?>