<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    AscetCMSEngine::session_start();
    sql_query('INSERT INTO `____history` (`title`,`text`,`time`) VALUES ("User logged off", ?, unix_timestamp());', array(
        'User '.AscetCMSEngine::session_get_key('sad_login').' logged off at '.gmdate('Y-m-d-H-i-sO')
    ));
    AscetCMSEngine::session_delete();
    header('location: //'.$sad_domain.'/'.AscetCMSEngine::$sad_baseurl);
    AscetCMSEngine::$http_status = 302;
?>