<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if (!AscetCMSEngine::iamd()) {
        AscetCMSEngine::$http_status = 403;
        echo AscetCMSEngine::$options->additional->LANGS['common']['developer_only'].'</body></html>';

        return;
    }

    AscetCMSEngine::set_template('adminpanel_showcode.html');
    AscetCMSEngine::set_prepare('title', AscetCMSEngine::$options->additional->LANGS['showcode']['title'].' / '.$sad_domain);
    AscetCMSEngine::set_prepare('head1', '<script src="'.$sad_baseurl.'/common/bquery_ajax.js"></script>
 <script src="./admin.js.php"></script>');
    AscetCMSEngine::set_prepare('infobox', AscetCMSEngine::$options->additional->LANGS['showcode']['infobox']);
    AscetCMSEngine::set_prepare('viewfolder_path', $_POST['path']);
?>