<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if (strlen($_POST['table']) > 0 and preg_match('_^sort$_i', $_POST['action'])) {
        foreach (array_keys($_POST) as $key) {
            if (!preg_match('_^rec([0-9]+)$_i', $key, $a)) {
                continue;
            }
            sql_query('update `'.sad_safe_mysql($sad_prefix.$_POST['table']).
                      '` set `i`="'.(int) $_POST[$key].'" where `id`='.$a[1]);
        }
        echo '<script>window.close();</script>You can safely close this window.</body></html>';

        return;
    }

    if (strlen($_GET['table']) == 0) {
        echo '<script>window.close();</script>No you can not do this!.</body></html>';

        return;
    }

    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', 'Sorting / '.$sad_domain);
    ob_start();

    $tt = $_GET['title'];
    if (strlen($tt) < 1) {
        $tt = 'title';
    }

    $q = sql_query('select `id`,`'.sad_safe_mysql($tt).'` as `title` from `'.
                   sad_safe_mysql($sad_prefix.$_GET['table']).
                   '` where `parent` like "'.sad_safe_mysql($_GET['parent']).
                   '" order by `i` asc');
    $ms_t = 32;
?>
    <style>
        div.record {
            width: 350px;
            border: 1px black solid;
            margin-left: 2px;
            padding: 3px;
            position: absolute;

            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            background-color: white;

            cursor: move;
        }

        div.record a {

        }
    </style>
    <script type="text/javascript" src="/common/jquery-2.1.1.min.js"></script>
    <script>
        var i_s = [];
        var id_s = [];
        <?php
        echo 'var ms_t = '.$ms_t.";\r\n";
        $records = array();$i = 1;$form = '';
        while ($q_c = sql_fetch($q)) {
            echo "i_s[$i]  = {$q_c['id']};\r\n";
            echo "id_s[{$q_c['id']}] = $i;\r\n";
            $form .= '<input type="hidden" id="rec'.$q_c['id'].'" name="rec'.$q_c['id'].'" value="'.$i.'">'."\r\n";

            $records[$q_c['id']] = array();
            $records[$q_c['id']]['id'] = $q_c['id'];
            $records[$q_c['id']]['i'] = $i;
            $i++;
            $records[$q_c['id']]['title'] = $q_c['title'];
        }
        echo 'var max_i = '.($i - 1).";\r\n";
        unset($q, $q_c, $i);
        ?>
    </script>
    <script src="<?php echo $sad_baseurl; ?>/common/adminpanel/sort.js"></script>
    <script>
        $(document).mouseup(obj_up).ready(function() {
            $('ul.toolbar').hide();
        });
    </script>

<?php
    foreach ($records as $q_c) {
        echo '<div id="div'.$q_c['id'].'" class="record" style="top: '.
             ($ms_t * ($q_c['i'] - 1)).'px;" onmousedown="obj_down(this,event);"
		onmousemove="obj_move(this,event);" >';
        echo '<a href="javascript:" onclick="rec_up('.$q_c['id'].');"><img src="'.
             $sad_baseurl.'/common/adminpanel/arrow_up.png" alt=" up " style="border: none"></a>';
        echo '<a href="javascript:" onclick="rec_down('.$q_c['id'].');"><img src="'.
             $sad_baseurl.'/common/adminpanel/arrow_down.png" alt=" down " style="border: none"></a>';// */
        echo htmlspecialchars($q_c['title']);
        if (strlen($_GET['edit']) > 0) {
            echo '<a href="'.$sad_baseurl.'/i/edit.php?table='.
                 urlencode($_GET['table']).'&amp;id='.$q_c['id'].'&amp;edit='.
                 urlencode($_GET['edit']).'" onclick="edit(this);" target="_blank">'.
                 '<img src="'.$sad_baseurl.'/common/adminpanel/receipt--pencil.png" alt=" edit " '.
                 'style="border: none"></a>';
        }
        echo '<a target="_blank" href="'.$sad_baseurl.'/i/delete.php?table='.
             urlencode($_GET['table']).'&amp;id='.$q_c['id'].'"><img src="'.
             $sad_baseurl.'/common/adminpanel/cross.png" alt=" delete " title="Delete="></a>';
        echo "</div>\r\n\r\n";
    }
    echo '<form method="post">'.$form.'<input type="hidden" name="action" '.
         'value="sort"><input type="hidden" name="table" value="'.
         htmlspecialchars($_GET['table']).'"><input type="submit" value="Sort" '.
         'style="position: absolute; top: '.($ms_t * $q_c['i']).'px"></form>';


    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);
?>