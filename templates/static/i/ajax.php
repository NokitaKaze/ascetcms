<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */

    /**
     * @param string $folder
     *
     * @return string
     */
    function getfolder_info($folder) {
        return '<size>'.filesize($folder).'</size>'.
               '<timec>'.gmdate('Y-m-d\TH:i:s', filectime($folder)).'</timec>'.
               '<timea>'.gmdate('Y-m-d\TH:i:s', fileatime($folder)).'</timea>'.
               '<timem>'.gmdate('Y-m-d\TH:i:s', filemtime($folder)).'</timem>'.
               '<group>'.filegroup($folder).'</group>'.
               '<owner>'.fileowner($folder).'</owner>'.
               '<perm>'.fileperms($folder).'</perm>'.
               '<read>'.(int) is_readable($folder).'</read>'.
               '<write>'.(int) is_writable($folder).'</write>';
    }

    function getfolder($folder_get, $folder_parent, $g_type) {
        /* g_type = 0 - only folders
                    1 - folders and images
                    2 - all */
        global $sad_root;
        $t1 = explode('/', str_replace('\\', '/', $folder_get));
        if (!file_exists("{$sad_root}/{$folder_parent}")) {
            mkdir("{$sad_root}/{$folder_parent}");
        }
        $path = '';
        foreach ($t1 as $t2) {
            if ($t2 == '.' or $t2 == '') {
                continue;
            }
            if ($t2 == '..') {
                $path = preg_replace('|/[^/]+?$|', '', $path);
            } else {
                $path .= '/'.$t2;
                if (!file_exists($sad_root.'/'.$folder_parent.$path)) {
                    mkdir($sad_root.'/'.$folder_parent.$path);
                }
            }
        }
        if ($path == '') {
            $path = '/';
        }
        $xml = '<view><path>'.AscetCMS\Helper\XML::sanify($path).'</path>';

        $d = scandir($sad_root.'/'.$folder_parent.$path);
        if (strlen($path) > 1) {
            $xml .= '<item><type>folder</type><name>..</name>'.getfolder_info($sad_root.'/'.$folder_parent.$path.'/../').
                    '</item>';
        }
        foreach ($d as $item) {
            if ($item == '.' or $item == '..') {
                continue;
            }
            $xml .= "\r\n\r\n".AscetCMS\Helper\XML::sanify_comment(preg_replace('_/{2,}_', '/',
                    $sad_root.'/'.$folder_parent.$path.'/'.$item));
            if (is_dir(preg_replace('_/{2,}_', '/', $sad_root.'/'.$folder_parent.$path.'/'.$item))) {
                $type = 'folder';
            } else {
                if ($g_type == 0) {
                    continue;
                }
                if ($g_type == 1) {
                    if (!preg_match('_\\.(jpe?g|bmp|gif|png)$_i', $item)) {
                        continue;
                    }
                }
                $type = 'file';
            }
            $xml .= '<item><type>'.$type.'</type><name>'.AscetCMS\Helper\XML::sanify($item).'</name>'.
                    getfolder_info($sad_root.'/'.$folder_parent.$path.'/'.$item)."</item>\r\n";
        }

        $xml .= '</view>';

        return $xml;
    }

    if ($sad_url != '/i/ajax.php') {
        AscetCMSEngine::$http_status = 400;
        echo 'no!';

        return;
    }

    if ($_GET['action'] == 'viewfolder') {
        $par = preg_replace('|[^a-z]|i', '', $_GET['parent']);
        if ($par !== 'storage' and $par !== 'templates') {
            AscetCMSEngine::$http_status = 403;

            return;
        }
        $sad_contype = 'text/xml; charset=utf-8';
        echo '<'.'?xml version="1.0"?'.'>';
        echo getfolder($_GET['folder'], $par, (int) ($_GET['g_type']));
    } elseif ($_GET['action'] == 'createfolder') {
        $t1 = explode('/', str_replace('\\', '/', $_GET['folder']));
        $par = preg_replace('|[^a-z]|i', '', $_GET['parent']);
        if ($par !== 'storage' and $par !== 'templates') {
            AscetCMSEngine::$http_status = 403;

            return;
        }
        if (!file_exists("$sad_root/storage")) {
            mkdir("$sad_root/storage");
        }
        if (!file_exists("$sad_root/templates")) {
            mkdir("$sad_root/templates");
        }
        $path = '';
        foreach ($t1 as $t2) {
            if ($t2 == '.' or $t2 == '') {
                continue;
            }
            if ($t2 == '..') {
                $path = preg_replace('|/[^/]+?$|', '', $path);
            } else {
                $path .= '/'.$t2;
                if (!file_exists($sad_root.'/'.$par.$path)) {
                    mkdir($sad_root.'/'.$par.$path);
                }
            }
        }
        if ($path == '') {
            $path = '/';
        }
        $sad_contype = 'text/xml; charset=utf-8';
        echo '<'.'?xml version="1.0"?'.'>';
        echo '<view><path>'.AscetCMS\Helper\XML::sanify($path).'</path></view>';
    } elseif ($_GET['action'] == 'uploadfiles') {
        $sad_contype = 'application/javascript; charset=utf-8';
        $dest = '';
        $a = explode('/', urldecode($_POST['dir']));
        foreach ($a as $d) {
            if ($d == '.' or $d == '..' or $d == '') {
                continue;
            }
            $dest .= $d.'/';
            if (!file_exists($sad_root.'/storage/'.$dest)) {
                mkdir($sad_root.'/storage/'.$dest);
            }
        }
        if (!is_writable($sad_root.'/storage/'.$dest)) {
            echo '{"status":"403"}';

            return;
        }
        $_FILES['pic']['name'] = sad_safe_path($_FILES['pic']['name']);

        $newfile = $sad_root.'/storage/'.$dest.'/'.$_FILES['pic']['name'];
        if ($_FILES['pic']['name'][0] == '.') {
            echo '{"status":"403"}';

            return;
        }
        if (file_exists($newfile)) {
            if ((int) $_POST['overwrite'] < 1) {
                echo '{"status":"skip"}';

                return;
            }
            if (!is_writable($newfile)) {
                echo '{"status":"403"}';

                return;
            }
        }
        copy($_FILES['pic']['tmp_name'], $newfile);

        echo '{"status":"true","dir":"'.sad_safe_mysql(substr($dest, 0, -1)).'","type":"file","name":"'.
             sad_safe_mysql($_FILES['pic']['name']).'","size":"'.filesize($newfile).'","timec":"'.
             gmdate('Y-m-d\TH:i:s', filectime($newfile)).'","timea":"'.gmdate('Y-m-d\TH:i:s', fileatime($newfile)).'","timem":"'.
             gmdate('Y-m-d\TH:i:s', filemtime($newfile)).'","group":"'.filegroup($newfile).'","owner":"'.fileowner($newfile).
             '","perm":"'.fileperms($newfile).'","read":"'.(int) is_readable($newfile).'","write":"'.(int) is_writable($newfile).
             '"}';
    }

?>