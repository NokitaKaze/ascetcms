<?php
/**
 *
 *
 * @var string $sad_url
 * @var string $sad_prefix
 * @var string $sad_root
 * @var string $sad_baseurl
 * @var string $sad_domain
 * @var string $sad_contype
 */
AscetCMSEngine::session_start();
if (AscetCMSEngine::iam()) {
    if (isset($_GET['url']) and (strlen($_GET['url']) > 0)) {
        header('location: '.$_GET['url']);
    } else {
        header('location: '.AscetCMSEngine::$sad_baseurl.'/i/');
    }
    AscetCMSEngine::$http_status = 302;

    return;
}

$error = '';

$q_c = sql_query_fetch('SELECT count(*) AS `count` FROM `____users`;');
if ($q_c['count'] == 0) {
    $error .= 'Table `'.$sad_prefix.'_users` is empty<br />';
}

if (isset($_POST['login']) and isset($_POST['password'])
                               and (strlen($_POST['login']) > 0) and (strlen($_POST['password']) > 0)
) {
    $q_c = sql_query_fetch('SELECT * FROM `____users` WHERE `login` = ?;', array($_POST['login']));
    if (($q_c['password'] !== '') and (hash('sha512', $q_c['login'].':'.$_POST['password']) == $q_c['password'])) {
        AscetCMSEngine::session_set_key('sad_login', $q_c['login']);
        AscetCMSEngine::session_set_key('sad_password', $q_c['password']);
        AscetCMSEngine::session_set_key('sad_rights', $q_c['rights']);
        AscetCMSEngine::session_set_key('sad_time', time());
        sql_query('INSERT INTO `____history` (`title`,`text`,`time`) VALUES ("User logged in", ?, unix_timestamp());',
            array(
                'User '.sad_safe_html($q_c['login']).' logged in at '.gmdate('Y-m-d-H-i-sO'),
            ));
        if (strlen($_POST['url']) > 0) {
            header('location: '.$_POST['url']);
        } else {
            header('location: '.AscetCMSEngine::$sad_baseurl.'/');
        }
        AscetCMSEngine::$http_status = 302;

        return;
    } else {
        sql_query('INSERT INTO `____history` (`title`,`text`,`time`) VALUES ("User password incorrect",?, unix_timestamp());',
            array(
                'User '.sad_safe_html($q_c['login']).' logged off cos incorrect password at '.gmdate('Y-m-d-H-i-sO')
            ));
    }

    //error
    if ($q_c['password'] == '') {
        $error .= 'Password of this user is null<br />';
    } else {
        if ($q_c['password'] !== hash('sha512', $q_c['login'].':'.$_POST['password'])) {
            $error .= 'Login or password is incorrect<br />';
        }
    }

}

?><!doctype html>
<html>
<head>
    <title>Please, login</title>
    <link rel="stylesheet" href="<?php
        echo AscetCMSEngine::$sad_baseurl;
    ?>/common/adminpanel/login.css" type="text/css" media="screen">
</head>
<body>
<div class="main">
    <form method="post">
        <div>Login: <input name="login" <?php
                if (isset($_POST['login']) and (strlen($_POST['login']) > 0)) {
                    echo 'value="'.sad_safe_html($_POST['login']).'"';
                } else {
                    if (AscetCMSEngine::iam()) {
                        echo 'value="'.sad_safe_html($_SESSION['sad_login']).'"';
                    }
                }
            ?> class="textbox"/></div>
        <div>Password: <input name="password" type="password" class="textbox"/></div>
        <input name="url" type="hidden" value="<?php echo sad_safe_html($_GET['url']); ?>"/>
        <input type="submit" class="submit" value="Log in"/>
        <?php
            if (AscetCMSEngine::iam()) {
                echo 'You already logged in<br />';
            }
            if (strlen($error) > 0) {
                echo '<span style="color: red;">'.$error.'</span>';
            }
        ?>
    </form>
</div>
</body>
</html>