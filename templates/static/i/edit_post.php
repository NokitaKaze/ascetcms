<?php
    /**
     * Сохранение записи
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */


    $edit = preg_replace('|[/\\\\]|', '', $_POST['opt_edit']);
    //echo '<pre>';print_r($_POST);print_r($_FILES);echo '</pre>';

    if (strlen($edit) < 1 or
        strlen($_POST['opt_id']) < 1 or
        strlen($_POST['opt_table']) < 1
    ) {
        echo 'No, you can\'t';

        return;
    }

    if (preg_match('|^_users$|i', $_GET['opt_table']) and !AscetCMSEngine::iamd()) {
        AscetCMSEngine::$http_status = 403;
        echo 'No, you can not edit records from admin table</body></html>';

        return;
    }

    if (file_exists($sad_root.'/templates/edit/'.$edit.'.before')) {
        include($sad_root.'/templates/edit/'.$edit.'.before');
    }

    $q3 = sql_query('SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS`
WHERE (`TABLE_SCHEMA`=database())AND(`TABLE_NAME`=?);',
        array($sad_prefix.$_POST['opt_table']));
    $existed_keys = array();
    while ($q3_c = sql_fetch($q3)) {
        $existed_keys[] = $q3_c['COLUMN_NAME'];
    }
    unset($q3_c, $q3);

    //create table
    sql_exec('CREATE TABLE IF NOT EXISTS `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).'` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`i` INT NOT NULL ,
`sname` TEXT NOT NULL ,
`parent` TEXT NOT NULL ,
INDEX (`i`)
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT="'.sad_safe_mysql($sad_domain.' / '.time().' / edit').'";');

    $sql = '';//update_sql
    $sql1 = '';
    $sql2 = '';//insert sql
    foreach ($_POST as $key => &$value) {
        if (!preg_match('|^data_([a-z0-9_]+)$|i', $key, $a)) {
            continue;
        }
        $key = $a[1];
        // add columns and longtext instead of text
        if (strlen($value) > 65500) {
            $l = 'long';
        } else {
            $l = '';
        }
        if (!in_array($key, $existed_keys)) {
            sql_query('ALTER TABLE `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).
                      '` ADD COLUMN `'.sad_safe_mysql($key).'` '.$l.'TEXT NOT NULL');
        } elseif (strlen($value) > 65500) {
            sql_query('ALTER TABLE `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).'` MODIFY COLUMN `'.
                      sad_safe_mysql($key).'` LONGTEXT NOT NULL;');
        }

        // add sql code
        $sql .= ', `'.sad_safe_mysql($key).'`="'.sad_safe_mysql($value).'"';
        $sql1 .= ', `'.sad_safe_mysql($key).'`';
        $sql2 .= ', "'.sad_safe_mysql($value).'"';
    }
    unset($a, $l, $key, $value);
    error_reporting(E_ALL);
    // add
    for ($img_i = 0; isset($_POST['opt_image_'.$img_i.'_oldvalue']); $img_i++) {
        if (!isset($_FILES['opt_image_'.$img_i.'_file'])) {
            $sql .= ', `'.sad_safe_mysql($_POST['opt_image_'.$img_i.'_name']).'`="'.
                    sad_safe_mysql($_POST['opt_image_'.$img_i.'_oldvalue']).'"';
            $sql1 .= ', `'.sad_safe_mysql($_POST['opt_image_'.$img_i.'_name']).'`';
            $sql2 .= ', "'.sad_safe_mysql($_POST['opt_image_'.$img_i.'_oldvalue']).'"';
            continue;
        }

        // upload image
        list($org_width, $org_height, $tt) = getimagesize($_FILES['opt_image_'.$img_i.'_file']['tmp_name']);
        if ($tt < 1 or $tt > 3) {
            continue;
        }
        if (filesize($_FILES['opt_image_'.$img_i.'_file']['tmp_name']) > $_POST['opt_image_'.$img_i.'_size']) {
            continue;
        }
        if ($_POST['opt_image_'.$img_i.'_width'] < $org_width) {
            continue;
        }
        if ($_POST['opt_image_'.$img_i.'_height'] < $org_height) {
            continue;
        }
        $t = '/storage/'.sad_safe_path($_POST['opt_image_'.$img_i.'_path']).'/'.
             hash_file('sha512', $_FILES['opt_image_'.$img_i.'_file']['tmp_name']).'.'.$imgs_ext[$tt];
        $new_file = $sad_root.$t;

        $a = explode('/', sad_safe_path($_POST['opt_image_'.$img_i.'_path']));
        $pp = $sad_root.'/storage/';
        foreach ($a as $dir) {
            $pp .= $dir.'/';
            mkdir($pp);
        }
        unset($pp, $a, $dir);

        copy($_FILES['opt_image_'.$img_i.'_file']['tmp_name'], $new_file);
        $sql .= ', `'.sad_safe_mysql($_POST['opt_image_'.$img_i.'_name']).'`="'.sad_safe_mysql($t).'"';
        $sql1 .= ', `'.sad_safe_mysql($_POST['opt_image_'.$img_i.'_name']).'`';
        $sql2 .= ', "'.sad_safe_mysql($t).'"';
        sql_query('ALTER TABLE `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).'` ADD COLUMN `'.
                  sad_safe_mysql($_POST['opt_image_'.$img_i.'_name']).'` TEXT NOT NULL');

        for ($img_j = 0; isset($_POST['opt_image_'.$img_i.'_'.$img_j.'_width']); $img_j++) {
            $a = explode('/', sad_safe_path($_POST['opt_image_'.$img_i.'_'.$img_j.'_path']));
            $pp = $sad_root.'/storage/';
            foreach ($a as $dir) {
                $pp .= $dir.'/';
                mkdir($pp);
            }
            unset($pp, $a, $dir);

            $thumb = '/storage/';
            $t1 = sad_safe_path($_POST['opt_image_'.$img_i.'_'.$img_j.'_path']).'/';
            if (strlen($t1) > 1) {
                $thumb .= $t1;
            }
            $thumb .= hash_file('sha512', $_FILES['opt_image_'.$img_i.'_file']['tmp_name']).'.'.
                      $_POST['opt_image_'.$img_i.'_'.$img_j.'_filetype'];
            if ($thumb == $t) {
                continue;
            }
            AscetCMS\Helper\Image::image_resize($new_file, $sad_root.$thumb, $_POST['opt_image_'.$img_i.'_'.$img_j.'_width'],
                $_POST['opt_image_'.$img_i.'_'.$img_j.'_height']);
        }
        unset($img_j);
    }
    unset($img_i);

    // update table
    if ((int) $_POST['opt_id'] > 0) {
        //edit
        sql_exec('update `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).
                 '` set '.substr($sql, 2).' where `id`='.$_POST['opt_id']);
        $record_id = (int) $_POST['opt_id'];
    } else {
        //add
        sql_exec('INSERT INTO `'.sad_safe_mysql($sad_prefix.$_POST['opt_table']).
                 '` ('.substr($sql1, 2).') VALUES ('.substr($sql2, 2).')');
        $record_id = sql_insert_id();
    }
    unset($sql, $sql1, $sql2);

    if (file_exists($sad_root.'/templates/edit/'.$edit.'.after')) {
        include($sad_root.'/templates/edit/'.$edit.'.after');
    }

?>
<script>window.close();</script>You can safely close this window.