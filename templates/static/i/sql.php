<?php
    /**
     *
     *
     * @var string $sad_url
     * @var string $sad_prefix
     * @var string $sad_root
     * @var string $sad_baseurl
     * @var string $sad_domain
     * @var string $sad_contype
     */
    if ($_GET['action'] == 'optimize') {
        $q = sql_query('SHOW TABLE STATUS');
        while ($q_c = sql_fetch($q)) {
            if (strpos($q_c['Name'], $sad_prefix) === false) {
                continue;
            }
            sql_query('optimize table `'.$q_c['Name'].'`');
        }
    }


    AscetCMSEngine::set_template('adminpanel_template.html');
    AscetCMSEngine::set_prepare('title', 'SQL Manager / '.$sad_domain);

    ob_start();
?>
    <style>
        abbr.byte0, abbr.byte1, abbr.byte2 {
            color: black;
            cursor: pointer;
            border-bottom: 1px dashed;
        }

        abbr.byte1, abbr.byte2 {
            display: none;
        }
    </style>
    <script>
        var type = 0;
        function change_byte() {
            type++;
            if (type > 2) {type = 0;}
            $('abbr.byte0').hide();
            $('abbr.byte1').hide();
            $('abbr.byte2').hide();
            $('abbr.byte' + type).show();
        }
    </script>
    <table>
        <tr class="head">
            <td>Name</td>
            <td>Engine</td>
            <td>Rows</td>
            <td>
                <nobr>Data length</nobr>
            </td>
            <td>
                <nobr>Index length</nobr>
            </td>
            <td><span title="Autoincrement index">Index<span></td>
            <td>
                <nobr>Collation</nobr>
            </td>
            <td>
                <nobr>Update time</nobr>
            </td>
        </tr>
<?php

    $q = sql_query('SHOW TABLE STATUS');
    $len_data = 0;
    $len_index = 0;
    while ($q_c = sql_fetch($q)) {
        if (strpos($q_c['Name'], $sad_prefix) === false) {
            continue;
        }
        echo '<tr><td>'.$q_c['Name'].'</td><td>'.$q_c['Engine'].'</td><td>'.$q_c['Rows'].
             '</td><td><nobr>';
        echo '<abbr class="byte0" onclick="change_byte()">'.$q_c['Data_length'].'</abbr>';
        echo '<abbr class="byte1" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($q_c['Data_length'], '1.2').
             '</abbr>';
        echo '<abbr class="byte2" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($q_c['Data_length'], '2.2').
             '</abbr>';
        echo '</nobr></td><td><nobr>';
        echo '<abbr class="byte0" onclick="change_byte()">'.$q_c['Index_length'].'</abbr>';
        echo '<abbr class="byte1" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($q_c['Index_length'], '1.2').
             '</abbr>';
        echo '<abbr class="byte2" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($q_c['Index_length'], '2.2').
             '</abbr>';
        echo '</td><td><span title="Autoincrement index"><nobr>'.$q_c['Auto_increment'].
             '</nobr></span></td><td>'.$q_c['Collation'].'</td><td><nobr>'.$q_c['Update_time'].'</nobr></td></tr>';
        $len_data += $q_c['Data_length'];
        $len_index += $q_c['Index_length'];
    }
    echo '<td colspan="3">Sum</td><td>'.
         '<abbr class="byte0" onclick="change_byte()">'.$len_data.'</abbr>'.
         '<abbr class="byte1" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($len_data, '1.2').'</abbr>'.
         '<abbr class="byte2" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($len_data, '2.2').'</abbr>'.
         '</td><td>'.
         '<abbr class="byte0" onclick="change_byte()">'.$len_index.'</abbr>'.
         '<abbr class="byte1" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($len_index, '1.2').'</abbr>'.
         '<abbr class="byte2" onclick="change_byte()">'.AscetCMS\Helper\Sizes::draw_size($len_index, '2.2').'</abbr>'.
         '</td></tr></table>';

    echo '<a href="?action=optimize" class="nicebutton mrgn">Optimize all tables</a>';

    $buf = ob_get_contents();
    ob_end_clean();
    AscetCMSEngine::set_prepare('html2', $buf);

?>