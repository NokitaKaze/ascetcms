/** @namespace expl_set */
var baseurl = '{#wait:baseurl#}';
var spider_locked = false;
var files = [];

/* redraw folder */
function refresh_all(base_path_id) {//meta function
    refresh_all_1(base_path_id);
}

function refresh_all_1(base_path_id) {
    var html_files = '';
    var html_folders = '';
    var k = 1;
    var html = '<img src="' + baseurl + '/common/adminpanel/folder-tree.png" style="border: none; width: 16px;"><b>' +
               expl_set[base_path_id].base_path + '</b><br>';

    for (var i = 0; i < files.length; i++) {
        var s = '<nobr>';
        if (files[i].type == 'folder') {
            if (expl_set[base_path_id].need_check) {s += '<input type="checkbox" disabled>';}
            s += '<a class="p' + files[i].read + files[i].write +
                 '" href="javascript:" title="Open folder" onclick="viewfolder(\'' + files[i].name + '\', ' +
                 base_path_id + ');"><img src="' + baseurl + '/common/adminpanel/folder.png">';
        } else {
            if (expl_set[base_path_id].need_check) {
                s +=
                '<input type="checkbox" value="' + urlencode(files[i].name) + '" id="file_' +
                k + '" name="file_' + k + '">';
                k++;
            }
            s += '<a class="p' + files[i].read + files[i].write + '" target="_blank" ';
            if (expl_set[base_path_id].g_type == 2) {
                s += 'href="' + baseurl + '/i/editcode.php?file=' +
                     urlencode(expl_set[base_path_id].base_path + '/' + files[i].name) +
                     '"><img src="' + baseurl + '/common/adminpanel/receipt--pencil.png">';
            } else {
                s += 'href="' + baseurl + '/' + expl_set[base_path_id].parent_folder + '/' +
                     expl_set[base_path_id].base_path + '/' + files[i].name +
                     '"><img src="' + baseurl + '/common/adminpanel/application-image.png">';
            }
        }

        s += files[i].name + '</a>' + space(50 - files[i].name.length) + '<span class="timec">' + files[i].timec +
             '</span><span class="timem">' + files[i].timem + '</span><span class="timea">' + files[i].timea +
             '</span>&nbsp;' + space(11 - files[i].size.length) + files[i].size;
        var s1 = '';
        var p = files[i].perm;
        for (var j = 0; j < 3; j++) {
            s1 = ((p & 4) ? 'r' : '-') + ((p & 2) ? 'w' : '-') + ((p & 1) ? 'x' : '-') + s1;
            p = p >> 3;
        }
        s += space(10 - s1.length) + s1 +
             space(6 - files[i].owner.length) + files[i].owner + '/' +
             space(5 - files[i].group.length) + files[i].group + "</nobr><br />\r\n";

        if (files[i].type == 'folder') {html_folders += s;} else {html_files += s;}
    }
    document.getElementById('browser' + base_path_id).innerHTML = html + html_folders + html_files;
}

/* View folder */
function viewfolder(folder, base_path_id) {
    if (spider_locked) {return;}
    spider_locked = true;
    document.getElementById('browser' + base_path_id).style.backgroundColor = 'grey';
    $ajax({
        url: '' + baseurl + '/i/ajax.php?action=viewfolder&parent=' + expl_set[base_path_id].parent_folder +
             '&g_type=' + expl_set[base_path_id].g_type + '&folder=' + urlencode(expl_set[base_path_id].base_path + '/' + folder),
        base_path_id: base_path_id,
        success: function(a, b, c) {
            spider_locked = false;
            var root = sad_x2n(c);
            expl_set[base_path_id].base_path = sad_xml_getnode(root, 'path#');
            var html = '<img src="' + baseurl + '/common/adminpanel/folder-tree.png" style="border: none; width: 16px;"><b>' +
                       expl_set[base_path_id].base_path + '</b><br>';
            var html_files = '';
            var html_folders = '';
            files = [];
            for (var i = 1; true; i++) {
                var f = sad_xml_getnode(root, 'item|' + i);
                if (f == undefined) {break;}
                files[files.length] = {
                    'name': sad_xml_getnode(f, 'name#'),
                    'size': sad_xml_getnode(f, 'size#'),
                    'type': sad_xml_getnode(f, 'type#'),
                    'read': sad_xml_getnode(f, 'read#'),
                    'write': sad_xml_getnode(f, 'write#'),
                    'owner': sad_xml_getnode(f, 'owner#'),
                    'group': sad_xml_getnode(f, 'group#'),
                    'timec': sad_xml_getnode(f, 'timec#'),
                    'timea': sad_xml_getnode(f, 'timea#'),
                    'timem': sad_xml_getnode(f, 'timem#'),
                    'perm': sad_xml_getnode(f, 'perm#')
                };
            }
            document.getElementById('browser' + base_path_id).style.backgroundColor = '';
            document.getElementById('path' + base_path_id).value = expl_set[base_path_id].base_path;

            refresh_all(base_path_id);
        },
        error: function() {
            setTimeout(viewfolder(folder, base_path_id), 1000);
        }
    });
}

function CreateFolder(parent_folder, g_type, base_id) {
    if (spider_locked) {return;}
    spider_locked = true;
    $ajax({
        url: baseurl + "/i/ajax.php?action=createfolder&parent=" + parent_folder + "&folder=" +
             urlencode(expl_set[base_id].base_path + '/' + document.getElementById('new_folder_name').value),
        success: function(a, b, c) {
            spider_locked = false;
            viewfolder(document.getElementById('new_folder_name').value, base_id);
        },
        error: function() {
            setTimeout(CreateFolder(), 1000);
        }
    });
}

function space(n) {
    var s = '';
    for (var i = 0; i < n; i++) {s += '&nbsp;';}
    return s;
}

function set_mode(type) {
    $('span.timea').hide();
    $('span.timec').hide();
    $('span.timem').hide();
    $('span.time' + type).show();
}