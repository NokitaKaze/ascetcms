<?php

    final class SModelStream {
        private $_offset = 0;
        private $_count;
        private $_order;
        private $_settings;
        private $_select;

        /**
         * SModelStream constructor.
         *
         * @param array|object $settings
         *
         * @throws Exception
         */
        function __construct($settings) {
            $this->_settings = is_object($settings) ? clone $settings : (object) $settings;
            if (!isset($this->_settings->get_models)) {
                throw new Exception('SModelStream must have closure get_models');
            }
            if (!isset($this->_settings->get_count)) {
                throw new Exception('SModelStream must have closure get_count');
            }
        }

        /**
         * @param mixed $select
         */
        function set_select($select) {
            $this->_select = $select;
        }

        /**
         * Получаем следующие N элементов
         *
         * @param integer $count
         * @param boolean $return_false_on_null
         *
         * @return SModel[]|boolean
         */
        function get_next_models($count = 1, $return_false_on_null = true) {
            $models = $this->get_models($this->_offset, $count);
            $this->_offset += count($models);

            if (count($models) > 0) {
                return $models;
            } else {
                return $return_false_on_null ? false : [];
            }
        }

        /**
         * Получаем следующие N элементов
         *
         * @return SModel|null
         */
        function get_next_model() {
            $models = $this->get_models($this->_offset, 1);
            $this->_offset += count($models);

            if (count($models) > 0) {
                return $models[0];
            } else {
                return null;
            }
        }

        /**
         * @param integer $offset
         */
        function set_seek($offset) {
            $this->_offset = max($offset, 0);
        }

        /**
         * @param string $order
         */
        function set_default_order($order) {
            $this->_order = $order;
        }

        /**
         * @param integer $offset
         * @param integer $count
         * @param string  $order
         *
         * @return SModel[]
         */
        function get_models($offset, $count, $order = null) {
            $get_models = $this->_settings->get_models;

            return $get_models($offset, $count, $order, $this->_select);
        }

        /**
         * Получаем кол-во элементов в стриме
         *
         * @return integer
         */
        function get_count() {
            if ($this->_count === null) {
                $get_count = $this->_settings->get_count;

                $this->_count = $get_count();
            }

            return $this->_count;
        }

        /**
         * @param integer  $element_count
         * @param object[] $received_items
         * @param integer  $received_offset
         * @param integer  $received_limit
         * @param integer  $offset
         * @param integer  $limit
         *
         * @return object[]|SModel[]|null
         * @throws SModelException
         */
        static function get_models_from_existed(
            $element_count, $received_items, $received_offset, $received_limit, $offset, $limit) {
            $existed_max_id = $element_count - 1;
            $received_min_id = $received_offset;
            $received_max_id = $received_offset + $received_limit - 1;
            // @hint $received_max_id не может быть больше $existed_max_id
            if ($received_max_id > $existed_max_id) {
                $received_max_id = $existed_max_id;
            }
            if ($offset + $limit > $existed_max_id) {
                $limit = $existed_max_id + 1 - $offset;
            }

            if (($offset >= $received_min_id) and ($offset + $limit - 1 <= $received_max_id)) {
                $data = [];
                $min_inner_id = $offset - $received_offset;
                for ($id = $min_inner_id; $id < $min_inner_id + $limit; $id++) {
                    $data[] = $received_items[$id];
                }

                return $data;
            } else {
                return null;
            }
        }
    }

?>