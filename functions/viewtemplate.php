<?php

    /**
     * Class ViewTemplate
     */
    abstract class ViewTemplate {
        const HEADER_INCLUDE = 0;
        const BODY_BEGINNING_INCLUDE = 1;
        const BODY_END_INCLUDE = 2;
        const PAGE_READY = 3;
        const PAGE_LOAD = 4;

        const INTEGRITY_COMMON = 1;
        const INTEGRITY_IMAGES = 3;
        const INTEGRITY_STORAGE = 7;

        /**
         * @var string[][][] Dictionary (подготовленные переменные) для html-шаблона. То, чем заменяются конструкции
         *     wait/prepared после окончания real_run
         */
        private static $_prepared = [];

        /**
         * @var object[][] Подключание link & JS
         */
        private static $_included = [];

        /**
         * @var object[][] Подключание link & JS
         */
        private static $_already_used = [];

        /**
         * @var object[]
         */
        private static $_options = [];

        /**
         * @var double[]
         */
        private static $_profiling = [];

        /**
         * @param string $key
         * @param mixed  $value
         * @param string $template_html
         */
        static function set_template_option($key, $value, $template_html = 'main') {
            if (!isset(self::$_options[$template_html])) {
                self::$_options[$template_html] = (object) [];
            }
            self::$_options[$template_html]->{$key} = $value;
        }

        /**
         * @param string     $template_filename
         * @param array|null $variables
         * @param boolean    $return
         * @param string     $global_template_name
         *
         * @return string|null
         */
        static function draw_view($template_filename, $variables = null, $return = true, $global_template_name = 'main') {
            if ($variables === null) {
                $variables = [];
            }
            $html = '';
            $filename = self::get_view_filename($template_filename, $global_template_name);
            if (file_exists($filename)) {
                $closure = function ($variables, $temporary_filename) {
                    extract($variables);
                    ob_start();
                    /** @noinspection PhpIncludeInspection */
                    require($temporary_filename);
                    $html = ob_get_contents();
                    ob_end_clean();

                    return $html;
                };
                $html = $closure($variables, $filename);
            } else {
                // @todo Пишем warning
            }
            $html = "<!-- view: {$template_filename} -->{$html}<!-- /view: {$template_filename} -->";

            if ($return) {
                return $html;
            } else {
                echo $html;

                return null;
            }
        }

        /**
         * Получаем спарсенный шаблон
         *
         * @param string $filename
         * @param string $template_name
         *
         * @return string Строка с пропарсенным шаблоном
         */
        static function get_html_template($filename = 'index.html', $template_name = 'main') {
            $filename = self::get_template_filename($filename, $template_name);
            // @todo Загрузка из memcache / kv-хранилища
            if (!file_exists($filename) or !is_readable($filename)) {
                // @todo Пишем warning
                return '';
            }
            $sad_template = @file_get_contents($filename);
            if ($sad_template === false) {
                // @todo Пишем warning
                return '';
            }

            // Include других шаблонов
            $sad_template = self::parse_template_for_include($sad_template, $template_name);

            if (!isset(self::$_prepared[$template_name])) {
                self::$_prepared[$template_name] = [];
            }
            $sad_template = self::parse_html_template($sad_template, $template_name);

            return $sad_template;
        }

        /**
         * Парсим шаблон
         *
         * @param string $html
         * @param string $template_name
         *
         * @return string
         */
        static function parse_template_for_include($html, $template_name = 'main') {
            $i = 1;
            while ($i > 0) {
                $html = preg_replace_callback('|{#include: *([a-z0-9_.\\/-]+) *#}|i',
                    function ($a) use ($template_name) {
                        // @todo Пишем warning
                        return @file_get_contents(self::get_template_filename($a[1], $template_name));
                    },
                    $html, -1, $i);
            }

            return $html;
        }

        /**
         * @param string $template_name
         * @param string $global_template_name
         *
         * @return string
         * @throws ViewTemplateException
         */
        static function get_template_filename($template_name, $global_template_name = 'main') {
            if (mb_substr($template_name, 0, 0) === '/') {
                return $template_name;
            } elseif (!isset(self::$_options[$global_template_name], self::$_options[$global_template_name]->html_template_path)) {
                throw new ViewTemplateException('HTML template path has not been set', 7);
            }
            $path = self::$_options[$global_template_name]->html_template_path;

            return $path.'/'.$template_name;
        }

        /**
         * @param string $template_name
         * @param string $global_template_name
         *
         * @return string
         * @throws ViewTemplateException
         */
        static function get_view_filename($template_name, $global_template_name = 'main') {
            if (mb_substr($template_name, 0, 0) === '/') {
                return $template_name;
            } elseif (!isset(self::$_options[$global_template_name], self::$_options[$global_template_name]->view_template_path)) {
                throw new ViewTemplateException('View template path has not been set', 7);
            }
            $path = self::$_options[$global_template_name]->view_template_path;

            return str_replace('//', '/', $path.'/'.$template_name);
        }

        /**
         * Парсим html-шаблон на wait, option, buttons
         *
         * @param string $html
         * @param string $template_name
         *
         * @return string
         */
        static function parse_html_template($html, $template_name) {
            self::init_template($template_name);
            // Wait
            $html = preg_replace_callback('/\\{#(wait|prepare|prepared): *([a-z0-9._-]+) *#\\}/i',
                function ($a) use ($template_name) {
                    $key = strtolower($a[2]);

                    if (!isset(self::$_prepared[$template_name][$key])) {
                        $hash = AscetCMSEngine::generate_hash();
                        self::$_prepared[$template_name][$key] = ['hash' => $hash, 'value' => ''];
                    } else {
                        $hash = self::$_prepared[$template_name][$key]['hash'];
                    }

                    return '{'.'#wait:'.$hash.':'.$key.'#}';
                }, $html);

            return $html;
        }

        /**
         * Задаём значение в шаблоне. Если ключа не существует, создаём его
         *
         * @param string $key           Ключ
         * @param string $value         Значение
         * @param string $template_name Значение
         */
        static function set_prepare($key, $value, $template_name = 'main') {
            $key = strtolower($key);
            if (!isset(self::$_prepared[$template_name])) {
                self::$_prepared[$template_name] = [];
            }
            if (!isset(self::$_prepared[$template_name][$key])) {
                self::$_prepared[$template_name][$key] = ['hash' => AscetCMSEngine::generate_hash()];
            }
            self::$_prepared[$template_name][$key]['value'] = $value;
        }

        /**
         * @param string $key
         * @param string $template_name
         *
         * @return string|null
         */
        static function get_prepare($key, $template_name = 'main') {
            if (isset(self::$_prepared[$template_name], self::$_prepared[$template_name][$key])) {
                return self::$_prepared[$template_name][$key]['value'];
            } else {
                return null;
            }
        }

        /**
         * @param string $template_html
         */
        static function init_template($template_html) {
            if (!isset(self::$_included[$template_html])) {
                self::$_included[$template_html] = [];
            }
            if (!isset(self::$_options[$template_html])) {
                self::$_options[$template_html] = (object) [];
            }
            if (!isset(self::$_prepared[$template_html])) {
                self::$_prepared[$template_html] = [];
            }
            if (!isset(self::$_already_used[$template_html])) {
                self::$_already_used[$template_html] = (object) ['filename' => [], 'sha512' => [],];
            }
        }

        /**
         * @param string $html
         * @param string $template_html
         *
         * @return string
         */
        static function html_template_finalize($html, $template_html = 'main') {
            self::init_template($template_html);
            $values = self::finalize_include(self::$_included[$template_html], self::$_options[$template_html]);
            foreach ($values as $key => &$value) {
                self::set_prepare($key, self::get_prepare($key, $template_html).$value, $template_html);
            }

            return preg_replace_callback('_\\{#(wait|prepare|prepared):([a-z0-9]{10,150}):(.+?)#\\}_i',
                function ($a) use ($template_html) {
                    $this_key = strtolower($a[3]);
                    $this_hash = $a[2];
                    if (!isset(self::$_prepared[$template_html][$this_key])) {
                        return '';
                    }
                    if (self::$_prepared[$template_html][$this_key]['hash'] !== $this_hash) {
                        return $a[0];
                    }

                    return self::$_prepared[$template_html][$this_key]['value'];
                }, $html);
        }

        /**
         * @param object[] $included
         * @param object   $options
         *
         * @return string[]
         * @throws ViewTemplateException
         */
        private static function finalize_include($included, $options) {
            $codes = [
                self::HEADER_INCLUDE => '',
                self::BODY_BEGINNING_INCLUDE => '',
                self::BODY_END_INCLUDE => '',
                self::PAGE_READY => '',
                self::PAGE_LOAD => '',
            ];
            if (isset($options->document_root)) {
                $sad_root = $options->document_root;
                $integrity_type = isset($options->integrity_type) ? $options->integrity_type : self::INTEGRITY_COMMON;
            } else {
                $sad_root = null;
                $integrity_type = 0;
            }
            foreach ($included as &$piece) {
                // $piece->place
                if (!array_key_exists($piece->place, $codes)) {
                    throw new ViewTemplateException('Finalize: malformed piece\'s place', 6);
                }

                if (isset($piece->filename) and !isset($piece->html_params['integrity']) and
                                                self::get_need_integrity($integrity_type, $piece->type, $piece->filename) and
                                                file_exists($sad_root.$piece->filename)
                ) {
                    $a = microtime(true);
                    $integrity_string = sprintf('sha512-%s sha384-%s',
                        base64_encode(pack('H*', hash_file('sha512', $sad_root.$piece->filename))),
                        base64_encode(pack('H*', hash_file('sha384', $sad_root.$piece->filename)))
                    );
                    $b = microtime(true);
                    self::add_profiling($b - $a, 'file_hash');
                    unset($a, $b);
                } else {
                    $integrity_string = null;
                }
                switch ($piece->type) {
                    case 'js_file':
                        $html_params = $piece->html_params;
                        $html_params['src'] = $piece->filename;
                        if ($integrity_string !== null) {
                            $html_params['integrity'] = $integrity_string;
                        }
                        $element = sprintf('<script%s></script>', \AscetCMS\HTML::compile_params($html_params));
                        break;
                    case 'js':
                        if (!in_array($piece->place, [self::PAGE_READY, self::PAGE_LOAD])) {
                            /** @noinspection BadExpressionStatementJS */
                            $element = sprintf("<script>\n%s\n</script>", $piece->code);
                        } else {
                            /** @noinspection BadExpressionStatementJS */
                            $element = $piece->code;
                        }
                        break;
                    case 'css_file':
                        $html_params = $piece->html_params;
                        $html_params['href'] = $piece->filename;
                        if ($integrity_string !== null) {
                            $html_params['integrity'] = $integrity_string;
                        }
                        $element = sprintf('<link%s />', \AscetCMS\HTML::compile_params($html_params));
                        break;
                    case 'css':
                        $element = sprintf("<style>\n%s\n</style>", $piece->code);
                        break;
                    default:
                        throw new ViewTemplateException('Finalize: malformed piece\'s type', 5);
                }

                $codes[$piece->place] .= $element."\n";
            }

            if ($codes[self::PAGE_READY] != null) {
                $codes[self::BODY_END_INCLUDE] .= sprintf(/** @lang text */
                    "\n<script>\n$(document).ready(function() {\n%s\n});\n</script>", $codes[self::PAGE_READY]);
            }
            if ($codes[self::PAGE_LOAD] != null) {
                $codes[self::BODY_END_INCLUDE] .= sprintf(/** @lang text */
                    "\n<script>\n$(window).load(function() {\n%s\n});\n</script>", $codes[self::PAGE_LOAD]);
            }

            return [
                'template_header' => $codes[self::HEADER_INCLUDE],
                'template_body_beginning' => $codes[self::BODY_BEGINNING_INCLUDE],
                'template_body_end' => $codes[self::BODY_END_INCLUDE],
            ];
        }

        /**
         * @param integer $integrity_type
         * @param integer $piece_type
         * @param string  $piece_filename
         *
         * @return boolean
         */
        private static function get_need_integrity($integrity_type, $piece_type, $piece_filename) {
            if (!in_array($piece_type, ['js_file', 'css_file'])) {
                return false;
            }
            if (!preg_match('_^/(common|images|storage)/_', $piece_filename, $a)) {
                return false;
            }
            switch ($a[1]) {
                case 'common':
                    return ((self::INTEGRITY_COMMON & $integrity_type) === self::INTEGRITY_COMMON);
                case 'images':
                    return ((self::INTEGRITY_IMAGES & $integrity_type) === self::INTEGRITY_IMAGES);
                case 'storage':
                    return ((self::INTEGRITY_STORAGE & $integrity_type) === self::INTEGRITY_STORAGE);
                default:
                    return false;
            }
        }

        /**
         * Подключаем к шаблону JS-файл
         *
         * @param string  $filename
         * @param integer $include_place
         * @param array   $html_params
         * @param string  $template_html
         *
         * @throws ViewTemplateException
         */
        static function include_js_file($filename, $include_place = self::HEADER_INCLUDE, $html_params = [],
                                        $template_html = 'main') {
            if (!in_array($include_place, [self::HEADER_INCLUDE, self::BODY_BEGINNING_INCLUDE, self::BODY_END_INCLUDE])) {
                throw new ViewTemplateException('Malformed param include_place', 1);
            }
            self::include_file($filename, $include_place, $html_params, 'js_file', $template_html);
        }

        /**
         * Подключаем к шаблону CSS-файл
         *
         * @param string  $filename
         * @param integer $include_place
         * @param array   $html_params
         * @param string  $template_html
         *
         * @throws ViewTemplateException
         */
        static function include_css_file($filename, $include_place = self::HEADER_INCLUDE, $html_params = [],
                                         $template_html = 'main') {
            if (!in_array($include_place, [self::HEADER_INCLUDE, self::BODY_BEGINNING_INCLUDE, self::BODY_END_INCLUDE])) {
                throw new ViewTemplateException('Malformed param include_place', 2);
            }
            if (!isset($html_params['rel'])) {
                $html_params['rel'] = 'stylesheet';
            }
            if (!isset($html_params['type'])) {
                $html_params['type'] = 'text/css';
            }
            self::include_file($filename, $include_place, $html_params, 'css_file', $template_html);
        }

        /**
         * @param string  $filename
         * @param integer $include_place
         * @param string  $type
         * @param array   $html_params
         * @param string  $template_html
         */
        private static function include_file($filename, $include_place, $html_params, $type, $template_html) {
            self::init_template($template_html);
            if (in_array($filename, self::$_already_used[$template_html]->filename)) {
                return;
            }
            self::$_included[$template_html][] = (object) [
                'type' => $type,
                'place' => $include_place,
                'filename' => $filename,
                'html_params' => $html_params,
            ];
        }

        /**
         * Подключаем к шаблону JS-код
         *
         * @param string  $js_code
         * @param integer $include_place
         * @param boolean $only_one
         * @param string  $template_html
         *
         * @throws ViewTemplateException
         */
        static function include_js($js_code, $include_place = self::PAGE_READY, $only_one = true, $template_html = 'main') {
            if (!in_array($include_place, [self::HEADER_INCLUDE, self::BODY_BEGINNING_INCLUDE, self::BODY_END_INCLUDE,
                                           self::PAGE_READY, self::PAGE_LOAD,])
            ) {
                throw new ViewTemplateException('Malformed param include_place', 3);
            }
            self::include_code($js_code, $include_place, $only_one, 'js', $template_html);
        }

        /**
         * Подключаем к шаблону inline CSS
         *
         * @param string  $css_code
         * @param integer $include_place
         * @param boolean $only_one
         * @param string  $template_html
         *
         * @throws ViewTemplateException
         */
        static function include_css($css_code, $include_place = self::HEADER_INCLUDE, $only_one = true, $template_html = 'main') {
            if (!in_array($include_place, [self::HEADER_INCLUDE, self::BODY_BEGINNING_INCLUDE, self::BODY_END_INCLUDE,])
            ) {
                throw new ViewTemplateException('Malformed param include_place', 4);
            }
            self::include_code($css_code, $include_place, $only_one, 'css', $template_html);
        }

        /**
         * @param string  $code
         * @param integer $include_place
         * @param string  $type
         * @param boolean $only_one
         * @param string  $template_html
         */
        private static function include_code($code, $include_place, $only_one, $type, $template_html) {
            if (!isset(self::$_included[$template_html])) {
                self::$_included[$template_html] = [];
                self::$_already_used[$template_html] = (object) ['filename' => [], 'sha512' => [],];
            }
            $sha512 = hash('sha512', $code);
            if ($only_one and in_array($sha512, self::$_already_used[$template_html]->sha512)) {
                return;
            }
            self::$_included[$template_html][] = (object) [
                'type' => $type,
                'place' => $include_place,
                'code' => $code,
                'sha512' => $sha512,
            ];
        }

        /**
         * @param double $time
         * @param string $action
         */
        private static function add_profiling($time, $action) {
            if (!array_key_exists($action, self::$_profiling)) {
                self::$_profiling[$action] = $time;
            } else {
                self::$_profiling[$action] += $time;
            }
        }

        /**
         * Весь профайлинг
         *
         * @return double[]|double[][]
         */
        static function get_profiling() {
            $data = [
                'action' => [],
                'all' => 0,
            ];
            foreach (self::$_profiling as $action => &$time) {
                if (array_key_exists($action, $data['action'])) {
                    $data['action'][$action] += $time;
                } else {
                    $data['action'][$action] = $time;
                }
                $data['all'] += $time;
            }

            return $data;
        }
    }

    class ViewTemplateException extends Exception {

    }

?>