<?php

    /**
     * Class SDBExpression
     * сохранение в базу данных
     */
    class SDBExpression {
        /**
         * @var string $_sql
         */
        protected $_sql;

        /**
         * @param string $sql
         */
        function __construct($sql) {
            $this->_sql = $sql;
        }

        /**
         * @return string
         */
        function get_sql() {
            return $this->_sql;
        }

        /**
         * @return string
         */
        function __toString() {
            return $this->_sql;
        }
    }

?>