<?php

    /**
     * Class SDBCriteria
     * Поиск для SDBRecord
     */
    class SDBCriteria {
        protected $_sql_compares = [];
        /**
         * @var string[]|integer[]|double[]|null[]
         */
        public $params = [];
        public $order = '';
        private $_inner_counter = 0;
        private $_prefix;
        /**
         * @var string|null
         */
        public $last_query_block = null;

        function __construct() {
            do {
                $this->_prefix = self::generate_hash(8);
            } while (!preg_match('_^[a-zA-Z]_', $this->_prefix));
        }

        /**
         * @param boolean $must_parenthesis
         *
         * @return string
         */
        function get_full_sql_query_string($must_parenthesis = true) {
            if ($this->is_empty()) {
                return '';
            }

            if (!$must_parenthesis and (count($this->_sql_compares) == 1)) {
                return $this->_sql_compares[0];
            } else {
                return '('.implode(')and(', $this->_sql_compares).')';
            }
        }

        /**
         * Критерия пуста
         *
         * @return boolean
         */
        function is_empty() {
            return (count($this->_sql_compares) == 0);
        }

        /**
         * @param string $sql
         */
        function add_condition($sql) {
            $this->_sql_compares[] = $sql;
        }

        /**
         * Сравнение со значением
         *
         * @param string  $field
         * @param mixed   $value
         * @param boolean $not
         */
        function compare($field, $value, $not = false) {
            if (is_array($value)) {
                $this->add_in_condition($field, $value, $not);

                return;
            }

            $this->add_condition($field.($not ? '<>' : '=').':'.$this->_prefix.$this->_inner_counter);
            $this->params["{$this->_prefix}{$this->_inner_counter}"] = $value;
            $this->_inner_counter++;
        }

        /**
         * Санируем данные для передачи в
         *
         * @param string|integer|double $value
         *
         * @return string
         */
        static function sql_like_sanation($value) {
            if (is_integer($value) or is_double($value)) {
                return $value;
            } else {
                return strtr($value, array('%' => '\\%', '_' => '\\_', '\\' => '\\\\'));
            }
        }

        /**
         * Сравнение строк
         *
         * @param string  $field
         * @param mixed   $value
         * @param boolean $partial
         * @param boolean $not
         * @param boolean $empty_can_be_null
         */
        function compare_string($field, $value, $partial = false, $not = false, $empty_can_be_null = false) {
            if ($partial) {
                // like
                if (($value === '') or is_null($value)) {
                    // Пустая строка
                    $this->add_condition($not ? 'false' : 'true');
                } else {
                    // Непустая строка
                    if (!$not) {
                        $this->add_condition("({$field} like :{$this->_prefix}{$this->_inner_counter})and({$field} is not null)");
                    } else {
                        $this->add_condition("({$field} not like :{$this->_prefix}{$this->_inner_counter})or({$field} is null)");
                    }
                    $this->params["{$this->_prefix}{$this->_inner_counter}"] = '%'.self::sql_like_sanation($value).'%';
                    $this->_inner_counter++;
                }
            } else {
                // =
                if ((($value === '') or is_null($value)) and $empty_can_be_null) {
                    // empty string equals null
                    if (!$not) {
                        $this->add_condition("({$field}='')or({$field} is null)");
                    } else {
                        $this->add_condition("({$field}<>'')and({$field} is not null)");
                    }
                } else {
                    $this->add_condition("{$field} ".($not ? '<>' : '=')." :{$this->_prefix}{$this->_inner_counter}");
                    $this->params["{$this->_prefix}{$this->_inner_counter}"] = is_null($value) ? '' : $value;
                    $this->_inner_counter++;
                }
            }
        }

        /**
         * Из сырого запроса делаем запрос нормальный
         *
         * @param string  $field
         * @param string  $raw_value
         * @param boolean $partial
         */
        function compare_from_text($field, $raw_value, $partial = false) {
            $raw_value = trim($raw_value);
            if ($raw_value == '') {
                return;
            }
            preg_match('_^\\s*(<>|<=|=<|>=|=>|<|>|==|=|!=)?\\s*(.*?)\\s*$_', $raw_value, $matches);
            $value = $matches[2];
            $op = $matches[1];
            $value = self::sanify_double_value($value);
            switch ($op) {
                case '':
                    $op = $partial ? 'like' : '=';
                    break;
                case '==':
                    $op = '=';
                    break;
                case '!=':
                    $op = '<>';
                    break;
                case '=>':
                    $op = '>=';
                    break;
                case '=<':
                    $op = '<=';
                    break;
            }
            if (($value === '') or is_null($value)) {
                $this->add_condition('false');

                return;
            } elseif ($op == 'like') {
                $this->compare_string($field, $value, true, false);

                return;
            }
            $this->add_condition("{$field} {$op} :{$this->_prefix}{$this->_inner_counter}");
            $this->params["{$this->_prefix}{$this->_inner_counter}"] =
                ($op != 'like') ? $value : '%'.self::sql_like_sanation($value).'%';
            $this->_inner_counter++;
        }

        /**
         * Санация пробелов внутри цифр
         *
         * @param string $value
         *
         * @return string
         */
        private static function sanify_double_value($value) {
            if (!preg_match('_^[0-9 ]+([.,][0-9 ]*)?$_', $value)) {
                return $value;
            }
            $value = preg_replace('_[^0-9.]_', '', str_replace(',', '.', $value));

            return preg_replace('_\\.$_', '', $value);
        }

        /**
         * @param string  $field
         * @param array   $value
         * @param boolean $not
         *
         * @throws Exception
         */
        function add_in_condition($field, $value, $not = false) {
            if (!is_array($value)) {
                throw new Exception('Malformed parameter');
            }
            if (!$not) {
                if (count($value) == 1) {
                    $this->add_condition($field.'=:'.$this->_prefix.$this->_inner_counter);
                    $this->params["{$this->_prefix}{$this->_inner_counter}"] = $value[0];
                    $this->_inner_counter++;
                } elseif (count($value) > 0) {
                    /** @noinspection PhpParamsInspection */
                    $this->add_condition($field.' in ('.implode(',', self::sanify_raw_value($value)).')');
                } else {
                    $this->add_condition('false');
                }
            } else {
                if (count($value) == 1) {
                    $this->add_condition($field.'<>:'.$this->_prefix.$this->_inner_counter);
                    $this->params["{$this->_prefix}{$this->_inner_counter}"] = $value[0];
                    $this->_inner_counter++;
                } elseif (count($value) > 0) {
                    /** @noinspection PhpParamsInspection */
                    $this->add_condition($field.' not in ('.implode(',', self::sanify_raw_value($value)).')');
                } else {
                    $this->add_condition('true');
                }
            }
        }

        /**
         * Between
         *
         * @param string  $field
         * @param mixed   $min
         * @param mixed   $max
         * @param boolean $not
         */
        function add_between_condition($field, $min, $max, $not = false) {
            $f1 = $this->_prefix.$this->_inner_counter;
            $this->_inner_counter++;
            $f2 = $this->_prefix.$this->_inner_counter;
            $this->_inner_counter++;
            $this->add_condition($field.($not ? ' not between' : ' between')." :{$f1} and :{$f2}");
            $this->params[$f1] = $min;
            $this->params[$f2] = $max;
        }

        /**
         * Санируем значение для передачи в запрос, без использования встроенной санации PDO
         *
         * @param mixed   $value
         * @param boolean $force_to_string
         *
         * @return mixed
         */
        static function sanify_raw_value($value, $force_to_string = false) {
            if (is_array($value)) {
                foreach ($value as &$raw_value) {
                    $raw_value = self::sanify_lonely_raw_value($raw_value, $force_to_string);
                }

                return $value;
            } else {
                return self::sanify_lonely_raw_value($value, $force_to_string);
            }
        }

        /**
         * @param mixed   $value
         * @param boolean $force_to_string
         *
         * @return string|integer|double
         * @throws Exception
         */
        private static function sanify_lonely_raw_value($value, $force_to_string) {
            if (is_integer($value) or is_double($value) or is_float($value)) {
                return ($force_to_string ? '"'.(string) $value.'"' : $value);
            } elseif (is_string($value)) {
                return '"'.sad_safe_mysql($value).'"';
            } elseif (is_null($value)) {
                return 'null';
            } elseif (is_bool($value)) {
                return ($value ? 'true' : 'false');
            } else {
                throw new Exception('Malformed value in sanify_lonely_raw_value');
            }
        }

        /**
         * Генерируем достаточно уникальный хеш
         *
         * @var integer $key_length Длина хеша
         *
         * @return string Достаточно уникальный хеш
         */
        static function generate_hash($key_length = 8) {
            $hashes = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
            $hashes_count_minus = count($hashes) - 1;
            $hash = '';
            for ($i = 0; $i < $key_length; $i++) {
                $hash .= $hashes[mt_rand(0, $hashes_count_minus)];
            }

            return $hash;
        }
    }

?>