<?php

    /**
     * Class CurlWrapper Обвязка на Curl'ом
     */
    class CurlWrapper {
        var $ch;
        private $_options = [];
        /**
         * @var integer Последний статус от curl при HTTP-запросе
         */
        var $last_errno = 0;
        /**
         * @var string|null Последнее полученное тело (без заголовков)
         */
        var $buffer;

        /**
         * @var string|null Последний полученный заголовок HTTP-запроса
         */
        var $header;

        /**
         * @var string|null Дополнительный накопительный header
         */
        var $additional_header = '';

        /**
         * @var integer|null Последний полученный HTTP-статус
         */
        var $http_status;

        /**
         * @var integer Число попыток для соединения по умолчанию
         */
        var $default_tries_count = 3;

        /**
         * @var string Дефолтное значение, если получить страницу не получилось
         */
        var $default_value = '';

        /**
         * @var string Encoding для сжатия
         */
        var $last_encoding = '';

        function __construct() {
            $this->ch = curl_init();
            $this->set_option(CURLOPT_RETURNTRANSFER, true);
            $this->set_verify_server(null);
            $this->set_option(CURLOPT_CAPATH, '/etc/ssl/certs');// @hint Ненавидьте меня за эту строку
            $this->set_verify_client(null);
            $this->set_option(CURLOPT_FOLLOWLOCATION, true);
            $this->set_option(CURLOPT_HEADER, true);
            $this->set_timeout(3600);
        }

        function __destruct() {
            curl_close($this->ch);
        }

        /**
         * @param integer $key
         * @param mixed   $value
         */
        function set_option($key, $value) {
            $this->_options[$key] = $value;
            curl_setopt($this->ch, $key, $value);
        }

        /**
         * @param integer $key
         *
         * @return mixed
         */
        function get_option($key) {
            return isset($this->_options[$key]) ? $this->_options[$key] : null;
        }

        /**
         * Выставляем таймаут для запросов
         *
         * @param integer $timeout Таймаут в секундах
         */
        function set_timeout($timeout) {
            $this->set_option(CURLOPT_TIMEOUT, $timeout);
        }

        /**
         * Вызываем curl-функции из load_page
         *
         * @hint Вытащено для возможности перегрузки, dependency injection и unit-тестирования
         * @return string
         */
        protected function curl_exec() {
            $buf = curl_exec($this->ch);
            $this->last_errno = curl_errno($this->ch);
            if ($this->last_errno !== 0) {
                return '';
            }

            $this->http_status = (int) curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

            return $buf;
        }

        /**
         * Загружаем страницу
         *
         * @param string            $url         Сам загружаемый URL
         * @param array|string|null $post_data   POST-дата, если есть
         * @param integer|null      $tries_count Количество попыток
         *
         * @return string Буффер
         */
        function load_page($url, $post_data = null, $tries_count = null) {
            $tries_count = ($tries_count === null) ? $this->default_tries_count : max($tries_count, 1);
            $this->set_option(CURLOPT_URL, $url);
            if ($post_data === null) {
                $this->set_option(CURLOPT_POST, false);
            } else {
                $this->set_option(CURLOPT_POST, true);
                $this->set_option(CURLOPT_POSTFIELDS, $post_data);
            }
            $this->last_encoding = '';

            for ($i = 0; $i < $tries_count; $i++) {
                $this->additional_header = '';
                $buf = $this->curl_exec();
                if ($this->last_errno !== 0) {
                    continue;
                }

                while (true) {
                    // @todo Сделать не так костыльно
                    $a = explode("\r\n\r\n", $buf, 3);
                    if (preg_match('_^HTTP/1\\.[01] +(100|300|301|302|200)_', $a[0]) and
                        preg_match('_^HTTP/1\\.[01] +([0-9]{3,3})_', $a[1]) and
                        (count($a) == 3) and (strlen($a[0]) < 4096) and (strlen($a[1]) < 4096)
                    ) {
                        $buf = $a[1]."\r\n\r\n".$a[2];
                        $this->additional_header .= "\r\n\r\n".$a[0];
                    } else {
                        $this->additional_header = (strlen($this->additional_header) > 4)
                            ? substr($this->additional_header, 4) : '';
                        $b = explode("\r\n\r\n", $buf, 2);
                        $this->header = $b[0];
                        $this->buffer = isset($b[1]) ? $b[1] : '';
                        unset($b);
                        break;
                    }
                }
                if (preg_match('|^content\\-encoding:\\s*gzip$|mi', $this->header)) {
                    $new_buffer = @zlib_decode($this->buffer);
                    if ($new_buffer !== false) {
                        $this->buffer = $new_buffer;
                        $this->last_encoding = 'gzip';
                    }
                }

                // @todo другие способы encoding'а

                return $this->buffer;
            }

            return $this->default_value;
        }

        /**
         * Ставим сертификат, которым мы будем проверять валидность сервера.
         * Или null, если сертификата нет и проверять не нужно
         *
         * @param string|null $crt Путь до файла с сертификатом сервера
         */
        function set_verify_server($crt = null) {
            if ($crt === null) {
                $this->set_option(CURLOPT_SSL_VERIFYPEER, false);
            } else {
                $this->set_option(CURLOPT_SSL_VERIFYPEER, true);
                $this->set_option(CURLOPT_CAINFO, ($crt !== '') ? $crt : self::get_crt_bundle_file());
            }
        }

        /**
         * Файл, в котором лежат все сертификаты системы
         *
         * @return string|null
         * @codeCoverageIgnore
         */
        static function get_crt_bundle_file() {
            if (strtolower(substr(PHP_OS, 0, 7)) === 'windows') {
                return null;
            }
            // @todo MacOS
            if (strtolower(substr(PHP_OS, 0, 5)) !== 'linux') {
                return null;
            }
            if (file_exists('/etc/debian_version')) {
                // Debian like
                return '/etc/ssl/certs/ca-certificates.crt';
            } elseif (file_exists('/etc/centos-release')) {
                // Centos
                return '/etc/ssl/certs/ca-bundle.crt';
            } else {
                return null;
            }
        }

        /**
         * Ставим pem-сертификат, которым мы будем доказывать свою валидность.
         * Или null, если сертификата нет и проверять не нужно
         *
         * @param string|null $crt Путь до файла с pem-сертификатом
         * @param string      $key Пароль от сертификата
         */
        function set_verify_client($crt = null, $key = '') {
            if ($crt === null) {
                $this->set_option(CURLOPT_SSL_VERIFYHOST, 0);
            } else {
                $this->set_option(CURLOPT_SSL_VERIFYHOST, 2);
                $this->set_option(CURLOPT_SSLCERT, $crt);
                $this->set_option(CURLOPT_SSLCERTPASSWD, $key);
            }
        }

        /**
         * Удачно ли прошёл последний сделанный запрос
         *
         * @return boolean
         */
        function is_success() {
            return ($this->last_errno === 0) and ($this->http_status >= 200) and ($this->http_status < 300);
        }

        /**
         * @var string $scheme
         *
         * @return integer
         */
        static function get_default_port_for_scheme($scheme) {
            switch (strtolower($scheme)) {
                case 'http':
                    return 80;
                case 'https':
                    return 443;
                default:
                    return -1;
            }
        }

        /**
         * @return string
         */
        function get_effective_url() {
            return curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
        }

        /**
         * Превращаем коллекцию из parse_url обратно в url
         *
         * @param array $parsed_url
         *
         * @return string
         */
        static function unparse_url($parsed_url) {
            if (array_key_exists('scheme', $parsed_url) and ($parsed_url['scheme'] != '')) {
                $scheme = $parsed_url['scheme'].'://';
                if (array_key_exists('port', $parsed_url) and
                    ($parsed_url['port'] != self::get_default_port_for_scheme($parsed_url['scheme']))
                ) {
                    $port = ':'.$parsed_url['port'];
                } else {
                    $port = '';
                }
            } else {
                $scheme = 'http://';
                $port = (array_key_exists('port', $parsed_url) and ($parsed_url['port'] != 80))
                    ? ':'.$parsed_url['port'] : '';
            }
            $host = array_key_exists('host', $parsed_url) ? $parsed_url['host'] : '';
            $user_block = self::unparse_url_user_block($parsed_url);
            $path_block = self::unparse_url_path_block($parsed_url);

            return "{$scheme}{$user_block}{$host}{$port}{$path_block}";
        }

        /**
         * @param array $parsed_url
         *
         * @return string
         */
        static function unparse_url_user_block($parsed_url) {
            if (!isset($parsed_url['user'])) {
                return '';
            }
            $pass = array_key_exists('pass', $parsed_url) ? ':'.$parsed_url['pass'] : '';

            return $parsed_url['user'].$pass.'@';
        }

        /**
         * @param array $parsed_url
         *
         * @return string
         */
        static function unparse_url_path_block($parsed_url) {
            $path = (array_key_exists('path', $parsed_url) and ($parsed_url['path'] != ''))
                ? $parsed_url['path'] : '/';
            $query = (array_key_exists('query', $parsed_url) and ($parsed_url['query'] != ''))
                ? '?'.$parsed_url['query'] : '';
            $fragment = (array_key_exists('fragment', $parsed_url) and ($parsed_url['fragment'] != ''))
                ? '#'.$parsed_url['fragment'] : '';

            return "{$path}{$query}{$fragment}";
        }

    }

?>