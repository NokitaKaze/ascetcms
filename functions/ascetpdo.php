<?php

    /**
     * Class AscetPDO расширение класса PDO специально для AscetCMS
     * Может вести логи в child connection
     *
     * @todo PostgreSQL ready
     */
    class AscetPDO extends PDO {
        /**
         * @var double Длительность выполнения последней операции
         */
        var $last_query_duration;
        /**
         * @var double длительность всех запросов
         */
        var $all_query_duration = 0;
        /**
         * @var integer Количество прошедших транзакций
         */
        var $transactions_count = 0;
        /**
         * @var string Текущий уровень транзакции
         */
        var $transaction_level = '';
        /**
         * @var integer Количество операций после последнего коммита, даже если его не было
         */
        var $query_count_after_last_commit = 0;
        /**
         * Инстанс для дебага, туда пишутся все логи
         *
         * @var AscetPDO|null Debug Instance
         */
        var $debug_instance = null;

        const DEBUG_INSERT_QUERY_BEFORE = 0;// Писать в debug_sql перед выполнением запроса
        const DEBUG_INSERT_QUERY_AFTER = 1;// Писать в debug_sql после выполнения запроса
        /**
         * @var integer Мы вставляем insert'ы о SQL Execution до или после
         */
        var $debug_insert_query_strategy = self::DEBUG_INSERT_QUERY_AFTER;

        /**
         * @var string|null Префикс для таблиц дебага
         */
        var $debug_table_prefix = null;

        /**
         * @var integer|null Последняя строка, добавленная в debug_sql
         */
        protected $debug_sql_insert_id = null;

        /**
         * @var integer|null Последняя строка, добавленная в debug_out
         */
        protected $debug_out_insert_id = null;

        /**
         * @var double|integer
         */
        var $debug_functions_all_exec = 0.0;

        /**
         * @var array Предподготовленные statements
         */
        var $prepared_statements = array();

        /**
         * @var boolean Пытаемся ли мы отловить управление транзакциями?
         * Если false, то считаем, что нигде в коде нет commit/rollback/start transaction напрямую, только через функции
         */
        var $dirty_transactions_methods = true;

        /**
         * @var string Префикс таблиц. То, на что меняется ___ в начале названия таблицы
         */
        var $table_prefix = '';

        /**
         * @var boolean Меняем ли мы префикс таблиц в запросах
         */
        var $table_prefix_change = true;

        /**
         * @var integer Попадание в кеш
         */
        var $prepared_statements_cache_hit = 0;

        /**
         * @var integer Попадание в кеш
         */
        var $prepared_statements_cache_loose = 0;

        /**
         * @var array
         */
        var $error_info_real = array('00000', '', '');

        /**
         * Выполнение запросов к БД
         *
         * Этот метод приватный, к нему не должно быть обращений из другого кода, потому что
         * для нормальных обращений есть публичные методы типа sql_exec
         *
         * @param string     $sql           Текст запроса
         * @param integer    $type          Тип запроса (0 - query, 1 - exec)
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|null Возвращенный запрос или null, если запроса не было или он неудачен
         *
         * @throws PDOException Если произошла ошибка синтаксиса/выполнения запроса, а мы внутри HHVM
         */
        protected function sql_interface($sql, $type, $prepared_keys = null) {
            $q = $this->clear_call($sql, $type, $prepared_keys);

            // Всё исполнено, осталось проверить
            // @todo Не работает нигде кроме MySQL
            if ($this->error_info_real[0] != '00000') {
                $this->sql_interface_debug_error($this->error_info_real);
                throw new PDOException(
                    'SQLSTATE['.$this->error_info_real[0].'] ('.$this->error_info_real[1].') '.$this->error_info_real[2],
                    (int) $this->error_info_real[0]);
            }
            $this->query_count_after_last_commit++;

            if ($type == 1) {
                $q->setFetchMode(PDO::FETCH_ASSOC);

                return $q;
            } else {
                return null;
            }
        }

        /**
         * Вытащен из sql_interface для лучшей читабельности
         *
         * @param string     $sql           Текст запроса
         * @param integer    $type          Тип запроса (0 - query, 1 - exec)
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|null Возвращенный запрос или null, если запроса не было или он неудачен
         */
        private function clear_call($sql, $type, $prepared_keys) {
            /**
             * @var {String} Текст запроса
             */
            $sql_debug_request_text = self::get_debug_request_text($sql, $prepared_keys);
            $this->sql_interface_debug_before($sql_debug_request_text);
            $q = null;

            // Обвешиваем try catch, потому что HHVM реагирует нестандартно и не хочет менять поведение
            $begin_stamp = microtime(true);
            try {
                if ($this->dirty_transactions_methods and ($type == 0)) {
                    // Слежение за включением/выключением транзакций прямо здесь
                    $cleared_sql = $this->get_cleared_sql_call($sql);
                    // @hint switch не завезли, а что?
                    if ($cleared_sql == 'commit') {
                        $this->commit();
                        $this->error_info_real = $this->errorInfo();
                    } elseif ($cleared_sql == 'rollback') {
                        $this->rollBack();
                        $this->error_info_real = $this->errorInfo();
                    } elseif (($cleared_sql == 'start transaction') || ($cleared_sql == 'begin')) {
                        $this->beginTransaction();
                        $this->error_info_real = $this->errorInfo();
                    } else {
                        $q = $this->sql_interface_clear_call($sql, $type, $prepared_keys);
                    }
                } else {
                    $q = $this->sql_interface_clear_call($sql, $type, $prepared_keys);
                }
            } catch (PDOException $e) {
                AscetCMSEngine::console_log($this->error_info_real, 'current', 'sql.log');
                AscetCMSEngine::console_log($e->errorInfo, 'next', 'sql.log');
                $this->error_info_real = $e->errorInfo;
            }
            $end_stamp = microtime(true);
            $this->last_query_duration = $end_stamp - $begin_stamp;
            $this->all_query_duration += $this->last_query_duration;

            $this->sql_interface_debug_after($sql_debug_request_text);

            return $q;
        }

        /**
         * Возвращаем запрос для добавляем в debug DB
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return string SQL-запрос
         */
        static function get_debug_request_text($sql, $prepared_keys) {
            if (($prepared_keys !== null) and (count($prepared_keys) > 0)) {
                $u = true;
                foreach ($prepared_keys as $key => $value) {
                    if (!is_integer($key)) {
                        $u = false;
                        break;
                    }
                }

                $sql .= ' -- @array:';
                foreach ($prepared_keys as $key => $value) {
                    $sql .= $u ? ' ' : " {$key} = ";
                    if (in_array(gettype($value), array('integer', 'double'))) {
                        $sql .= $value.';';
                    } elseif ($value === null) {
                        $sql .= 'null;';
                    } elseif ($value === true) {
                        $sql .= 'true;';
                    } elseif ($value === false) {
                        $sql .= 'false;';
                    } else {
                        $sql .= '"'.sad_safe_mysql($value).'";';
                    }
                }
                unset($key, $value);
            }

            return $sql;
        }

        /**
         * @param string     $sql           Текст запроса
         * @param integer    $type          Тип запроса (0 - query, 1 - exec)
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|integer|null Возвращенный запрос или null, если запроса не было или он неудачен
         *
         * @throws PDOException Если произошла ошибка синтаксиса/выполнения запроса, а мы внутри HHVM
         */
        private function sql_interface_clear_call($sql, $type, $prepared_keys) {
            $sql = $this->get_sql_after_table_prefix_change($sql);
            if ($prepared_keys === null) {
                if ($type == 1) {
                    $q = $this->query($sql);
                } elseif ($type == 0) {
                    $q = $this->exec($sql);
                } else {
                    throw new PDOException('AscetPDO error, type is '.$type, -1);
                }
                $this->error_info_real = $this->errorInfo();
            } else {
                $q = $this->get_statement($sql);
                $q->closeCursor();
                $q->execute($prepared_keys);
                $this->error_info_real = $q->errorInfo();
            }

            return $q;
        }

        /**
         * Правим ___ на название таблицы
         *
         * @param string $sql
         *
         * @returns string
         */
        function get_sql_after_table_prefix_change($sql) {
            if ($this->table_prefix_change) {
                return preg_replace('|`___([a-z0-9_]+)`|i', '`'.$this->table_prefix.'$1`', $sql);
            } else {
                return $sql;
            }
        }

        /**
         * Пытаемся получить название функции или операнда из SQL запроса
         *
         * Вычищаем все переходы строки, табуляцию etc
         *
         * @param string $sql Запрос, который надо почистить
         *
         * @return string
         */
        static function get_cleared_sql_call($sql) {
            if (!preg_match('|^[ \\t\\n\\r]*?(.*?)[ \\t\\n\\r]*;?$|si', strtolower($sql), $clear_sql)) {
                return $sql;
            }

            $call = preg_replace('|[ \\t\\r\\n]+|', ' ', $clear_sql[1]);
            $call = preg_replace('|^[ \\t\\r\\n]*(.+?)[ \\t\\r\\n]*$|', '$1', $call);

            return $call;
        }

        /**
         * Раздел, отвечающий за Debug Instance
         */

        /**
         * Инициируем Debug Instance
         *
         * @param stdClass $settings
         */
        function debug_instance_init(stdClass $settings) {
            $timestamp1 = microtime(true);
            try {
                $database_connection_debug = new AscetPDO(
                    $settings->driver.':host='.$settings->host.';dbname='.$settings->db,
                    $settings->login, $settings->password);
            } catch (PDOException $e) {
                // @hint Если отвалился debug sql сервер, то и хер с ним
                $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);

                return;
            }

            // Меняем политику Insert Query Strategy
            if (isset($settings->insert_query_strategy)) {
                $this->debug_insert_query_strategy = $settings->insert_query_strategy;
            }

            if (isset($settings->prefix)) {
                $this->debug_table_prefix = $settings->prefix;
            }

            // Настраиваем это подключение
            $database_connection_debug->sql_exec('SET NAMES utf8;');
            ob_start();
            print_r($_POST);
            $p = ob_get_contents();
            ob_end_clean();
            // @todo posix_getpid не работает под Windows
            // @todo connection_id работает только под MySQL
            $database_connection_debug->sql_exec(
                'INSERT INTO `'.$this->debug_table_prefix.'debug_out` (`domain`, `url`, `post_data`, '.
                '`time`, `ip`, `pid`, `sql_pid`) VALUES ("'.sad_safe_mysql($_SERVER['HTTP_HOST']).'","'.
                sad_safe_mysql($_SERVER['REQUEST_URI']).'", "'.sad_safe_mysql($p).'", '.microtime(true).
                ', inet_aton("'.$_SERVER['REMOTE_ADDR'].'"), '.posix_getpid().', CONNECTION_ID());');
            unset($p);

            $this->debug_out_insert_id = (int) $database_connection_debug->lastInsertId();
            $database_connection_debug->sql_exec('set @sql_debug_id='.$this->debug_out_insert_id.';');

            // Вносим в дочернее подключение
            $this->debug_instance = $database_connection_debug;
            $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);
        }

        /**
         * Метод, выполняющийся до непосредственно выполнения sql-выражения
         *
         * @param string $sql SQL-выражение, которое надо выполнить
         */
        protected function sql_interface_debug_before($sql) {
            if (($this->debug_instance !== null) and ($this->debug_insert_query_strategy == self::DEBUG_INSERT_QUERY_BEFORE)) {
                $timestamp1 = microtime(true);
                try {
                    $this->debug_instance->sql_exec(
                        'INSERT INTO `'.$this->debug_table_prefix.
                        'debug_sql` (`time`, `sql`, `request_id`) VALUES(:time, :sql, @sql_debug_id);',
                        array(':time' => microtime(true), ':sql' => $sql));
                    $this->debug_sql_insert_id = (int) $this->debug_instance->lastInsertId();
                } catch (Exception $e) {
                    $this->debug_sql_insert_id = 0;
                }
                $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);
            }
        }

        /**
         * Метод, выполняющийся до непосредственно после sql-выражения
         *
         * @param string $sql SQL-выражение, которое надо выполнить
         */
        protected function sql_interface_debug_after($sql) {
            if ($this->debug_instance !== null) {
                $timestamp1 = microtime(true);
                try {
                    if ($this->debug_insert_query_strategy == self::DEBUG_INSERT_QUERY_BEFORE) {
                        $this->debug_instance->sql_exec(
                            'update `'.$this->debug_table_prefix.
                            'debug_sql` set `duration`=:duration where `id`=:id;',
                            array(':duration' => $this->last_query_duration, ':id' => $this->debug_sql_insert_id)
                        );
                    } elseif ($this->debug_insert_query_strategy == self::DEBUG_INSERT_QUERY_AFTER) {
                        $this->debug_instance->sql_exec(
                            'INSERT INTO `'.$this->debug_table_prefix.
                            'debug_sql` (`time`, `sql`, `request_id`, `duration`) VALUES(:time, :sql, @sql_debug_id, :duration);',
                            array(
                                ':duration' => $this->last_query_duration,
                                ':sql' => $sql,
                                ':time' => microtime(true)
                            )
                        );
                        $this->debug_sql_insert_id = (int) $this->debug_instance->lastInsertId();
                    }
                } catch (Exception $e) {
                    // @hint Если отвалился debug sql сервер, то и хер с ним
                }
                $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);
            }
        }

        /**
         * Метод, выполняющийся при SQL ошибке
         *
         * @param array $errorInfo Данные, отданные PDO::errorInfo
         */
        protected function sql_interface_debug_error(array $errorInfo) {
            if ($this->debug_instance !== null) {
                $timestamp1 = microtime(true);
                try {
                    $this->debug_instance->sql_exec(
                        'INSERT INTO `'.$this->debug_table_prefix.'debug_sql_error` (`sql_id`, `text`) VALUES ('.
                        $this->debug_sql_insert_id.', "'.sad_safe_mysql($errorInfo[0].' ('.$errorInfo[1].'): '.$errorInfo[2]).
                        '");');
                } catch (Exception $e) {
                    // @hint Если отвалился debug sql сервер, то и хер с ним
                }
                $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);
            }
        }

        /**
         * Метод, выполняющийся в самом конце
         *
         * @param double  $duration Время, между init и after_last_echo
         * @param integer $size     Размер тела вывода
         */
        function debug_close_session($duration, $size) {
            if ($this->debug_instance !== null) {
                $timestamp1 = microtime(true);
                try {
                    $this->debug_instance->sql_exec(
                        'update `'.$this->debug_table_prefix.'debug_out` '.
                        'set `duration`='.(float) $duration.', `size`='.(int) $size.' where `id`=@sql_debug_id;');
                } catch (Exception $e) {
                    // @hint Если отвалился debug sql сервер, то и хер с ним
                }
                $this->debug_functions_all_exec += (float) (microtime(true) - $timestamp1);
            }
        }


        /**
         * Раздел, отвечающий за транзакции
         */

        /**
         * Начинаем транзакцию
         *
         * @return boolean PDO::beginTransaction
         */
        function beginTransaction() {
            if (!$this->inTransaction()) {
                $this->sql_interface_debug_before('start transaction; -- @ func');
                $r = parent::beginTransaction();
                $this->sql_interface_debug_after('start transaction; -- @ func');

                return $r;
            }

            return true;
        }

        function commit() {
            if ($this->inTransaction()) {
                $this->sql_interface_debug_before('commit; -- @ func');
                $r = parent::commit();
                $this->sql_interface_debug_after('commit; -- @ func');

                return $r;
            }

            $this->query_count_after_last_commit = 0;

            return true;
        }

        function rollBack() {
            if ($this->inTransaction()) {
                $this->sql_interface_debug_before('rollback; -- @ func');
                $r = parent::rollBack();
                $this->sql_interface_debug_after('rollback; -- @ func');

                return $r;
            }

            $this->query_count_after_last_commit = 0;

            return true;
        }


        /**
         * Запрос к БД с ожиданием возврата с последующим возвратом первого вхожения
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return array|null Возвращенное первое вхождение или null, если запрос неудачен
         */
        function sql_query_fetch($sql, $prepared_keys = null) {
            $q = $this->sql_interface($sql, 1, $prepared_keys);
            if (!is_object($q)) {
                return null;
            }

            return $q->fetch();
        }

        /**
         * Запрос к БД без ожидания возврата
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         */
        function sql_exec($sql, $prepared_keys = null) {
            $this->sql_interface($sql, 0, $prepared_keys);
        }

        /**
         * Запрос к БД с ожиданием возврата
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|null Возврат. Или null, если запрос неудачен
         */
        function sql_query($sql, $prepared_keys = null) {
            return $this->sql_interface($sql, 1, $prepared_keys);
        }

        /**
         * ID последней внесённой в БД записи
         *
         * @return integer Номер записи
         */
        function sql_insert_id() {
            return (int) $this->lastInsertId();
        }

        /**
         * Коммит старой транзакции в БД и начало новой
         */
        function sql_next_transaction() {
            if ($this->query_count_after_last_commit == 0) {
                return;
            }

            $this->commit();
            $this->beginTransaction();
            $this->transactions_count++;
        }

        /**
         * Смена уровня изоляции транзакций
         *
         * @param string $transaction_level Уровень изоляции
         */
        function sql_level($transaction_level = '') {
            if ($transaction_level == '') {
                $transaction_level = 'read uncommitted';
            }
            if ($transaction_level == $this->transaction_level) {
                return;
            }
            $this->transaction_level = $transaction_level;
            $this->commit();
            $this->sql_exec('set session transaction isolation level '.$transaction_level.';');
            $this->beginTransaction();
            $this->transactions_count++;
        }

        /**
         * Берём/создаёт statement для выполнения запроса
         *
         * @param string $sql Текст запроса
         *
         * @return PDOStatement Возвращенный statement
         */
        protected function get_statement($sql) {
            $hash = hash('sha512', $sql);
            if (isset($this->prepared_statements[$hash])) {
                $this->prepared_statements_cache_hit++;

                return $this->prepared_statements[$hash];
            } else {
                $this->prepared_statements_cache_loose++;
                $new_statement = $this->prepare($sql);
                $this->prepared_statements[$hash] = $new_statement;

                return $new_statement;
            }
        }

        /**
         * Очичащаем все prepared statement
         */
        function flush_statements() {
            $this->prepared_statements = array();
        }

        /**
         * Деструктор
         */
        function __destruct() {
            // Насильственно дропаем коннект
            $this->debug_instance = null;
        }
    }// class AscetPDO


    // Global functions, не использующие AscetCMSEngine
    /**
     * Санация запроса к БД
     *
     * @param string $text Текст, который надо санировать для передачи напрямую в запрос
     *
     * @return string
     */
    function sad_safe_mysql($text) {
        $a = array(
            array('\\', '\\\\'),
            array('"', '\\"'),
            array("'", "\\'"),
            array(chr(0), '\\0'),
            array('`', '\\`')
        );
        foreach ($a as $pair) {
            $text = str_replace($pair[0], $pair[1], $text);
        }

        return $text;
    }

    /**
     * Возврат количества найденных строк от запроса к БД
     *
     * @param PDOStatement|null $q Уже сделанный запрос к БД
     *
     * @return integer
     */
    function sql_num($q) {
        if (!is_object($q)) {
            return 0;
        }

        return $q->rowCount();
    }

    /**
     * Дёргаем следующую строку из запроса
     *
     * @param PDOStatement|null $q Уже сделанный запрос к БД
     *
     * @return array|null
     */
    function sql_fetch($q) {
        if (!is_object($q)) {
            return null;
        }

        $r = $q->fetch();

        return (is_array($r) ? $r : null);
    }

    /**
     * Дёргание всех строк в запросе к БД и превращение их в один массив
     *
     * @param PDOStatement|null $q
     * @param array|object      $fields    Название полей в (int/float), которые надо превратить в int и float
     * @param boolean           $is_object Превращаем ли мы строку SQL-сервера в объект или оставляем как массив
     *
     * @return array|object
     */
    function sql_fetch_all($q, $fields = array(), $is_object = true) {
        if (!is_object($q)) {
            return array();
        }
        $fields = (object) $fields;

        $value = array();
        while ($q_c = $q->fetch()) {
            $datum = $q_c;
            if (isset($fields->int)) {
                foreach ($fields->int as $key) {
                    $datum[$key] = (int) $datum[$key];
                }
            }
            if (isset($fields->float)) {
                foreach ($fields->float as $key) {
                    $datum[$key] = (float) $datum[$key];
                }
            }

            $value[] = $is_object ? (object) $datum : $datum;
        }

        return $value;
    }


?>