<?php

    /*
       Copyright 2012 BiSe Trojanov

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    */

    namespace AscetCMS {

        class HTML {
            private static $_i;
            private static $_blind;

            /**
             * @var string[]
             */
            private static $_code_from =
                ['&quot;', '&amp;', '&apos;', '&lt;', '&gt;', '&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;',
                 '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&shy;', '&reg;', '&macr;', '&deg;',
                 '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;',
                 '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;',
                 '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;',
                 '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;',
                 '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;',
                 '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;',
                 '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;',
                 '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;',
                 '&yuml;', '&OElig;', '&oelig;', '&Scaron;', '&scaron;', '&Yuml;', '&fnof;', '&circ;', '&tilde;', '&Alpha;',
                 '&Beta;', '&Gamma;', '&Delta;', '&Epsilon;', '&Zeta;', '&Eta;', '&Theta;', '&Iota;', '&Kappa;', '&Lambda;',
                 '&Mu;', '&Nu;', '&Xi;', '&Omicron;', '&Pi;', '&Rho;', '&Sigma;', '&Tau;', '&Upsilon;', '&Phi;', '&Chi;', '&Psi;',
                 '&Omega;', '&alpha;', '&beta;', '&gamma;', '&delta;', '&epsilon;', '&zeta;', '&eta;', '&theta;', '&iota;',
                 '&kappa;', '&lambda;', '&mu;', '&nu;', '&xi;', '&omicron;', '&pi;', '&rho;', '&sigmaf;', '&sigma;', '&tau;',
                 '&upsilon;', '&phi;', '&chi;', '&psi;', '&omega;', '&thetasym;', '&upsih;', '&piv;', '&ensp;', '&emsp;',
                 '&thinsp;', '&zwnj;', '&zwj;', '&lrm;', '&rlm;', '&ndash;', '&mdash;', '&lsquo;', '&rsquo;', '&sbquo;',
                 '&ldquo;', '&rdquo;', '&bdquo;', '&dagger;', '&Dagger;', '&bull;', '&hellip;', '&permil;', '&prime;', '&Prime;',
                 '&lsaquo;', '&rsaquo;', '&oline;', '&frasl;', '&euro;', '&image;', '&weierp;', '&real;', '&trade;', '&alefsym;',
                 '&larr;', '&uarr;', '&rarr;', '&darr;', '&harr;', '&crarr;', '&lArr;', '&uArr;', '&rArr;', '&dArr;', '&hArr;',
                 '&forall;', '&part;', '&exist;', '&empty;', '&nabla;', '&isin;', '&notin;', '&ni;', '&prod;', '&sum;', '&minus;',
                 '&lowast;', '&radic;', '&prop;', '&infin;', '&ang;', '&and;', '&or;', '&cap;', '&cup;', '&int;', '&there4;',
                 '&sim;', '&cong;', '&asymp;', '&ne;', '&equiv;', '&le;', '&ge;', '&sub;', '&sup;', '&nsub;', '&sube;', '&supe;',
                 '&oplus;', '&otimes;', '&perp;', '&sdot;', '&lceil;', '&rceil;', '&lfloor;', '&rfloor;', '&lang;', '&rang;',
                 '&loz;', '&spades;', '&clubs;', '&hearts;', '&diams;',];

            /**
             * @var integer[]
             */
            private static $_code_from_number =
                [34, 38, 39, 60, 62, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177,
                 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199,
                 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221,
                 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243,
                 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 338, 339, 352, 353, 376, 402, 710, 732, 913, 914,
                 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 931, 932, 933, 934, 935, 936, 937,
                 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966,
                 967, 968, 969, 977, 978, 982, 8194, 8195, 8201, 8204, 8205, 8206, 8207, 8211, 8212, 8216, 8217, 8218, 8220, 8221,
                 8222, 8224, 8225, 8226, 8230, 8240, 8242, 8243, 8249, 8250, 8254, 8260, 8364, 8465, 8472, 8476, 8482, 8501, 8592,
                 8593, 8594, 8595, 8596, 8629, 8656, 8657, 8658, 8659, 8660, 8704, 8706, 8707, 8709, 8711, 8712, 8713, 8715, 8719,
                 8721, 8722, 8727, 8730, 8733, 8734, 8736, 8743, 8744, 8745, 8746, 8747, 8756, 8764, 8773, 8776, 8800, 8801, 8804,
                 8805, 8834, 8835, 8836, 8838, 8839, 8853, 8855, 8869, 8901, 8968, 8969, 8970, 8971, 9001, 9002, 9674, 9824, 9827,
                 9829, 9830,];
            /**
             * @var string[]
             */
            private static $_code_to = null;

            /**
             * @param string $html
             *
             * @return array[]
             */
            static private function sad_html_parse_a($html) {
                $a = array('_tag' => '', '_closed' => false, '_selfclosed' => false);
                $i = self::$_i + 1;
                if ($html[$i] == '/') {
                    $i++;
                    $a['_closed'] = true;
                }
                for (; $i < strlen($html); $i++) {
                    if (preg_match('_[!a-z0-9]_i', $html[$i])) {
                        $a['_tag'] .= $html[$i];
                    } else {
                        break;
                    }
                }
                for (; $i < strlen($html); $i++) {
                    if ($html[$i] == ' ' or $html[$i] == "\r" or $html[$i] == "\n" or $html[$i] == "\t") {
                        continue;
                    }
                    if ($html[$i] == '>') {
                        break;
                    }
                    if ($html[$i].$html[$i + 1] == '/>') {
                        $i++;
                        $a['_selfclosed'] = true;
                        break;
                    }
                    if (preg_match('|[a-z0-9-]|i', $html[$i])) {
                        $s = $html[$i];
                        $s1 = '';
                        $i++;
                        while (preg_match('|[a-z0-9_:-]|i', $html[$i])) {
                            $s .= $html[$i];
                            $i++;
                        }
                        if ($html[$i] == "=") {
                            $c = $html[$i + 1];
                            if ($c !== '"' and $c !== "'") {
                                $s1 .= $c;
                            }
                            for ($i = $i + 2; $i < strlen($html); $i++) {
                                if (($c == '"') or ($c == "'")) {
                                    if ($html[$i] == $c) {
                                        break;
                                    }
                                } else {
                                    if (preg_match('_[ />]_', $html[$i])) {
                                        $i--;
                                        break;
                                    }
                                }
                                $s1 .= $html[$i];
                            }
                        } else {
                            $i--;
                        }
                        if (!isset($a[$s])) {
                            $a[$s] = $s1;
                        }
                    }
                }
                self::$_i = $i;

                return $a;
            }

            /**
             * @param string $html
             */
            private static function parse_set_blind($html) {
                self::$_blind = [];
                $off = 0;
                while (true) {
                    $tag_comment = strpos($html, '<!--', $off);
                    $tag_script = strpos($html, '<script', $off);
                    if (($tag_comment === false) and ($tag_script === false)) {
                        break;
                    }
                    if (($tag_comment === false) or (($tag_script !== false) and ($tag_script < $tag_comment))) {
                        $start = $tag_script;
                        $end = strpos($html, '</script>', $start + 8);
                        if ($end === false) {
                            self::$_blind[] = array($start, strlen($html));

                            return;
                        }
                        $end += 8;
                    } else {
                        $start = $tag_comment;
                        $end = strpos($html, '-->', $start + 4);
                        if ($end === false) {
                            self::$_blind[] = array($start, strlen($html));

                            return;
                        }
                        $end += 2;
                    }
                    $off = $end + 1;
                    self::$_blind[] = array($start, $end);
                }
            }

            /**
             * @param string $html
             *
             * @return array[]
             */
            static function parse($html) {
                $a = array(0 => array('_text' => '', '_tag' => ''));
                self::$_i = 0;
                self::parse_set_blind($html);

                for (self::$_i = 0; self::$_i < strlen($html); self::$_i++) {
                    $u = false;
                    $j = count(self::$_blind);
                    for ($i = 0; $i < $j; $i++) {
                        if (self::$_i >= self::$_blind[$i][0] and
                            self::$_i <= self::$_blind[$i][1]
                        ) {
                            self::$_i = self::$_blind[$i][1];
                            $u = true;
                            break;
                        }
                    }
                    if ($u) {
                        continue;
                    }

                    if (substr($html, self::$_i, 1) == '<' and
                        preg_match('_[\\/!a-z]_i', substr($html, self::$_i + 1, 1))
                    ) {
                        $b = self::sad_html_parse_a($html);
                        $a[] = $b;
                        $a[count($a) - 1]['_text'] = '';
                        continue;
                    }
                    $a[count($a) - 1]['_text'] .= $html[self::$_i];
                }

                return $a;
            }

            /**
             * @param array[] $a
             * @param string  $ex
             *
             * @return string
             */
            static function get_text_from_tags($a, $ex = '') {
                $charset = '';
                foreach ($a as &$node) {
                    if (!isset($node['_tag']) or (strtolower($node['_tag']) !== 'meta') or
                        !isset($node['http-equiv']) or (strtolower($node['http-equiv']) !== 'content-type') or
                        !preg_match('_charset=(.+)$_i', $node['content'], $b)
                    ) {
                        continue;
                    }
                    $charset = $b[1];
                    break;
                }
                if ($charset == '') {
                    $charset = 'utf-8';
                }

                $text = '';
                foreach ($a as &$node) {
                    if (preg_match('_(style|script)_i', $node['_tag']) or (strlen($node['_text']) < 1)) {
                        continue;
                    }
                    $text .= @iconv($charset, 'utf-8', $node['_text']).$ex;
                }

                return $text;
            }

            /**
             * @param string $text
             *
             * @return string
             */
            static function encode($text) {
                return htmlspecialchars($text);
            }

            /**
             * Инициализируем _code_to
             */
            private static function init_code_to() {
                if (self::$_code_to !== null) {
                    return;
                }

                self::$_code_to = [];
                foreach (self::$_code_from_number as &$code) {
                    /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
                    self::$_code_to[] = \AscetCMS\Helper\UTF8::chr($code);
                }
            }

            /**
             * @param string  $text
             * @param boolean $utf_code_too
             *
             * @return string
             */
            static function decode($text, $utf_code_too = true) {
                self::init_code_to();

                if ($utf_code_too) {
                    /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
                    $text = \AscetCMS\Helper\UTF8::HTMLToUTF($text);
                }

                return str_replace(self::$_code_from, self::$_code_to, $text);
            }

            /**
             * @param string  $href
             * @param string  $anchor
             * @param array   $params
             * @param boolean $is_anchor_html
             *
             * @return string
             */
            static function link($href, $anchor, $params = [], $is_anchor_html = false) {
                if ($href != '') {
                    $params['href'] = $href;
                }
                if (!$is_anchor_html) {
                    $anchor = self::encode($anchor);
                }

                return sprintf('<a%s>%s</a>', self::compile_params($params), $anchor);
            }

            /**
             * @param array $params
             *
             * @return string
             */
            static function compile_params($params) {
                $ret = '';
                $k = 0;
                foreach ($params as $key => &$value) {
                    // @hint Здесь специально строгое сравнение: (0 != 'key') === true
                    if ($k !== $key) {
                        $ret .= sprintf(' %s="%s"', self::encode($key), self::encode($value));
                    } else {
                        $ret .= sprintf(' %s', self::encode($value));
                    }
                    if (is_integer($key)) {
                        $k++;
                    }
                }

                return $ret;
            }

        }
    }

    namespace {

        /**
         * Class HTML
         *
         * @deprecated
         * @codeCoverageIgnore
         */
        class HTML {
            /**
             * @param string $html
             *
             * @return array[]
             */
            static function parse($html) {
                return AscetCMS\HTML::parse($html);
            }

            /**
             * @param array[] $a
             * @param string  $ex
             *
             * @return string
             */
            static function get_text_from_tags($a, $ex = '') {
                return AscetCMS\HTML::get_text_from_tags($a, $ex);
            }

            /**
             * @param string $text
             *
             * @return string
             */
            static function encode($text) {
                return AscetCMS\HTML::encode($text);
            }

            /**
             * @param string $text
             *
             * @return string
             */
            static function decode($text) {
                return AscetCMS\HTML::decode($text);
            }

            /**
             * @param string  $href
             * @param string  $anchor
             * @param array   $params
             * @param boolean $is_anchor_html
             *
             * @return string
             */
            static function link($href, $anchor, $params = [], $is_anchor_html = false) {
                return AscetCMS\HTML::link($href, $anchor, $params, $is_anchor_html);
            }

            /**
             * @param array $params
             *
             * @return string
             */
            static function compile_params($params) {
                return AscetCMS\HTML::compile_params($params);
            }
        }

        /**
         * @param string $text
         *
         * @return string
         * @codeCoverageIgnore
         */
        function sad_safe_html($text) {
            return htmlspecialchars($text);
        }

        /**
         * @param string $html
         *
         * @return array[]
         * @deprecated
         * @codeCoverageIgnore
         */
        function sad_html_parse($html) {
            return \AscetCMS\HTML::parse($html);
        }

        /**
         * @param array[] $a
         * @param string  $ex
         *
         * @return string
         * @deprecated
         * @codeCoverageIgnore
         */
        function sad_html_text($a, $ex = '') {
            return \AscetCMS\HTML::get_text_from_tags($a, $ex);
        }
    }

?>