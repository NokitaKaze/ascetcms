<?php
    /*
       Copyright 2011 BiSe Trojanov

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    */

    /**
     * Этот файл устарел и является лишь набором враппер-функций
     */

    /**
     * @param DomNode $node
     * @param string  $path
     *
     * @return DOMNode|string|null
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_xml_getnode($node, $path) {
        return AscetCMS\Helper\XML::get_node($node, $path);
    }

    /**
     * @param DomNode $node
     * @param string  $pattern
     *
     * @return DOMNode[]|null
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_xml_get_all_nodes($node, $pattern) {
        return AscetCMS\Helper\XML::get_all_nodes($node, $pattern);
    }

    /**
     * @param DomNode $node
     * @param string  $name
     *
     * @return string|null
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_xml_attribute($node, $name) {
        return AscetCMS\Helper\XML::get_attribute($node, $name);
    }

    /**
     * @param string $text
     *
     * @return DOMElement
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_xml_rawtext($text) {
        return AscetCMS\Helper\XML::get_dom_from_raw_text($text);
    }

    /**
     * @param string  $text
     * @param boolean $put_in_cdata
     *
     * @return mixed|string
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_safe_xml($text, $put_in_cdata = true) {
        return AscetCMS\Helper\XML::sanify($text, $put_in_cdata);
    }

    /**
     * @param string  $text
     * @param boolean $put_in_comment
     *
     * @return mixed|string
     * @deprecated
     * @codeCoverageIgnore
     */
    function sad_safe_xmlc($text, $put_in_comment = true) {
        return AscetCMS\Helper\XML::sanify_comment($text, $put_in_comment);
    }

?>