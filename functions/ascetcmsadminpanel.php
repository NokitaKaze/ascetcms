<?php

    class AscetCMSAdminPanel {
        static function parse_url() {
            if (preg_match('|^/i/(.*?)$|', AscetCMSEngine::$sad_url, $a)
                and (AscetCMSEngine::$sad_url !== '/i/login.php') and (AscetCMSEngine::$sad_url !== '/i/logout.php')
            ) {
                AscetCMSEngine::add_event_listener('before_main_code', function () {
                    AscetCMSAdminPanel::main_before_main_code();
                }, 0);
            }
        }

        static function main_before_main_code() {
            AscetCMSEngine::session_start();
            AscetCMSEngine::error_reporting(E_ALL ^ E_NOTICE);
            AscetCMSEngine::$header_cache_control[] = 'private';
            if (AscetCMSEngine::iam()) {
                $lang_list = AscetCMSEngine::get_clean_accept_language_list();
                AscetCMSEngine::$options->additional->LANGS = null;
                $LANGS_t = null;
                foreach ($lang_list as $lang) {
                    if ($lang === 'en') {
                        /** @noinspection PhpIncludeInspection */
                        include(AscetCMSEngine::$sad_root.'/functions/ascetcms_langpack/en.php');
                        /** @var array $LANGS_t */
                        AscetCMSEngine::$options->additional->LANGS = $LANGS_t;
                        break;
                    } elseif (($lang === 'ru') or ($lang === 'ru-ru')) {
                        /** @noinspection PhpIncludeInspection */
                        include(AscetCMSEngine::$sad_root.'/functions/ascetcms_langpack/ru.php');
                        /** @var array $LANGS_t */
                        AscetCMSEngine::$options->additional->LANGS = $LANGS_t;
                        break;
                    }
                }
                if (AscetCMSEngine::$options->additional->LANGS === null) {
                    /** @noinspection PhpIncludeInspection */
                    include(AscetCMSEngine::$sad_root.'/functions/ascetcms_langpack/en.php');
                    /** @var array $LANGS_t */
                    AscetCMSEngine::$options->additional->LANGS = $LANGS_t;
                }
                AscetCMSEngine::set_prepare('baseurl', AscetCMSEngine::$sad_baseurl);
                AscetCMSEngine::set_prepare('domain', AscetCMSEngine::$options->domain);

                /*
                 @todo session
                AscetCMSEngine::set_prepare('rss_url',
                    AscetCMSEngine::$options->protocol.'://'.AscetCMSEngine::$options->domain.AscetCMSEngine::$sad_baseurl.
                    '/i/rss.php?pass='.md5(AscetCMSEngine::$options->domain.':'.$_SESSION['sad_password']));
                */

                self::toolbar_generate();

                return null;
            } else {
                header('location: '.AscetCMSEngine::$sad_baseurl.'/i/login.php?url='.
                       urlencode($_SERVER['REQUEST_URI']));
                AscetCMSEngine::$http_status = 302;

                return false;
            }
        }

        static function toolbar_generate() {
            ob_start();
            $LANGS = AscetCMSEngine::$options->additional->LANGS;
            // Генерируем
            echo '<ul class="toolbar">';
            $sad_toolbar = array(
                array(
                    'label' => $LANGS['title']['main'],
                    'title' => $LANGS['title']['l_main'],
                    'href' => '/i/',
                    'children' => array(
                        array('label' => '<img src="'.AscetCMSEngine::$sad_baseurl.
                                         '/common/adminpanel/wand.png" style="border: none; width: 16px; height: 16px; margin-top: -1px;">'.
                                         $LANGS['title']['option'], 'title' => $LANGS['title']['l_option'],
                              'href' => '/i/options.php'),
                        array('label' => $LANGS['title']['phpinfo'], 'title' => $LANGS['title']['l_phpinfo'],
                              'href' => '/i/phpinfo.php'),
                        array('label' => $LANGS['title']['sql'], 'title' => $LANGS['title']['l_sql'], 'href' => '/i/sql.php')
                    )
                ),
                array(
                    'label' => $LANGS['title']['upload'],
                    'title' => $LANGS['title']['l_upload'],
                    'href' => '/i/uploadfiles.php',
                    'children' => array(
                        array('label' => $LANGS['title']['pictures'], 'title' => $LANGS['title']['l_pictures'],
                              'href' => '/i/pictures.php')
                    )
                ),
                array(
                    'label' => $LANGS['title']['visits'],
                    'title' => $LANGS['title']['l_visits'],
                    'href' => '/i/visits.php',
                    'children' => array(
                        array('label' => $LANGS['title']['history'], 'href' => '/i/history.php')
                    )
                ),
            );
            if (AscetCMSEngine::iamd()) {
                $sad_toolbar[1]['children'][] =
                    array('label' => '<img src="'.AscetCMSEngine::$sad_baseurl.
                                     '/common/adminpanel/blue-document-attribute-p.png" style="border: none; margin-top: -1px;">'.
                                     $LANGS['title']['code'], 'title' => $LANGS['title']['l_code'],
                          'href' => '/i/showcode.php');
            }

            if (file_exists(AscetCMSEngine::$sad_root.'/templates/static/i/toolbar.php')) {
                /** @noinspection PhpIncludeInspection */
                include(AscetCMSEngine::$sad_root.'/templates/static/i/toolbar.php');
            }
            $sad_toolbar[] = array(
                'label' => $LANGS['title']['logout'],
                'title' => $LANGS['title']['l_logout'],
                'href' => '/i/logout.php',
            );

            $sad_buf = "\r\n";
            $j = count($sad_toolbar);
            for ($menu1 = 0; $menu1 < $j; $menu1++) {
                echo '<li><a href="'.AscetCMSEngine::$sad_baseurl.$sad_toolbar[$menu1]['href'].'" title="'.
                     $sad_toolbar[$menu1]['title'].
                     '" id="tool'.$menu1.'" onMouseOver="toolbarover(this)" onMouseOut="toolbarout(this)">'.
                     $sad_toolbar[$menu1]['label'].'</a></li>';
                if ($menu1 + 1 < count($sad_toolbar)) {
                    /** @noinspection HtmlUnknownAnchorTarget */
                    /** @noinspection HtmlUnknownTarget */
                    echo '<li><img src="'.AscetCMSEngine::$sad_baseurl.
                         '/common/adminpanel/delim.png" usemap="#nav'.$menu1.'"></li>';
                    $sad_buf .= '<map name="nav'.$menu1.'"><area shape="poly" coords="0,2,15,17,0,32" href="'.
                                AscetCMSEngine::$sad_baseurl.$sad_toolbar[$menu1]['href'].'" title="'.
                                $sad_toolbar[$menu1]['title'].
                                '"/><area shape="poly" coords="2,2,22,2,22,32,2,32,17,17" href="'.AscetCMSEngine::$sad_baseurl.
                                $sad_toolbar[$menu1 + 1]['href'].'" title="'.$sad_toolbar[$menu1 + 1]['title'].'"/></map>';
                }
                if (!isset($sad_toolbar[$menu1]['children'])) {
                    $sad_toolbar[$menu1]['children'] = array();
                }
                $sad_buf .= '<div class="toolbar_hid" id="hid_tool'.$menu1.
                            '" onMouseOver="toolbarover(this)" onMouseOut="toolbarout(this)">';
                $n = count($sad_toolbar[$menu1]['children']);
                for ($menu2 = 0; $menu2 < $n; $menu2++) {
                    $sad_buf .= "<a href=\"".AscetCMSEngine::$sad_baseurl.$sad_toolbar[$menu1]['children'][$menu2]['href'].
                                "\" title=\"".
                                $sad_toolbar[$menu1]['children'][$menu2]['title']."\">".
                                $sad_toolbar[$menu1]['children'][$menu2]['label']."</a>\r\n";
                }
                $sad_buf .= "</div>\r\n";
            }
            /** @noinspection HtmlUnknownTarget */
            echo '</ul>'.$sad_buf.'<script src="'.AscetCMSEngine::$sad_baseurl.'/common/adminpanel/toolbar.js"></script>';
            // Теперь пишем
            $buf = ob_get_contents();
            ob_end_clean();
            AscetCMSEngine::set_prepare('adminpanel_toolbar_html', $buf);
        }

        /**
         * @param array    $q_c
         * @param string   $t_id
         * @param string[] $a
         * @param integer  $image_count
         *
         * @return array
         */
        static function admin_edit_parse_tab_null($q_c, $t_id, $a, $image_count) {
            $pattern = '<label for="el'.$t_id.'" title="'.sad_safe_html($a[1]).'">'.sad_safe_html($a[4]).
                       '</label><input class="textbox" id="el'.$t_id.'" name="data_'.$a[1].
                       '" value="'.sad_safe_html($q_c[$a[1]]).'"><br />'."\r\n";

            return array($pattern, $image_count);
        }

        /**
         * @param array    $q_c
         * @param string   $t_id
         * @param string[] $a
         * @param integer  $image_count
         *
         * @return array
         */
        static function admin_edit_parse_tab_text($q_c, $t_id, $a, $image_count) {
            $pattern = '';
            if ($a[3] == 'text') {
                $t = 'tinyMCE';
            } else {
                $t = '';
            }
            $pattern .= '<label for="el'.$t_id.'" class="textarea" title="'.sad_safe_html($a[1]).'">'.
                        sad_safe_html($a[4]).'</label>';

            if ($a[3] == 'text') {
                $pattern .= '(<a href="javascript:" onclick="tinyMCE.get(\'el'.
                            $t_id.'\').show();" title="Show MCE Tiny">show</a>/'.
                            '<a href="javascript:" onclick="tinyMCE.get(\'el'.$t_id.
                            '\').hide();" title="Hide MCE Tiny">hide</a>)';
            }
            $pattern .= '<br /><textarea class="'.$t.'" id="el'.$t_id.'" name="data_'.
                        sad_safe_html($a[1]).'">'.sad_safe_html($q_c[$a[1]]).
                        '</textarea><br />'."\r\n";

            return array($pattern, $image_count);
        }

        /**
         * @param array    $q_c
         * @param string   $t_id
         * @param string[] $a
         * @param integer  $image_count
         *
         * @return array
         */
        static function admin_edit_parse_tab_date($q_c, $t_id, $a, $image_count) {
            if (strlen($q_c[$a[1]]) < 1) {
                $q_c[$a[1]] = time();
            }
            $pattern = '<label for="el'.$t_id.'" title="'.sad_safe_html($a[1]).'">'.sad_safe_html($a[4]).
                       '</label><script>DrawCalendar("'.$t_id.'","'.sad_safe_html($a[1]).
                       '",'.(int) ($q_c[$a[1]]).');</script><noscript><input class="" id="el'.
                       $t_id.'" name="data_'.$a[1].'" value="'.sad_safe_html($q_c[$a[1]]).
                       '"></noscript><br />'."\r\n";


            return array($pattern, $image_count);
        }

        /**
         * @param array    $q_c
         * @param string   $t_id
         * @param string[] $a
         * @param integer  $image_count
         *
         * @return array
         */
        static function admin_edit_parse_tab_checkbox($q_c, $t_id, $a, $image_count) {
            if (strlen($q_c[$a[1]]) < 1) {
                $q_c[$a[1]] = 0;
            }
            $pattern = '<label title="'.sad_safe_html($a[1]).'">'.sad_safe_html($a[4]).
                       '</label><input id="el'.$t_id.'_1" name="data_'.$a[1].
                       '" type="radio" style="width: 10px;" value="1"';
            if ($q_c[$a[1]] > 0) {
                $pattern .= ' checked';
            }
            $pattern .= '><label for="el'.$t_id.'_1" style="width: auto; color: green;">'.
                        'On</label><input id="el'.$t_id.'_0" name="data_'.$a[1].
                        '" type="radio" style="width: 10px;" value="0"';
            if ($q_c[$a[1]] < 1) {
                $pattern .= ' checked';
            }
            $pattern .= '><label for="el'.$t_id.'_0" style="width: auto; color: red;">'.
                        "Off</label><br />\r\n";


            return array($pattern, $image_count);
        }

        /**
         * @param string  $thumb_set
         * @param integer $count
         *
         * @return array
         */
        function admin_edit_parse_tab_image_additional1($thumb_set, $count) {
            $image_thumb =
                array('width' => 200, 'height' => 200, 'path' => '', 'prefix' => '', 'filetype' => 'jpg');
            $tmp_c = explode(' ', $thumb_set);
            $num_num1 = 0;
            $num_str1 = 0;
            $error = '';
            foreach ($tmp_c as $tmp_cs) {
                if ($tmp_cs == '') {
                    continue;
                }

                if (preg_match('_^([0-9]+|auto)$_i', $tmp_cs)) {
                    $num_num1++;
                    switch ($num_num1) {
                        case 1:
                            $image_thumb['width'] = $tmp_cs;
                            break;
                        case 2:
                            $image_thumb['height'] = $tmp_cs;
                            break;
                        default:
                            $error =
                                "Error parse image-tag near `<b>{$tmp_cs}</b>`, invalid count of numeric ".
                                "in thumb settings #".($count + 1)."<br />";
                    }
                    continue;
                }

                if (preg_match('|^"([a-z0-9/._-]*)"$|i', $tmp_cs, $tmp_cs2)) {
                    $num_str1++;
                    $tmp_cs2 = $tmp_cs2[1];
                    switch ($num_str1) {
                        case 1:
                            $image_thumb['path'] = sad_safe_path($tmp_cs2);
                            break;
                        case 2:
                            $image_thumb['prefix'] = sad_safe_path($tmp_cs2);
                            break;
                        case 3:
                            $tmp_cs2 = strtolower($tmp_cs2);
                            if ($tmp_cs2 == 'jpeg') {
                                $tmp_cs2 = 'jpg';
                            }
                            if ($tmp_cs2 == 'jpg' or $tmp_cs2 == 'png' or $tmp_cs2 == 'gif') {
                                $image_thumb['filetype'] = $tmp_cs2;
                            } else {
                                $error .= "Error parse image-tag near `<b>".sad_safe_html($tmp_cs2).
                                          "</b>`, invalid filetype in thumb settings #".($count + 1).
                                          ", only gif, png, jpg allowed<br />";
                            }
                            break;
                        default:
                            $error .= "Error parse image-tag near `<b>".sad_safe_html($tmp_cs2).
                                      "</b>`, invalid count of string in thumb settings #".
                                      ($count + 1)."<br />";
                    }
                    continue;
                }
            }

            return array($image_thumb, $error);
        }

        /**
         * @param array  $image_set
         * @param string $option_string
         *
         * @return array
         */
        function admin_edit_parse_tab_image_additional2($image_set, $option_string) {
            $tmp_c = explode(' ', preg_replace('_\\[.+?\\]_', '', $option_string));
            $num_num2 = 0;
            // main set
            foreach ($tmp_c as $tmp_cs) {
                if ($tmp_cs == '') {
                    continue;
                }
                if ($tmp_cs == 'nohash') {
                    $image_set['nohash'] = true;
                    continue;
                }

                if (preg_match('_^[0-9]+$_', $tmp_cs)) {
                    $num_num2++;
                    switch ($num_num2) {
                        case 1:
                            $image_set['width'] = (int) $tmp_cs;
                            break;
                        case 2:
                            $image_set['height'] = (int) $tmp_cs;
                            break;
                        case 3:
                            $image_set['maxsize'] = (int) $tmp_cs;
                            break;
                        default:
                            $image_set['error'] .= "Error parse image-tag near <b>{$tmp_cs}</b>, invalid count of numeric in main settings<br />";
                    }
                    continue;
                }

                if (preg_match('|^"([a-z0-9./_-]*)"$|i', $tmp_cs, $tmp_cs2)) {
                    $image_set['path'] = sad_safe_path($tmp_cs2[1]);
                    continue;
                }
            }

            return $image_set;
        }

        /**
         * @param array    $q_c
         * @param string   $t_id
         * @param string[] $a
         * @param integer  $image_count
         *
         * @return array
         */
        static function admin_edit_parse_tab_image($q_c, $t_id, $a, $image_count) {
            $pattern = '';
            preg_match('_^ *({(.+?)})? *(.+?) *$_', $a[4], $tmp_b);
            $a[4] = $tmp_b[3];
            $image_set =
                array('width' => 10000, 'height' => 10000, 'maxsize' => 50 * 1024 * 1024, 'path' => '',
                      'nohash' => false,
                      'error' => '');
            $image_thumbs = array();
            $image_count++;
            preg_match_all('_(\\[(.+?)\\])_', $tmp_b[2], $tmp_c);
            // thumbs
            foreach ($tmp_c[2] as $thumb_set) {
                list($t, $error) = self::admin_edit_parse_tab_image_additional1($thumb_set, count($image_thumbs));
                $image_thumbs[] = $t;
                $image_set['error'] = $error;
            }

            $image_set = self::admin_edit_parse_tab_image_additional2($image_set, $tmp_b[2]);
            unset($t, $error, $tmp_c, $tmp_b);

            //Draw
            if ($image_set['error'] !== '') {
                $pattern .= $image_set['error'];
            } else {
                $pattern .= '<label for="el'.$t_id.'" title="'.sad_safe_html($a[1]).'">'.sad_safe_html($a[4]).
                            '</label><input id="el'.$t_id.'" class="textbox" name="opt_image_'.$image_count.
                            '_oldvalue" value="'.sad_safe_html($q_c[$a[1]]).'"><input name="opt_image_'.$image_count.
                            '_file" type="file"><br />Maxsize <b>'.$image_set['maxsize'].'</b> bytes; Resolution <b>'.
                            $image_set['width'].'</b>x<b>'.$image_set['height'].
                            '</b><input type="hidden" name="opt_image_'.
                            $image_count.'_name" value="'.$a[1].'"><input type="hidden" name="opt_image_'.
                            $image_count.
                            '_width" value="'.$image_set['width'].'"><input type="hidden" name="opt_image_'.
                            $image_count.
                            '_height" value="'.$image_set['height'].'"><input type="hidden" name="opt_image_'.
                            $image_count.
                            '_path" value="'.$image_set['path'].'"><input type="hidden" name="opt_image_'.
                            $image_count.
                            '_size" value="'.$image_set['maxsize'].'">';

                $tmp_b = 0;
                foreach ($image_thumbs as $image_thumb) {
                    if (AscetCMSEngine::iamd()) {
                        $pattern .= '; thumb [Resolution <b>'.$image_thumb['width'].'</b>x<b>'.$image_thumb['height'].
                                    '</b>; Filetype: <b>'.$image_thumb['filetype'].'</b>] ';
                    }
                    $pattern .= '<input type="hidden" name="opt_image_'.$image_count.'_'.$tmp_b.'_width" value="'.
                                $image_thumb['width'].'"><input type="hidden" name="opt_image_'.$image_count.'_'.$tmp_b.
                                '_height" value="'.$image_thumb['height'].'"><input type="hidden" name="opt_image_'.
                                $image_count.'_'.$tmp_b.'_path" value="'.$image_thumb['path'].
                                '"><input type="hidden" name="opt_image_'.$image_count.'_'.$tmp_b.'_prefix" value="'.
                                $image_thumb['prefix'].'"><input type="hidden" name="opt_image_'.$image_count.'_'.
                                $tmp_b.'_filetype" value="'.$image_thumb['filetype'].'">';
                    $tmp_b++;
                }
            }

            return array($pattern, $image_count);
        }

        /**
         * @param string  $tab
         * @param array   $q_c
         * @param integer $image_count
         * @param boolean $used_sname
         * @param boolean $used_parent
         *
         * @return array
         */
        static function admin_edit_parse_tab($tab, $q_c, $image_count, $used_sname, $used_parent) {
            ob_start();
            /** @noinspection PhpIncludeInspection */
            include(AscetCMSEngine::$sad_root.'/templates/edit/'.$tab);
            $button_code = ob_get_contents();
            ob_end_clean();

            $b_s = preg_split('_(\r|\n)_', $button_code);
            $pattern = '';
            srand();
            foreach ($b_s as &$pat) {
                if (!preg_match('|^([0-9a-z_-]+) +(\\[(.*?)\\] +)?(.*)$|i', $pat, $a)) {
                    $pattern .= $pat."\r\n";
                    continue;
                }

                unset($a[0], $a[2]);
                $a[3] = preg_replace('|^ *(.*?) *$|', '$1', strtolower($a[3]));
                if ($a[1] == 'sname') {
                    $used_sname = true;
                } elseif ($a[1] == 'parent') {
                    $used_parent = true;
                }
                $t_id = mt_rand(1, 10000000);

                if ($a[3] === '') {
                    //textbox
                    list($t, $image_count) =
                        self::admin_edit_parse_tab_null($q_c, $t_id, $a, $image_count);
                    $pattern .= $t;
                } elseif (($a[3] == 'simpletext') or ($a[3] == 'text')) {
                    //textarea
                    list($t, $image_count) =
                        self::admin_edit_parse_tab_text($q_c, $t_id, $a, $image_count);
                    $pattern .= $t;
                } elseif ($a[3] == 'date') {
                    //date
                    list($t, $image_count) =
                        self::admin_edit_parse_tab_date($q_c, $t_id, $a, $image_count);
                    $pattern .= $t;
                } elseif ($a[3] == 'checkbox') {
                    //checkbox
                    list($t, $image_count) =
                        self::admin_edit_parse_tab_checkbox($q_c, $t_id, $a, $image_count);
                    $pattern .= $t;
                } elseif ($a[3] == 'image') {
                    //image
                    list($t, $image_count) =
                        self::admin_edit_parse_tab_image($q_c, $t_id, $a, $image_count);
                    $pattern .= $t;
                }
            }

            $full_template = '<!-- tab '.$tab.' --><div class="form" id="tab'.$tab.'">'."\r\n{$pattern}</div>\n\n";

            return array($full_template, $image_count, $used_sname, $used_parent);
        }

    }

?>