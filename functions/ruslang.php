<?php

    /*
       Copyright 2011 BiSe Trojanov

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
    */

    abstract class RusLang {
        static $months_rus = [
            null,
            array(null, 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август',
                  'сентябрь', 'октябрь', 'ноябрь',
                  'декабрь'),
            array(null, 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа',
                  'сентября', 'октября', 'ноября',
                  'декабря'),

        ];

        /**
         * Транслитерация
         *
         * Не ГОСТовая
         *
         * @param string  $rus
         * @param boolean $diacritical
         * @param integer $limit
         *
         * @return string
         */
        static function transliteration($rus, $diacritical = true, $limit = 50) {
            $kir = array(
                'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ',
                'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э',
                'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', 'ё',
                'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ',
                'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э',
                'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'ё'
            );
            $lat = array(
                'j', 'c', 'u', 'k', 'e', 'n', 'g', 'sh', 'sch', 'z', 'kh', $diacritical ? '-' : '',
                'f', 'y', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'zh', 'e',
                'ya', 'ch', 's', 'm', 'i', 't', $diacritical ? '-' : '', 'b', 'yu', 'yo',
                'J', 'C', 'U', 'K', 'E', 'N', 'G', 'Sh', 'Sch', 'Z', 'Kh', $diacritical ? '-' : '',
                'F', 'Y', 'V', 'A', 'P', 'R', 'O', 'L', 'D', 'Zh', 'E',
                'Ya', 'Ch', 'S', 'M', 'I', 'T', $diacritical ? '-' : '', 'B', 'Yu', 'Yo'
            );
            $lat = str_replace($kir, $lat, $rus);
            $lat = preg_replace('|[^a-z0-9-]|i', '-', $lat);//все нелатинские превращаем в тире
            $lat = preg_replace('|-+|', '-', $lat);      //много тире подряд превращаем в одно тире
            $lat = preg_replace('|^-+|', '', $lat);      //удалить все тире в начале
            $lat = preg_replace('|-+$|', '', substr($lat, 0, $limit));

            return $lat;
        }

        /**
         * Склонение числительных
         *
         * @param integer $num   Число
         * @param string  $form0 Орехов
         * @param string  $form1 Орех
         * @param string  $form2 Ореха
         *
         * @return string
         */
        static function declension($num, $form0, $form1, $form2) {
            $num = $num % 100;
            if ($num >= 5 and $num <= 20) {
                return $form0;
            } else {
                switch ($num % 10) {
                    case 0:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        return $form0;
                    case 1:
                        return $form1;
                    case 2:
                    case 3:
                    case 4:
                        return $form2;
                    default:
                        // @hint Это невозможно
                        return '';
                }
            }
        }

        /**
         * @param $text
         *
         * @return string
         */
        static function first_letter_smart($text) {
            $text = preg_replace_callback('_([^а-я])([а-я])_ui', function ($a) {
                return $a[1].mb_strtoupper($a[2]);
            }, ' '.mb_strtolower($text));

            return mb_substr($text, 1);
        }

        /**
         * numeric-функции
         */

        /**
         * @var string[][]
         */
        static $rus_numeric_array = [
            null,
            [// именительный падеж
             0 => 'ноль',
             1 => '',// в женском/среднем роде отличие
             2 => '', // в женском/среднем роде отличие
             3 => 'три',
             4 => 'четыре',
             5 => 'пять',
             6 => 'шесть',
             7 => 'семь',
             8 => 'восемь',
             9 => 'девять',
             10 => 'десять',
             11 => 'одинадцать',
             12 => 'двенадцать',
             13 => 'тринадцать',
             14 => 'четырнадцать',
             15 => 'пятнадцать',
             16 => 'шестнадцать',
             17 => 'семнадцать',
             18 => 'восемнадцать',
             19 => 'девятнадцать',
             20 => 'двадцать',
             30 => 'тридцать',
             40 => 'сорок',
             50 => 'пятьдесят',
             60 => 'шестьдесят',
             70 => 'семьдесят',
             80 => 'восемьдесят',
             90 => 'девяносто',
             100 => 'сто',
             200 => 'двести',
             300 => 'триста',
             400 => 'четыреста',
             500 => 'пятьсот',
             600 => 'шестьсот',
             700 => 'семьсот',
             800 => 'восемьсот',
             900 => 'девятьсот',
            ],
        ];

        /**
         * @param integer $num
         * @param integer $gender
         *
         * @return string
         */
        static function rus_numeric($num, $gender) {
            $hundred = floor($num / 100);
            $num = $num % 100;
            if ($hundred > 0) {
                $s = self::$rus_numeric_array[1][$hundred * 100].' ';
            } else {
                $s = '';
            }

            if ($num >= 20) {
                $s .= self::$rus_numeric_array[1][floor($num / 10) * 10].' ';
                $num = $num % 10;
                switch ($num) {
                    case 0:
                        break;
                    case 1:
                        switch ($gender) {
                            case 0:
                                $s .= 'одна ';
                                break;
                            case 1:
                                $s .= 'один ';
                                break;
                            case 2:
                                $s .= 'одно ';
                                break;
                        }
                        break;
                    case 2:
                        switch ($gender) {
                            case 0:
                                $s .= 'две ';
                                break;
                            case 1:
                                $s .= 'два ';
                                break;
                            case 2:
                                $s .= 'два ';
                                break;
                        }
                        break;
                    default:
                        $s .= self::$rus_numeric_array[1][$num].' ';
                        break;
                }
            } else {
                switch ($num) {
                    case 0:
                        break;
                    case 1:
                        switch ($gender) {
                            case 0:
                                $s .= 'одна ';
                                break;
                            case 1:
                                $s .= 'один ';
                                break;
                            case 2:
                                $s .= 'одно ';
                                break;
                        }
                        break;
                    case 2:
                        switch ($gender) {
                            case 0:
                                $s .= 'две ';
                                break;
                            case 1:
                                $s .= 'два ';
                                break;
                            case 2:
                                $s .= 'два ';
                                break;
                        }
                        break;
                    default:
                        $s .= self::$rus_numeric_array[1][$num].' ';
                        break;
                }
            }

            return $s;
        }

        /**
         * @param $f
         * @param $gender
         *
         * @return string
         */
        static function in_words($f, $gender) {
            if ($f == 0) {
                return 'ноль';
            }
            $s = '';
            $mil = floor($f / 1000000);
            $f = $f % 1000000;
            if ($mil > 0) {
                $s .= self::rus_numeric($mil, 1).' '.self::declension($mil, 'миллионов', 'миллион', 'миллиона');
            }

            $thou = floor($f / 1000);
            $f = $f % 1000;
            if ($thou > 0) {
                $s .= self::rus_numeric($thou, 0).' '.self::declension($thou, 'тысяч', 'тысяча', 'тысячи').' ';
            }
            $s .= self::rus_numeric($f, $gender);
            $s = preg_replace('| {2,}|', ' ', $s);
            $s = preg_replace('|^ *(.+?) *$|', '$1', $s);

            return $s;
        }

        /**
         * @param string  $f
         * @param integer $gender
         * @param string  $form0
         * @param string  $form1
         * @param string  $form2
         *
         * @return string
         */
        static function in_words_full($f, $gender, $form0, $form1, $form2) {
            return self::in_words($f, $gender).' '.self::declension($f, $form0, $form1, $form2);
        }

        /**
         * Число даты прописью
         *
         * @param integer $d
         * @param integer $tail
         *
         * @return string
         */
        static function date_in_words($d, $tail) {
            switch ($d) {
                case 1:
                    $day = 'перво';
                    break;
                case 2:
                    $day = 'второ';
                    break;
                case 3:
                    $day = 'третье';
                    break;
                case 4:
                    $day = 'четвёрто';
                    break;
                case 5:
                    $day = 'пято';
                    break;
                case 6:
                    $day = 'шесто';
                    break;
                case 7:
                    $day = 'седьмо';
                    break;
                case 8:
                    $day = 'восьмо';
                    break;
                case 9:
                    $day = 'девято';
                    break;

                case 10:
                    $day = 'десято';
                    break;
                case 11:
                    $day = 'одинадцато';
                    break;
                case 12:
                    $day = 'двенадцато';
                    break;
                case 13:
                    $day = 'трмнадцато';
                    break;
                case 14:
                    $day = 'четырнадцато';
                    break;
                case 15:
                    $day = 'пятнадцато';
                    break;
                case 16:
                    $day = 'шестнадцато';
                    break;
                case 17:
                    $day = 'семнадцато';
                    break;
                case 18:
                    $day = 'восемнадцато';
                    break;
                case 19:
                    $day = 'девятнадцато';
                    break;

                case 21:
                    $day = 'двадцать перво';
                    break;
                case 22:
                    $day = 'двадцать второ';
                    break;
                case 23:
                    $day = 'двадцать треть';
                    break;
                case 24:
                    $day = 'двадцать четвёрто';
                    break;
                case 25:
                    $day = 'двадцать пято';
                    break;
                case 26:
                    $day = 'двадцать шесто';
                    break;
                case 27:
                    $day = 'двадцать седьмо';
                    break;
                case 28:
                    $day = 'двадцать восьмо';
                    break;
                case 29:
                    $day = 'двадцать девято';
                    break;

                case 30:
                    $day = 'тридцатое';
                    break;
                case 31:
                    $day = 'тридцать первое';
                    break;
                default:
                    return null;
            }
            switch ($tail) {
                case 1:
                    return $day.'е';
                    break;
                case 2:
                    return $day.'го';
                    break;
                default:
                    // @todo Дописать
                    return null;
            }
        }

        /**
         * Год прописью
         *
         * @param integer $y
         * @param integer $case
         *
         * @return string
         * @todo Переписать порнографию
         */
        static function year_in_words($y, $case) {
            switch ($case) {
                case 1:
                    switch ($y) {
                        case 2012:
                            return 'две тысячи двенадцатый';
                        case 2013:
                            return 'две тысячи тринадцатый';
                        case 2014:
                            return 'две тысячи четырнадцатый';
                        case 2015:
                            return 'две тысячи пятнадцатый';
                        case 2016:
                            return 'две тысячи шестнадцатый';
                        case 2017:
                            return 'две тысячи семнадцатый';
                        case 2018:
                            return 'две тысячи восемнадцатый';
                        case 2019:
                            return 'две тысячи девятнадцатый';
                        case 2020:
                            return 'две тысячи двадцатый';
                        case 2021:
                            return 'две тысячи двадцать первый';
                        case 2022:
                            return 'две тысячи двадцать второй';
                        default:
                            // @todo Переписать порнографию
                            return '';
                    }
                case 2:
                    switch ($y) {
                        case 2012:
                            return 'две тысячи двенадцатого';
                        case 2013:
                            return 'две тысячи тринадцатого';
                        case 2014:
                            return 'две тысячи четырнадцатого';
                        case 2015:
                            return 'две тысячи пятнадцатого';
                        case 2016:
                            return 'две тысячи шестнадцатого';
                        case 2017:
                            return 'две тысячи семнадцатого';
                        case 2018:
                            return 'две тысячи восемнадцатого';
                        case 2019:
                            return 'две тысячи девятнадцатого';
                        case 2020:
                            return 'две тысячи двадцатого';
                        case 2021:
                            return 'две тысячи двадцать первого';
                        case 2022:
                            return 'две тысячи двадцать второго';
                        default:
                            // @todo Переписать порнографию
                            return '';
                    }
                default:
                    // @todo Переписать порнографию
                    return '';
            }
        }
    }

    /**
     * @param string  $rus
     * @param boolean $diactik
     * @param integer $limit
     *
     * @return string
     * @codeCoverageIgnore
     */
    function Translit($rus, $diactik = true, $limit = 50) {
        return RusLang::transliteration($rus, $diactik, $limit);
    }

    /**
     * Функция-враппер для склонения числительных
     *
     * @param integer $num
     * @param string  $form0
     * @param string  $form1
     * @param string  $form2
     *
     * @return string
     * @codeCoverageIgnore
     */
    function RusCase($num, $form0, $form1, $form2) {
        return RusLang::declension($num, $form0, $form1, $form2);
    }

    /**
     * @param $text
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function first_letter_smart($text) {
        return RusLang::first_letter_smart($text);
    }

    /**
     * @param $f
     * @param $gender
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function in_words($f, $gender) {
        return RusLang::in_words($f, $gender);
    }

    /**
     * @param string  $f
     * @param integer $gender
     * @param string  $form0
     * @param string  $form1
     * @param string  $form2
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function in_words_full($f, $gender, $form0, $form1, $form2) {
        return RusLang::in_words_full($f, $gender, $form0, $form1, $form2);
    }

    /**
     * @param integer $num
     * @param integer $gender
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function rus_numeric($num, $gender) {
        return RusLang::rus_numeric($num, $gender);
    }

    /**
     * Число даты прописью
     *
     * @param integer $d
     * @param integer $tail
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function date_in_words($d, $tail) {
        return RusLang::date_in_words($d, $tail);
    }

    /**
     * @param $y
     * @param $case
     *
     * @return string
     * @deprecated
     * @codeCoverageIgnore
     */
    function year_in_words($y, $case) {
        return RusLang::year_in_words($y, $case);
    }

?>