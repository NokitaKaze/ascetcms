<?php

    /**
     * Class KeyValueStorageMemcache Key-value хранилище, использующее MemCache-сервер
     */
    class KeyValueStorageMemcache extends iAscetKeyValueStorage {
        var $memcache;
        const ERROR_CODE = 200;

        /**
         * @param object $settings
         *
         * @throws KeyValueException
         */
        function __construct($settings) {
            $host = isset($settings->host) ? $settings->host : '127.0.0.1';
            $port = isset($settings->port) ? $settings->port : 11211;
            $timeout = isset($settings->timeout) ? $settings->timeout : 10;
            $this->memcache = new Memcache();

            if (!$this->memcache->connect($host, $port, $timeout)) {
                throw new KeyValueException('Can not connect to Memcached server '.$host.':'.$port, self::ERROR_CODE + 2);
            }

            $this->settings = $settings;
            $this->standard_prefix_strategy();
        }

        /**
         * @param string  $key
         * @param mixed   $value
         * @param integer $ttl
         *
         * @throws KeyValueException
         */
        function set_value($key, $value, $ttl = 86400) {
            $ts1 = microtime(true);
            $data = $this->form_datum_value($value, $ttl);
            $this->get_lock($key);
            $result = $this->memcache->set($this->get_fullkey($key), serialize($data), 0, $ttl);
            if ($result === false) {
                throw new KeyValueException('Can not save value', self::ERROR_CODE + 3);
            }
            $this->release_lock();
            self::add_profiling(microtime(true) - $ts1, static::class, 'set_value');
        }

        /**
         * @param string $key
         *
         * @return mixed|null
         */
        protected function get_value_full_clear($key) {
            $this->get_lock($key);
            $buf = $this->memcache->get($this->get_fullkey($key));
            $this->release_lock();
            if ($buf === false) {
                return null;
            }
            if (!is_string($buf)) {
                return null;
            }
            $data = @unserialize($buf);
            if (($data === false) and ($buf != serialize(false))) {
                return null;
            }

            return $data;
        }

        /**
         * @param string $key
         *
         * @return string
         */
        protected function get_fullkey($key) {
            return 'ascetkey_'.$this->prefix.hash('sha512', $key);
        }

        /**
         * @param string $key
         */
        function delete_value($key) {
            $ts1 = microtime(true);
            $this->get_lock($key);
            $this->memcache->delete($key);
            $this->release_lock();
            self::add_profiling(microtime(true) - $ts1, static::class, 'set_value');
        }
    }


?>