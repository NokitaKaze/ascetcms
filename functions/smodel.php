<?php

    /**
     * Class SModel
     * Модель
     *
     * @property-read string|null $default_name Дефолтное имя
     * @property-read string|null $default_url  Дефолтная ссылка
     * @property      array       $attributes
     */
    abstract class SModel {
        const HAS_MANY = 0;
        const BELONGS_TO = 1;
        const MANY_TO_MANY = 2;

        private $_scenario;

        private $_errors = [];

        /**
         * @var SModel[][] $_relations Коллекция реляционных объектов для этого конкретного объекта
         */
        private $_relations = [];

        /**
         * @var string[][]|integer[][] Коллекция примарных ключей для реляционных объектов для этого конкретного объекта
         */
        private $_is_relations_static = [];

        /**
         * @var SModel[][] Коллекция реляционных объектов для статики
         */
        private static $_relations_static_collection = [];

        /**
         * @var string[] Список релляций, которые были загружены для этого объекта
         */
        protected $_relations_loaded = [];

        /**
         * @var array $_attributes
         */
        private $_attributes = [];

        function __construct($scenario = 'default') {
            $this->setScenario($scenario);
            foreach (array_keys(static::relations()) as $relation_name) {
                $this->_is_relations_static[$relation_name] = [];
            }
        }

        /**
         * @param string $new_scenario
         */
        function setScenario($new_scenario) {
            $this->_scenario = $new_scenario;
        }

        /**
         * @return string
         */
        function getScenario() {
            return $this->_scenario;
        }

        /**
         * Метод со список атрибутов для переопределения через IOC
         *
         * @return array[]
         */
        static function get_attributes_list_overload() {
            return [];
        }

        /**
         * Получаем список атрибутов, их имён и типов
         *
         * @return array[]
         */
        static function get_attributes_list() {
            return static::get_attributes_list_overload();
        }

        /**
         * Примарный ключ
         *
         * @return string|string[]|null
         */
        static function getPrimary_key() {
            return 'id';
        }

        /**
         * Получаем тип атрибута из общего списка атрибутов, но не из правил
         *
         * @param string $name
         *
         * @return string
         */
        static function get_attribute_type($name) {
            $list = static::get_attributes_list();
            if (isset($list[$name]) and array_key_exists('type', $list[$name])) {
                return strtolower($list[$name]['type']);
            }

            return 'mixed';
        }

        /**
         * Получаем тип атрибута из общего списка атрибутов, но не из правил
         *
         * @param string $name
         *
         * @return string|null
         */
        static function get_attribute_additional_type($name) {
            $list = static::get_attributes_list();
            if (array_key_exists($name, $list) and
                array_key_exists('additional_type', $list[$name]) and ($list[$name]['additional_type'] != null)
            ) {
                return strtolower($list[$name]['additional_type']);
            }

            return null;
        }

        /**
         * Название поля
         *
         * @param string $name
         *
         * @return string|null
         */
        static function get_attribute_name($name) {
            $list = static::get_attributes_list();
            if (isset($list[$name], $list[$name]['name'])) {
                return $list[$name]['name'];
            }

            return self::get_default_name($name);
        }

        /**
         * @param string $name
         *
         * @return string
         */
        static function get_default_name($name) {
            $raw_list = explode("_", $name);
            foreach ($raw_list as &$item) {
                $item = mb_strtoupper(mb_substr($item, 0, 1)).mb_strtolower(mb_substr($item, 1));
            }

            return implode(' ', $raw_list);
        }

        /**
         * Настройки поля
         *
         * @param string $name
         *
         * @return array|null
         */
        static function get_attribute_setting($name) {
            $list = static::get_attributes_list();

            return array_key_exists($name, $list) ? $list[$name] : null;
        }

        /**
         * Санируем значение через описания поля. Для записи в поля или при чтении из полей
         *
         * @param string $name
         * @param mixed  $value
         *
         * @return mixed
         */
        static function sanation_value_type($name, $value) {
            /**
             * Санация значений
             */
            $need_type = static::get_attribute_type($name);
            $need_type_additional = static::get_attribute_additional_type($name);
            $value = self::convert_value_to_type($value, $need_type, $need_type_additional);

            return $value;
        }

        /**
         * @param mixed       $value
         * @param string      $need_type
         * @param string|null $additional_type
         *
         * @return mixed
         */
        static function convert_value_to_type($value, $need_type, $additional_type = null) {
            if ($need_type == 'mixed') {
                // Ничего не надо
            } elseif (preg_match('_^(.+?)(\\|null)_', $need_type, $a)) {
                $need_type = $a[1];
                if ($need_type == 'integer') {
                    $value = ($value !== null) ? (is_object($value) ? 0 : (int) $value) : null;
                } elseif ($need_type == 'string') {
                    $value = ($value !== null) ? self::valueToString($value) : null;
                    if ($additional_type != null) {
                        $value = self::stringToAdditionalType($value, $additional_type, true);
                    }
                } elseif ($need_type == 'double') {
                    $value = ($value !== null) ? (is_object($value) ? 0 : (double) $value) : null;
                } elseif ($need_type == 'boolean') {
                    $value = ($value !== null) ? ($value ? true : false) : null;
                } elseif ($need_type == 'array') {
                    if (is_object($value)) {
                        $value = (array) $value;
                    }
                } elseif ($need_type == 'object') {
                    if (is_array($value)) {
                        $value = (object) $value;
                    }
                }
            } elseif ($need_type == 'integer') {
                $value = is_object($value) ? 0 : (int) $value;
            } elseif ($need_type == 'string') {
                $value = self::valueToString($value);
                if ($additional_type != null) {
                    $value = self::stringToAdditionalType($value, $additional_type, false);
                }
            } elseif ($need_type == 'double') {
                $value = is_object($value) ? 0 : (double) $value;
            } elseif ($need_type == 'boolean') {
                $value = ($value ? true : false);
            } elseif ($need_type == 'array') {
                if (is_object($value)) {
                    $value = (array) $value;
                }
            } elseif ($need_type == 'object') {
                if (is_array($value)) {
                    $value = (object) $value;
                }
            }

            return $value;
        }

        /**
         * Берём значение функции или атрибута, санируя его
         *
         * @param string $name
         *
         * @return mixed
         * @throws SModelException
         */
        function __get($name) {
            $getter_function = 'get'.$name;
            $value = null;
            if (method_exists($this, $getter_function)) {
                $value = $this->$getter_function();
            } elseif (array_key_exists($name, $this->get_attributes_list())) {
                return $this->get_attribute($name);
            } elseif (preg_match('|^_relation_([0-9a-z_]+)$|i', $name, $a) and isset(static::relations()[$a[1]])) {
                return static::get_relation($a[1]);
            } elseif (method_exists($this, 'set'.$name)) {
                throw new SModelException('Cannot read write-only property '.get_class($this).'::'.$name, E_ERROR);
            }

            return self::sanation_value_type($name, $value);
        }

        /**
         * Выставляем значение функции, без санации данных
         *
         * @param string $name
         * @param mixed  $value
         *
         * @throws SModelException
         */
        function __set($name, $value) {
            if (!$this->is_attribute_writable($name)) {
                return;
            }

            $setter_function = 'set'.$name;
            if (method_exists($this, $setter_function)) {
                $this->$setter_function($value);
            } elseif (array_key_exists($name, $this->get_attributes_list())) {
                $this->set_attribute($name, $value);
            } elseif (method_exists($this, 'get'.$name)) {
                throw new SModelException('Cannot write read-only property '.get_class($this).'::'.$name, E_ERROR);
            }
        }

        /**
         * Isset для полей
         *
         * @param string $name
         *
         * @return boolean
         */
        function __isset($name) {
            if (method_exists($this, 'get'.$name)) {
                return true;
            } elseif (array_key_exists($name, $this->get_attributes_list())) {
                return true;
            } elseif (preg_match('|^_relation_([0-9a-z_]+)$|i', $name, $a) and isset(static::relations()[$a[1]])) {
                return true;
            }

            return false;
        }

        /**
         * Получаем значение атрибута
         *
         * @param string  $name
         * @param boolean $need_sanation Нужно санировать значение
         *
         * @return mixed
         */
        function get_attribute($name, $need_sanation = true) {
            if (!array_key_exists($name, $this->_attributes)) {
                return null;
            }

            return $need_sanation ? static::sanation_value_type($name, $this->_attributes[$name]) : $this->_attributes[$name];
        }

        /**
         * @param string  $name
         * @param mixed   $value
         * @param boolean $force         Пишем, не взирая на запрет записи
         * @param boolean $need_sanation Нужно санировать значение
         */
        function set_attribute($name, $value, $force = false, $need_sanation = true) {
            if (!$force and !$this->is_attribute_writable($name)) {
                return;
            }
            $this->_attributes[$name] = $need_sanation ? static::sanation_value_type($name, $value) : $value;
        }

        /**
         * @param string $name
         *
         * @return boolean
         */
        function has_attribute($name) {
            return array_key_exists($name, static::get_attributes_list());
        }

        /**
         * @param string $name
         *
         * @return boolean
         */
        function attribute_has_been_set($name) {
            return array_key_exists($name, $this->_attributes);
        }

        /**
         * Удаляем атрибут
         *
         * @param string $name
         */
        function unset_attribute($name) {
            if (array_key_exists($name, $this->_attributes)) {
                unset($this->_attributes[$name]);
            }
        }

        /**
         * @brief Дефолтное приведение к строке
         *
         * @return string|null
         */
        function getDefault_name() {
            return null;
        }

        /**
         * @brief Дефолтный URL для этого объекта
         *
         * @return string|null
         */
        function getDefault_url() {
            return null;
        }

        /**
         * @return string
         */
        function __toString() {
            return ''.$this->default_name;
        }

        /**
         * Получаем значения всех атрибутов
         *
         * @param string|null $origin
         *
         * @return mixed[]
         */
        function getAttributes($origin = null) {
            $list = static::get_attributes_list();
            $data = [];
            foreach ($list as $key => $value) {
                if (($origin !== null) and (!isset($value['origin']) or ($value['origin'] != $origin))) {
                    continue;
                }
                $data[$key] = $this->get_attribute($key);
            }

            return $data;
        }

        /**
         * Получаем названия всех атрибутов
         *
         * @param string|null $origin
         *
         * @return string[]
         */
        static function getAttributesKeys($origin = null) {
            $list = static::get_attributes_list();
            $keys = [];
            foreach ($list as $key => $value) {
                if (($origin !== null) and (!isset($value['origin']) or ($value['origin'] != $origin))) {
                    continue;
                }
                $keys[] = $key;
            }

            return $keys;
        }

        /**
         * Задаём значения всех атрибутов, которые были переданы
         *
         * @param mixed[] $values
         */
        function setAttributes($values) {
            $list = $this->getWritable_attributes();
            foreach ($values as $key => $value) {
                if (strtolower($key) === 'attributes') {
                    continue;
                }
                if (($list !== null) and !in_array($key, $list)) {
                    continue;
                }
                $this->__set($key, $value);
            }
        }

        /**
         * @return string[][]
         */
        function getErrors() {
            return $this->_errors;
        }

        /**
         * @return string
         */
        function getErrors_as_string() {
            $b = [];
            foreach ($this->_errors as $a) {
                $b[] = $a[1];
            }

            return implode('; ', $b);
        }

        /**
         * @param string $property
         * @param string $string
         */
        function addError($property, $string) {
            $this->_errors[] = [$property, $string];
        }

        /**
         * @brief Есть ли ошибки
         *
         * @return boolean
         */
        function hasErrors() {
            return count($this->_errors) > 0;
        }

        /**
         * Правила санации параметров при их SET
         *
         * @return array[]
         */
        static function sanation_rules() {
            return [];
        }

        /**
         * Список атрибутов, доступных для записи
         *
         * @return string[]|null
         */
        function getWritable_attributes() {
            if ($this->_scenario == 'default') {
                return null;
            }

            $raw_writable = [];
            $raw_non_writable = [];
            foreach (static::sanation_rules() as $rule) {
                $attributes = self::string_to_array($rule[0]);
                $scenarios = isset($rule['on']) ? self::string_to_array($rule['on']) : null;
                if (($scenarios !== null) and !in_array($this->_scenario, $scenarios)) {
                    continue;
                }
                $rule[1] = strtolower($rule[1]);

                if ($rule[1] == 'unsafe') {
                    foreach ($attributes as &$attribute_name) {
                        if (!in_array($attribute_name, $raw_writable) and !in_array($attribute_name, $raw_non_writable)) {
                            $raw_non_writable[] = $attribute_name;
                        }
                    }
                } else {
                    foreach ($attributes as &$attribute_name) {
                        if (!in_array($attribute_name, $raw_writable) and !in_array($attribute_name, $raw_non_writable)) {
                            $raw_writable[] = $attribute_name;
                        }
                    }
                }
            }
            $real_raw_writable = $raw_writable;
            foreach (array_keys(static::get_attributes_list()) as $key) {
                if (!in_array($key, $raw_non_writable)) {
                    $real_raw_writable[] = $key;
                }
            }

            return $real_raw_writable;
        }

        /**
         * @param string $name
         *
         * @return boolean
         */
        function is_attribute_writable($name) {
            if (in_array($name, ['attributes'])) {
                return true;
            }

            $writable_attribute_list = $this->getWritable_attributes();
            if (is_null($writable_attribute_list)){
                return true;
            }
            foreach ($writable_attribute_list as &$key) {
                $key = strtolower($key);
            }

            return in_array(strtolower($name), $writable_attribute_list);
        }

        /**
         * @param string[]|string $value
         *
         * @return string[]
         */
        static function string_to_array($value) {
            if (is_array($value)) {
                return $value;
            } elseif (is_string($value)) {
                return preg_split('|[,\s]+|', trim($value));
            } else {
                return [];
            }
        }

        /**
         * Валидируем через правила санации
         *
         * @param string[]|null $need_test_attribute_list
         *
         * @return boolean
         */
        function validate($need_test_attribute_list = null) {
            $can_be_null_list = [];
            foreach (static::sanation_rules() as $rule) {
                if (isset($rule['on']) and !in_array($this->_scenario, self::string_to_array($rule['on']))) {
                    continue;
                }
                $rule[1] = strtolower($rule[1]);
                if (in_array($rule[1], ['can be null', 'can_be_null'])) {
                    $attributes = self::string_to_array($rule[0]);
                    foreach ($attributes as $attribute_name) {
                        $can_be_null_list[] = $attribute_name;
                    }
                }
            }
            $can_be_null_list = array_unique($can_be_null_list);
            $self_reflection = new ReflectionClass(static::class);

            foreach (static::sanation_rules() as $rule) {
                $attributes = self::string_to_array($rule[0]);
                if (isset($rule['on']) and !in_array($this->_scenario, self::string_to_array($rule['on']))) {
                    continue;
                }
                $rule[1] = strtolower($rule[1]);

                if ($rule[1] == 'required') {
                    foreach ($attributes as $attribute_name) {
                        if (!property_exists($this, $attribute_name) and !array_key_exists($attribute_name, $this->_attributes)) {
                            $this->addError($attribute_name,
                                'Property "'.self::get_attribute_name($attribute_name).'" has not been set');
                        }
                    }
                    continue;
                } elseif (in_array($rule[1], ['safe', 'unsafe'])) {
                    continue;
                }

                foreach ($attributes as $attribute_name) {
                    if (($need_test_attribute_list !== null) and !in_array($attribute_name, $need_test_attribute_list)) {
                        continue;
                    }
                    if ($this->validate_attribute_can_be_skip($attribute_name, $can_be_null_list)) {
                        continue;
                    }
                    $value = $this->{$attribute_name};
                    $errors = self::test_attribute_with_rule($rule, $attribute_name, $value, $self_reflection);
                    foreach ($errors as &$error) {
                        $this->addError($attribute_name, $error[1]);
                    }
                }
            }

            return !$this->hasErrors();
        }

        /**
         * @param array           $rule
         * @param string          $attribute_name
         * @param mixed           $value
         * @param ReflectionClass $reflection
         *
         * @return string[][]
         */
        private static function test_attribute_with_rule($rule, $attribute_name, $value, $reflection) {
            $resolved_attribute_name = $reflection->getMethod('get_attribute_name')->invoke(null, $attribute_name);

            $return = [];
            if ($rule[1] == 'min') {
                if ($value < $rule['value']) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is less than the minimum'];
                }
            } elseif ($rule[1] == 'max') {
                if ($value > $rule['value']) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is more than the maximum'];
                }
            } elseif ($rule[1] == 'min_length') {
                if (mb_strlen($value) < $rule['value']) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is shorter than the minimum length'];
                }
            } elseif ($rule[1] == 'max_length') {
                if (mb_strlen($value) > $rule['value']) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is longer than the maximum length'];
                }
            } elseif (preg_match('_^\\s*(>|>=|=>|<|<=|=<|<>|!=|=|==)\\s*([0-9]+)\\s*$_', $rule[1], $a)) {
                switch ($a[1]) {
                    case '>=':
                    case '=>':
                        if ($value < $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is less than the minimum'];
                        }
                        break;
                    case '<=':
                    case '=<':
                        if ($value > $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is more than the maximum'];
                        }
                        break;
                    case '==':
                    case '=':
                        if ($value != $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is not equal to constant'];
                        }
                        break;
                    case '<>':
                    case '!=':
                        if ($value == $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is equal to illegal constant'];
                        }
                        break;
                    case '>':
                        if ($value <= $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is less than the minimum'];
                        }
                        break;
                    case '<':
                        if ($value >= $a[2]) {
                            $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" is more than the maximum'];
                        }
                        break;
                }
            } elseif (in_array($rule[1], ['in dictionary', 'in_dictionary'])) {
                $values = array_key_exists('values', $rule)
                    ? $rule['values']
                    : array_keys($reflection->getStaticPropertyValue($attribute_name.'_dictionary'));

                if (!in_array($value, $values)) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" does not contain legal value'];
                }
            } elseif ($rule[1] == 'in') {
                if (!in_array($value, $rule['value'])) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" does not contain legal value'];
                }
            } elseif (in_array($rule[1], ['not in', 'not_in'])) {
                if (in_array($value, $rule['value'])) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" contains illegal value'];
                }
            } elseif ($rule[1] == 'regexp') {
                if (!preg_match($rule['value'], $value)) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" does not match with the mask'];
                }
            } elseif (in_array($rule[1], ['not regexp', 'not_regexp'])) {
                if (preg_match($rule['value'], $value)) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" matches with illegal mask'];
                }
            } elseif ($rule[1] == 'between') {
                if (($value < $rule['value'][0]) or ($value > $rule['value'][1])) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" out of range'];
                }
            } elseif (in_array($rule[1], ['not between', 'not_between'])) {
                if (($value >= $rule['value'][0]) and ($value <= $rule['value'][1])) {
                    $return[] = [$rule[1], 'Property "'.$resolved_attribute_name.'" out of range'];
                }
            }

            // @todo Дописать. email, url

            return $return;
        }

        /**
         * Валидируем через правила санации
         *
         * @param string[]|null $need_test_attribute_list
         *
         * @return boolean
         */
        function full_validate($need_test_attribute_list = null) {
            if (!$this->before_validate()) {
                return false;
            } elseif (!$this->validate($need_test_attribute_list)) {
                return false;
            } elseif (!$this->after_validate()) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * @param string   $attribute_name
         * @param string[] $can_be_null_list
         *
         * @return boolean
         */
        protected function validate_attribute_can_be_skip($attribute_name, $can_be_null_list) {
            $value = $this->{$attribute_name};
            if (($value === null) and in_array($attribute_name, $can_be_null_list)) {
                return true;
            }

            return false;
        }

        /**
         * Выставляем все property и атрибуты
         */
        function sanation_set_default_and_empty() {
            foreach (static::sanation_rules() as $rule) {
                $attributes = self::string_to_array($rule[0]);
                if (isset($rule['on']) and !in_array($this->_scenario, self::string_to_array($rule['on']))) {
                    continue;
                }
                $rule[1] = strtolower($rule[1]);

                if ($rule[1] == 'default') {
                    foreach ($attributes as $attribute_name) {
                        $value = $this->{$attribute_name};
                        if (is_object($value) and (get_class($value) == 'SDBExpression')) {
                            continue;
                        }
                        // @hint Здесь специально не is_empty
                        if (isset($rule['setOnEmpty']) and (($value === null) or ($value === ''))) {
                            $this->{$attribute_name} = $rule['value'];
                        } elseif (isset($rule['setOnZero']) and ($value === 0)) {
                            $this->{$attribute_name} = $rule['value'];
                        }
                    }
                    continue;
                }
            }
        }

        /**
         * Правильное название типа
         *
         * @param string $type
         *
         * @return string
         */
        static function get_real_type_name($type) {
            switch (strtolower($type)) {
                case 'int':
                case 'integer':
                case 'number':
                case 'numeric':
                case 'numerical':
                    return 'integer';

                case 'float':
                case 'double':
                    return 'double';

                case 'string':
                case 'text':
                    return 'string';

                default:
                    return strtolower($type);
            }
        }

        /**
         * Функция-заглушка, вызывающаяся перед validate
         *
         * @return boolean
         */
        function before_validate() {
            $this->sanation_set_default_and_empty();

            return true;
        }

        /**
         * Функция-заглушка, вызывающаяся после validate
         *
         * @return boolean
         */
        function after_validate() {
            return true;
        }

        /**
         * Значение любого типа превращаем в строку
         *
         * @param mixed $instance
         *
         * @return string|null
         */
        static function valueToString($instance) {
            if (is_object($instance)) {
                return method_exists($instance, '__toString') ? $instance->__toString() : get_class($instance);
            } elseif (is_bool($instance)) {
                return $instance ? 'true' : 'false';
            } elseif (is_array($instance)) {
                return 'array['.count($instance).']';
            } elseif ($instance === null) {
                return '';
            } else {
                return strval($instance);
            }
        }

        /**
         * Дефолтные значения для разных типов
         *
         * @param string $additional_type
         *
         * @return string
         */
        static function get_default_value_for_additional_type($additional_type) {
            switch ($additional_type) {
                case 'date':
                    return '0000-00-00';
                case 'datetime':
                    return '0000-00-00 00:00:00';
                default:
                    return '';
            }
        }

        /**
         * @param string  $string
         * @param string  $additional_type
         * @param boolean $can_be_null
         *
         * @return string
         */
        static function stringToAdditionalType($string, $additional_type, $can_be_null = true) {
            switch ($additional_type) {
                case 'date':
                    if ($string == '') {
                        return $can_be_null ? null : self::get_default_value_for_additional_type('date');
                    } elseif ($string == '0000-00-00') {
                        return '0000-00-00';
                    }
                    try {
                        $date = new DateTime($string);
                    } catch (Exception $e) {
                        return $can_be_null ? null : self::get_default_value_for_additional_type('date');
                    }

                    return $date->format('Y-m-d');
                case 'datetime':
                    if ($string == '') {
                        return $can_be_null ? null : self::get_default_value_for_additional_type('date');
                    } elseif ($string == '0000-00-00 00:00:00') {
                        return '0000-00-00 00:00:00';
                    }
                    try {
                        $date = new DateTime($string);
                    } catch (Exception $e) {
                        return $can_be_null ? null : self::get_default_value_for_additional_type('datetime');
                    }

                    return $date->format('Y-m-d H:i:s');
            }

            return $string;
        }

        /**
         * @param mixed[]     $fields
         * @param string      $key_prefix
         * @param string|null $scenario
         * @param boolean     $need_trim
         */
        function set_values_from_raw_fields($fields, $key_prefix = '', $scenario = null, $need_trim = false) {
            if ($scenario !== null) {
                $old_scenario = $this->getScenario();
                $this->setScenario($scenario);
            }
            $prefix_length = mb_strlen($key_prefix);
            foreach ($fields as $key => $value) {
                if (mb_substr($key, 0, $prefix_length) == $key_prefix) {
                    $real_key = mb_substr($key, $prefix_length);
                    if (!$this->is_attribute_writable($real_key)) {
                        continue;
                    }
                    $this->set_attribute($real_key, $need_trim ? trim($value) : $value);
                }
            }

            if ($scenario !== null) {
                /** @noinspection PhpUndefinedVariableInspection */
                $this->setScenario($old_scenario);
            }
        }

        protected $_date_input_list = [];
        /**
         * @var integer
         */
        protected $_search_offset = null;
        /**
         * @var integer|null $_search_limit
         */
        protected $_search_limit = null;
        /**
         * @var string|null $_search_order
         */
        protected $_search_order = null;

        /**
         * Установка полей для поиска
         *
         * @param mixed[]           $fields
         * @param string            $key_prefix
         * @param DateTimeZone|null $timezone
         */
        function set_search($fields, $key_prefix, $timezone = null) {
            $old_scenario = $this->getScenario();
            $this->setScenario('search');
            if ($timezone === null) {
                $timezone = new DateTimeZone(date_default_timezone_get());
            }

            $prefix_length = mb_strlen($key_prefix);
            foreach ($fields as $key => $value) {
                if (mb_substr($key, 0, $prefix_length) != $key_prefix) {
                    continue;
                }
                if (mb_substr($key, $prefix_length, 6) == 'value_') {
                    $value_type = 1;
                    $real_key = mb_substr($key, $prefix_length + 6);
                } elseif (mb_substr($key, $prefix_length, 4) == 'min_') {
                    $value_type = 2;
                    $real_key = mb_substr($key, $prefix_length + 4);
                } elseif (mb_substr($key, $prefix_length, 4) == 'max_') {
                    $value_type = 3;
                    $real_key = mb_substr($key, $prefix_length + 4);
                } elseif (mb_substr($key, $prefix_length, 5) == 'limit') {
                    $this->set_search_limit($value);
                    continue;
                } elseif (mb_substr($key, $prefix_length, 6) == 'offset') {
                    $this->set_search_offset($value);
                    continue;
                } elseif (mb_substr($key, $prefix_length, 5) == 'order') {
                    $this->set_search_order($value);
                    continue;
                } else {
                    continue;
                }

                if (!$this->is_attribute_writable($real_key) or ($value === '')) {
                    continue;
                }
                // @todo Проверяем, что это виртуальное поле, сделанное для поиска
                switch ($value_type) {
                    case 1:
                        $this->set_attribute($real_key, $value, false, false);
                        break;
                    case 2:
                    case 3:
                        $attribute_setting = static::get_attributes_list()[$real_key];
                        if (!in_array($attribute_setting['additional_type'], ['date', 'datetime', 'timestamp'])
                            and ($attribute_setting['type'] != 'integer')
                        ) {
                            // Так быть не должно
                            continue;
                        }

                        if (!array_key_exists($real_key, $this->_date_input_list)) {
                            $this->_date_input_list[$real_key] = (object) array(
                                'min' => null,
                                'max' => null,
                            );
                        }
                        if (($value === '') || ($value === null)) {
                            $this->_date_input_list[$real_key]->{($value_type == 2) ? 'min' : 'max'} = null;
                        } else {
                            $this->_date_input_list[$real_key]->{($value_type == 2) ? 'min' : 'max'} =
                                new DateTime($value, $timezone);
                        }
                        break;
                }
            }

            $this->setScenario($old_scenario);
        }

        /**
         * @param string|null $value
         */
        function set_search_offset($value) {
            $this->_search_offset = ($value == '') ? null : (int) $value;
        }

        /**
         * @return integer|null
         */
        function get_search_offset() {
            return $this->_search_offset;
        }

        /**
         * @param string|null $value
         */
        function set_search_limit($value) {
            $this->_search_limit = ($value == '') ? null : (int) $value;
        }

        /**
         * @return integer|null
         */
        function get_search_limit() {
            return $this->_search_limit;
        }

        /**
         * @param string|null $value
         */
        function set_search_order($value) {
            $this->_search_order = ($value == '') ? null : $value;
        }

        /**
         * @return string|null
         */
        function get_search_order() {
            return $this->_search_order;
        }

        /**
         * @return array
         */
        function get_date_input_list() {
            return $this->_date_input_list;
        }

        /**
         * @param array $value
         */
        function set_date_input_list($value) {
            $this->_date_input_list = $value;
        }

        /**
         * Low level метод для санации всех существующих атрибутов в переданных объекта
         *
         * Эта функция используется, например, в find_all
         *
         * @param SModel[] $objects
         * @param boolean  $force_all_attributes Форсированно санировать все атрибуты, даже те, которых нет
         */
        protected static function sanify_many_objects($objects, $force_all_attributes = false) {
            $chunks = [];
            foreach ($objects as &$object) {
                if (isset($chunks[get_class($object)])) {
                    $chunks[get_class($object)][] = $object;
                } else {
                    $chunks[get_class($object)] = [$object];
                }
            }
            unset($objects, $object);

            foreach ($chunks as $class_name => &$objects) {
                self::sanify_many_objects_one_class($class_name, $objects, $force_all_attributes);
            }
        }

        /**
         * @param string   $class_name
         * @param SModel[] $objects
         * @param boolean  $force_all_attributes Форсированно санировать все атрибуты, даже те, которых нет
         */
        protected static function sanify_many_objects_one_class($class_name, $objects, $force_all_attributes = false) {
            /**
             * @var SModel $class_name
             */
            $attributes = array_keys($class_name::get_attributes_list());
            foreach ($attributes as &$name) {
                $type = $class_name::get_attribute_type($name);
                $additional_type = $class_name::get_attribute_additional_type($name);
                foreach ($objects as &$object) {
                    if (!array_key_exists($name, $object->_attributes) and !$force_all_attributes) {
                        continue;
                    } elseif ($type == 'integer') {
                        $object->_attributes[$name] = (integer) $object->_attributes[$name];
                    } elseif ($type == 'double') {
                        $object->_attributes[$name] = (double) $object->_attributes[$name];
                    } else {
                        $object->_attributes[$name] =
                            self::convert_value_to_type($object->_attributes[$name], $type, $additional_type);
                    }
                }
            }
        }

        /**
         * Описание реляций
         *
         * Реляции должны быть в виде [RELATION_TYPE, CLASS_NAME]
         *
         * @return array[]
         */
        static function relations() {
            return [];
        }

        /**
         * @param string $relation_name
         *
         * @return boolean
         */
        function is_relations_static($relation_name) {
            return isset($this->_is_relations_static[$relation_name]);
        }

        /**
         * @param string  $relation_name
         * @param boolean $is_static
         *
         * @throws SModelException
         */
        function set_relations_static($relation_name, $is_static) {
            if (!isset(static::relations()[$relation_name])) {
                throw new SModelException(sprintf('Relation "%s" does not exist in class "%s"', $relation_name, static::class));
            } elseif ($is_static) {
                $this->_is_relations_static[$relation_name] = [];
            } else {
                unset($this->_is_relations_static[$relation_name]);
            }
        }

        /**
         * @param string $relation_name
         */
        protected function lazy_relation_load($relation_name) { }

        /**
         * Получаем конкретную реляцию
         *
         * @param string $relation_name
         *
         * @return null|SModel|SModel[]
         * @throws SModelException
         */
        function get_relation($relation_name) {
            if (!isset(static::relations()[$relation_name])) {
                return null;
            }
            $this->lazy_relation_load($relation_name);
            if ($this->is_relations_static($relation_name)) {
                $relation = static::relations()[$relation_name];
                if (in_array($relation[0], [self::HAS_MANY, self::MANY_TO_MANY])) {
                    if (!isset(self::$_relations_static_collection[$relation[1]])) {
                        return [];
                    } else {
                        $ret = [];
                        foreach ($this->_is_relations_static[$relation_name] as $primary_key) {
                            $ret[] = self::$_relations_static_collection[$relation[1]][$primary_key];
                        }

                        return $ret;
                    }
                } elseif ($relation[0] == self::BELONGS_TO) {
                    $keys = $this->_is_relations_static[$relation_name];
                    if (count($keys) == 0) {
                        return null;
                    }
                    $primary_key = $keys[0];

                    return self::$_relations_static_collection[$relation[1]][$primary_key];
                } else {
                    throw new SModelException(sprintf('Relation #%d isn\'t implemented', $relation[0]));
                }
            } elseif (isset($this->_relations[$relation_name])) {
                return $this->_relations[$relation_name];
            } else {
                return null;
            }
        }

        /**
         * Устанавливаем конкретную реляцию
         *
         * @param string          $relation_name
         * @param SModel[]|SModel $value
         *
         * @throws SModelException
         */
        function set_relation($relation_name, $value) {
            if (!isset(static::relations()[$relation_name])) {
                throw new SModelException('Relation does not exist');
            }
            $relation = static::relations()[$relation_name];
            if (($relation[0] == self::HAS_MANY) and !is_array($value)) {
                throw new SModelException('Has-many relation must be array');
            } elseif (($relation[0] == self::MANY_TO_MANY) and !is_array($value)) {
                throw new SModelException('Many-to-many relation must be array');
            } elseif (($relation[0] == self::BELONGS_TO) and is_array($value) and (count($value) == 1)) {
                $value = $value[0];
            } elseif (($relation[0] == self::BELONGS_TO) and is_array($value) and (count($value) == 0)) {
                $value = null;
            } elseif (($relation[0] == self::BELONGS_TO) and !is_object($value) and !is_null($value)) {
                throw new SModelException('Belong-to relation must be object or null');
            }
            if ($this->is_relations_static($relation_name)) {
                /**
                 * @var string|string[] $primary_key_name
                 */
                $primary_key_name = $relation[1]::getPrimary_key();
                if (is_array($primary_key_name)) {
                    throw new SModelException('Composite primary key isn\'t implemented');
                }
                if (is_array($value)) {
                    foreach ($value as &$item) {
                        $this->add_object_to_static_relation_collection($relation[1],
                            $item->get_attribute($primary_key_name), $item);
                        if (!in_array($item->get_attribute($primary_key_name), $this->_is_relations_static[$relation_name])) {
                            $this->_is_relations_static[$relation_name][] = $item->get_attribute($primary_key_name);
                        }
                    }
                } elseif (is_null($value)) {
                    $this->_is_relations_static[$relation_name] = [];
                } else {
                    $this->add_object_to_static_relation_collection($relation[1],
                        $value->get_attribute($primary_key_name), $value);
                    $this->_is_relations_static[$relation_name] = [$value->get_attribute($primary_key_name)];
                }
            } else {
                $this->_relations[$relation_name] = $value;
            }
        }

        /**
         * @param string         $class_name
         * @param string|integer $primary_key
         * @param SModel         $model
         */
        static function add_object_to_static_relation_collection($class_name, $primary_key, $model) {
            if (!isset(self::$_relations_static_collection[$class_name])) {
                self::$_relations_static_collection[$class_name] = [$primary_key => $model];
            } elseif (!isset(self::$_relations_static_collection[$class_name][$primary_key])) {
                self::$_relations_static_collection[$class_name][$primary_key] = $model;
            }
        }

        /**
         * @return SModel[][]
         */
        static function get_relations_static_collections() {
            return self::$_relations_static_collection;
        }

        /**
         * @return string[][]|integer[][]
         */
        function get_relations_static_primaries() {
            return $this->_is_relations_static;
        }

        /**
         * @param DateTime|integer|string|null $value
         *
         * @return DateTime|null
         */
        static function get_datetime_from_mixed($value) {
            if (is_null($value)) {
                return null;
            } elseif (is_object($value) and (get_class($value) == 'DateTime')) {
                return new $value;
            } elseif (is_integer($value)) {
                $d = new DateTime();
                $d->setTimestamp($value);

                return $d;
            } elseif (is_string($value)) {
                return new DateTime($value);
            } else {
                return null;
            }
        }

        /**
         * Берём время из атрибута
         *
         * @param string  $attribute_name
         * @param integer $type (0-DateTime,1-string full,2-string date,3-integer)
         *
         * @return DateTime|integer|string|null
         * @throws SModelException
         */
        final function get_attribute_time($attribute_name, $type = 0) {
            $d = self::get_datetime_from_mixed($this->{$attribute_name});
            if (is_null($d)) {
                return null;
            }

            switch ($type) {
                case 0:
                    return $d;
                case 1:
                    return $d->format('Y-m-d H:i:s');
                case 2:
                    return $d->format('Y-m-d');
                case 3:
                    return $d->getTimestamp();
                default:
                    throw new SModelException('Code flow exception');
            }
        }

        /**
         * @param string                  $attribute_name
         * @param DateTime|string|integer $value
         *
         * @throws SModelException
         */
        final function set_attribute_time($attribute_name, $value) {
            if (is_null($value)) {
                $this->{$attribute_name} = null;

                return;
            } elseif (is_object($value) and (get_class($value) == 'DateTime')) {
                $d = new $value;
            } elseif (is_integer($value)) {
                $d = new DateTime();
                $d->setTimestamp($value);
            } elseif (is_string($value)) {
                $d = new DateTime($value);
            } else {
                $this->{$attribute_name} = null;

                return;
            }

            $type = self::get_attribute_type($attribute_name);

            if (!preg_match('_^(integer|string)(\\|null)_', $type, $a)) {
                $this->{$attribute_name} = null;
            }
            if ($a[1] == 'integer') {
                $this->{$attribute_name} = $d->getTimestamp();
            } elseif ($a[1] == 'string') {
                $additional_type = self::get_attribute_additional_type($attribute_name);
                if ($additional_type == 'date') {
                    $this->{$attribute_name} = $d->format('Y-m-d');
                } elseif ($additional_type == 'datetime') {
                    $this->{$attribute_name} = $d->format('Y-m-d H:i:s');
                } else {
                    $this->{$attribute_name} = null;
                }
            } else {
                $this->{$attribute_name} = null;

                return;
            }
        }
    }

    class SModelException extends Exception {
    }

?>