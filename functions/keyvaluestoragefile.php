<?php

    /**
     * Class KeyValueStorageFile Key-value хранилище, использующее обыкновенные файлы
     */
    class KeyValueStorageFile extends iAscetKeyValueStorage {
        var $folder;
        protected $_multi_folder = false;
        const ERROR_CODE = 100;

        /**
         * @param object $settings
         *
         * @throws KeyValueException
         */
        function __construct($settings) {
            if (!isset($settings->storage_type)) {
                $settings->storage_type = self::StorageTemporary;
            }
            switch ($settings->storage_type) {
                case self::StorageTemporary:
                    $this->folder = sys_get_temp_dir();
                    break;
                case self::StoragePersistent:
                    $this->folder = SmartMutex::getDirectoryString();
                    break;
                default:
                    throw new KeyValueException('Constructor settings is malformed. Storage type can not be equal '.
                                                $settings->storage_type, self::ERROR_CODE + 1);
            }
            if (isset($settings->folder)) {
                $this->folder = $settings->folder;
            }
            if (isset($settings->multi_folder)) {
                $this->_multi_folder = (boolean) $settings->multi_folder;
            }

            $this->settings = $settings;
            $this->standard_prefix_strategy();
        }

        /**
         * @param string         $key   Название ключа
         * @param mixed          $value Новое значение
         * @param double|integer $ttl   Кол-во секунд, после которых значение будет считаться просроченным
         *
         * @throws KeyValueException
         */
        function set_value($key, $value, $ttl = 86400) {
            $ts1 = microtime(true);
            $data = $this->form_datum_value($value, $ttl);
            self::create_path($this->folder, $this->_multi_folder, $key);
            $this->get_lock($key);
            $temporary_filename = $this->get_filename($key).'.tmp';
            $result = @file_put_contents($temporary_filename, serialize($data), LOCK_EX);
            if ($result === false) {
                $this->release_lock();
                throw new KeyValueException('Can not save value', self::ERROR_CODE + 3);
            }
            chmod($temporary_filename, 6 << 6);
            if (!rename($temporary_filename, $this->get_filename($key))) {
                // @codeCoverageIgnoreStart
                @unlink($temporary_filename);
                $this->release_lock();
                throw new KeyValueException('Can not rename db file', self::ERROR_CODE + 4);
                // @codeCoverageIgnoreEnd
            }
            $this->release_lock();
            self::add_profiling(microtime(true) - $ts1, static::class, 'set_value');
        }

        /**
         * Создаём папку
         *
         * @param string  $folder
         * @param boolean $multi_folder
         * @param string  $key
         *
         * @throws KeyValueException
         */
        protected static function create_path($folder, $multi_folder, $key) {
            if (!file_exists($folder)) {
                if (!file_exists(dirname($folder))) {
                    throw new KeyValueException(sprintf('Folder %s does not exist', dirname($folder)), self::ERROR_CODE + 5);
                } else {
                    mkdir($folder);
                    chmod($folder, (7 << 6) | (7 << 3));
                }
            }

            if (!$multi_folder) {
                return;
            }
            $hash = hash('sha512', $key);
            $folder .= '/'.substr($hash, 0, 2);
            if (!file_exists($folder)) {
                mkdir($folder);
                chmod($folder, (7 << 6) | (7 << 3));
            }
            $folder .= '/'.substr($hash, 2, 2);
            if (!file_exists($folder)) {
                mkdir($folder);
                chmod($folder, (7 << 6) | (7 << 3));
            }
        }

        /**
         * @param string $key
         *
         * @return object|null
         */
        protected function get_value_full_clear($key) {
            if (!file_exists($this->get_filename($key))) {
                return null;
            }
            $this->get_lock($key);
            $buf = @file_get_contents($this->get_filename($key));
            $this->release_lock();
            if ($buf === false) {
                return null;
            }
            $data = @unserialize($buf);
            if (($data === false) and ($buf != serialize(false))) {
                return null;
            }

            return $data;
        }

        /**
         * @param string $key
         *
         * @return string
         */
        function get_filename($key) {
            return $this->get_folder($key).'/ascetkey_'.$this->prefix.hash('sha512', $key).'.dat';
        }

        function get_folder($key) {
            $full_folder = $this->folder;
            if ($this->_multi_folder) {
                $hash = hash('sha512', $key);
                $full_folder .= sprintf('/%s/%s', substr($hash, 0, 2), substr($hash, 2, 2));
            }

            return $full_folder;
        }

        /**
         * @param string $key
         *
         * @return string
         */
        function get_mutex_key_name($key) {
            return 'ascetkey_'.hash('sha512', $this->get_filename($key));
        }

        /**
         * Создаём мьютекс
         *
         * @param string $key
         */
        protected function create_lock($key) {
            $this->_locks[$key] = new SmartMutex($this->get_mutex_key_name($key), SmartMutex::SmartMutex_Server,
                $this->get_folder($key));
        }

        /**
         * @param string $key
         */
        function delete_value($key) {
            $ts1 = microtime(true);
            if (!file_exists($this->get_filename($key))) {
                self::add_profiling(microtime(true) - $ts1, static::class, 'delete_value');

                return;
            }
            $this->get_lock($key);
            @unlink($this->get_filename($key));
            $this->release_lock();
            self::add_profiling(microtime(true) - $ts1, static::class, 'delete_value');
        }
    }


?>