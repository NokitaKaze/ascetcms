<?php

    /** @noinspection PhpInconsistentReturnPointsInspection */
    class AscetCMSEngine {
        const version = '0.7.8c';
        const session_index_name = 'sadsessid';
        /**
         * @var object Точки профилирования
         */
        public static $profiling_stamps;
        /**
         * @var object Опции при загрузке класса
         */
        public static $options;
        /**
         * @var AscetPDO Database connection
         */
        public static $database_connection;
        /**
         * @var iAscetKeyValueStorage Default Key Value Storage
         */
        public static $storage;
        /**
         * @var string Файл с будущим подгруженным кодом, который надо запустить
         */
        public static $source_file;
        /**
         * @var integer HTTP-статус, возвращённый подгруженным кодом
         */
        public static $http_status;

        /**
         * @var string E-Tag Hash Контента
         */
        public static $etag_hash = null;
        /**
         * @var boolean Если выставлено в true, то над выводом на экран будут произведены функции замены
         */
        public static $need_prepare_release;
        /**
         * @var string Буффер, в котром хранится контент, который мы выведем на экран. Игнорируется, если
         *     is_need_show_template=true
         */
        public static $output_buffer;
        /**
         * @var string HTML текущего html-шаблона
         */
        public static $template_html;
        /**
         * @var boolean Нужно ли выводить текущий template и игнорировать output_buffer
         */
        public static $is_need_show_template = false;
        /**
         * @var boolean Мы под хип-хопом?
         */
        public static $is_hhvm = false;
        /**
         * @var boolean|null null - real run не запущен, false - real run не остановился, true - завершился
         */
        public static $real_run_ended;

        /**
         * @var array|null Список отловленных ошибок
         */
        public static $error_handled;
        /**
         * @var integer|null Текущий error_reporting внутри скрипта
         */
        public static $error_reporting_level;
        /**
         * @var boolean|null Показываем ли мы ошибки на экран
         */
        public static $error_display;
        /**
         * @var boolean|null Нужно ли коммиттить текущую транзакцию
         */
        public static $need_commit;
        /**
         * @var integer|null Время модификации контента
         */
        public static $header_time_last_modified = null;
        /**
         * @var integer|null Время просрочки контента
         */
        public static $header_time_expires = null;
        /**
         * @var string[] Cache-Control
         */
        public static $header_cache_control = [];

        /**
         * @var boolean Нужно ли выставлять по Cache-control по дефолту
         * @deprecated
         */
        public static $is_need_set_default_cache_control = true;

        /**
         * @var object Список событий, на которых висят обработчики событий
         * before_main_code    Перед самым исполнением основного кода
         * before_scope_exit   После исполнения основного кода, но до выхода из real_run
         * after_last_echo     После исполнения основного кода, после последнего echo
         * execution_error     В случае ошибки, в обработчике ошибок, если никто не перехватил
         * execution_sql_error В случае SQL ошибки
         * shutdown            В конце всего скрипта: shutdown_function
         * error_handler       Error handler (В set_error_handler)
         */
        public static $event_listeners;

        /**
         * @var integer|null
         */
        public static $write_out_content_size = null;

        /**
         * @var Exception|null Отловленная ошибка исполнения внутри этой функции
         */
        public static $execution_exception = null;

        /**
         * @var string document root of cms e.g. /var/www/site
         */
        public static $sad_root;

        /**
         * @var string URL root of cms e.g. /cms
         */
        public static $sad_baseurl;

        /**
         * @var string Clean url of request (REQUEST_URL)
         */
        public static $sad_url;

        /**
         * @var string Текущий Content Type документа
         */
        public static $content_type;

        /**
         * @var string Ключ сессии (аналог значения phpsessid)
         */
        public static $session_name = null;

        /**
         * @var object Значения сессии
         */
        public static $session_value = null;

        /**
         * @var Callable[][] Список замыканий для генерации значений внутри кнопок
         */
        protected static $button_parse_functions = [];

        // Блок констант
        const DATABASE_CONNECTION_STRATEGY_BEFORE = 0;// Инициализировать подключение к БД до запуска основного кода
        const DATABASE_CONNECTION_STRATEGY_LAZY = 1;// Инициализировать подключение к БД при первом обращении к БД
        const DATABASE_CONNECTION_STRATEGY_INCODE = 2;// Неинициализировать подключение к БД
        const DATABASE_CONNECTION_ERROR_STRATEGY_MESSAGE = 0;// При ошибке подключения к БД, выводить warning
        const DATABASE_CONNECTION_ERROR_STRATEGY_PAGE = 1;// @todo При ошибке подключения к БД, выводить особую страницу на экран
        const DATABASE_CONNECTION_ERROR_STRATEGY_NOTHING = 2;//При ошибке подключения к БД ничего не делать
        const DATABASE_ERROR_STRATEGY_EXIT = 0;// При ошибке SQL-запроса, вылетать
        const DATABASE_ERROR_STRATEGY_EXCEPTION = 1;// При ошибке SQL-запроса, рейзить ошибку
        const DATABASE_ERROR_STRATEGY_NOTHING = 2;// При ошибке SQL-запроса, ничего не делать
        const DATABASE_ERROR_STRATEGY_WARNING = 3;// При ошибке SQL-запроса, выводить warning
        const EXECUTION_ERROR_STRATEGY_SILENCE_EXIT = 0;// При ошибке исполнения кода, молча вылетать
        const EXECUTION_ERROR_STRATEGY_MESSAGE = 1;// При ошибке исполнения кода, выводить warning
        const EXECUTION_ERROR_STRATEGY_PAGE = 2;// @todo При ошибке исполнения кода, выводить особую страницу на экран

        /**
         * Инициализатор класса
         *
         * Определяет несколько базовых переменных, включая точки профилирования и дефолтные настройки опций
         * Подготавливает Engine к запуску. Сборка и прогрев ядерной печи. От функции run_init отличается тем, что не
         * проверяет входящие параметры типа header'ов и не влияет на поведение кроме необрабатываемого AliasMatch и не
         * вносит изменения в окружение, например, не подключается к БД
         *
         * @param stdClass $sad_options           Набор опций, которыми инициализируется класс.
         * @param integer  $first_profiling_stamp Первый удачно полученный timestamp при дёргании страницы
         */
        static function init(stdClass $sad_options, $first_profiling_stamp = null) {
            if ($first_profiling_stamp === null) {
                $first_profiling_stamp = microtime(true);
            }

            // Создаём пустые массивы с обработчиками событий
            self::$event_listeners = (object) array();

            self::$profiling_stamps = (object) array(
                'init' => $first_profiling_stamp,
                'constructed' => microtime(true)
            );

            self::$profiling_stamps->init_start = microtime(true);

            // Загружаем опции
            self::set_options_from_object($sad_options);

            // Проверяем под хипхопом ли мы
            if (isset($_ENV['HHVM'])) {
                self::$is_hhvm = ($_ENV['HHVM'] == 1);
            }

            // Переопределять суперглобал это просто и понятно!
            $_SERVER['REQUEST_URI'] = preg_replace('|/{2,}|', '/', $_SERVER['REQUEST_URI']);
            self::$sad_root = preg_replace('|/[^/]+$|', '', $_SERVER['SCRIPT_FILENAME']);
            self::$sad_baseurl = substr(self::$sad_root, strlen($_SERVER['DOCUMENT_ROOT']));
            if (self::$sad_baseurl === false) {
                self::$sad_baseurl = '';
            }

            // Выставляем правильный Error Level, а также обработчик ошибок, которого в HHVM нет
            self::error_reporting(self::$options->error_reporting_level, self::$options->error_display);
            set_error_handler(function ($errorNumber, $message, $errfile, $errline) {
                self::default_error_handler($errorNumber, $message, $errfile, $errline);

                return false;
            });
            self::$error_handled = array();

            if (strpos($_SERVER['SCRIPT_FILENAME'], $_SERVER['DOCUMENT_ROOT'].'/') === 0) {
                self::$options->rurl =
                    preg_replace('|\\?.*$|', '', $_SERVER['REQUEST_URI']);            // request URL e.g. /cms/script.php
                self::$options->purl =
                    substr($_SERVER['REQUEST_URI'], strlen(self::$sad_baseurl));// param URL   e.g. /script.php?param
                self::$sad_url =
                    substr(self::$options->rurl, strlen(self::$sad_baseurl));   // TRUE URL without params e.g. /script.php
                // @hint Try to use $sad_url instead of the others
            } else {
                //AliasMatch in .htaccess
                header('http/1.1 501');
                echo 'Alias Match error';

                return;
            }

            // Настраиваем mb_ кодировку по умолчанию
            mb_internal_encoding('UTF-8');

            // content-type in headers
            self::$content_type = self::get_mime_type_from_url(self::$sad_url);
            if (self::$content_type !== 'text/html; charset=utf-8') {
                self::$options->hidetimestamp = true;
                self::$options->hidewtime = true;
            }

            // @todo Проверить это на работоспособность
            self::$options->protocol = 'http';
            if (isset($_SERVER['HTTPS']) and ($_SERVER['HTTPS'] == 'on')) {
                self::$options->protocol = 'https';
            }

            self::$options->domain = sad_safe_domain($_SERVER['HTTP_HOST']);

            // spl_autoload_register
            spl_autoload_register(function ($class_name) {
                AscetCMSEngine::autoload_class($class_name);
            });

            // Подгружаем модули
            foreach ($sad_options->modules_list as $module) {
                /** @noinspection PhpIncludeInspection */
                require_once(self::$sad_root.'/functions/'.$module);
            }

            // Выставляем последние значения
            self::$http_status = 200;
            self::$profiling_stamps->init_end = microtime(true);
            self::$database_connection = null;
        }// init

        /**
         * @param stdClass $sad_options
         */
        static function set_options_from_object(stdClass $sad_options) {
            // Выставляем дефолтные значения
            self::$options = (object) array(
                //'current_id'   =>null,                      // for buttons and {#property
                'hidewtime' => true, // hide run  time in the of page; May be set in config.php
                'hidetimestamp' => true, // hide timestamp in the of page; May be set in config.php
                'etag_cache' => true, // E-tag/Last-modified; May be set in config.php
                'content_type' => null,

                'in_admin' => false,// true if index.php included from administration panel
                'include_only' => false,// true if index.php included from some non-AscetCMS place as library with functions
                'hide_cms' => false,

                'database_connection_strategy' => self::DATABASE_CONNECTION_STRATEGY_BEFORE,       // Как инициализировать SQL
                'database_connection_error_strategy' => self::DATABASE_CONNECTION_ERROR_STRATEGY_MESSAGE,
                // Как на ошибки подключения sql
                'database_error_strategy' => self::DATABASE_ERROR_STRATEGY_EXCEPTION,         // Как на ошибки исполнения sql
                'database_transaction_start' => true,            // Запускать ли транзакции автоматом
                'database_transaction_level' => 'read committed',// Уровень изоляции транзакций
                'database_debug' => null,

                'error_reporting_level' => E_ALL,
                'error_display' => false,
                'execution_error_strategy' => self::EXECUTION_ERROR_STRATEGY_MESSAGE,
                'error_log_file' => $_SERVER['DOCUMENT_ROOT'].'/error.log',
                'log_file' => $_SERVER['DOCUMENT_ROOT'].'/info.log',

                'keyvalue_storage_engine' => '',
                'keyvalue_storage_settings' => null,

                'additional' => (object) array(),
                'need_show_profiling_stamps' => true,

                'not_found_page' => null,

                // Список папок для автозагрузки
                'autoload_folders' => [],
            );

            //preset $sad_
            foreach (array('hidewtime', 'hidetimestamp', 'etag_cache', 'in_admin', 'include_only', 'hide_cms',
                           'database_connection_strategy', 'database_connection_error_strategy',
                           'database_error_strategy', 'database_transaction_start', 'database', 'database_debug',
                           'database_transaction_level', 'additional',
                           'error_reporting_level', 'error_display', 'execution_error_strategy', 'error_log_file', 'log_file',
                           'keyvalue_storage_engine', 'keyvalue_storage_settings', 'need_show_profiling_stamps',
                           'not_found_page', 'autoload_folders') as $key) {
                if (!isset($sad_options->{$key})) {
                    continue;
                }
                self::$options->{$key} = $sad_options->{$key};
                if (in_array($key, array('database', 'database_debug'))) {
                    if (self::$options->{$key} !== null) {
                        self::sanify_database_option_object(self::$options->{$key});
                    }
                }

            }
        }

        /**
         * @param stdClass $object
         */
        static function sanify_database_option_object(stdClass $object) {
            if (!isset($object->driver)) {
                $object->driver = 'mysql';
            }
            if (!isset($object->prefix)) {
                $object->prefix = '';
            }
            if (!isset($object->table_prefix_change)) {
                $object->table_prefix_change = true;
            }
            if (!isset($object->dirty_transactions_methods)) {
                $object->dirty_transactions_methods = true;
            }
            if (!isset($object->database_connection_strategy)) {
                $object->database_connection_strategy = self::DATABASE_CONNECTION_STRATEGY_BEFORE;
            }
        }

        /**
         * Получаем MIME-type по URL
         *
         * @param string $url Сам URL
         *
         * @return string MIME-type
         */
        static function get_mime_type_from_url($url) {
            if (!preg_match('|\\.([a-z0-9]+)(\\?[^?]*)?$|', strtolower($url), $sad_t1)) {
                return 'text/html; charset=utf-8';
            }
            $extension = strtolower($sad_t1[1]);

            $mimetypes = array(
                'css' => 'text/css; charset=utf-8',
                'js' => 'application/javascript; charset=utf-8',
                'txt' => 'text/plain; charset=utf-8',
                'jpg' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'ico' => 'image/vnd.microsoft.icon',
            );

            return isset($mimetypes[$extension]) ? $mimetypes[$extension] : 'text/html; charset=utf-8';
        }

        /**
         * Пишет в header'ы профайлинг, если нужно. Функция сама проверяет нужно ли выводить заголовоки
         */
        protected static function write_profiling_header() {
            if (self::$need_prepare_release and self::$options->need_show_profiling_stamps) {
                $last_time = null;
                foreach (self::$profiling_stamps as $key => $value) {
                    header('X-profiling-'.$key.': '.$value.(($last_time != null) ? ', '.($value - $last_time) : ''));
                    $last_time = $value;
                }
                if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
                    header('X-profiling-between-request-init: '.
                           (self::$profiling_stamps->init - (float) $_SERVER['REQUEST_TIME_FLOAT']));
                    header('X-profiling-all: '.(self::$profiling_stamps->real_run_end - (float) $_SERVER['REQUEST_TIME_FLOAT']));
                } else {
                    header('X-profiling-all: '.(self::$profiling_stamps->real_run_end - self::$profiling_stamps->init_start));
                }

                // Пишем сколько ушло на дебаг-функции SQL
                if (self::$database_connection !== null) {
                    header('X-profiling-sql-debug-functions-duration: '.self::$database_connection->debug_functions_all_exec);
                    header('X-profiling-sql-all-queries: '.self::$database_connection->all_query_duration);
                }

                // Пишем сколько ушло на key-value
                if (class_exists('iAscetKeyValueStorage', false)) {
                    $profiling = iAscetKeyValueStorage::get_profiling();
                    header('X-key-value-all: '.$profiling['all']);
                    foreach ($profiling['class'] as $class => &$value) {
                        header('X-key-value-class-'.$class.': '.$value);
                    }
                    foreach ($profiling['action'] as $action => &$value) {
                        header('X-key-value-action-'.$action.': '.$value);
                    }

                    unset($profiling);
                }

                // Пишем сколько ушло на View Template
                if (class_exists('ViewTemplate', false)) {
                    $profiling = ViewTemplate::get_profiling();
                    header('X-view-template-all: '.$profiling['all']);
                    foreach ($profiling['action'] as $action => &$value) {
                        header('X-view-template-action-'.$action.': '.$value);
                    }

                    unset($profiling);
                }
            }
        }

        /**
         * Подготавливаем контент для выдачи, эта функция вызывается перед write header profiling, чтобы
         * отследить профилирование ViewTemplate и в целом учитывать врумя на html_template_finalize
         *
         * Сюда мы попадаем только если need_prepare_release = true
         */
        protected static function prepare_content_for_output() {
            // Пишем наше название
            if (!self::$options->hide_cms) {
                header('X-powered-by: AscetCMS/'.self::version);
            }

            // Меняем шаблон, если нужно. Старый затираем
            if (self::$is_need_show_template) {
                self::$output_buffer = self::$template_html;
            }

            // Производим над шаблоном магию смены prepared-значений
            self::init_view_template();
            self::$output_buffer = ViewTemplate::html_template_finalize(self::$output_buffer, 'ascetcms_template');
        }

        /**
         * Выводим контент. Функция форсированная, в ней не проверяется нужно ли его на самом деле выводить.
         * К телу вывода добавляется профайлинг, если необходимо
         *
         * Сюда мы попадаем только если need_prepare_release = true
         */
        protected static function write_out_content() {
            if ((self::$http_status == 200) and (self::write_out_content_set_and_check_304())) {
                header('http/1.1 304');
                flush();
                self::finish_request();

                return;
            }

            // Добавляем после-каменты к контенту
            self::add_after_comments_to_output_buffer();

            // Пишем header'ы, размер контента
            self::$write_out_content_size = strlen(self::$output_buffer);
            header('http/1.1 '.self::$http_status);
            if (self::$content_type != '') {
                header('Content-Type: '.self::$content_type);
            }
            header('Content-Length: '.self::$write_out_content_size);
            echo self::$output_buffer;
            flush();
            self::finish_request();
        }

        /**
         * Добавляем после-каменты к контенту
         *
         * Функция сама решает по типу контента какой именно добавочный контент выводить
         */
        protected static function add_after_comments_to_output_buffer() {
            if (preg_match('_^text/html;_', AscetCMSEngine::$content_type)) {
                self::add_after_comments_to_output_buffer_html();
            }
            // @todo Добавить сюда JS, CSS
        }

        /**
         * Добавляем после-каменты к HTML-контенту
         *
         * Функция форсированная, она не проверяет нужно ли вызывать контент на самом деле
         */
        protected static function add_after_comments_to_output_buffer_html() {
            if (!(self::$options->hidetimestamp or self::$options->hide_cms)) {
                // Текущая дата
                self::$output_buffer .= '<!-- '.gmdate('d.m.Y H:i:sO').' -->';
            }
            if (!(self::$options->hidewtime or self::$options->hide_cms)) {
                // Время на, собственно, выполнение кода и вообще всё время
                self::$output_buffer .=
                    '<!-- '.(self::$profiling_stamps->clear_exec_end - self::$profiling_stamps->clear_exec_start).', '.
                    (microtime(true) - self::$profiling_stamps->init).'-->';
            }
        }

        /**
         * Закрываем текущее подключение от front-end'а
         */
        protected static function finish_request() {
            if (function_exists('fastcgi_finish_request')) {
                fastcgi_finish_request();
            }
        }

        /**
         * Проверяем, нужно ли выдавать ответ #304, заодно выставляет Last Modified, Expires, Etag и Cache-Control
         *
         * @return boolean Функция посчитала, что 304 ответ
         */
        protected static function write_out_content_set_and_check_304() {
            $object = self::check_304_and_get_headers(self::$header_time_last_modified, self::$header_time_expires,
                self::$header_cache_control, $_SERVER, self::$options->etag_cache, self::$etag_hash);
            foreach ($object->headers as $key => &$value) {
                header("{$key}: {$value}");
            }

            return $object->result;
        }

        /**
         * @var integer|null $header_time_last_modified
         * @var integer|null $header_time_expires
         * @var string[]     $server
         * @var string[]     $header_cache_control
         * @var boolean      $etag_cache
         * @var string|null  $etag_hash
         *
         * @return object
         */
        static function check_304_and_get_headers(
            $header_time_last_modified, $header_time_expires, $header_cache_control, $server, $etag_cache, $etag_hash) {
            $headers = [];
            // Выставляем заголовки Last Modified и Expires
            if ($header_time_last_modified !== null) {
                $headers['Last-Modified'] = gmdate('r', $header_time_last_modified).' GMT';
            }
            if (($header_time_expires !== null) and ($header_time_expires > time())) {
                $header_cache_control[] = 'max-age='.floor(max($header_time_expires - time(), 0));
                $headers['Expires'] = gmdate('r', $header_time_expires).' GMT';
            }

            // Кеширование через E-tag
            if ($etag_cache) {
                if ($etag_hash === null) {
                    $etag_hash = hash('sha512', self::$output_buffer);
                }
                $headers['Etag'] = $etag_hash;
                // Проверяем на If-None-Match
                if (isset($server['HTTP_IF_NONE_MATCH']) and
                    ($etag_hash != null) and ($server['HTTP_IF_NONE_MATCH'] === $etag_hash)
                ) {
                    return (object) [
                        'result' => true,
                        'headers' => $headers,
                    ];
                }
            }
            if (count($header_cache_control) > 0) {
                $headers['Pragma'] = '';
                $headers['Cache-Control'] = implode(', ', $header_cache_control);
            }

            // Проверяем на If-Modified-Since
            if ((self::$header_time_last_modified !== null) and isset($server['HTTP_IF_MODIFIED_SINCE'])) {
                $if_modified_since = strtotime(substr($server['HTTP_IF_MODIFIED_SINCE'], 5));

                if (($if_modified_since <= self::$header_time_last_modified) and ($if_modified_since > 0)) {
                    return (object) [
                        'result' => true,
                        'headers' => $headers,
                    ];
                }
                unset($if_modified_since);
            }

            return (object) [
                'result' => false,
                'headers' => $headers,
            ];
        }

        /**
         * Запуск ядерной печи. Начинаем обрабатывать входящие параметры, включая URL, header'ы и так далее.
         *
         * Настраивает переменные потока типа sad_url. Инициализирует подключение к БД
         */
        static function run() {
            if (self::$options->include_only or self::$options->in_admin) {
                return;
            }

            // Инициализируем первичные параметры запуска, проверяем существует ли файл
            if (!self::run_init()) {
                return;
            }

            // Ставим обработчик завершения скрипта. Он дёрнется при любом раскладе, даже если скрипт завершится успехом
            register_shutdown_function(function () {
                self::unexpected_shutdown_handler();
            });

            // Настоящий запуск, завёрнутый в try-catch
            $sad_need_output = true;
            self::$real_run_ended = false;
            self::$profiling_stamps->real_run_start = microtime(true);
            try {
                self::real_run();

                if (self::$database_connection === null) {
                } elseif (self::$need_commit === true) {
                    self::$database_connection->commit();
                } elseif (self::$need_commit === false) {
                    self::$database_connection->rollBack();
                }
            } catch (Exception $e) {
                self::ob_end();
                $sad_need_output = false;
                self::$execution_exception = $e;
                if (self::$database_connection !== null) {
                    try {
                        self::$database_connection->rollBack();
                    } catch (PDOException $e1) {
                    }
                }

                if (self::event_raise('execution_error') !== false) {
                    self::safe_header('http/1.1 500');
                    switch (self::$options->execution_error_strategy) {
                        case self::EXECUTION_ERROR_STRATEGY_MESSAGE:
                            var_dump($e);
                            break;
                        case self::EXECUTION_ERROR_STRATEGY_SILENCE_EXIT:
                            break;
                    }
                }
            } finally {
                self::$real_run_ended = true;
            }
            self::$profiling_stamps->real_run_end = microtime(true);

            // Подготавливаем контент для выдачи
            if (self::$need_prepare_release and $sad_need_output) {
                self::prepare_content_for_output();

                // Пишем профайлинг в header
                self::write_profiling_header();

                // Выводим контент
                self::write_out_content();
            } else {
                // Пишем профайлинг в header
                self::write_profiling_header();
            }

            self::event_raise('after_last_echo');
            // Насильно закрываем MySQL пкподключение
            AscetCMSEngine::$database_connection = null;
        }// run

        /**
         * Выставляем хидер, если можно
         *
         * @param string $header
         */
        static function safe_header($header) {
            @header($header);
        }

        /**
         * Инициализируем параметры, необходимые для запуска. Проверяем существует ли файл с исходным кодом,
         * подключаемся к Базе данных и KeyValue storage, если необходимо. Эта функция вносит изменения в окружение
         *
         * @return boolean Если false, значит настройка решила, что дальше ничего делать не надо и уже стандартный ответ
         */
        protected static function run_init() {
            // Стандартные парсеры из AscetCMSAdminPanel
            if (class_exists('AscetCMSAdminPanel', false)) {
                AscetCMSAdminPanel::parse_url();
            }

            // Ставим хандлеры
            self::init_handlers_file();

            // Запускаем KeyValue Engine
            if (self::$options->keyvalue_storage_engine !== '') {
                $f1 = 'KeyValueStorage'.self::$options->keyvalue_storage_engine;
                self::$storage = new $f1(self::$options->keyvalue_storage_settings);
                unset($f1);
            }

            // Подключение к базе данных, если необходимо
            if (self::$options->database_connection_strategy == self::DATABASE_CONNECTION_STRATEGY_BEFORE) {
                self::$profiling_stamps->sql_connect_start = microtime(true);
                self::database_connect();
                self::$profiling_stamps->sql_connect_end = microtime(true);
            }

            // Ставим Роутинг путей
            self::$source_file = self::get_source_file_path();

            if (!file_exists(self::$sad_root.'/templates/static'.self::$source_file)) {
                // @todo Определить поведение через options. Вставлять "стандартный" файл ошибок
                if (self::$options->not_found_page !== null) {
                    if (!file_exists(self::$sad_root.'/templates'.self::$options->not_found_page)) {
                        header('http/1.1 500');
                        echo 'Not found page NOT FOUND';

                        return false;
                    }
                    self::$http_status = 404;
                    self::$source_file = self::$options->not_found_page;
                } else {
                    header('http/1.1 404');
                    echo 'default 404 page. File not found';

                    return false;
                }
            }

            // @todo Определить поведение через options
            if (!preg_match('_/$_', self::$source_file) and is_dir(self::$sad_root.'/templates/static/'.self::$source_file)
            ) {
                header('http/1.1 301 goto');
                header('location: '.self::$sad_baseurl.self::$source_file.'/');

                return false;
            }

            return true;
        }

        /**
         * Установка роутинга путей
         *
         * Возвращает путь к файлу с исходным кодом, исходя из self::$sad_url. Подгружает htaccess.php
         *
         * @return string Путь к файлу с исходным кодом
         */
        static function get_source_file_path() {
            $sad_url = self::$sad_url;
            /** @noinspection PhpIncludeInspection */
            include(self::$sad_root.'/htaccess.php');
            $sad_url = preg_replace('_/{2,}_', '/', $sad_url);
            if ($sad_url[0] != '/') {
                $sad_url = '/'.$sad_url;
            }

            // Выбираем путь до файла
            return preg_replace('_/$_', '/index.php', $sad_url);
        }

        /**
         * Запускаем handlers.php
         */
        static function init_handlers_file() {
            $filename = self::$sad_root.'/handlers.php';
            if (!file_exists($filename)) {
                return;
            }
            /** @noinspection PhpIncludeInspection */
            require_once($filename);
        }

        /**
         * Функция, настраивающая поведение послеподготовки контента. Включает послеподготовку
         */
        static function ob_start() {
            self::$need_prepare_release = true;
            ob_start();
        }

        /**
         * Функция, настраивающая поведение послеподготовки контента. Выключает послеподготовку
         */
        static function ob_end() {
            self::$need_prepare_release = false;
            ob_end_clean();
        }

        /**
         * Настоящий запуск исходного кода
         *
         * Погружение твеллов в воду. В output_buffer заносится тело, которое надо вывести на экран. Также сохраняется
         * переменная content_type
         */
        protected static function real_run() {
            global $sad_url, $sad_root, $sad_baseurl, $sad_contype, $sad_prefix, $sad_domain;
            $sad_url = self::$sad_url;
            $sad_root = self::$sad_root;
            $sad_baseurl = self::$sad_baseurl;
            $sad_contype = self::$content_type;
            $sad_old_contype = $sad_contype;
            $sad_prefix = isset(self::$options->database, self::$options->database->prefix)
                ? self::$options->database->prefix : null;
            $sad_domain = sad_safe_domain($_SERVER['HTTP_HOST']);

            self::ob_start();
            self::$profiling_stamps->before_main_code = microtime(true);
            if (self::event_raise('before_main_code') !== false) {
                // Собственно, запуск. Scope этой функции
                self::$profiling_stamps->clear_exec_start = microtime(true);
                /** @noinspection PhpIncludeInspection */
                require(self::$sad_root.'/templates/static/'.self::$source_file);
            }
            self::$profiling_stamps->clear_exec_end = microtime(true);

            // Правим Content Type после его изменения из основного кода
            if ($sad_contype !== $sad_old_contype) {
                self::$content_type = $sad_contype;
            }
            unset($sad_old_contype, $sad_contype);

            if (self::$need_prepare_release) {
                $sad_body = ob_get_contents();
                ob_end_clean();
                self::$output_buffer = $sad_body;
            }

            self::event_raise('before_scope_exit');
        }// real run

        /**
         * Проверка на то админ ли я
         *
         * @param string $right_name
         *
         * @return boolean
         */
        static function iam($right_name = 'a') {
            if (self::$session_value === null) {
                self::session_start();
            }
            $sad_rights = self::session_get_key('sad_rights');
            if ($sad_rights === null) {
                return false;
            }

            return (strpos(' '.$sad_rights, $right_name) > 0);
        }

        /**
         * Проверка на то разработчик ли я
         *
         * @return boolean
         */
        static function iamd() {
            return self::iam('d');
        }

        /**
         * Смена error_reporting и display_errors
         *
         * @param integer      $new_level   Новый уровень error_reporting
         * @param boolean|null $new_display Выводим ли мы это на экран. Если null, то поведение не меняется
         */
        static function error_reporting($new_level, $new_display = null) {
            error_reporting($new_level);
            self::$error_reporting_level = $new_level;
            if ($new_display !== null) {
                self::$error_display = $new_display;
                ini_set('display_errors', self::$error_display ? 'stdout' : 'stderr');
            }
        }

        /**
         * Обработчик ошибок вместо стандартного
         *
         * Сюда приходит код, когда случилась небольшая/большая беда.
         * HHVM передаст сюда управление в случае фатальной ошибки (н-р, ошибки синтакса), обычный PHP — нет.
         * Пишем в наш лог ошибок саму ошибку для будущей обработки.
         *
         * @param integer $errorNumber Номер ошибки
         * @param string  $message     Текст ошибки
         * @param string  $errfile     Название файла, спровоцировавшего ошибку
         * @param integer $errline     Номер строки внутри файла, спровоцировавшего ошибку
         *
         * @throws ErrorException
         */
        static function default_error_handler($errorNumber, $message, $errfile, $errline) {
            self::$error_handled[] = (object) array(
                'code' => $errorNumber,
                'message' => $message,
                'file' => $errfile,
                'line' => $errline
            );

            if ((error_reporting() & $errorNumber) == $errorNumber) {
                // @hint Некоторая часть из этих ошибок не может быть отловлена через error_handler
                switch ($errorNumber) {
                    case E_ERROR           :
                        $errorLevel = 'Error';
                        break;
                    case E_WARNING         :
                        $errorLevel = 'Warning';
                        break;
                    case E_PARSE           :
                        $errorLevel = 'Parse error';
                        break;
                    case E_NOTICE          :
                        $errorLevel = 'Notice';
                        break;
                    case E_CORE_ERROR      :
                        $errorLevel = 'Core error';
                        break;
                    case E_CORE_WARNING    :
                        $errorLevel = 'Core warning';
                        break;
                    case E_COMPILE_ERROR   :
                        $errorLevel = 'Compile error';
                        break;
                    case E_COMPILE_WARNING :
                        $errorLevel = 'Compile warning';
                        break;
                    case E_USER_ERROR      :
                        $errorLevel = 'User error';
                        break;
                    case E_USER_WARNING    :
                        $errorLevel = 'User warning';
                        break;
                    case E_USER_NOTICE     :
                        $errorLevel = 'User notice';
                        break;
                    case E_STRICT          :
                        $errorLevel = 'Strict';
                        break;
                    case E_RECOVERABLE_ERROR:
                        $errorLevel = 'Recoverable error';
                        break;
                    case E_DEPRECATED      :
                        $errorLevel = 'Deprecated';
                        break;
                    case E_USER_DEPRECATED :
                        $errorLevel = 'User deprecated';
                        break;
                    case 16777217          :
                        $errorLevel = 'Fatal error [HHVM]';
                        break;
                    default:
                        $errorLevel = 'Undefined ('.$errorNumber.')';
                }

                if (self::$error_display) {
                    $this_message = '<br/><strong>'.$errorLevel.'</strong>: '.$message.' in <strong>'.$errfile.
                                    '</strong> on line <strong>'.$errline.'</strong><br/>';
                    echo $this_message;
                }
                self::error_log($_SERVER['REQUEST_URI']."\t".$errorLevel.': '.$message.' in '.$errfile.' on line '.$errline);
            }

            // Реагируем на ошибку, если она в списке критических. Да, большая часть из этих ошибок неперехватываема
            if (in_array($errorNumber, array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR,
                                             E_USER_ERROR, E_RECOVERABLE_ERROR))) {
                throw new ErrorException($message, 0, $errorNumber, $errfile, $errline);
            }

            self::event_raise('error_handler');
        }

        /**
         * Функция для обработки завершения работы скрипта
         *
         * Сюда приходит код в случае ЛЮБОГО завершения работы. Мы проверяем через эту функцию завершились ли мы
         * нормально или внезапно сдохли. Эта функция считает, что мы можем сдохнуть только внутри погружения твеллов
         */
        static function unexpected_shutdown_handler() {
            self::event_raise('shutdown');

            $error = error_get_last();
            if (($error === null) or (self::$real_run_ended)) {
                self::event_raise('shutdown_normal');
            } else {
                self::event_raise('shutdown_unexpected');
                self::error_log($_SERVER['REQUEST_URI']."\tCritical error: ".$error['type'].': '.$error['message'].
                                ' in '.$error['file'].' on line '.$error['line']);

                // В HHVM error_get_last не будет содержать строки с ошибками, нужно использовать error_handled
                // В обычном PHP error_handled не будет содержать ошибки, вызвавшей это исключение
            }
            self::$database_connection = null;
        }

        /**
         * Пишем в логи ошибку
         *
         * @param string $message Текст ошибки
         */
        static function error_log($message) {
            @file_put_contents(self::$options->error_log_file, gmdate('Y-m-d H:i:sO').' '.$message."\n", FILE_APPEND | LOCK_EX);
            // @todo Писать в стандартный error_log, если задано настройками
        }

        /**
         * @param mixed       $text     Текст сообщения
         * @param string      $type     Тип сообщения
         * @param string|null $log_file Файл, в который пишем. Если не задан, то log_file
         */
        static function console_log($text, $type = 'info', $log_file = null) {
            if ($log_file === null) {
                $log_file = self::$options->log_file;
            } elseif (!preg_match('|^/|', $log_file)) {
                $log_file = self::$sad_root.'/'.$log_file;
            }
            $backtrace = debug_backtrace();
            $backtrace_text = '';
            $j = count($backtrace);
            for ($i = 1; $i < $j; $i++) {
                $backtrace_text .= ' ['.$backtrace[$i]['file'].' '.$backtrace[$i]['line'].']';
            }
            if (!in_array(gettype($text), array('double', 'string', 'integer'))) {
                ob_start();
                var_dump($text);
                $text = str_replace("\r\n", "   ", ob_get_contents());
                ob_end_clean();
            }
            $full_message = gmdate('Y-m-d H:i:sO')."\t{$type}\t{$text}\t{$backtrace_text}\n";

            @file_put_contents($log_file, $full_message, FILE_APPEND | LOCK_EX);
        }

        /**
         * Функция дёргает все замыкания повешенные как обработчики событий в основном вызываемом коде
         *
         * @param string $event_name Название события (ивента)
         *
         * @return mixed|null Значение последней функции, вернувшей значение, отличное от null'а
         */
        static function event_raise($event_name) {
            if (!isset(self::$event_listeners->{$event_name})) {
                return null;
            }
            self::$profiling_stamps->{'event-start-'.$event_name} = microtime(true);

            $current_result = null;
            foreach (self::$event_listeners->{$event_name} as $ring) {
                foreach ($ring as $closure) {
                    $this_closure_result = $closure();
                    if ($this_closure_result !== null) {
                        $current_result = $this_closure_result;
                    }
                }
            }

            self::$profiling_stamps->{'event-end-'.$event_name} = microtime(true);

            return $current_result;
        }


        /**
         * Добавляем обработчик событий
         *
         * @param string   $event_name  Название события
         * @param callable $closure     Обработчик
         * @param integer  $ring_number Номер кольца для события
         */
        static function add_event_listener($event_name, $closure, $ring_number = 1) {
            if ($event_name == '') {
                return;
            }
            if (!isset(self::$event_listeners->{$event_name})) {
                self::$event_listeners->{$event_name} = array(array(), array(), array());
            }

            self::$event_listeners->{$event_name}[$ring_number][] = $closure;
        }

        /**
         * Берём чистый список языков
         *
         * @param string $raw_list
         *
         * @return array
         */
        static function get_clean_accept_language_list($raw_list = null) {
            if ($raw_list === null) {
                $raw_list = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            }
            $list = explode(',', $raw_list);
            $a = array();
            foreach ($list as $t1) {
                $a[] = preg_replace('|;.*?$|', '', strtolower($t1));
            }

            return $a;
        }

        // template engine
        private static $_view_template_initialized = false;

        /**
         * Инициализируем ViewTemplate
         */
        protected static function init_view_template() {
            if (!self::$_view_template_initialized) {
                // Выставляем пути до шаблонов для ViewTemplate
                self::$_view_template_initialized = true;
                ViewTemplate::init_template('ascetcms_template');
                ViewTemplate::set_template_option('document_root', self::$sad_root, 'ascetcms_template');
                ViewTemplate::set_template_option('html_template_path', self::$sad_root.'/templates/html', 'ascetcms_template');
                ViewTemplate::set_template_option('view_template_path', self::$sad_root.'/templates/view', 'ascetcms_template');
            }
        }

        /**
         * Выставляем текущий html-шаблон, меняем данные о шаблоне в переменной is_need_show_template и template_html
         *
         * @param string $filename Название файла с шаблоном
         */
        static function set_template($filename) {
            self::init_view_template();
            self::$is_need_show_template = true;
            self::$template_html = ViewTemplate::get_html_template($filename, 'ascetcms_template');
            // Option
            self::$template_html = preg_replace_callback('/\\{#option: *([a-z0-9._-]+) *#\\}/i',
                function ($a) {
                    return self::get_option($a[1]);
                }, self::$template_html);

            // Buttons
            self::$template_html = preg_replace_callback('/\\{#but: *([a-z0-9._-]+) *#\\}/i',
                function ($a) {
                    return self::get_button_html($a[1]);
                }, self::$template_html);
        }

        /**
         * Задаём значение в шаблоне. Если ключа не существует, создаём его
         *
         * @param string $key   Ключ
         * @param string $value Значение
         */
        static function set_prepare($key, $value) {
            self::init_view_template();
            ViewTemplate::set_prepare($key, $value, 'ascetcms_template');
        }

        /**
         * @param string  $filename
         * @param integer $include_place
         * @param array   $html_params
         */
        static function include_js_file($filename, $include_place = ViewTemplate::HEADER_INCLUDE, $html_params = []) {
            ViewTemplate::include_js_file($filename, $include_place, $html_params, 'ascetcms_template');
        }

        /**
         * @param string  $js_code
         * @param integer $include_place
         * @param boolean $only_one
         */
        static function include_js($js_code, $include_place = ViewTemplate::PAGE_READY, $only_one = true) {
            ViewTemplate::include_js($js_code, $include_place, $only_one, 'ascetcms_template');
        }

        /**
         * @param string  $filename
         * @param integer $include_place
         * @param array   $html_params
         */
        static function include_css_file($filename, $include_place = ViewTemplate::HEADER_INCLUDE, $html_params = []) {
            ViewTemplate::include_css_file($filename, $include_place, $html_params, 'ascetcms_template');
        }

        /**
         * @param string  $css_code
         * @param integer $include_place
         * @param boolean $only_one
         */
        static function include_css($css_code, $include_place = ViewTemplate::HEADER_INCLUDE, $only_one = true) {
            ViewTemplate::include_css($css_code, $include_place, $only_one, 'ascetcms_template');
        }

        /**
         * Генерируем достаточно уникальный хеш, который по идее нигде не должен встретиться
         *
         * @var integer $key_length Длина хеша
         * Знаете откуда здесь это магическое число? ceil(LOG(3^200)/LOG(62))*2
         * А на два мы умножаем, потому что атака на дни рождения
         *
         * @return string Достаточно уникальный хеш
         */
        static function generate_hash($key_length = 108) {
            // @todo Добавить сюда поддержку http://php.net/manual/ru/function.random-bytes.php
            if (function_exists('openssl_random_pseudo_bytes')) {
                return self::generate_hash_openssl($key_length, true);
            } else {
                return self::generate_hash_trivial($key_length);
            }
        }

        /**
         * Генерируем достаточно уникальный хеш через mt_rand
         *
         * @var integer $key_length Длина хеша
         *
         * @return string Достаточно уникальный хеш
         */
        static function generate_hash_trivial($key_length = 108) {
            $hashes = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
            $hashes_count_minus = count($hashes) - 1;
            $hash = '';
            for ($i = 0; $i < $key_length; $i++) {
                $hash .= $hashes[mt_rand(0, $hashes_count_minus)];
            }

            return $hash;
        }

        /**
         * Генерируем достаточно уникальный хеш через OpenSSL
         *
         * В данный момент алгоритм неоптимален
         *
         * @var integer $key_length Длина хеша
         * @var boolean $crypto     Необходима криптографическая безопасность
         *
         * @return string Достаточно уникальный хеш
         * @hint Этот алгоритм работает только при count($hashes)<=64
         */
        static function generate_hash_openssl($key_length = 108, $crypto = true) {
            $hashes = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
            $hashes_len = count($hashes);
            $hash = '';
            while (strlen($hash) < $key_length) {
                $raw = openssl_random_pseudo_bytes(3, $crypto);
                $int = (ord($raw[0]) << 16) | ord($raw[1]) << 8 | ord($raw[2]);
                for ($i = 0; ($i < 4) and (strlen($hash) < $key_length); $i++) {
                    $b = $int & 63;
                    $int >>= 8;
                    if ($b < $hashes_len) {
                        $hash .= $hashes[$b];
                    }
                }
            }

            return substr($hash, 0, $key_length);
        }

        /**
         * @deprecated
         *
         * @param integer $key_length
         *
         * @return string
         */
        static function sad_create_hash($key_length = 108) {
            return self::generate_hash($key_length);
        }

        /**
         * Проверяем XCRF-атаку, запрос с другого домена
         *
         * @return boolean
         */
        static function is_safe_referer() {
            if (!isset($_SERVER['HTTP_REFERER']) || (strlen($_SERVER['HTTP_REFERER']) == 0)) {
                return true;
            }
            if (!preg_match('|^(.+?)://([a-z0-9.-]+)|i', $_SERVER['HTTP_REFERER'], $a)) {
                return true;
            }

            return strtolower($a[2]) == strtolower($_SERVER['HTTP_HOST']);
        }

        // База данных и SQL
        /**
         * Подключение к базе данных
         *
         * Подключение к БД с настройками из нашей ядерной печи
         *
         * @return boolean Удачно ли удалось подключиться
         *
         * @throws PDOException Если подключение не было успешным и выбрана стратегия дальнейшей ошибки
         */
        static function database_connect() {
            try {
                self::$database_connection = new AscetPDO(
                    self::$options->database->driver.':host='.self::$options->database->host.
                    ';dbname='.self::$options->database->db,
                    self::$options->database->login,
                    self::$options->database->password);
            } catch (PDOException $e) {
                $backtrace = debug_backtrace();
                self::error_log(
                    'Can not connect to SQL Server `'.$e->getMessage().'` on '.$backtrace[0]['file'].' at line '.
                    $backtrace[0]['line']
                );
                unset($backtrace);

                switch (self::$options->database_connection_error_strategy) {
                    case self::DATABASE_CONNECTION_ERROR_STRATEGY_MESSAGE:
                        header('HTTP/1.0 503');
                        echo $e->getMessage();
                        exit;
                    case self::DATABASE_CONNECTION_ERROR_STRATEGY_PAGE:
                        // @todo Имплементировать
                        return false;
                    case self::DATABASE_CONNECTION_ERROR_STRATEGY_NOTHING:
                        return false;
                }
            }

            // Подключаемся к базе данных с логами
            self::database_connect_debug();

            // Всё в порядке, мы подключены к базе данных
            self::$database_connection->sql_exec('SET NAMES utf8;');
            if (self::$options->database_transaction_start) {
                self::$database_connection->sql_exec('SET autocommit=0;');
                self::$database_connection->sql_exec('set session transaction isolation level '.
                                                     self::$options->database_transaction_level.';');
                self::$database_connection->transaction_level = self::$options->database_transaction_level;
                self::$database_connection->beginTransaction();
                self::$need_commit = true;
            }
            self::$database_connection->dirty_transactions_methods = self::$options->database->dirty_transactions_methods;
            self::$database_connection->table_prefix_change = self::$options->database->table_prefix_change;
            self::$database_connection->table_prefix = self::$options->database->prefix;
            // Рейзим event подключения к базе данных
            self::event_raise('database_connect');

            return true;
        }

        /**
         * Подключаемся к Debug Database, если надо
         *
         * Функция нефорсирована
         */
        protected static function database_connect_debug() {
            if (self::$options->database_debug !== null) {
                self::$database_connection->debug_instance_init(self::$options->database_debug);
                self::add_event_listener('after_last_echo', function () {
                    if (self::$database_connection !== null) {
                        self::$database_connection->debug_close_session(
                            microtime(true) - self::$profiling_stamps->init,
                            self::$write_out_content_size);
                    }
                });
            }
        }

        /**
         * Инициализируем подключение, если нужно
         *
         * @throws Exception
         */
        protected static function sql_init_if_need() {
            if ((self::$options->database_connection_strategy == self::DATABASE_CONNECTION_STRATEGY_LAZY) and
                (self::$database_connection === null)
            ) {
                self::$profiling_stamps->sql_connect_start = microtime(true);
                if (self::database_connect() === false) {
                    throw new Exception('Can not init SQL conenection');
                }
                self::$profiling_stamps->sql_connect_end = microtime(true);
            }
        }

        /**
         * Выполнение запросов к БД через AscetPDO
         *
         * Сюда стекаются все запросы, выполненные через функции-неметоды класса и через методы класса.
         * Здесь же мы можем обрабатывать все возникшие ошибки
         *
         * @param string     $sql           Текст запроса
         * @param integer    $type          Тип запроса (0 - query, 1 - exec)
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|integer|null Возвращенный запрос или null, если запроса не было или он неудачен
         *
         * @throws PDOException Если произошла ошибка
         */
        protected static function sql_interface($sql, $type, $prepared_keys = null) {
            self::sql_init_if_need();
            try {
                if ($type === 1) {
                    self::$database_connection->sql_exec($sql, $prepared_keys);

                    return null;
                } else {
                    return self::$database_connection->sql_query($sql, $prepared_keys);
                }
            } catch (PDOException $e) {
                $backtrace = debug_backtrace();
                self::error_log(
                    $_SERVER['REQUEST_URI'].
                    "\tSQL Error `".$e->getMessage().'` on '.$backtrace[1]['file'].' at line '.
                    $backtrace[1]['line'].', sql: `'.mb_substr($sql, 0, 100)."`, stack trace: \n".$e->getTraceAsString()
                );
                unset($backtrace);

                if (self::event_raise('execution_sql_error') === false) {
                    return null;
                }

                // Обратываем ошибку в завимости от выбранной стратегии
                switch (self::$options->database_error_strategy) {
                    case self::DATABASE_ERROR_STRATEGY_WARNING:
                        echo $e->getMessage();
                        break;
                    case self::DATABASE_ERROR_STRATEGY_EXIT:
                        self::safe_header('http/1.1 500');
                        exit;
                    case self::DATABASE_ERROR_STRATEGY_EXCEPTION:
                        self::safe_header('http/1.1 500');
                        throw $e;
                    case self::DATABASE_ERROR_STRATEGY_NOTHING:
                        break;
                }
            }

            return null;
        }

        /**
         * Запрос к БД с ожиданием возврата
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return PDOStatement|null Возврат. Или null, если запрос неудачен
         */
        static function sql_query($sql, $prepared_keys = null) {
            return self::sql_interface($sql, 0, $prepared_keys);
        }

        /**
         * Запрос к БД без ожидания возврата
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         */
        static function sql_exec($sql, $prepared_keys = null) {
            self::sql_interface($sql, 1, $prepared_keys);
        }

        /**
         * Запрос к БД с ожиданием возврата с последующим возвратом первого вхожения
         *
         * @param string     $sql           Текст запроса
         * @param array|null $prepared_keys Массив с ключами для prepared-запросов
         *
         * @return array|null Возвращенное первое вхождение или null, если запрос неудачен
         */
        static function sql_query_fetch($sql, $prepared_keys = null) {
            return sql_fetch(self::sql_query($sql, $prepared_keys));
        }

        /**
         * ID последней внесённой в БД записи
         *
         * @return integer Номер записи
         */
        static function sql_insert_id() {
            return self::$database_connection->sql_insert_id();
        }

        /**
         * Коммит старой транзакции в БД и начало новой
         */
        static function sql_next_transaction() {
            self::sql_init_if_need();
            self::$database_connection->sql_next_transaction();
        }

        /**
         * Смена уровня изоляции транзакций
         *
         * @param string $transaction_level Уровень изоляции
         */
        static function sql_level($transaction_level = '') {
            self::sql_init_if_need();
            self::$database_connection->sql_level($transaction_level);
        }

        // Система сессий
        /**
         * Начинаем сессию
         *
         * @param string $session_name
         */
        static function session_start($session_name = null) {
            if ($session_name === null) {
                if (isset($_COOKIE[self::session_index_name]) and
                    preg_match('|^[a-z0-9]+$|i', $_COOKIE[self::session_index_name])
                ) {
                    $session_name = $_COOKIE[self::session_index_name];
                } else {
                    $session_name = self::generate_hash(50);
                }
            }
            self::$session_name = $session_name;
            @setcookie(self::session_index_name, $session_name, time() + 30 * 24 * 3600,
                (self::$sad_baseurl !== '') ? self::$sad_baseurl : '/', self::$options->domain, null, true);
            self::session_reload();
        }

        /**
         * Lazy-инициализация сессии
         *
         * @param string|null $session_name
         */
        static function session_start_if_need($session_name = null) {
            if (self::$session_name === null) {
                self::session_start($session_name);
            }
        }

        /**
         * Удаляем текущую сессию
         */
        static function session_delete() {
            self::session_start_if_need();
            self::$storage->delete_value('session.'.self::$session_name);
        }

        /**
         * Считываем сессию ещё раз
         *
         * Вызывается из session_start, поэтому само session_start вызывать не может
         */
        static function session_reload() {
            self::$session_value = self::$storage->get_value('session.'.self::$session_name, null);
            if (self::$session_value === null) {
                self::$session_value = (object) array();
            }
        }

        /**
         * Читаем значение ключа
         *
         * @param string $key
         *
         * @returns mixed
         */
        static function session_get_key($key) {
            self::session_start_if_need();

            return isset(self::$session_value->{$key}) ? self::$session_value->{$key} : null;
        }

        /**
         * Пишем значение ключа
         *
         * @param string $key
         * @param mixed  $value
         */
        static function session_set_key($key, $value) {
            self::session_start_if_need();
            $temporary_value = self::$storage->get_value('session.'.self::$session_name, null);
            if ($temporary_value === null) {
                $temporary_value = (object) [];
            }
            $temporary_value->{$key} = $value;
            self::$session_value->{$key} = $value;
            self::$storage->set_value('session.'.self::$session_name, $temporary_value, 10 * 365.25 * 24 * 3600);
        }

        /**
         * Берём значение ключа из таблицы _options
         *
         * @param string $key
         *
         * @return string
         */
        static function get_option($key) {
            /** @noinspection SqlResolve */
            try {
                $q_c = sql_query_fetch(/** @lang text */
                    "select `{$key}` as `value` from `____options` limit 1;");

                return $q_c['value'];
            } catch (PDOException $e) {
                if ($e->getCode() == 42) {
                    return '';
                }
                throw $e;
            }
        }

        /**
         * Берём код кнопок
         *
         * @param string  $key
         * @param integer $sad_temp_id
         *
         * @return string
         */
        static function get_button_html($key, $sad_temp_id = null) {
            if (self::iam()) {
                return self::get_button_html_force($key, $sad_temp_id);
            } else {
                return '';
            }
        }

        /**
         * Берём код кнопок
         *
         * @param string  $key
         * @param integer $sad_temp_id
         *
         * @return string
         */
        static function get_button_html_force($key, $sad_temp_id = null) {
            $filename = self::$sad_root.'/templates/buttons/'.$key;
            if (file_exists($filename)) {
                $buf = file_get_contents($filename);
                $buf = str_replace('{#id#}', $sad_temp_id, $buf);
                $buf = preg_replace_callback('|{#call:([a-z0-9_]+)#}|', function ($a) use ($key, $sad_temp_id) {
                    if (array_key_exists($key, self::$button_parse_functions) and
                        array_key_exists($a[1], self::$button_parse_functions[$key])
                    ) {
                        /**
                         * @var Callable $closure
                         */
                        $closure = self::$button_parse_functions[$key][$a[1]];

                        return $closure($sad_temp_id);
                    } else {
                        return '';
                    }
                }, $buf);
                // @todo таблица
            } else {
                $buf = '';
            }
            if (self::iamd()) {
                // Код редактирования кнопки
                $buf .= '<a href="/i/editcode.php?file=buttons%2F'.urlencode($key).'" target="_blank">'.
                        '<img src="'.AscetCMSEngine::$sad_baseurl.
                        '/common/adminpanel/blue-document-attribute-p.png" alt="Edit button code"></a>';
            }

            return $buf;
        }

        /**
         * @param string   $button_name
         * @param string   $call_name
         * @param Callable $closure
         */
        static function add_button_parse_callable($button_name, $call_name, $closure) {
            if (!array_key_exists($button_name, self::$button_parse_functions)) {
                self::$button_parse_functions[$button_name] = [];
            }
            self::$button_parse_functions[$button_name][$call_name] = $closure;
        }

        /**
         * @return string[]
         */
        static function autoload_class_get_available_folders() {
            $available_folders = ['/functions',
                                  '/functions/models',
                                  '/functions/libs',
                                  '/functions/other',];
            if (isset(self::$options->autoload_folders) and
                is_array(self::$options->autoload_folders) and
                (count(self::$options->autoload_folders) > 0)
            ) {
                $available_folders = array_unique(array_merge($available_folders, self::$options->autoload_folders));
            }

            return $available_folders;
        }

        /**
         * @param string $class_name
         *
         * @return string[]
         */
        static function autoload_class_get_filename($class_name) {
            $class_name = ltrim($class_name, '\\');

            return array_unique([
                $class_name.'.php',
                mb_strtolower($class_name).'.php',
                str_replace('_', '', $class_name).'.php',
                mb_strtolower(str_replace('_', '', $class_name)).'.php',
                preg_replace_callback('|_(.)|', function ($a) {
                    return mb_strtoupper($a[1]);
                }, $class_name).'.php',
                preg_replace_callback('|([a-z])([A-Z])|', function ($a) {
                    return $a[1].'_'.mb_strtolower($a[2]);
                }, $class_name).'.php',
                mb_strtolower(preg_replace_callback('|([a-z])([A-Z])|', function ($a) {
                    return $a[1].'_'.mb_strtolower($a[2]);
                }, $class_name)).'.php',
            ]);
        }

        /**
         * @param string  $class_name
         * @param boolean $suppress_ascetcms
         *
         * @return string
         */
        static function get_psr0($class_name, $suppress_ascetcms = true) {
            $class_name = ltrim($class_name, '\\');
            if ($suppress_ascetcms) {
                $class_name = preg_replace('_^ascetcms\\\\_i', '', $class_name);
            }
            $file_name = '';
            if ($lastNsPos = strrpos($class_name, '\\')) {
                $namespace = substr($class_name, 0, $lastNsPos);
                $class_name = substr($class_name, $lastNsPos + 1);
                $file_name = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR;
            }
            $file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name).'.php';

            return $file_name;
        }

        /**
         * SPL: Автозагрузка классов
         *
         * @param string $class_name
         */
        static function autoload_class($class_name) {
            $psr0_filename = self::get_psr0($class_name);
            if (file_exists(self::$sad_root.'/functions/'.$psr0_filename)) {
                /** @noinspection PhpIncludeInspection */
                require_once(self::$sad_root.'/functions/'.$psr0_filename);

                return;
            } elseif (file_exists(strtolower(self::$sad_root.'/functions/'.$psr0_filename))) {
                /** @noinspection PhpIncludeInspection */
                require_once(strtolower(self::$sad_root.'/functions/'.$psr0_filename));

                return;
            }
            $available_folders = self::autoload_class_get_available_folders();
            $filenames = self::autoload_class_get_filename($class_name);
            foreach ($available_folders as $folder) {
                if (!file_exists(self::$sad_root.$folder) or !is_dir(self::$sad_root.$folder)) {
                    continue;
                }
                foreach ($filenames as &$filename) {
                    $full_filename = self::$sad_root.$folder.'/'.$filename;
                    if (file_exists($full_filename)) {
                        /** @noinspection PhpIncludeInspection */
                        require_once($full_filename);

                        return;
                    }
                }
            }
        }
    } // CoreEngine class

    // Global functions
    // base functions
    /**
     * Санация домена, удаление ведущего www и номера порта
     *
     * @param string $domain Домен/Хост
     *
     * @return string Санированный домен
     */
    function sad_safe_domain($domain) {
        list($s,) = explode(':', strtolower($domain), 2);
        if (preg_match_all('|\\.|', $s, $sad_tmp) > 1) {
            $s = preg_replace('|^www\\.(.+?)$|', '$1', $s);
        }

        return $s;
    }

    /**
     * Санация части regexp для добавления напрямую в regexp
     *
     * @param string $text Часть, которую над санировать
     *
     * @return string
     */
    function sad_safe_reg($text) {
        $ar = '.-\\/[]{}()*?+^$|';
        $s = '';
        for ($i = 0; $i < strlen($text); $i++) {
            if (strpos($ar, $text[$i]) !== false) {
                $s .= '\\'.$text[$i];
            } else {
                $s .= $text[$i];
            }
        }

        return $s;
    }

    /**
     * Санация файлового пути на сервере
     *
     * Это не аналог realpath, он не должен обрабатывать .. и ., он должен их выкидывать
     *
     * @param string $path Путь
     *
     * @return string
     */
    function sad_safe_path($path) {
        // @todo \
        $a = explode('/', $path);
        $s = '';
        foreach ($a as &$e) {
            if (($e == '') or ($e == '.') or ($e == '..')) {
                continue;
            }
            $s .= '/'.$e;
        }

        return substr($s, 1);
    }

    // SQL globals functions. Эти функции дёргают последний инстанс AscetCMSEngine
    /**
     * Запрос к БД без ожидания возврата через последний созданный AscetCMSEngine
     *
     * @param string     $sql           Текст запроса
     * @param array|null $prepared_keys Массив с ключами для prepared-запросов
     */
    function sql_exec($sql, $prepared_keys = null) {
        AscetCMSEngine::sql_exec($sql, $prepared_keys);
    }

    /**
     * Запрос к БД с ожиданием возврата через последний созданный AscetCMSEngine
     *
     * @param string     $sql           Текст запроса
     * @param array|null $prepared_keys Массив с ключами для prepared-запросов
     *
     * @return PDOStatement|null Возврат. Или null, если запрос неудачен
     */
    function sql_query($sql, $prepared_keys = null) {
        return AscetCMSEngine::sql_query($sql, $prepared_keys);
    }

    /**
     * Запрос к БД с ожиданием возврата с последующим возвратом первого вхожения через последний созданный
     * AscetCMSEngine
     *
     * @param string     $sql           Текст запроса
     * @param array|null $prepared_keys Массив с ключами для prepared-запросов
     *
     * @return array|null Возвращенное первое вхождение или null, если запрос неудачен
     */
    function sql_query_fetch($sql, $prepared_keys = null) {
        return AscetCMSEngine::sql_query_fetch($sql, $prepared_keys);
    }

    /**
     * ID последней внесённой в БД записи через последний созданный AscetCMSEngine
     *
     * @return integer Номер записи
     */
    function sql_insert_id() {
        return AscetCMSEngine::sql_insert_id();
    }

    /**
     * Коммит старой транзакции в БД и начало новой через последний созданный AscetCMSEngine
     */
    function sql_next_transaction() {
        AscetCMSEngine::sql_next_transaction();
    }

    /**
     * Смена уровня изоляции транзакций через последний созданный AscetCMSEngine
     *
     * @param string $transaction_level Уровень изоляции
     */
    function sql_level($transaction_level = '') {
        AscetCMSEngine::sql_level($transaction_level);
    }

?>