<?php

    $LANGS_t = array(
        'title' => array(
            'main' => 'Main',
            'option' => 'Options',
            'code' => 'Source',
            'visits' => 'Visits',
            'history' => 'History',
            'upload' => 'Upload',
            'pictures' => 'Pictures',
            'sql' => 'SQL',
            'phpinfo' => 'PHPInfo',
            'logout' => 'Logout',
            'l_logout' => 'Logout from CMS',
            'l_main' => 'Main page of administration panel',
            'l_option' => 'Options',
            'l_code' => 'Source code editor',
            'l_visits' => 'Visits statistic',
            'l_history' => 'Admin log',
            'l_upload' => 'Upload files',
            'l_pictures' => 'Picture manager / Picture converter',
            'l_sql' => 'Database manager',
            'l_phpinfo' => 'Information about PHP',
        ),
        'common' => array(
            'developer_only' => 'Sorry. For developers only.',
        ),
        'index' => array(
            'title' => 'Admin panel',
            'l_version' => 'Version',
            'l_doc_root' => 'Document root',
            'l_cmsroot' => 'CMS root',
            'l_protocol' => 'Protocol',
            'l_domain' => 'Domain',
            'l_cmsurl' => 'CMS URL',
            'mn_dom' => 'Your server does not have module <b>dom</b>. Functions from "xml.php" and DOMDocument does not work',
            'mn_libxml' => 'Your server does not have module <b>libxml</b>. Functions from "xml.php" does not work',
            'mn_gd' => 'Your server does not have module <b>gd</b>. Functions from "imagefunctions.php" does not work',
            'mn_iconv' => 'Your server does not have module <b>iconv</b>.',
            'mn_curl' => 'Your server does not have module <b>curl</b>.',
            'mn_mbstring' => 'Your server does not have module <b>mbstring</b>.',
            'l_handler' => 'Handler',
            'l_post_max' => 'Upload limit for <b>all</b> files',
            'l_upload_max' => 'Upload limit for <b>one</b> file',
            'l_memory_lim' => 'Memory limit',
            'l_int_enc' => 'MB internal encoding',
            'l_disp_er' => 'Display error',
            'ma_rewrite' => 'Your Apache does not have <b>mod_rewrite</b>. CMS <b>can not</b> work without it.',
            'badbrowser' => 'You are using a program that is not in the list of supported web browsers. Please download one of three submitted by browsers, otherwise correct operation of CMS is not guaranteed.',
        ),
        'showcode' => array(
            'title' => 'File lists',
            'infobox' => 'Edit source code of templates. Creating new templates.',
        ),
        'uploadfiles' => array(
            'title' => 'File upload',
            'infobox' => 'Upload files into directory "storage".
<ol>
 <li>Select destination folder via tree or input it directly. Folders will be created if not exists.</li>
 <li value="2">a) Select files (multiselecing is enabled. IE can not into multiselecting)</li>
 <li value="3">a) Click "upload files"</li>
 <li value="2">b) Drag and drop files to special panel in right</li>
</ol>',
            'drop_here' => 'Drop file HERE',
            'overwrite' => 'Overwrite files',
            'overwrite_title' => 'Overwrite existing files',
            'folder' => 'Folder',
            'folder_title' => 'Destination folder for new files',
            'files' => 'Files',
            'uploadbutton' => 'Upload files',
        ),
        'pictures' => array(
            'title' => 'Picture Manager',
            'infobox' => 'Copy existing images with creating preview. All file change their names to "sha512.ext" e.g. cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e.png
<ol>
 <li>Select source folder</li>
 <li>Select files by checking checkbox</li>
 <li>Select destination folder via tree or input it directly</li>
 <li>Select height and width of preview. Preview will be central region of image. If height or width is "auto", it will be calculated, given original sizes (e.g. 1024x768 with "300xAuto" will be 300x225, and with "Autox300" will be 400x300).</li>
 <li>Click "Copy images"</li>
</ol>',
        ),
        'edit' => array(
            'title' => 'Edit record',
            'edit' => 'Edit',
            'add' => 'Add',
            'delete' => 'Delete',
        ),
        'delete' => array(
            'title' => 'Delete',
            'text' => 'DELETE RECORD <b>{#id#}</b>. ARE YOU <b>SURE</b>?',
            'yes' => 'Yes',
            'no' => 'NO',
        ),
        'error_reporting' => array(
            'display_error' => 'Show errors',
            'E_ERROR' => 'Fatal run-time errors',
            'E_WARNING' => 'Run-time warnings',
            'E_PARSE' => 'Compile-time parse errors',
            'E_NOTICE' => 'Run-time notices',
            'E_CORE_ERROR' => 'Fatal errors that occur during PHP\'s initial startup',
            'E_CORE_WARNING' => 'Warnings (non-fatal errors) that occur during PHP\'s initial startup',
            'E_COMPILE_ERROR' => 'Fatal compile-time errors. Zend',
            'E_COMPILE_WARNING' => 'Compile-time warnings. Zend',
            'E_USER_ERROR' => 'User-generated error message',
            'E_USER_WARNING' => 'User-generated warning message',
            'E_USER_NOTICE' => 'User-generated notice message',
            'E_STRICT' => 'Enable to have PHP suggest changes to your code which will ensure the best interoperability and forward compatibility of your code',
            'E_RECOVERABLE_ERROR' => 'Catchable fatal error',
            'E_DEPRECATED' => 'Run-time notices. Enable this to receive warnings about code that will not work in future versions'
        ),


    );
?>