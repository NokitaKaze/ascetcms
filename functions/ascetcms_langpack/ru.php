<?php
    $LANGS_t = array(
        'title' => array(
            'main' => 'Главная',
            'option' => 'Опции',
            'code' => 'Код',
            'visits' => 'Визиты',
            'history' => 'История',
            'upload' => 'Загрузка',
            'pictures' => 'Картинки',
            'sql' => 'SQL',
            'phpinfo' => 'PHPInfo',
            'logout' => 'Выйти',
            'l_main' => 'Главная страница администраторской панели',
            'l_option' => 'Главные опции',
            'l_code' => 'Редактор исходного кода',
            'l_visits' => 'Статистика посещений',
            'l_history' => 'Админский журнал',
            'l_upload' => 'Загрузка файлоа',
            'l_pictures' => 'Менеджер изображений / Конвертатор изображений',
            'l_sql' => 'Менеджер баз данных',
            'l_phpinfo' => 'Информация о установленном PHP',
            'l_logout' => 'Выйти из системы',
        ),
        'common' => array(
            'developer_only' => 'Извините, только для разработчиков.',
        ),
        'index' => array(
            'title' => 'Панель администрирования',
            'l_version' => 'Версия',
            'l_doc_root' => 'Document root',
            'l_cmsroot' => 'CMS root',
            'l_protocol' => 'Протокол',
            'l_domain' => 'Домен',
            'l_cmsurl' => 'CMS URL',
            'mn_dom' => 'На вашем сервере не установлен модуль <b>dom</b>. Функции из "xml.php" и DOMDocument не будут работать',
            'mn_libxml' => 'На вашем сервере не установлен модуль <b>libxml</b>. Функции из "xml.php" не будут работать',
            'mn_gd' => 'На вашем сервере не установлен модуль <b>gd</b>. Функции из "imagefunctions.php" не будут работать',
            'mn_iconv' => 'На вашем сервере не установлен модуль <b>iconv</b>.',
            'mn_curl' => 'На вашем сервере не установлен модуль <b>curl</b>.',
            'mn_mbstring' => 'На вашем сервере не установлен модуль <b>mbstring</b>.',
            'l_handler' => 'Обработчик',
            'l_post_max' => 'Лимит размера для <b>всех</b> загружаемых файлов',
            'l_upload_max' => 'Лимит размера для <b>одного</b> загружаемого файла',
            'l_memory_lim' => 'Лимит памяти',
            'l_int_enc' => 'Внутренняя кодировка функций MB',
            'l_disp_er' => 'Показывать ошибки',
            'ma_rewrite' => 'На вашем Апаче не установлен <b>mod_rewrite</b>. CMS <b>не будет</b> работать без него.',
            'badbrowser' => 'Вы используете программу, не входящую в список поддерживаемых браузеров. Пожалуйста, скачайте один из трёх представленных браузеров, в обратном случае правильная работа CMS не гарантируется.',
        ),
        'showcode' => array(
            'title' => 'Список файлов',
            'infobox' => 'Редактирование кода шаблонов. Создание новых шаблонов.',
        ),
        'uploadfiles' => array(
            'title' => 'Загрузка файлов',
            'infobox' => 'Загрузка файлов в папку "storage".
<ol>
 <li>Через дерево папок или ручным вводом выберите папку, в которую надо загружать файлы. Если папка не существует, она будет создана.</li>
 <li value="2">a) Выберите файлы (можно несколько, но не в Internet Explorer\'e)</li>
 <li value="3">a) Нажмите "Загрузить файлы"</li>
 <li value="2">b) Перетащите файлы на специальную панель справа</li>
</ol>',
            'drop_here' => 'Перетащите файлы на ЭТУ панель',
            'overwrite' => 'Перезаписывать файлы',
            'overwrite_title' => 'Перезаписывать существующие файлы',
            'folder' => 'Папка',
            'folder_title' => 'Папка, в которую будут загружаться файлы',
            'files' => 'Файлы',
            'uploadbutton' => 'Загрузить файлы',
        ),
        'pictures' => array(
            'title' => 'Менеджер изображений',
            'infobox' => 'Копирование картинок <b>вместе с</b> созданием эскизов. Все файлы меняют свои названия на "sha512.ext", н-р, cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e.png
<ol>
 <li>Выберите папку, в которой лежат изображения</li>
 <li>Выберите файлы, устанавливая галки</li>
 <li>Через древо папок или ручным вводом выберите папку, в которую надо копировать изображения</li>
 <li>Выберите высоту и ширину эскизов. Все эскизы будут браться из середины изображений. Если ширина или высота установлена в "auto", требуемая высота или ширина будут расчитаны автоматически, исходя из оригинального разрешения (например, 1024x768 с "300xAuto" станет 300x225, а с "Autox300" станет 400x300).</li>
 <li>Нажмите "Copy images"</li>
</ol>',
        ),
        'edit' => array(
            'title' => 'Редактирование записи',
            'edit' => 'Сохранить',
            'add' => 'Добавить',
            'delete' => 'Удалить',
        ),
        'delete' => array(
            'title' => 'Удаление записи',
            'text' => 'УДАЛИТЬ ЗАПИСЬ <b>{#id#}</b>. ВЫ <b>УВЕРЕНЫ</b>?',
            'yes' => 'Да',
            'no' => 'НЕТ',
        ),
        'error_reporting' => array(
            'display_error' => 'Показывать ошибки',
            'E_ERROR' => 'Фатальные ошибки времени выполнения',
            'E_WARNING' => 'Предупреждения времени выполнения',
            'E_PARSE' => 'Ошибки на этапе компиляции',
            'E_NOTICE' => 'Уведомления времени выполнения',
            'E_CORE_ERROR' => 'Фатальные ошибки, которые происходят во время запуска РНР',
            'E_CORE_WARNING' => 'Предупреждения (нефатальные ошибки), которые происходят во время начального запуска РНР',
            'E_COMPILE_ERROR' => 'Фатальные ошибки на этапе компиляции. Zend',
            'E_COMPILE_WARNING' => 'Предупреждения на этапе компиляции. Zend',
            'E_USER_ERROR' => 'Сообщения об ошибках сгенерированные пользователем',
            'E_USER_WARNING' => 'Предупреждения сгенерированные пользователем',
            'E_USER_NOTICE' => 'Уведомления сгенерированные пользователем',
            'E_STRICT' => 'Включаются для того, чтобы PHP предлагал изменения в коде, которые в обеспечат лучшее взаимодействие и совместимость кода',
            'E_RECOVERABLE_ERROR' => 'Фатальные ошибки с возможностью обработки',
            'E_DEPRECATED' => 'Уведомления времени выполнения об использовании устаревших конструкций'
        ),

    );

?>