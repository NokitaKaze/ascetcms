<?php

    /**
     * Class SDBRecord
     * Запись из Базы данных
     *
     * @property-read string $table_name
     * @property-read string $class_name
     * @property-read string $primary_key
     * @property-read string $attributes_exists_in_table
     * @property SDBCriteria $dbcriteria
     */
    abstract class SDBRecord extends SModel {
        /**
         * Стратегии сохранения
         *
         * SAVE_ONLY_CHANGED — Сохраняем только изменнённые после ПОСЛЕДНЕГО СОХРАНЕНИЯ атрибуты
         * SAVE_ONLY_CHANGED — Сохраняем только изменнённые после ЗАГРУЗКИ атрибуты
         */
        const SAVE_ONLY_CHANGED = 1;
        const SAVE_ONLY_CHANGED_AFTER_LOAD = 2;
        public $_is_new_record = true;

        public $table_alias = 't';

        /**
         * Размер Batch'а
         */
        const RELATION_BATCH_LOAD_SIZE = 100;

        /**
         * Размер Batch'а
         */
        const RELATION_BATCH_LOAD_SIZE_DEFAULT = 1000;

        /**
         * @var boolean Будет произведена загрузка схемы таблицы, если нужно
         */
        protected static $need_load_table_schema = true;

        /**
         * @var array[] Список с загруженными из таблицы филдами. Чер
         */
        private static $table_schema_field_lists;

        private $_attributes_additional = [
            'loaded' => [
                'values' => [],
                'changed' => [],
            ],
            'saved' => [
                'values' => [],
                'changed' => [],
            ],
        ];

        /**
         * @var SDBCriteria|null
         */
        protected $_dbcriteria = null;

        /**
         * @var array[]
         */
        private static $_get_attributes_list_cached = [];

        /**
         * @return string
         */
        static function getTable_name() {
            return 'tablename';
        }

        function __construct($scenario = 'create') {
            parent::__construct($scenario);
            self::load_schema_if_need();
        }

        /**
         * @return SDBCriteria|null
         */
        function getDbcriteria() {
            return $this->_dbcriteria;
        }

        /**
         * @param SDBCriteria|null $criteria
         */
        function setDbcriteria(SDBCriteria $criteria) {
            $this->_dbcriteria = $criteria;
        }

        /**
         * @return array[]
         */
        final static function get_attributes_list() {
            if (isset(self::$_get_attributes_list_cached[static::class])) {
                return self::$_get_attributes_list_cached[static::class];
            }
            $a = static::get_attributes_list_overload();
            self::load_schema_if_need();
            if (!isset(self::$table_schema_field_lists[static::getTable_name()])) {
                return $a;
            }
            // @hint Возможно, array_merge делает это также. Убедиться и поправить
            foreach (self::$table_schema_field_lists[static::getTable_name()] as $key => &$value) {
                if (!array_key_exists($key, $a)) {
                    $a[$key] = $value;
                }
            }

            self::$_get_attributes_list_cached[static::class] = $a;

            return $a;
        }

        /**
         * @param string  $name
         * @param mixed   $value
         * @param boolean $force         Пишем, не взирая на запрет записи
         * @param boolean $need_sanation Нужно санировать значение
         */
        function set_attribute($name, $value, $force = false, $need_sanation = true) {
            if ($force or $this->is_attribute_writable($name)) {
                // Записываем в список поменявшихся атрибутов
                $this->_attributes_additional['loaded']['changed'][] = $name;
                $this->_attributes_additional['saved']['changed'][] = $name;

                parent::set_attribute($name, $value, true, !is_object($value) and $need_sanation);
            }
        }

        /**
         * @param string $name
         * @param mixed  $value
         */
        private function set_attribute_force_without_sanation($name, $value) {
            parent::set_attribute($name, $value, true, false);
        }

        /**
         * Параметр, из определённого комплекта
         *
         * @param string $key
         * @param string $type
         *
         * @return mixed|null
         * @throws SModelException
         */
        function get_attribute_value($key, $type = 'current') {
            if ($type == 'current') {
                return $this->get_attribute($key);
            } elseif (in_array($type, ['loaded', 'saved'])) {
                $need_type = static::get_attribute_type($key);

                return array_key_exists($key, $this->_attributes_additional[$type]['values'])
                    ? self::convert_value_to_type($this->_attributes_additional[$type]['values'][$key], $need_type) : null;
            } else {
                throw new SModelException('This attribute collection isn\'t implemented');
            }
        }

        /**
         * Очищаем список поменявшихся атрибутов
         *
         * @param string $type
         *
         * @throws SModelException
         */
        private function clear_diff_list($type) {
            if (in_array($type, ['loaded', 'saved'])) {
                $this->_attributes_additional[$type]['changed'] = [];
            } else {
                throw new SModelException('This attribute collection isn\'t implemented');
            }
        }

        /**
         * Берём список поменявшихся атрибутов
         *
         * @param string $type
         *
         * @throws SModelException
         * @return string[]
         */
        function get_diff_list($type) {
            if (in_array($type, ['loaded', 'saved'])) {
                return $this->_attributes_additional[$type]['changed'];
            } else {
                throw new SModelException('This attribute collection isn\'t implemented');
            }
        }

        /**
         * @param SDBCriteria[] $criteria
         * @param boolean       $force_string
         *
         * @return mixed[]
         */
        protected static function getWhere_string($criteria, $force_string = false) {
            $sql = [];
            $params = [];
            foreach ($criteria as $criterium) {
                if ($criterium === null) {
                    continue;
                }
                $sql_this = $criterium->get_full_sql_query_string();
                if ($sql_this !== '') {
                    $sql[] = $sql_this;
                }
                $params = array_merge($params, $criterium->params);
            }

            if ($force_string) {
                $sql = implode('and', $sql);
            }

            return [$sql, $params];
        }

        /**
         * Находим все записи из БД
         *
         * @param SDBCriteria|SDBCriteria[]|null $criteria null - искать всё
         * @param integer|null                   $limit
         * @param string|string[]|null           $select
         * @param string                         $table_alias
         * @param integer                        $offset
         *
         * @return SDBRecord[]
         * @throws SModelException
         */
        static function find_all($criteria = null, $limit = null, $select = null, $table_alias = 't', $offset = 0) {
            $query = self::get_query_from_criteria($criteria, $limit, $select, $table_alias, $offset);
            list($query_string, $query_params) = $query->get_query();
            unset($query);

            //
            self::load_schema_if_need();
            $model_name = static::class;
            $q = sql_query($query_string, $query_params);
            if (is_null($q)) {
                throw new SModelException('Can not execute sql query');
            }
            /**
             * @var SDBRecord[] $data
             */
            $data = [];
            while ($q_c = sql_fetch($q)) {
                /**
                 * @var SDBRecord $record
                 */
                $record = new $model_name('update');
                foreach ($q_c as $key => &$value) {
                    if (property_exists($record, $key)) {
                        $record->{$key} = $value;
                    } else {
                        $record->set_attribute_force_without_sanation($key, $value);
                    }
                }

                $data[] = $record;
            }
            unset($q, $q_c, $key, $value, $record);

            self::sanify_many_objects($data);
            $exist_in_tables = static::getAttributes_exists_in_table();
            foreach ($data as &$record) {
                foreach ($exist_in_tables as &$key) {
                    $record->_attributes_additional['saved']['values'][$key] = $record->get_attribute($key, false);
                }
                $record->_attributes_additional['loaded']['values'] = $record->_attributes_additional['saved']['values'];
                $record->_is_new_record = false;
            }

            return $data;
        }

        /**
         * @param SDBCriteria|SDBCriteria[]|null $criteria null - искать всё
         * @param integer|null                   $limit
         * @param string|string[]|null           $select
         * @param string                         $table_alias
         * @param integer                        $offset
         *
         * @return SDBQuery
         */
        static function get_query_from_criteria($criteria = null, $limit = null, $select = null, $table_alias = 't',
                                                $offset = 0) {
            $criteria_all = !is_array($criteria) ? [$criteria] : $criteria;
            list($where_string, $where_params) = self::getWhere_string($criteria_all);
            // @todo Сделать выборку филдов через yw1, yw2, yw3...
            $query = new SDBQuery();
            $query->set_select($select);
            $query->set_from(static::getTable_name().' as `'.$table_alias.'`');
            $query->set_where($where_string, $where_params);
            $query->set_limit($limit);
            $query->set_offset($offset);
            if ((count($criteria_all) > 0) and (get_class($criteria_all[0]) == 'SDBCriteria') and
                                               ($criteria_all[0]->order != '')
            ) {
                $query->set_order($criteria_all[0]->order);
            }
            if ((count($criteria_all) > 0) and (get_class($criteria_all[0]) == 'SDBCriteria')) {
                $query->set_last_query_block($criteria_all[0]->last_query_block);
            }

            return $query;
        }

        /**
         * Находим все записи из БД
         *
         * @param SDBCriteria|null $criteria
         * @param integer|null     $limit
         * @param null             $select
         * @param integer          $offset
         *
         * @return SDBRecord[]
         */
        function find($criteria = null, $limit = null, $select = null, $offset = 0) {
            $all_criteria = [];
            if ($this->_dbcriteria !== null) {
                $all_criteria[] = $this->_dbcriteria;
            } else {
                $temporary_criteria = $this->getAbstractCriteria();
                if (!$temporary_criteria->is_empty()) {
                    $all_criteria[] = $temporary_criteria;
                }
            }
            if ($criteria !== null) {
                $all_criteria[] = $criteria;
            }

            return self::find_all($all_criteria, $limit, $select, $this->table_alias, $offset);
        }

        /**
         * Находим количество по критерии
         *
         * @param SDBCriteria|null $criteria
         * @param string           $table_alias
         *
         * @return integer
         */
        function find_count($criteria = null, $table_alias = 't') {
            $all_criteria = [];
            if ($this->_dbcriteria !== null) {
                $all_criteria[] = $this->_dbcriteria;
            } else {
                $temporary_criteria = $this->getAbstractCriteria();
                if (!$temporary_criteria->is_empty()) {
                    $all_criteria[] = $temporary_criteria;
                }
            }
            if ($criteria !== null) {
                $all_criteria[] = $criteria;
            }

            return self::find_count_from_criteria($all_criteria, $table_alias);
        }

        /**
         * @param SDBCriteria|SDBCriteria[]|null $criteria
         * @param string                         $table_alias
         *
         * @return integer
         */
        static function find_count_from_criteria($criteria = null, $table_alias = 't') {
            if (is_array($criteria)) {
                $all_criteria = $criteria;
            } elseif (is_object($criteria)) {
                $all_criteria = [$criteria];
            } else {
                $all_criteria = [];
            }

            list($where_string, $where_params) = self::getWhere_string($all_criteria);
            $query = new SDBQuery();
            $query->set_select('count(*) as `count`');
            $query->set_from(static::getTable_name().' as `'.$table_alias.'`');
            $query->set_where($where_string, $where_params);
            list($query_string, $query_params) = $query->get_query();

            //
            self::load_schema_if_need();
            $q_c = sql_query_fetch($query_string, $query_params);

            return (int) $q_c['count'];
        }

        /**
         * @return SDBCriteria
         */
        function getAbstractCriteria() {
            $criteria = new SDBCriteria();

            // @todo Дописать. Перебор значений полей

            return $criteria;
        }

        /**
         * Находим одну запись из БД
         *
         * @param SDBCriteria|null     $criteria null - искать всё
         * @param string|string[]|null $select
         * @param string               $table_alias
         * @param integer              $offset
         *
         * @return SDBRecord|null
         **/
        static function find_one($criteria = null, $select = null, $table_alias = 't', $offset = 0) {
            $record = self::find_all($criteria, 1, $select, $table_alias, $offset);

            return (count($record) > 0) ? $record[0] : null;
        }

        /**
         * Список атрибутов, которые есть в базе данных
         *
         * @return string[]
         */
        static function getAttributes_exists_in_table() {
            $attributes = [];
            foreach (self::get_attributes_list() as $key => &$attribute) {
                if ($attribute['exists_in_db']) {
                    $attributes[] = $key;
                }
            }

            return array_unique(array_merge($attributes, static::getAttributesKeys('db')));
        }

        /**
         * Загружаем схему из БД, если необходимо
         * @throws SModelException
         */
        protected static function load_schema_if_need() {
            $table_name = static::getTable_name();
            if (isset(self::$table_schema_field_lists[$table_name]) or !static::$need_load_table_schema) {
                return;
            }

            // Загружаем схему и дописываем в список атрибутов
            $q = sql_query("SHOW FULL COLUMNS FROM `{$table_name}`;");
            if (is_null($q)) {
                throw new SModelException('Can not execute sql query');
            }
            self::$table_schema_field_lists[$table_name] = [];
            while ($q_c = sql_fetch($q)) {
                list($type, $additional_type) =
                    self::get_type_and_additional_from_mysql_series($q_c['Type'], $q_c['Null'] === 'YES');

                $field = [
                    'type' => $type,
                    'additional_type' => $additional_type,
                    'can_be_null' => ($q_c['Null'] === 'YES'),
                    'exists_in_db' => true,
                    'comment' => $q_c['Comment'],
                    'origin' => 'db',
                ];
                self::$table_schema_field_lists[$table_name][$q_c['Field']] = $field;
            }
        }

        /**
         * @param string  $raw_type
         * @param boolean $can_be_null
         *
         * @return array
         */
        static function get_type_and_additional_from_mysql_series($raw_type, $can_be_null) {
            $type = 'mixed';
            $additional_type = null;
            if (preg_match('_^(int|bigint|tinyint|smallint)_', $raw_type)) {
                $type = 'integer';
            } elseif (preg_match('_^(longtext|text|longblob|blob|varchar)_', $raw_type)) {
                $type = 'string';
            } elseif (preg_match('_^(float|double|real|decimal)_', $raw_type)) {
                $type = 'double';
            } elseif (preg_match('_^(date)$_', $raw_type)) {
                $type = 'string';
                $additional_type = 'date';
            } elseif (preg_match('_^(datetime|timestamp)$_', $raw_type)) {
                $type = 'string';
                $additional_type = 'datetime';
            }

            if (($type !== 'mixed') and $can_be_null) {
                $type .= '|null';
            }

            return [$type, $additional_type];
        }

        /**
         * Функция-заглушка, которая вызывается перед сохранением
         *
         * @return boolean
         */
        protected function before_save() {
            return true;
        }

        /**
         * Функция-заглушка, которая вызывается после сохранения (н-р, тянущиеся behaviour за этим сохранением)
         *
         * Её возврат ни на что не влияет
         */
        protected function after_save() {
            return true;
        }

        /**
         * @param string[] Список атрибутов, которые были переданы в метод save
         */
        protected $_put_save_attribute_list = null;

        /**
         * @param string[]|integer|null $attribute_list
         */
        protected $_put_save_attribute_mode = null;

        /**
         * @param boolean
         */
        protected $_put_save_need_validate = true;

        /**
         * Атрибут находится среди тех, что надо проверять
         *
         * @param string $attribute_name
         *
         * @return boolean
         */
        function is_attribute_in_save_list($attribute_name) {
            return in_array($attribute_name, $this->_put_save_attribute_list);
        }

        /**
         * Сохранение одиночной модели
         *
         * @param string[]|integer|null $attribute_mode
         * @param boolean               $need_validate
         *
         * @return boolean
         */
        function save($attribute_mode = null, $need_validate = true) {
            $this->_put_save_attribute_list = $this->get_need_save_list_from_raw_attribute_mode($attribute_mode);
            $this->_put_save_attribute_mode = $attribute_mode;
            $this->_put_save_need_validate = $need_validate;
            if (!$need_validate) {
                // Функции валидации не вызываются
            } elseif (!$this->before_validate()) {
                return false;
            } elseif (!$this->validate()) {
                return false;
            } elseif (!$this->after_validate()) {
                return false;
            }
            if (!$this->before_save()) {
                return false;
            } elseif ($this->_is_new_record ? !$this->real_insert() : !$this->real_save($attribute_mode)) {
                return false;
            }
            $this->after_save();
            $this->_is_new_record = false;
            $this->clear_diff_list('saved');

            return true;
        }

        /**
         * @return boolean
         */
        function is_new_record() {
            return $this->_is_new_record;
        }

        /**
         * Список полей, которые надо сохранять на текущем запросе
         *
         * @param string[]|integer|null $attribute_mode
         *
         * @return string[]
         */
        function get_need_save_list_from_raw_attribute_mode($attribute_mode) {
            if ($attribute_mode === self::SAVE_ONLY_CHANGED) {
                return array_intersect($this->get_diff_list('saved'), static::getAttributes_exists_in_table());
            } elseif ($attribute_mode === self::SAVE_ONLY_CHANGED_AFTER_LOAD) {
                return array_intersect($this->get_diff_list('loaded'), static::getAttributes_exists_in_table());
            } elseif (is_array($attribute_mode)) {
                return $attribute_mode;
            } else {
                return static::getAttributes_exists_in_table();
            }
        }

        /**
         * Сохраняем существующую запись
         *
         * @param string[]|integer|null $attribute_mode
         *
         * @return boolean
         * @throws SModelException
         */
        private function real_save($attribute_mode) {
            $primaries = static::getPrimary_key();
            if ($primaries === null) {
                throw new SModelException(sprintf('Table "%s" does not have primary key', static::getTable_name()));
            } elseif (is_string($primaries)) {
                $primaries = [$primaries];
            }
            $need_save_list = $this->get_need_save_list_from_raw_attribute_mode($attribute_mode);

            $fields_sql = [];
            $params = [];
            foreach ($need_save_list as &$key) {
                if (!$this->has_attribute($key) or in_array($key, $primaries)) {
                    // Нет значения или является примарным ключом
                    continue;
                }

                // Нужно сохранять
                $current_attribute_value = $this->get_attribute($key, false);
                if (is_object($current_attribute_value) and (get_class($current_attribute_value) == 'SDBExpression')) {
                    /**
                     * @var SDBExpression $current_attribute_value
                     */
                    $fields_sql[] .= '`'.$key.'`='.$current_attribute_value->get_sql();
                } else {
                    $fields_sql[] .= '`'.$key.'`=?';
                    if (($current_attribute_value === null) and !preg_match('_\\|null$_', $this->get_attribute_type($key))) {
                        $current_attribute_value = '';
                    }
                    $params[] = $current_attribute_value;
                }
            }
            if (count($fields_sql) == 0) {
                // Ни одного поля, которое нужно изменить
                return true;
            }
            $sql = 'update `'.static::getTable_name().'` set '.implode(',', $fields_sql).' where (true)';
            foreach ($primaries as &$key) {
                $sql .= "and(`{$key}`=?)";
                $params[] = $this->get_attribute($key);
            }
            sql_exec($sql.';', $params);

            return true;
        }

        /**
         * Создаём новую запись
         *
         * @return boolean
         * @throws SModelException
         */
        function real_insert() {
            $primaries = static::getPrimary_key();
            if ($primaries === null) {
            } elseif (is_string($primaries)) {
                $primaries = [$primaries];
            }

            $sql_keys = [];
            $sql_values = [];
            $params = [];
            $primaries_set_count = 0;
            foreach (static::getAttributes_exists_in_table() as $key) {
                if (!$this->has_attribute($key)) {
                    // Нет значения или является примарным ключом
                    continue;
                }

                // Нужно сохранять
                $current_attribute_value = $this->get_attribute($key, false);
                if (in_array($key, $primaries) and ($current_attribute_value !== null)) {
                    $primaries_set_count++;
                }
                $sql_keys[] = "`{$key}`";
                if (is_object($current_attribute_value) and (get_class($current_attribute_value) == 'SDBExpression')) {
                    /**
                     * @var SDBExpression $current_attribute_value
                     */
                    $sql_values[] = $current_attribute_value->get_sql();
                } else {
                    $sql_values[] = '?';
                    if (($current_attribute_value === null) and !preg_match('_\\|null$_', $this->get_attribute_type($key))) {
                        $current_attribute_value = '';
                    }
                    $params[] = $current_attribute_value;
                }
            }
            if (count($sql_keys) == 0) {
                // @todo Вставить примарный ключ
                // Ни одного поля, которое нужно изменить
                return true;
            }
            $sql = 'INSERT INTO `'.static::getTable_name().'` ('.implode(',', $sql_keys).')
            VALUES ('.implode(',', $sql_values).');';
            sql_exec($sql, $params);
            unset($sql_keys, $sql_values, $params, $current_attribute_value);
            if ($primaries_set_count == count($primaries)) {
                // Примарник уже задан
            } elseif (count($primaries) == 1) {
                $this->{static::getPrimary_key()} = sql_insert_id();
            } else {
                // @todo Получение нового примарного ключа
            }

            return true;
        }

        /**
         * @param string   $attribute_name
         * @param string[] $can_be_null_list
         *
         * @return boolean
         */
        protected function validate_attribute_can_be_skip($attribute_name, $can_be_null_list) {
            $value = $this->{$attribute_name};
            if (($value === null) and in_array($attribute_name, $can_be_null_list)) {
                return true;
            }
            if (is_object($value) and (get_class($value) == 'SDBExpression')) {
                return true;
            }

            return false;
        }

        /**
         * Экспортируем SDBCriteria, соответствующий записи
         *
         * @return SDBCriteria
         */
        function export_search_sdbcriteria() {
            $criteria = new SDBCriteria();
            $criteria->order = $this->_search_order;

            foreach (array_keys($this->attributes) as $key) {
                if (!$this->attribute_has_been_set($key)) {
                    continue;
                }
                $value = $this->get_attribute($key, false);
                if (is_array($value)) {
                    $criteria->add_in_condition($this->table_alias.'.'.$key, $value);
                    continue;
                } elseif (is_null($value)) {
                    $criteria->add_condition($this->table_alias.'.'.$key.' is null');
                }

                // @todo partial
                $criteria->compare_from_text($this->table_alias.'.'.$key, $value);
            }
            $temporary_name_prefix = null;
            foreach ($this->_date_input_list as $key => &$value) {
                $full_field_name = $this->table_alias.'.'.$key;
                if (!preg_match('_^(.+?)(\\|null)?$_', $this->get_attribute_type($key), $a)) {
                    continue;
                }
                $additional_type = $this->get_attribute_additional_type($key);
                /**
                 * @var DateTime|string|integer|null $min
                 * @var DateTime|string|integer|null $max
                 */
                $min = $value->min;
                $max = $value->max;
                if (($a[1] == 'string') and in_array($additional_type, ['datetime'])) {
                    $min = ($min === null) ? null : $min->format('Y-m-d H:i:s');
                    $max = ($max === null) ? null : $max->format('Y-m-d H:i:s');
                } elseif (($a[1] == 'string') and in_array($additional_type, ['date'])) {
                    $min = ($min === null) ? null : $min->format('Y-m-d');
                    $max = ($max === null) ? null : $max->format('Y-m-d');
                } elseif (($a[1] == 'integer') and in_array($additional_type, ['timestamp'])) {
                    $min = ($min === null) ? null : $min->getTimestamp();
                    $max = ($max === null) ? null : $max->getTimestamp();
                } else {
                    continue;
                }

                if ($temporary_name_prefix === null) {
                    $temporary_name_prefix = SDBCriteria::generate_hash(20).'_';
                }
                $temporary_name = $temporary_name_prefix.$key;
                if (($min !== null) and ($max !== null)) {
                    $criteria->add_between_condition($full_field_name, $min, $max);
                } elseif ($min !== null) {
                    $criteria->add_condition("{$full_field_name}>=:{$temporary_name}_min");
                    $criteria->params[$temporary_name.'_min'] = $min;
                } elseif ($max !== null) {
                    $criteria->add_condition("{$full_field_name}<=:{$temporary_name}_max");
                    $criteria->params[$temporary_name.'_max'] = $max;
                }
            }

            return $criteria;
        }

        /**
         * Экспортируем стрим моделей
         *
         * @param SDBCriteria $criteria
         * @param string      $table_alias
         *
         * @return SModelStream
         */
        static function export_model_stream(SDBCriteria $criteria, $table_alias = 't') {
            $get_models = function ($offset, $count, $order, $select) use ($criteria, $table_alias) {
                if ($order === null) {
                    $new_criteria = $criteria;
                } else {
                    $new_criteria = clone $criteria;
                    $new_criteria->order = $order;
                }
                $models = self::find_all($new_criteria, $count, $select, $table_alias, $offset);

                return $models;
            };
            $get_count = function () use ($criteria) {
                return self::find_count_from_criteria($criteria);
            };

            $stream = new SModelStream([
                'get_models' => $get_models,
                'get_count' => $get_count,
            ]);

            return $stream;
        }

        /**
         * Подгрузка реляционных объектов к текущему классу из нескольких parent-объектов в static-хранилище
         *
         * Метод должен использоваться через static::
         *
         * @param string      $relation_name
         * @param SDBRecord[] $objects
         * @param integer     $limit
         * @param integer     $offset
         *
         * @throws SModelException
         */
        static function load_batch_static_relation(
            $relation_name, $objects, $limit = self::RELATION_BATCH_LOAD_SIZE_DEFAULT, $offset = 0) {
            if (!isset(static::relations()[$relation_name])) {
                throw new SModelException(sprintf('Relation "%s" does not exist in class "%s"', $relation_name, static::class));
            }
            $relation = static::relations()[$relation_name];
            if (($relation[0] == self::HAS_MANY) or ($relation[0] == self::BELONGS_TO)) {
                self::load_batch_static_relation_one_to_many_or_flip(
                    $relation_name, $objects, $relation[0], $offset, $limit);
            } else {
                throw new SModelException(sprintf('Relation "%d" is not implemented', $relation[0]));
            }
        }

        /**
         * Подгрузка реляционных объектов к текущему классу из нескольких parent-объектов
         * в отношении HAS_MANY/BELONGS_TO в static-хранилище
         *
         * @param string      $relation_name
         * @param SDBRecord[] $objects
         * @param integer     $relation_type
         * @param integer     $limit
         * @param integer     $begin_offset
         *
         * @throws SModelException
         */
        private static function load_batch_static_relation_one_to_many_or_flip(
            $relation_name, $objects, $relation_type, $begin_offset, $limit) {
            if (count($objects) == 0) {
                return;
            }
            list($this_composite_key, $other_composite_key, $class_name) =
                self::get_composite_key_name_from_relation($relation_name, $relation_type);
            /**
             * @var SDBRecord $class_name
             */
            $criteria = self::get_relation_criteria_from_objects($this_composite_key, $other_composite_key, $objects);
            $objects_relation = [];
            $object_count = count($objects);
            for ($i = 0; $i < $object_count; $i++) {
                $objects_relation[] = [];
            }
            for ($offset = $begin_offset; $offset < $begin_offset + $limit; $offset += static::RELATION_BATCH_LOAD_SIZE) {
                /**
                 * @var SDBRecord[] $records
                 */
                $records = $class_name::find_all($criteria, static::RELATION_BATCH_LOAD_SIZE, null, 't', $offset);
                foreach ($records as $record) {
                    // @todo Имплементировать композитный примарный ключ
                    foreach ($objects as $object_id => &$object) {
                        if ($record->get_attribute($other_composite_key) == $object->get_attribute($this_composite_key)) {
                            $objects_relation[$object_id][] = $record;
                        }
                    }
                }
                if (count($records) < static::RELATION_BATCH_LOAD_SIZE) {
                    break;
                }
            }
            foreach ($objects as $object_id => &$object) {
                if (count($objects_relation[$object_id]) > 0) {
                    $object->set_relation($relation_name, $objects_relation[$object_id]);
                }
                $object->_relations_loaded = array_unique(array_merge($object->_relations_loaded, [$relation_name]));
            }
        }

        /**
         * @param string  $relation_name
         * @param integer $relation_type
         *
         * @return string[]
         *
         * @throws SModelException
         */
        static function get_composite_key_name_from_relation($relation_name, $relation_type) {
            $relation = static::relations()[$relation_name];
            if (count($relation) < 3) {
                throw new SModelException(sprintf('Relation "%s" is malformed', $relation_name));
            }
            if ($relation_type == self::HAS_MANY) {
                $this_composite_key = isset($relation[3]) ? $relation[3] : static::getPrimary_key();
                $other_composite_key = $relation[2];
            } elseif ($relation_type == self::BELONGS_TO) {
                $this_composite_key = $relation[2];
                /** @noinspection PhpUndefinedMethodInspection */
                $other_composite_key = isset($relation[3]) ? $relation[3] : $relation[1]::getPrimary_key();
            } else {
                throw new SModelException('Code flow exception');
            }

            return [$this_composite_key, $other_composite_key, $relation[1]];
        }

        /**
         * Кол-во объектов по реляции
         *
         * @param string      $relation_name
         * @param SDBRecord[] $objects
         *
         * @return integer
         * @throws SModelException
         */
        static function get_count_relation($relation_name, $objects) {
            if (count($objects) == 0) {
                return 0;
            } elseif (!isset(static::relations()[$relation_name])) {
                throw new SModelException(sprintf('Relation "%s" does not exist in class "%s"', $relation_name, static::class));
            }
            $relation = static::relations()[$relation_name];
            if (($relation[0] == self::HAS_MANY) or ($relation[0] == self::BELONGS_TO)) {
                list($this_composite_key, $other_composite_key, $class_name) =
                    self::get_composite_key_name_from_relation($relation_name, $relation[0]);
                /**
                 * @var SDBRecord $class_name
                 */
                $criteria = self::get_relation_criteria_from_objects($this_composite_key, $other_composite_key, $objects);

                return $class_name::find_count_from_criteria($criteria);
            } else {
                throw new SModelException(sprintf('Relation "%d" is not implemented', $relation[0]));
            }
        }

        /**
         * Составляем критерию по переданным parent-объектам и названиям полей
         *
         * @param string      $this_composite_key
         * @param string      $other_table_key
         * @param SDBRecord[] $objects
         *
         * @return SDBCriteria
         * @throws SModelException
         */
        private static function get_relation_criteria_from_objects($this_composite_key, $other_table_key, $objects) {
            if (is_array($this_composite_key)) {
                throw new SModelException('Composite id is not implemented');
            }
            if (is_array($other_table_key)) {
                throw new SModelException('Composite id is not implemented');
            }
            $sql_raw = [];
            foreach ($objects as &$object) {
                // @todo Когда тут будет is_array($this_composite_key), надо делать предикаты
                $primary = $object->get_attribute($this_composite_key);
                if ($primary != null) {
                    $sql_raw[] = [$primary];
                }
            }
            $used_sha = [];
            $sql = [];
            foreach ($sql_raw as &$raw) {
                $sha = is_array($raw) ? ((count($raw) > 1) ? hash('sha512', serialize($raw)) : $raw[0]) : $raw;
                if (in_array($sha, $used_sha)) {
                    continue;
                }
                $used_sha[] = $sha;
                if (is_array($raw)) {
                    $s = [];
                    foreach ($raw as &$value) {
                        $s[] = SDBCriteria::sql_like_sanation($value);
                    }
                    $sql[] = sprintf('(%s)', implode(',', $s));
                    unset($s);
                } else {
                    $sql[] = SDBCriteria::sql_like_sanation($raw);
                }
            }
            unset($used_sha, $sha, $sql_raw, $raw);
            $criteria = new SDBCriteria();
            if (count($sql) > 0) {
                $criteria->add_condition(sprintf('%s in (%s)',
                    is_array($other_table_key) ? '('.implode(',', $other_table_key).')' : $other_table_key, implode(',', $sql)));
            } else {
                $criteria->add_condition('false');
            }

            return $criteria;
        }

        /**
         * @param string $relation_name
         *
         * @throws SModelException
         */
        protected function lazy_relation_load($relation_name) {
            if (!isset(static::relations()[$relation_name]) or in_array($relation_name, $this->_relations_loaded)) {
                return;
            }
            $this->_relations_loaded[] = $relation_name;

            if ($this->is_relations_static($relation_name)) {
                static::load_batch_static_relation($relation_name, [$this]);
            } else {
                throw new SModelException('Not implemented');
            }
        }
    }

?>