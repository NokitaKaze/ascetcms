<?php

    class SmartQueue {
        const ProducerThreadCount = 5;
        const SortCount = 10;

        const StorageTemporary = 0;
        const StoragePersistent = 1;
        private $_folder;
        private $_prefix;
        private $_name;

        /**
         * @var SmartMutex|null
         */
        private $_producer_mutex = null;

        /**
         * @var SmartMutex|null
         */
        private $_index_mutex = null;

        /**
         * @var integer[][] Данные с индексом
         */
        private $_index_data = [];

        /**
         * @var boolean Эксклюзивный режим
         */
        private $_exclusive_mode = false;

        /**
         * SmartQueue constructor.
         *
         * @param object $settings
         *
         * @throws SmartQueueException
         */
        function __construct($settings) {
            if (!isset($settings->storage_type)) {
                $settings->storage_type = self::StorageTemporary;
            }
            if (!isset($settings->name)) {
                throw new SmartQueueException('Settings don\'t have field name', 11);
            }
            switch ($settings->storage_type) {
                case self::StorageTemporary:
                    $this->_folder = sys_get_temp_dir();
                    break;
                case self::StoragePersistent:
                    $this->_folder = SmartMutex::getDirectoryString();
                    break;
                default:
                    throw new SmartQueueException(
                        'Constructor settings is malformed. Storage type can not be equal '.
                        $settings->storage_type, 1
                    );
            }
            if (isset($settings->folder)) {
                $this->_folder = $settings->folder;
            }
            $this->_name = $settings->name;
            $this->_index_data = array_fill(0, self::SortCount, []);

            $this->standard_prefix_strategy($settings);
        }

        function __destruct() {
            if ($this->_producer_mutex !== null) {
                $this->_producer_mutex->release_lock();
            }
            if ($this->_index_mutex !== null) {
                $this->_index_mutex->release_lock();
            }
        }

        /**
         * @param object $settings
         */
        private function standard_prefix_strategy($settings) {
            if (isset($settings->prefix)) {
                $this->_prefix = $settings->prefix;
            } else {
                $this->_prefix = '';
            }
        }

        /**
         * Загоняем сообщение в очередь и явно сохраняем
         *
         * @param mixed       $data
         * @param string|null $name Название сообщения. Если null, то можно дублировать
         * @param integer     $sort
         *
         * @throws SmartQueueException
         */
        function produce_message($data, $name = null, $sort = 5) {
            $this->push(self::build_message($data, $name, $sort));
            $this->save();
        }

        /**
         * Формируем сообщение для очереди
         *
         * @param mixed       $data
         * @param string|null $name Название сообщения. Если null, то можно дублировать
         * @param integer     $sort
         *
         * @return object
         */
        static function build_message($data, $name = null, $sort = 5) {
            return (object) [
                'name' => is_null($name) ? null : (string) $name,
                'data' => $data,
                'time_created' => microtime(true),
                'sort' => min(max($sort, 0), self::SortCount - 1),
                'is_read' => false,
            ];
        }

        /**
         * @var object[]
         */
        private $_pushed_for_save = [];

        /**
         * Загоняем сообщение в очередь необходимых для сохранения сообщений
         *
         * @param object[]|object $stream
         *
         * @throws SmartQueueException
         */
        function push($stream) {
            $this->set_same_time_flag(1);
            if (is_array($stream)) {
                foreach ($stream as &$message) {
                    $this->_pushed_for_save[] = self::sanify_event_object($message);
                }
            } else {
                $this->_pushed_for_save[] = self::sanify_event_object($stream);
            }
        }

        private $_same_time_consumer_and_producer = 0;

        /**
         * @param integer $flag
         *
         * @throws SmartQueueException
         */
        private function set_same_time_flag($flag) {
            $this->_same_time_consumer_and_producer |= $flag;
            if ($this->_same_time_consumer_and_producer === 3) {
                throw new SmartQueueException('Consumer and producer at the same time', 18);
            }
        }

        /**
         * @return integer
         */
        function get_same_time_consumer_and_producer_flag() {
            return $this->_same_time_consumer_and_producer;
        }

        /**
         * @param object $object
         *
         * @return object
         * @throws SmartQueueException
         */
        static function sanify_event_object($object) {
            $ret = clone $object;
            /** @noinspection PhpParamsInspection */
            if (!array_key_exists('name', $ret)) {
                $ret->name = null;
            }
            /** @noinspection PhpParamsInspection */
            if (!array_key_exists('data', $ret)) {
                throw new SmartQueueException('Datum does not have field data', 12);
            }
            if (!isset($ret->sort)) {
                $ret->sort = 5;
            } else {
                $ret->sort = min(max($ret->sort, 0), self::SortCount - 1);
            }

            return $ret;
        }

        /**
         * Блокируем index mutex
         *
         * @param double|integer $time
         *
         * @return boolean
         */
        protected function index_mutex_lock($time = -1) {
            if (!$this->_exclusive_mode) {
                return $this->_index_mutex->get_lock($time);
            } else {
                return true;
            }
        }

        /**
         * Блокируем index mutex
         */
        protected function index_mutex_release_lock() {
            if (!$this->_exclusive_mode) {
                $this->_index_mutex->release_lock();
            }
        }

        /**
         * Устанавливаем или снимаем монопольный режим
         *
         * @param boolean $mode
         */
        function set_exclusive_mode($mode) {
            $this->init_producer_mutex();
            $this->_exclusive_mode = $mode;
            if ($mode) {
                $this->_index_mutex->get_lock();
            } else {
                $this->_index_mutex->release_lock();
            }
        }

        /**
         * @return object[]
         */
        private function get_used_keys_and_real_need_for_save() {
            $real_need_save = [];
            $used_keys = [];
            foreach ($this->_pushed_for_save as &$message) {
                if ($message->name === null) {
                    $real_need_save[] = $message;
                    continue;
                }
                if (in_array($message->name, $used_keys)) {
                    continue;
                }
                foreach ($this->_index_data as $sort_id => &$index_datum) {
                    if (array_key_exists($message->name, $index_datum)) {
                        continue 2;
                    }
                }
                $used_keys[] = $message->name;
                $real_need_save[] = $message;
            }

            return $real_need_save;
        }

        /**
         * Сохраняем все сообщения, подготовленные для сохранения, в файл сообщений
         *
         * @throws SmartQueueException
         */
        function save() {
            if (count($this->_pushed_for_save) == 0) {
                return;
            }
            $this->init_producer_mutex();

            $this->index_mutex_lock();
            $this->index_data_load();
            $real_need_save = $this->get_used_keys_and_real_need_for_save();
            if (count($real_need_save) == 0) {
                $this->index_mutex_release_lock();

                return;
            }

            $this->_producer_mutex->get_lock();
            $current_thread_id = static::get_current_producer_thread_id();
            $data_in = $this->get_data_for_thread($current_thread_id);
            foreach ($real_need_save as &$message) {
                if (!isset($message->sort)) {
                    $message->sort = 5;
                }
                if (!isset($message->time_created)) {
                    $message->time_created = microtime(true);
                }
                $data_in[] = (object) [
                    'name' => $message->name,
                    'data' => $message->data,
                    'time_created' => $message->time_created,
                    'sort' => $message->sort,
                    'is_read' => false,
                    // @codeCoverageIgnoreStart
                ];
                // @codeCoverageIgnoreEnd
                $key = self::get_real_key_for_message($message);
                $this->_index_data[$message->sort][$key] = $current_thread_id;
            }
            $this->write_full_data_to_file($current_thread_id, $data_in);
            unset($data_in);

            $this->_producer_mutex->release_lock();
            $this->index_data_save();
            $this->index_mutex_release_lock();
            $this->_pushed_for_save = [];
        }

        /**
         * Берём данные для конкретного внутренного треда в очереди сообщений
         *
         * @param integer $thread_id
         *
         * @return object[]
         */
        private function get_data_for_thread($thread_id) {
            $filename = $this->get_producer_filename_for_thread($thread_id);
            if (!file_exists($filename)) {
                return [];
            }
            $buf = file_get_contents($filename, LOCK_EX);

            return ($buf !== '') ? unserialize($buf) : [];
        }

        /**
         * Внутренний id треда
         *
         * @return integer
         */
        protected static function get_current_producer_thread_id() {
            return posix_getpid() % self::ProducerThreadCount;
        }

        /**
         * Берём название файла с данными для конкретного внутренного треда в очереди сообщений
         *
         * @param integer $thread_id
         *
         * @return string
         */
        private function get_producer_filename_for_thread($thread_id) {
            return $this->_folder.'/smartqueue_'.$this->_prefix.'_'.hash('sha512', $this->_name).'-'.$thread_id.'.que';
        }

        /**
         * Инициализируем мьютекс для текущего треда
         */
        private function init_producer_mutex() {
            if ($this->_producer_mutex === null) {
                $this->_producer_mutex = $this->get_mutex_for_thread(static::get_current_producer_thread_id());
                $this->_index_mutex = $this->get_index_mutex();
            }
        }

        /**
         * Мьютекс для конкретного треда
         *
         * @param integer $thread_id
         *
         * @return SmartMutex
         */
        private function get_mutex_for_thread($thread_id) {
            return new SmartMutex(
                'ascetqueue_'.$this->_prefix.'_'.hash('sha512', $this->_name).'-'.$thread_id,
                SmartMutex::SmartMutex_Server, $this->_folder
            );
        }

        /**
         * Создание мьютекса для файла с индексом на всю текущую очередь сообщений
         *
         * @return SmartMutex
         */
        private function get_index_mutex() {
            return new SmartMutex(
                'ascetqueue_'.$this->_prefix.'_'.hash('sha512', $this->_name).'-index',
                SmartMutex::SmartMutex_Server, $this->_folder
            );
        }

        /**
         * Сохраняем данные в файл внутреннего треда (без индекса)
         *
         * @param integer  $thread_id
         * @param object[] $data_in
         *
         * @throws SmartQueueException
         */
        private function write_full_data_to_file($thread_id, $data_in) {
            $filename = $this->get_producer_filename_for_thread($thread_id);
            if (!file_exists(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" does not exist', 7);
            } elseif (!is_dir(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" is not a folder', 14);
            } elseif (!is_writable(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" is not writable', 8);
            } elseif (file_exists($filename) and !is_writable($filename)) {
                throw new SmartQueueException('File "'.$filename.'" is not writable', 9);
            }
            $filename_tmp = $filename.'-'.mt_rand(10000, 99999).'.tmp';
            touch($filename_tmp);
            // @todo fileperms
            chmod($filename_tmp, 6 << 6);
            if (@file_put_contents($filename_tmp, serialize($data_in), LOCK_EX) === false) {
                // @codeCoverageIgnoreStart
                throw new SmartQueueException('Can not save queue stream', 2);
                // @codeCoverageIgnoreEnd
            }
            if (!rename($filename_tmp, $filename)) {
                // @codeCoverageIgnoreStart
                throw new SmartQueueException('Can not rename temporary database file', 15);
                // @codeCoverageIgnoreEnd
            }
        }

        /**
         * Удалить сообщение и сразу же записать это в БД
         *
         * @param object $message
         */
        function delete_message($message) {
            $this->delete_messages([$message]);
        }

        /**
         * @param $messages
         *
         * @return string[][]|integer[][]
         */
        protected function get_keys_for_delete_group_by_thread($messages) {
            $keys = [];
            foreach ($messages as &$message) {
                $keys[] = self::get_real_key_for_message($message);
            }
            unset($message);

            $threads_keys = [];
            for ($i = 0; $i < self::ProducerThreadCount; $i++) {
                $threads_keys[] = [];
            }
            foreach ($this->_index_data as $sort_id => &$index_datum) {
                if (count($index_datum) == 0) {
                    continue;
                }
                foreach ($index_datum as $key => &$thread_id) {
                    if (in_array($key, $keys)) {
                        $threads_keys[$thread_id][] = $key;
                        unset($index_datum[$key]);
                    }
                }
            }

            return $threads_keys;
        }

        /**
         * Удалить сообщения и сразу же записать это в БД
         *
         * @param object[] $messages
         *
         * @return integer
         */
        function delete_messages($messages) {
            $this->init_producer_mutex();
            $this->index_mutex_lock();
            $this->index_data_load();

            $threads_keys = $this->get_keys_for_delete_group_by_thread($messages);
            $count = 0;

            foreach ($threads_keys as $thread_id => &$thread_keys) {
                if (count($thread_keys) == 0) {
                    continue;
                }
                $mutex = $this->get_mutex_for_thread($thread_id);
                $mutex->get_lock();
                $data_in = $this->get_data_for_thread($thread_id);
                $u = false;
                foreach ($data_in as $inner_id => &$inner_message) {
                    $real_key = self::get_real_key_for_message($inner_message);
                    if (in_array($real_key, $thread_keys)) {
                        unset($data_in[$inner_id]);
                        $u = true;
                        $count++;
                    }
                }
                if ($u) {
                    // @hint Это always true condition. Иначе данные неконсистентны
                    if (count($data_in) > 0) {
                        $this->write_full_data_to_file($thread_id, $data_in);
                    } else {
                        unlink($this->get_producer_filename_for_thread($thread_id));
                    }
                }

                $mutex->release_lock();
                $this->index_data_save();
            }
            $this->index_mutex_release_lock();

            return $count;
        }

        /**
         * Обновляем сообщение и сразу же сохраняем всё
         *
         * Эта функция не рейзит ошибку, если сообщения не найдено
         *
         * @param object      $message
         * @param string|null $key форсированно задаём ключ сообщения
         *
         * @return boolean
         */
        function update_message($message, $key = null) {
            $this->init_producer_mutex();
            $this->index_mutex_lock();
            $this->index_data_load();

            if ($key === null) {
                $key = self::get_real_key_for_message($message);
            }
            $exists = false;
            foreach ($this->_index_data as $index_id => &$index_datum) {
                if (!array_key_exists($key, $index_datum)) {
                    continue;
                }
                $thread_id = $index_datum[$key];
                $mutex = $this->get_mutex_for_thread($thread_id);
                $mutex->get_lock();
                $data_in = $this->get_data_for_thread($thread_id);
                // Ищем то же сообщение и заменяем его
                foreach ($data_in as $inner_id => &$inner_message) {
                    if ($message->name !== null) {
                        if ($inner_message->name === $message->name) {
                            $data_in[$inner_id] = $message;
                            $exists = true;
                            break;
                        }
                    } elseif ($inner_message->time_created === $message->time_created) {
                        $data_in[$inner_id] = $message;
                        $exists = true;
                        break;
                    }
                }
                $this->write_full_data_to_file($thread_id, $data_in);
                $mutex->release_lock();
                $this->index_data_save();
                break;
            }
            $this->index_mutex_release_lock();

            return $exists;
        }

        /**
         * @var string[] Список индексов полученных сообщений
         */
        private $_consumed_keys = [];

        /**
         * @var Callable|null
         */
        private $_callback_closure = null;

        /**
         * @param double|integer $wait_time
         *
         * @return object|null
         */
        function consume_next_message($wait_time = -1) {
            $this->set_same_time_flag(2);
            $start = microtime(true);
            $this->init_producer_mutex();
            $this->index_mutex_lock();
            $this->index_data_load();
            while (true) {
                for ($sort_id = 0; $sort_id < self::SortCount; $sort_id++) {
                    foreach ($this->_index_data[$sort_id] as $key => $thread_id) {
                        if (in_array($key, $this->_consumed_keys)) {
                            continue;
                        }
                        $mutex = $this->get_mutex_for_thread($thread_id);
                        $mutex->get_lock();
                        $data_in = $this->get_data_for_thread($thread_id);
                        $this->_consumed_keys[] = $key;
                        foreach ($data_in as $message) {
                            $this_key = self::get_real_key_for_message($message);
                            if ($this_key == $key) {
                                $message->time_consumed = microtime(true);
                                $message->thread_consumed = $thread_id;
                                $message->queue = $this;
                                $mutex->release_lock();
                                $this->index_mutex_release_lock();

                                return $message;
                            }
                        }
                        // @hint Сюда может передаться код только в случае неконсистентности БД, когда в
                        // index'е ключ есть, а в data его нет
                        unset($mutex);
                    }
                }
                if (($wait_time !== -1) and ($start + $wait_time < microtime(true))) {
                    $this->index_mutex_release_lock();

                    return null;
                }
            }

            // @hint Это для IDE
            return null;
        }

        /**
         * @param Callable $closure
         */
        function set_callback_closure($closure) {
            $this->_callback_closure = $closure;
        }

        /**
         * @param double|integer $wait_time
         *
         * @throws SmartQueueException
         */
        function start_listen($wait_time = -1) {
            $start = microtime(true);
            $closure = $this->_callback_closure;
            if ($closure === null) {
                throw new SmartQueueException('Event listener has not been set', 3);
            }
            do {
                $message = $this->consume_next_message($wait_time);
                if ($message !== null) {
                    $closure($message);
                } else {
                    usleep(10000);
                }
            } while (($wait_time === -1) or ($start + $wait_time >= microtime(true)));
        }

        function clear_consumed_keys() {
            $this->_consumed_keys = [];
        }
        // @todo Удаление конкретных ключей из consumed. Причем туда передаётся кложур, в который передаётся название ключа
        // @todo Удаление конкретных ключей из индекса и очереди, с указанием max_create_timestamp, чтобы не хранить в очереди те же сообщения, пришедшие ещё раз

        /**
         * Индексы
         */

        /**
         * Название файла для содержания даты
         *
         * @return string
         */
        private function get_index_filename() {
            return $this->_folder.'/smartqueue_'.$this->_prefix.'_'.hash('sha512', $this->_name).'-index.que';
        }

        private function index_data_load() {
            // @todo Проверять время-размер
            $filename = $this->get_index_filename();
            if (!file_exists($filename)) {
                $this->_index_data = array_fill(0, self::SortCount, []);

                return;
            }
            $this->_index_data = unserialize(file_get_contents($filename));
        }

        /**
         * Сохраняем данные в индекс
         *
         * @throws SmartQueueException
         */
        private function index_data_save() {
            $filename = $this->get_index_filename();
            if (!file_exists(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" does not exist', 4);
            } elseif (!is_dir(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" is not a folder', 13);
            } elseif (!is_writable(dirname($filename))) {
                throw new SmartQueueException('Folder "'.dirname($filename).'" is not writable', 5);
            } elseif (file_exists($filename) and !is_writable($filename)) {
                throw new SmartQueueException('File "'.$filename.'" is not writable', 6);
            }
            $filename_tmp = $filename.'-'.mt_rand(0, 50000).'.tmp';
            touch($filename_tmp);
            chmod($filename_tmp, 6 << 6);
            if (@file_put_contents($filename_tmp, serialize($this->_index_data)) === false) {
                // @codeCoverageIgnoreStart
                throw new SmartQueueException('Can not save index file', 10);
                // @codeCoverageIgnoreEnd
            }
            if (!rename($filename_tmp, $filename)) {
                // @codeCoverageIgnoreStart
                throw new SmartQueueException('Can not rename temporary index file', 16);
                // @codeCoverageIgnoreEnd
            }
        }

        /**
         * @param object $message
         *
         * @return string
         */
        static function get_real_key_for_message($message) {
            return ($message->name !== null) ? $message->name : '_'.number_format($message->time_created, 20, '.', '');
        }
    }

    /**
     * Класс с Exception, который рейзится SmartQueue
     *
     * @codeCoverageIgnore
     */
    class SmartQueueException extends Exception {
    }

?>