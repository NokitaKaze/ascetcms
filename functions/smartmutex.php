<?php

    class SmartMutex {
        /**
         * @var string Название файла, на котором будет работать мьютекс
         */
        var $filename = '';
        var $mutex_type;
        /**
         * @var string|null Название мьютекса, должно состоять из валидных для имени файлов символов
         */
        var $mutex_name;
        /**
         * @var string|null Место, где расположен файл.
         * Не используется в логике класса
         */
        var $mutex_folder;
        /**
         * @var double Время, когда мьютекс был получен
         */
        var $lock_acquired_time;
        /**
         * @var boolean Текущее состояние мьютекса
         */
        var $lock_acquired = false;
        /**
         * @var resource|false Файл, открывающийся через fopen
         */
        private $_file_handler = false;
        /**
         * @var boolean Удалять файл при анлоке
         * В данный момент это unsafe поведение, из-за немандаторного доступа к файлам из PHP
         */
        var $delete_on_release = false;

        const SmartMutex_Domain = 0;// В пределах одного домена
        const SmartMutex_Directory = 1;// В пределах одной папки DOCUMENT_ROOT
        const SmartMutex_Server = 2;// All virtual hosts on server with one tmp-folder e.g. "/tmp"

        /**
         * @param string       $mutex_name
         * @param integer|null $mutex_type
         * @param string|null  $mutex_folder
         */
        function __construct($mutex_name, $mutex_type = null, $mutex_folder = null) {
            $this->mutex_type = ($mutex_type === null) ? self::SmartMutex_Domain : $mutex_type;
            $this->mutex_folder = ($mutex_folder === null) ? sys_get_temp_dir() : $mutex_folder;
            $this->mutex_name = $mutex_name;
            $this->lock_acquired_time = 0;

            $prefix = '';
            if ($this->mutex_type == self::SmartMutex_Domain) {
                $prefix = hash('sha512', self::getDomainString()).'_';
            } elseif ($this->mutex_type == self::SmartMutex_Directory) {
                $prefix = hash('sha512', strtolower(self::getDirectoryString())).'_';
            }
            $this->filename = $this->mutex_folder.DIRECTORY_SEPARATOR.'smartmutex_'.$prefix.$this->mutex_name.'.lock';
        }

        function __destruct() {
            $this->release_lock();
        }

        /**
         * @return string
         */
        static function getDomainString() {
            if (isset($_SERVER['HTTP_HOST']) and ($_SERVER['HTTP_HOST'] != '')) {
                return $_SERVER['HTTP_HOST'];
            } elseif (isset($_SERVER['HOSTNAME']) and ($_SERVER['HOSTNAME'] != '')) {
                return $_SERVER['HOSTNAME'];
            } elseif (isset($_SERVER['SCRIPT_NAME'])) {
                return $_SERVER['SCRIPT_NAME'];
            } else {
                return 'none';
            }
        }

        /**
         * @return string
         */
        static function getDirectoryString() {
            if (isset($_SERVER['DOCUMENT_ROOT']) and ($_SERVER['DOCUMENT_ROOT'] != '')) {
                return $_SERVER['DOCUMENT_ROOT'];
            } elseif (isset($_SERVER['PWD']) and ($_SERVER['PWD'] != '')) {
                return $_SERVER['PWD'];
            } elseif (isset($_SERVER['SCRIPT_NAME'])) {
                return $_SERVER['SCRIPT_NAME'];
            } else {
                return 'none';
            }
        }

        /**
         * @return boolean
         *
         * @throws SmartMutexException
         */
        function is_free() {
            if ($this->lock_acquired) {
                return false;
            }
            if (!file_exists(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" does not exist', 1);
            } elseif (!is_dir(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" does not exist', 4);
            } elseif (!is_writable(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" is not writable', 2);
            } elseif (file_exists($this->filename) and !is_writable($this->filename)) {
                throw new SmartMutexException('File "'.$this->filename.'" is not writable', 3);
            }

            $fo = fopen($this->filename, 'ab');
            $result = flock($fo, LOCK_EX | LOCK_NB);
            flock($fo, LOCK_UN);
            fclose($fo);

            return $result;
        }

        /**
         * @param double|integer $time
         *
         * @return bool
         * @throws SmartMutexException
         */
        function get_lock($time = -1) {
            $tmp_time = microtime(true);
            if ($this->lock_acquired) {
                return true;
            }

            if (!file_exists(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" does not exist', 1);
            } elseif (!is_dir(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" is not a folder', 4);
            } elseif (!is_writable(dirname($this->filename))) {
                throw new SmartMutexException('Folder "'.dirname($this->filename).'" is not writable', 2);
            } elseif (file_exists($this->filename) and !is_writable($this->filename)) {
                throw new SmartMutexException('File "'.$this->filename.'" is not writable', 3);
            }

            // Открываем файл
            $this->_file_handler = fopen($this->filename, file_exists($this->filename) ? 'ab' : 'wb');
            while (($this->_file_handler === false) and (
                    ($tmp_time + $time >= microtime(true)) or ($time == -1)
                )) {
                usleep(10000);
                $this->_file_handler = fopen($this->filename, 'ab');
            }
            if ($this->_file_handler === false) {
                return false;
            }

            // Блочим файл
            if ($time >= 0) {
                $result = flock($this->_file_handler, LOCK_EX | LOCK_NB);
                while ((!$result) and ($tmp_time + $time >= microtime(true))) {
                    // U MAD?
                    usleep(10000);
                    $result = flock($this->_file_handler, LOCK_EX | LOCK_NB);
                }
            } else {
                $result = flock($this->_file_handler, LOCK_EX);
            }

            if ($result) {
                $this->lock_acquired_time = microtime(true);
                // @todo Не работает под Windows
                fwrite($this->_file_handler, posix_getpid()."\n".microtime(true)."\n".posix_getuid()."\n\n");
                fflush($this->_file_handler);
                $this->lock_acquired = true;
            } else {
                fclose($this->_file_handler);
                $this->_file_handler = false;
            }

            return $result;
        }

        function release_lock() {
            if (!$this->lock_acquired) {
                return;
            }
            if (is_resource($this->_file_handler)) {// @hint Это на время, это должно быть Always true
                flock($this->_file_handler, LOCK_UN);
                fclose($this->_file_handler);
            }
            $this->_file_handler = false;
            $this->lock_acquired = false;
            $this->lock_acquired_time = 0;

            if ($this->delete_on_release and file_exists($this->filename)) {
                @unlink($this->filename);
            }
        }

    }

    /**
     * Class SmartMutexException
     *
     * @codeCoverageIgnore
     */
    class SmartMutexException extends Exception {

    }

?>