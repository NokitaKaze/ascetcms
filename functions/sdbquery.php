<?php

    /**
     * Class SDBQuery
     */
    class SDBQuery {
        /**
         * @var string|string[]|null $_select
         * @var string|string[]|null $_where
         * @var array                $_where_params
         * @var integer|null         $_limit
         * @var integer              $_offset
         * @var string|null          $_order
         * @var string|null          $_last_query_block
         */
        protected $_select, $_where, $_where_params, $_limit, $_offset, $_from, $_order, $_last_query_block;

        function __construct() {
            $this->_select = [];
            $this->_from = '';
            $this->_where = [];
            $this->_where_params = [];
            $this->_limit = null;
            $this->_order = '';
            $this->_offset = 0;
        }

        /**
         * @return mixed[]
         */
        function get_query() {
            // @todo В одну строку и suppress Error'а в Jet Brains Idea
            $full_sql = sprintf('SELECT'.' %s FROM %s %s %s %s %s;',
                self::get_select_block($this->_select), self::get_from_block($this->_from), self::get_where_block($this->_where),
                self::get_order_block($this->_order), self::get_limit_block($this->_limit, $this->_offset),
                self::get_last_block($this->_last_query_block));

            return [
                $full_sql,
                $this->_where_params
            ];
        }

        /**
         * @param string|string[]|null $select
         *
         * @return string|null
         */
        static function get_select_block($select) {
            if ($select === null) {
                return '*';
            } elseif (is_string($select)) {
                return $select;
            } elseif (is_array($select)) {
                return implode(',', $select);
            } else {
                return null;
            }
        }

        /**
         * @param string|string[]|null $from
         *
         * @return string|null
         */
        static function get_from_block($from) {
            if (is_string($from)) {
                return ' '.$from;
            } elseif (is_array($from)) {
                return implode(',', $from);
            } else {
                return null;
            }
        }

        /**
         * @param string|string[]|null $where
         *
         * @return string|null
         */
        static function get_where_block($where) {
            if (($where === '') or ($where === null)) {
                return '';
            } elseif (is_string($where)) {
                return ' where '.$where;
            } elseif (is_array($where)) {
                if (count($where) > 0) {
                    return ' where ('.implode(')and(', $where).')';
                } else {
                    return '';
                }
            } else {
                return null;
            }
        }

        /**
         * @param integer|null $limit
         * @param integer|null $offset
         *
         * @return string|null
         */
        static function get_limit_block($limit, $offset) {
            if ($limit != 0) {
                return ' limit '.$offset.', '.(int) $limit;
            } elseif ($offset > 0) {
                return ' limit '.$offset.', 1000000000';// @todo Убрать этот DAT костыль
            } else {
                return '';
            }
        }

        /**
         * @param string|string[]|null $order
         *
         * @return string|null
         */
        static function get_order_block($order) {
            if ($order != '') {
                return ' order by '.$order.' ';
            } else {
                return '';
            }
        }

        /**
         * @param string|string[]|null $last_block
         *
         * @return string|null
         */
        static function get_last_block($last_block) {
            if ($last_block != '') {
                return ' '.$last_block;
            } else {
                return '';
            }
        }

        /**
         * @param string|string[] $from
         */
        function set_from($from) {
            $this->_from = $from;
        }

        /**
         * @param string|string[]|null $select
         */
        function set_select($select) {
            $this->_select = $select;
        }

        /**
         * @param string|string[] $where_string
         * @param array           $where_params
         */
        function set_where($where_string, $where_params) {
            $this->_where = $where_string;
            $this->_where_params = $where_params;
        }

        /**
         * @param string $order_string
         */
        function set_order($order_string) {
            $this->_order = $order_string;
        }

        /**
         * @param integer|null $limit
         */
        function set_limit($limit) {
            $this->_limit = ($limit !== null) ? (int) $limit : null;
        }

        /**
         * @param integer $offset
         */
        function set_offset($offset) {
            $this->_offset = (int) $offset;
        }

        /**
         * @param string|null $block
         */
        function set_last_query_block($block) {
            $this->_last_query_block = $block;
        }
    }

?>