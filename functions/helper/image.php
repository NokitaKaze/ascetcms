<?php
    namespace AscetCMS\Helper;

    abstract class Image {
        static $images_ext = array('', 'gif', 'jpg', 'png');

        /**
         * @param string $extension
         *
         * @return integer
         */
        static function extension_to_type($extension) {
            $real_ext = preg_replace('|^(.*\\.)?([a-z]+)$|', '$2', strtolower($extension));
            if ($real_ext == 'gif') {
                return 1;
            }
            if (($real_ext == 'jpg') or ($real_ext == 'jpeg')) {
                return 2;
            }
            if ($real_ext == 'png') {
                return 3;
            }

            return 0;
        }

        /**
         * @param string  $source  path to source
         * @param string  $dest    path to destination
         * @param integer $width   new width  or "auto"
         * @param integer $height  new height or "auto"
         * @param string  $circum  '' for cut center of image
         * @param bool    $quality image type of new file = extension of new file
         *
         * @return integer
         */
        static function image_resize($source, $dest, $width, $height, $circum = '', $quality = true) {
            if (((int) $width < 1) and ($width !== 'auto')) {
                return 1;
            }
            if (((int) $height < 1) and ($height !== 'auto')) {
                return 1;
            }
            if (($width == 'auto') and ($height == 'auto')) {
                return 1;
            }
            if (($circum !== '') and !preg_match('|^[0-9a-f]{6,6}$|i', $circum)) {
                return 1;
            }

            list($org_width, $org_height, $tt) = getimagesize($source);
            if (($tt < 1) or ($tt > 3)) {
                return 2;
            }

            switch ($tt) {
                case 1:
                    $img_source = imagecreatefromgif($source);
                    break;
                case 3:
                    $img_source = imagecreatefrompng($source);
                    imagesavealpha($img_source, true);
                    break;
                case 2:
                    $img_source = imagecreatefromjpeg($source);
                    break;
                default:
                    return 2;
            }

            //Select params
            list($sour_x, $sour_y, $sour_w, $sour_h, $dest_x, $dest_y, $dest_w, $dest_h) =
                self::get_crop_sizes($width, $height, $org_width, $org_height, ($circum == '') ? 0 : 1);

            //$dest
            $dd = substr($dest, strlen(\AscetCMSEngine::$sad_root) + 1);
            $ar = explode('/', $dd);
            $path = \AscetCMSEngine::$sad_root;
            unset($ar[count($ar) - 1]);
            foreach ($ar as $dir) {
                $path .= '/'.$dir;
                if (!file_exists($path)) {
                    mkdir($path);
                }
            }
            unset($ar, $path, $dir, $dd);

            //Create new image
            $img_dist = imagecreatetruecolor($dest_w, $dest_h);
            if ($quality) {
                imagecopyresampled($img_dist, $img_source, $dest_x, $dest_y, $sour_x, $sour_y,
                    $dest_w, $dest_h, $sour_w, $sour_h);
            } else {
                imagecopyresized($img_dist, $img_source, $dest_x, $dest_y, $sour_x, $sour_y, $dest_w, $dest_h, $sour_w, $sour_h);
            }

            //Save
            switch (self::extension_to_type($dest)) {
                case 1:
                    imagegif($img_dist, $dest);
                    break;
                case 3:
                    imagepng($img_dist, $dest);
                    break;
                case 2:
                    imagejpeg($img_dist, $dest, 98);
                    break;
                default:
                    return 3;
            }

            return 0;
        }

        /**
         * @param integer $width
         * @param integer $height
         * @param integer $original_width
         * @param integer $original_height
         * @param integer $crop (0-по центру)
         *
         * @return integer|integer[]
         */
        static function get_crop_sizes($width, $height, $original_width, $original_height, $crop) {
            $sour_x = 0;
            $sour_y = 0;
            $sour_w = $original_width;
            $sour_h = $original_height;
            $dest_x = 0;
            $dest_y = 0;
            $dest_w = $width;
            $dest_h = $height;

            if ($width == 'auto') {
                $dest_h = $height;
                $dest_w = (int) ($original_width * ($dest_h / $original_height));
            }
            if ($height == 'auto') {
                $dest_w = $width;
                $dest_h = (int) ($original_height * ($dest_w / $original_width));
            }
            if ((int) $width > 0 and (int) $height > 0) {
                if ($crop == 0) {
                    //cut
                    if ($height * $sour_w / $width <= $sour_h) {
                        $sour_h = floor($height * $original_width / $width);
                    } else {
                        $sour_w = floor($width * $original_height / $height);
                    }
                    if ($original_width !== $sour_w) {
                        $sour_x = floor($original_width / 2 - $sour_w / 2);
                    }
                    if ($original_height !== $sour_h) {
                        $sour_y = floor($original_height / 2 - $sour_h / 2);
                    }
                } else {
                    //wings
                    return -1;

                }
            }

            return [$sour_x, $sour_y, $sour_w, $sour_h, $dest_x, $dest_y, $dest_w, $dest_h];
        }

        /**
         * @param integer $H
         * @param integer $S
         * @param integer $V
         *
         * @return integer[]
         */
        static function hsv2rgb($H, $S, $V) {
            $Hi = floor($H / 60) % 6;
            $f = $H / 60 - floor($H / 60);
            $S = $S / 100;
            $V = $V / 100;

            $p = $V * (1 - $S);
            $q = $V * (1 - $f * $S);
            $t = $V * (1 - (1 - $f) * $S);

            $a = array();
            switch ($Hi) {
                case 0:
                    $a[0] = $V;
                    $a[1] = $t;
                    $a[2] = $p;
                    break;
                case 1:
                    $a[0] = $q;
                    $a[1] = $V;
                    $a[2] = $p;
                    break;
                case 2:
                    $a[0] = $p;
                    $a[1] = $V;
                    $a[2] = $t;
                    break;
                case 3:
                    $a[0] = $p;
                    $a[1] = $q;
                    $a[2] = $V;
                    break;
                case 4:
                    $a[0] = $t;
                    $a[1] = $p;
                    $a[2] = $V;
                    break;
                case 5:
                    $a[0] = $V;
                    $a[1] = $p;
                    $a[2] = $q;
                    break;
            }
            $a[0] = round($a[0] * 255);
            $a[1] = round($a[1] * 255);
            $a[2] = round($a[2] * 255);

            return $a;
        }

        /**
         * @param integer $r
         * @param integer $g
         * @param integer $b
         *
         * @return integer[]
         */
        static function rgb2hsv($r, $g, $b) {
            $r = $r / 255;
            $g = $g / 255;
            $b = $b / 255;
            $max = max($r, $g, $b);
            $min = min($r, $g, $b);
            if ($max == $min) {
                $h = 0;
            } else {
                $ar = array(
                    ($g - $b) / ($max - $min) * 60 + 0,
                    ($g - $b) / ($max - $min) * 60 + 360,
                    ($b - $r) / ($max - $min) * 60 + 120,
                    ($r - $g) / ($max - $min) * 60 + 240
                );
                if (($max == $r) && ($g >= $b)) {
                    $h = $ar[0];
                } elseif (($max == $r) && ($g < $b)) {
                    $h = $ar[1];
                } elseif ($max == $g) {
                    $h = $ar[2];
                } else { // @hint implicit $max == $b
                    $h = $ar[3];
                }
            }

            if ($max == 0) {
                $s = 0;
            } else {
                $s = 1 - ($min / $max);
            }

            /** @noinspection PhpUndefinedVariableInspection */
            return array(round($h), round($s * 100), round($max * 100));
        }

        /**
         * @param string $text
         *
         * @return integer[]|null
         */
        static function parseHC($text) {
            if (!preg_match('|([0-9a-f]{2,2})([0-9a-f]{2,2})([0-9a-f]{2,2})|i', $text, $a)) {
                if (!preg_match('|([0-9a-f])([0-9a-f])([0-9a-f])|i', $text, $a)) {
                    return null;
                }
                $a = array(0, $a[1].$a[1], $a[2].$a[2], $a[3].$a[3]);
            }

            return array(hexdec($a[1]), hexdec($a[2]), hexdec($a[3]));
        }

        /**
         * @param integer $r
         * @param integer $g
         * @param integer $b
         *
         * @return string
         */
        static function createHC($r, $g, $b) {
            $c = ($r << 16) | ($g << 8) | $b;
            $s = dechex($c);
            while (strlen($s) < 6) {
                $s = '0'.$s;
            }

            return $s;
        }
    }

    ?>