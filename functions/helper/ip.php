<?php
    namespace AscetCMS\Helper;

    abstract class IP {
        static $robots_ips = [
            'google' => ['64.233.160.0/19', '66.249.64.0/19', '66.102.0.0/20', '67.221.224.0/20', '72.14.192.0/18',
                         '74.125.0.0/16', '209.85.128.0/17'],
            'yandex' => ['77.88.0.0/18', '77.88.24.0/22', '77.88.28.0/22', '77.88.42.0/23', '95.108.128.0/17', '95.108.150.0/23',
                         '95.108.244.0/22', '95.108.248.0/23', '93.158.128.0/18', '178.154.128.0/17', '199.36.240.0/22',
                         '213.180.206.0/23'],
            'yahoo' => ['67.195.0.0/16', '98.136.0.0/14'],
            'bing' => ['65.52.0.0/14', '77.75.104.0/21', '207.46.0.0/16'],
            'mailru' => ['217.69.128.0/20'],
            'baidu' => ['180.76.0.0/16'],
            'nigma' => ['195.239.178.0/24'],
            'rambler' => ['81.19.64.0/19'],
        ];

        /**
         * @param integer $ip
         *
         * @return integer
         */
        static function ip_s2i($ip) {
            $r = ip2long($ip);
            if ($r < 0) {
                $r += 0x100000000;
            }

            return $r;
        }

        /**
         * @param string $mask
         *
         * @return integer[]
         */
        static function ip_mask($mask) {
            preg_match('|^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})(/(.*?))?$|', $mask, $a);
            if (preg_match('_^([0-9]+)$_', $a[3], $b)) {
                if ($b[1] == 0) {
                    return array('min' => 0, 'max' => 0xFFFFFFFF);
                }
                $i = 32 - $b[1];
                if ($i == 0) {
                    $mask = 0xFFFFFFFF;
                } else {
                    $mask = 0xFFFFFFFF - (1 << $i) + 1;
                }
            }
            if (preg_match('_([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})_', $a[3], $b)) {
                $mask = self::ip_s2i($a[3]);
            }
            if ($mask == 0) {
                return array('min' => 0, 'max' => 0xFFFFFFFF);
            }
            if ($mask == 0xFFFFFFFF) {
                return array('min' => self::ip_s2i($a[1]), 'max' => self::ip_s2i($a[1]));
            }
            //$ip = ip_s2i($a[1]);
            $r = array('min' => ip2long($a[1]) & $mask);
            if ($r['min'] < 0) {
                $r['min'] += 0x100000000;
            }
            $r['max'] = $r['min'] | (0xFFFFFFFF ^ $mask);
            if ($r['max'] < 0) {
                $r['max'] += 0x100000000;
            }

            return $r;
        }

        /**
         * @param integer $ip_num
         *
         * @return array
         */
        static function ip_is_robot($ip_num) {
            $rr = array();
            foreach (self::$robots_ips as $name => $robot) {
                foreach ($robot as $ip) {
                    $r = self::ip_mask($ip);
                    if ($ip_num >= $r['min'] and $ip_num <= $r['max']) {
                        array_push($rr, $name);
                        break;
                    }
                }
            }

            return $rr;
        }

    }

    ?>