<?php
    namespace AscetCMS\Helper;

    abstract class XML {
        /**
         * @param \DomNode $node
         * @param string   $path
         *
         * @return \DOMNode|string|null
         */
        static function get_node($node, $path) {
            if ('' == $path) {
                return $node;
            }
            if ('#' == $path) {
                return $node->nodeValue;
            }

            preg_match('|^([a-z0-9.:_-]*)(\|[0-9]+)?(/?)(.*)|i', $path, $a);

            if ($a[1] == '') {
                return null;
            }
            $k = (int) str_replace('|', '', $a[2]);
            if ($k == 0) {
                $k = 1;
            }
            if (isset($node->childNodes)) {
                for ($i = 0; $i < $node->childNodes->length; $i++) {
                    $t = $node->childNodes->item($i);//FreeBSD patch
                    if (preg_match('|^'.sad_safe_reg($a[1]).'$|i', $t->nodeName)) {
                        $k--;
                    }
                    if ($k == 0) {
                        return self::get_node($node->childNodes->item($i), $a[4]);
                    }
                }
            }

            return null;
        }

        /**
         * @param \DomNode $node
         * @param string   $pattern
         *
         * @return \DOMNode[]|null
         */
        static function get_all_nodes($node, $pattern) {
            if ($node == null) {
                return null;
            }
            $a = array();
            if (isset($node->childNodes)) {
                for ($i = 0; $i < $node->childNodes->length; $i++) {
                    $t = $node->childNodes->item($i);
                    if (preg_match('|^'.sad_safe_reg($pattern).'$|i', $t->nodeName)) {
                        $a[] = $node->childNodes->item($i);
                    }
                }
            }

            return $a;
        }

        /**
         * @param \DomNode $node
         * @param string   $name
         *
         * @return string|null
         */
        static function get_attribute($node, $name) {
            if ($node == null) {
                return null;
            }
            if (!($node->hasAttributes())) {
                return '';
            }
            foreach ($node->attributes as $attr) {
                if (preg_match('|^'.sad_safe_reg($name).'$|i', $attr->nodeName)) {
                    return $attr->nodeValue;
                }
            }

            return '';
        }

        /**
         * @param string $text
         *
         * @return \DOMElement
         */
        static function get_dom_from_raw_text($text) {
            $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
            $xmlDoc->loadXML($text);
            $x = $xmlDoc->documentElement;

            return $x;
        }

        /**
         * @param string  $text
         * @param boolean $put_in_cdata
         *
         * @return mixed|string
         */
        static function sanify($text, $put_in_cdata = true) {
            $b = str_replace(']]>', ']]]]><![CDATA[>', $text);
            if ($put_in_cdata) {
                $b = '<![CDATA['.$b.']]>';
            }

            return $b;
        }

        /**
         * @param string  $text
         * @param boolean $put_in_comment
         *
         * @return mixed|string
         */
        static function sanify_comment($text, $put_in_comment = true) {
            while (preg_match('_\\-\\-_', $text)) {
                $text = preg_replace('_\\-\\-_', '- -', $text);
            }
            if ($put_in_comment) {
                $text = '<!-- '.$text.' -->';
            }

            return $text;
        }
    }

    ?>