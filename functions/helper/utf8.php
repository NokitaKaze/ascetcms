<?php
    namespace AscetCMS\Helper;

    abstract class UTF8 {
        /* UTF-8   <--->      A
            /|\               r
             |                r
             |                a
           HTML    <----      y
         */
        /**
         * @param string $utf
         *
         * @return integer[]
         */
        static function UTFToArray($utf) {
            $a = array();
            for ($i = 0; $i < strlen($utf); $i++) {
                if (ord($utf[$i]) < 0x80) {
                    $a[count($a)] = ord($utf[$i]);
                    continue;
                }
                if (ord($utf[$i]) < 0xE0) {
                    if ((ord($utf[$i + 1]) & 0xC0) !== 0x80) {
                        continue;
                    }
                    $a[count($a)] = ((ord($utf[$i]) & 31) << 6) | (ord($utf[$i + 1]) & 63);
                    $i++;
                    continue;
                }
                if (ord($utf[$i]) < 0xF0) {
                    if ((ord($utf[$i + 1]) & 0xC0) !== 0x80 or (ord($utf[$i + 2]) & 0xC0) !== 0x80) {
                        continue;
                    }
                    $a[count($a)] = ((ord($utf[$i]) & 15) << 12) | ((ord($utf[$i + 1]) & 63) << 6) | (ord($utf[$i + 2]) & 63);
                    $i += 2;
                    continue;
                }
                if (ord($utf[$i]) < 0xF8) {
                    if ((ord($utf[$i + 1]) & 0xC0) !== 0x80 or (ord($utf[$i + 2]) & 0xC0) !== 0x80 or
                        (ord($utf[$i + 3]) & 0xC0) !== 0x80
                    ) {
                        continue;
                    }
                    $a[count($a)] =
                        ((ord($utf[$i]) & 7) << 18) | ((ord($utf[$i + 1]) & 63) << 12) | ((ord($utf[$i + 2]) & 63) << 6) |
                        (ord($utf[$i + 3]) & 63);
                    $i += 3;
                    continue;
                }
            }

            return $a;
        }

        /**
         * @param integer[] $a
         *
         * @return string
         */
        static function ArrayToHTML($a) {
            $s = '';
            foreach ($a as &$code) {
                if ($code < 0x80) {
                    $s .= chr($code);
                } else {
                    $s .= '&#'.$code.';';
                }
            }

            return $s;
        }

        /**
         * @param integer $code
         *
         * @return string
         */
        static function chr($code) {
            if ($code < 0x80) {
                return chr($code);
            } elseif ($code < 0x800) {
                return chr(($code >> 6) | 192).chr(($code & 63) | 128);
            } elseif ($code < 0x10000) {
                return chr(($code >> 12) | 224).chr((($code >> 6) & 63) | 128).chr(($code & 63) | 128);
            } elseif ($code < 0x200000) {
                return chr(($code >> 18) | 240).chr((($code >> 12) & 63) | 128).chr((($code >> 6) & 63) | 128).
                       chr(($code & 63) | 128);
            } else {
                return chr(0);
            }
        }

        /**
         * @param integer[] $a
         *
         * @return string
         */
        static function ArrayToUTF($a) {
            $s = '';
            foreach ($a as &$code) {
                $s .= self::chr($code);
            }

            return $s;
        }

        /**
         * @param string $html
         *
         * @return string
         */
        static function HTMLToUTF($html) {
            return preg_replace_callback('|&#([0-9]+);|', function ($a) {
                return self::chr($a[1]);
            }, $html);
        }
    }

    ?>