<?php
    namespace AscetCMS\Helper;

    abstract class Sizes {
        /**
         * @param string  $string
         * @param integer $size
         *
         * @return double|integer
         */
        static function parse_size($string, $size = 1024) {
            if (!preg_match('_([0-9 ]+)([.,][0-9]+)?([ \\t]*)(k|m|g|t)?_i', $string, $a)) {
                return 0;
            }
            if (!isset($a[2])) {
                $a[2] = '0';
            }
            $a[1] = preg_replace('_ _', '', $a[1]).'.'.substr($a[2], 1);
            $num = 0;
            $s1 = strtolower($a[4]);
            if ($s1 == 'k') {
                $num = 1;
            }
            if ($s1 == 'm') {
                $num = 2;
            }
            if ($s1 == 'g') {
                $num = 3;
            }
            if ($s1 == 't') {
                $num = 4;
            }
            $r = 1;
            for ($i = 0; $i < $num; $i++) {
                $r *= $size;
            }

            return round($a[1] * $r);
        }

        /**
         * $mode = 0*.2
         * 0 = Factor. 1 for kbytes, 2 for mbytes,.. null for auto
         * * = rounding algorithm
         * 2 = number of digits after a decimal point
         *
         * @param integer $size
         * @param string  $mode
         *
         * @return string|null
         */
        static function draw_size($size, $mode = '') {
            if (!preg_match('_([0-4])?([+*-])?(\\.([0-4]))?_i', $mode, $a)) {
                return null;
            }
            if (strlen($a[2]) < 1) {
                $a[2] = '+';
            }
            if (strlen($a[4]) < 1) {
                $a[4] = 3;
            } else {
                $a[4] = (int) $a[4];
            }

            $num = 0;
            while (($num < $a[1] and strlen($a[1]) > 0) or
                   ($size >= 1024 and strlen($a[1]) == 0)) {
                $num++;
                $size = $size / 1024;
            }
            $r = 1;
            for ($i = 0; $i < $a[4]; $i++) {
                $r *= 10;
            }
            switch ($a[2]) {
                case '+':
                    $size = ceil($size * $r);
                    break;
                case '-':
                    $size = floor($size * $r);
                    break;
                case '*':
                    $size = round($size * $r);
                    break;
            }
            switch ($num) {
                case 0:
                    $s = '';
                    break;
                case 1:
                    $s = ' KB';
                    break;
                case 2:
                    $s = ' MB';
                    break;
                case 3:
                    $s = ' GB';
                    break;
                case 4:
                    $s = ' TB';
                    break;
                default:
                    return null;
            }

            return ($size / $r).$s;
        }
    }

    ?>