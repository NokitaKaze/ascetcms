<?php
    // Front Controller
    $sad_first_stamp = microtime(true);
    $sad_root = preg_replace('|/[^/]+$|', '', $_SERVER['SCRIPT_FILENAME']);// document root of cms e.g. /var/www/site
    /** @noinspection PhpIncludeInspection */
    require_once($sad_root.'/functions/ascetcmsengine.php');
    /** @noinspection PhpIncludeInspection */
    require_once($sad_root.'/functions/ascetpdo.php');
    /** @noinspection PhpIncludeInspection */
    require_once($sad_root.'/functions/keyvaluestorage.php');
    /** @noinspection PhpIncludeInspection */
    require_once($sad_root.'/config.php');

    $sad_options->modules_list = array_unique(array_merge(array(
        'html.php',
        'ruslang.php',
    ), $sad_options->modules_list));

    AscetCMSEngine::init($sad_options, $sad_first_stamp);
    unset($sad_first_stamp, $sad_options);
    AscetCMSEngine::run();
?>