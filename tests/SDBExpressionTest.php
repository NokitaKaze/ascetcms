<?php
    require_once dirname(__FILE__).'/../functions/sdbexpression.php';

    class SDBExpressionTest extends PHPUnit_Framework_TestCase {
        /**
         * @covers SDBExpression::get_sql
         * @covers SDBExpression::__toString
         * @covers SDBExpression::__construct
         */
        function testGet_sql() {
            $reflection = new ReflectionProperty('SDBExpression', '_sql');
            $reflection->setAccessible(true);
            foreach (['true', 'false', 'now()>pasu', '"nyan"', "'pasu'"] as $sql) {
                $cr = new SDBExpression($sql);
                $this->assertEquals($sql, $reflection->getValue($cr));
                $this->assertEquals($sql, $cr->get_sql());
                $this->assertEquals($sql, (string) $cr);
            }
        }
    }

?>