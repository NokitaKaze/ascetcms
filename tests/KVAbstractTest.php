<?php
    require_once dirname(__FILE__).'/../functions/smartmutex.php';
    require_once dirname(__FILE__).'/../functions/keyvaluestorage.php';

    abstract class KVAbstractTest extends PHPUnit_Framework_TestCase {
        private $_class_name;
        private static $_overloads_exist = [];
        private $_class_filename;
        private $_overloads_exist_in_this_instance = false;

        function setUp() {
            parent::setUp();
            $this->require_overload();
        }

        private function require_overload() {
            $this->_class_name = substr(static::class, 0, -4);
            if (in_array($this->_class_name, self::$_overloads_exist)) {
                return;
            }
            $this->_class_filename = tempnam(sys_get_temp_dir(), 'ascetcms_test_');
            chmod($this->_class_filename, 6 << 6);
            $res = file_put_contents($this->_class_filename, sprintf('<'.'?php
    class %s extends %s implements KVTestOverload {
        var $_lock_ack = [];

        function release_lock_ack() {
            $this->_lock_ack = [];
        }

        /**
         * @return boolean
         */
        function get_lock_ack($key) {
            return in_array($key, $this->_lock_ack);
        }

        function get_lock($key) {
            $this->_lock_ack[$key] = true;
            parent::get_lock($key);
        }
    }
?>', $this->_class_name.'TestOverload', $this->_class_name));
            if ($res === false) {
                throw new Exception('Can not create test overload class file');
            }
            /** @noinspection PhpIncludeInspection */
            require_once($this->_class_filename);
            self::$_overloads_exist[] = $this->_class_name;
            $this->_overloads_exist_in_this_instance = true;
        }

        function tearDown() {
            if ($this->_overloads_exist_in_this_instance) {
                unlink($this->_class_filename);
            }
            parent::tearDown();
        }

        static function generate_hash($key_length = 20) {
            $hashes = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            $hash = '';
            for ($i = ord('a'); $i <= ord('z'); $i++) {
                array_push($hashes, chr($i), strtoupper(chr($i)));
            }
            for ($i = 0; $i < $key_length; $i++) {
                $hash .= $hashes[mt_rand(0, count($hashes) - 1)];
            }

            return $hash;
        }

        /**
         * @param array $params
         *
         * @return object
         */
        protected function get_full_params($params) {
            return (object) $params;
        }

        /**
         * @var array $params
         *
         * @return iAscetKeyValueStorage
         */
        protected function get_kv_storage($params) {
            $class_name = $this->_class_name;

            return new $class_name($this->get_full_params($params));
        }

        /**
         * @var iAscetKeyValueStorage|null
         */
        protected $_last_kv = null;

        /**
         * Общий тест, проверяющий то, что значения не теряются
         *
         * @param array $params
         *
         * @throws Exception
         */
        function set_value_sub($params) {
            $server_original = $_SERVER;
            $_SERVER['HTTP_HOST'] = 'example.com';
            $_SERVER['DOCUMENT_ROOT'] = '/dev/shm';
            try {
                $this->set_value_sub_low_level($params);
            } catch (Exception $e) {
                $_SERVER = $server_original;
                throw $e;
            }
            unset($_SERVER['HTTP_HOST'], $_SERVER['DOCUMENT_ROOT']);
            try {
                $this->set_value_sub_low_level($params);
            } catch (Exception $e) {
                $_SERVER = $server_original;
                throw $e;
            }
            $_SERVER = $server_original;
        }

        protected function set_value_sub_low_level($params) {
            /**
             * @var iAscetKeyValueStorage $kv
             * @var iAscetKeyValueStorage $kv1
             */
            $kv = $this->get_kv_storage($params);
            $this->assertEquals($this->_class_name, get_class($kv));
            $this->_last_kv = $kv;
            $kv1 = $this->get_kv_storage($params);
            $this->assertEquals($this->_class_name, get_class($kv1));
            $kv->delete_value('key');
            $this->assertNull($kv->get_value('key', null));
            $this->assertNull($kv->get_change_time('key'));
            $this->assertNull($kv1->get_value('key', null));
            $this->assertNull($kv1->get_change_time('key'));
            $t = $kv->get_value('key', '');
            $this->assertNotNull($t);
            $this->assertEquals('', $t);
            $t = $kv->get_value('key', 'nyan');
            $this->assertNotNull($t);
            $this->assertEquals('nyan', $t);

            //
            $kv->set_value('key', 'value', 3600);
            $t = $kv->get_value('key', null);
            $this->assertNotNull($t);
            $this->assertNotNull($kv->get_change_time('key'));
            $this->assertEquals('value', $t);
            //
            if (!self::time_expires_accurate()) {
                $kv->set_value('key', 'value', 0);
                $t = $kv->get_value('key', null);
                $this->assertNull($t);
                $this->assertNull($kv->get_change_time('key'));
                $t = $kv->get_value('key', 'nyan');
                $this->assertEquals('nyan', $t);
            }
            //
            foreach (['pasu', '', "\\", "'", '"', 1, 1.0, 2.234, true, null, [], ['nyan' => 'pasu'], (object) []] as $value) {
                $kv->set_value('key', $value, 3600);
                $t = $kv->get_value('key', null);
                if (is_null($value)) {
                    $this->assertNull($t);
                    continue;
                }

                $this->assertNotNull($t);
                $this->assertInternalType(gettype($value), $t);
                if (is_array($t)) {
                    $this->assertEquals(count($value), count($t));
                } elseif (!is_bool($t) and !is_object($t)) {
                    $this->assertEquals($value, $t);
                }

                $full_t = $kv->get_value_full('key');
                $this->assertLessThan(2, abs($full_t->time_create - $kv->get_change_time('key')));

                $kv1->set_value('key', $value, 3600);
                $t1 = $kv1->get_value('key', null);
                if (!is_array($t) and !is_object($t)) {
                    $this->assertEquals($t, $t1);
                }
                $kv1->delete_value('key');
                $this->assertNull($kv1->get_value('key', null));
            }

            // @todo Большой текст

            $kv->delete_value('key');
            $kv1->delete_value('key');
        }

        /**
         * Проверяем, что с одной стороны мьютекс дёргается но с другой — освобождается, а не зависает
         */
        function testGet_lockBehavior() {
            $reflection_lock = new ReflectionProperty($this->_class_name, '_locks');
            $reflection_lock->setAccessible(true);

            /**
             * Проверяем, что с одной стороны мьютекс дёргается но с другой — освобождается, а не зависает
             * @var iAscetKeyValueStorage|KVTestOverload $instance
             */
            $class_name = $this->_class_name.'TestOverload';
            $instance = new $class_name($this->get_full_params([]));
            // Сначала удаляем, чтобы точно не было
            $instance->delete_value('key');
            $this->assertLockNotAcquired($reflection_lock->getValue($instance), 'key');
            // Пишем
            $instance->set_value('key', null, 0);
            $this->assertTrue($instance->get_lock_ack('key'));
            $instance->release_lock_ack();
            $this->assertLockNotAcquired($reflection_lock->getValue($instance), 'key');
            // Читаем
            $instance->get_value('key', null);
            $this->assertTrue($instance->get_lock_ack('key'));
            $instance->release_lock_ack();
            $this->assertLockNotAcquired($reflection_lock->getValue($instance), 'key');
            // Удаляем
            $instance->delete_value('key');
            $this->assertTrue($instance->get_lock_ack('key'));
            $instance->release_lock_ack();
            $this->assertLockNotAcquired($reflection_lock->getValue($instance), 'key');
        }

        function testGet_lock() {
            if (method_exists($this->_class_name, 'get_lock')) {
                // Нет овверайдинга этой функции
            }
            $reflection_lock = new ReflectionProperty($this->_class_name, '_locks');
            $reflection_lock->setAccessible(true);
            $reflection_method = new ReflectionMethod($this->_class_name, 'get_lock');
            $reflection_method->setAccessible(true);
            $reflection_release_method = new ReflectionMethod($this->_class_name, 'release_lock');
            $reflection_release_method->setAccessible(true);
            $class_name = $this->_class_name;
            /**
             * @var iAscetKeyValueStorage $instance_plain
             */
            $instance_plain = new $class_name($this->get_full_params([]));
            $old_filenames_for_locks = [];
            foreach (['key', 'nyan', 'pasu', 'foo', 'bar'] as $num => &$key) {
                $instance_plain->get_value($key, null);
                $reflection_method->invoke($instance_plain, $key);
                $reflection_release_method->invoke($instance_plain);
                /**
                 * @var SmartMutex[]|null[] $locks
                 */
                $locks = $reflection_lock->getValue($instance_plain);
                $this->assertInternalType('array', $locks);
                $this->assertEquals($num + 1, count($locks));
                foreach ($locks as $lock_name => &$lock) {
                    if (is_null($lock)) {
                        continue;
                    }
                    $this->assertEquals('SmartMutex', get_class($lock));
                    if (isset($old_filenames_for_locks[$lock_name])) {
                        $this->assertEquals($old_filenames_for_locks[$lock_name], $lock->filename);
                    } else {
                        $old_filenames_for_locks[$lock_name] = $lock->filename;
                    }
                }
            }
            unset($old_filenames_for_locks);

            $locks = $reflection_lock->getValue($instance_plain);
            $used_files = [];
            foreach ($locks as &$lock) {
                if (is_null($lock)) {
                    continue;
                }
                $this->assertNotContains($lock->filename, $used_files);
                $used_files[] = $lock->filename;
            }
        }

        /**
         * @param SmartMutex[]|null[] $locks
         * @param string              $key
         */
        function assertLockNotAcquired($locks, $key) {
            if (isset($locks[$key]) and $locks[$key]->lock_acquired) {
                $this->fail(sprintf('Lock has not been released on %s', $this->_class_name));
            }
        }

        function testGet_expires_time() {
            /**
             * @var iAscetKeyValueStorage $kv
             */
            $class_name = $this->_class_name;
            foreach ([false, true] as $u) {
                $kv = new $class_name($this->get_full_params([]));
                $kv->delete_value('key');
                if ($u) {
                    $kv = new $class_name($this->get_full_params([]));
                }
                for ($i = 0; $i < 100; $i++) {
                    if (!$u) {
                        $kv = new $class_name($this->get_full_params([]));
                    }
                    $ttl = mt_rand(10, 10000);
                    $this_time = microtime(true);
                    $kv->set_value('key', 'value', $ttl);
                    $after_action_time = microtime(true);
                    $expires_time = $kv->get_expires_time('key');
                    if (!static::time_expires_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($this_time + $ttl - $expires_time));
                        $this->assertLessThanOrEqual(3, abs($after_action_time + $ttl - $expires_time));
                    } else {
                        $this->assertGreaterThanOrEqual($this_time + $ttl, $expires_time);
                        $this->assertLessThanOrEqual($after_action_time + $ttl, $expires_time);
                    }
                    $change_time = $kv->get_change_time('key');
                    if (!static::time_expires_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($this_time - $change_time));
                        $this->assertLessThanOrEqual(3, abs($after_action_time - $change_time));
                    } else {
                        $this->assertGreaterThanOrEqual($this_time, $change_time);
                        $this->assertLessThanOrEqual($after_action_time, $change_time);
                    }

                    $raw = $kv->get_value_full('key');
                    if (!static::time_expires_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($expires_time - $raw->time_expires));
                    } else {
                        $this->assertEquals($expires_time, $raw->time_expires);
                    }
                    if (!static::time_create_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($change_time - $raw->time_create));
                    } else {
                        $this->assertEquals($change_time, $raw->time_create);
                    }
                    $this->assertEquals($change_time, $raw->time_create);

                    // Set Expires time
                    $ttl = mt_rand(10, 10000);
                    $this_time = microtime(true);
                    $kv->set_expires_time('key', $ttl);
                    $after_action_time = microtime(true);
                    $expires_time = $kv->get_expires_time('key');
                    if (!static::time_expires_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($this_time + $ttl - $expires_time));
                    } else {
                        $this->assertGreaterThanOrEqual($this_time + $ttl, $expires_time);
                    }
                    if (!static::time_expires_accurate()) {
                        $this->assertLessThanOrEqual(3, abs($after_action_time + $ttl - $expires_time));
                    } else {
                        $this->assertLessThanOrEqual($after_action_time + $ttl, $expires_time);
                    }
                }
            }
            $kv = new $class_name($this->get_full_params([]));
            $kv->delete_value('key');
            $this->assertNull($kv->get_value('key', null));
            $kv->delete_value('key');// @hint Нет, это не ошибка, я специально делаю это дважды
            $this->assertNull($kv->get_value('key', null));
        }

        /**
         * Время просрочки верное
         *
         * @return bool
         */
        static function time_expires_accurate() {
            return true;
        }

        /**
         * Время создания/обновление записи верное
         *
         * @return bool
         */
        static function time_create_accurate() {
            return true;
        }
    }

    interface KVTestOverload {
        function release_lock_ack();

        /**
         * @param string $key
         *
         * @return boolean
         */
        function get_lock($key);

        /**
         * @param string $key
         *
         * @return boolean
         */
        function get_lock_ack($key);
    }


?>