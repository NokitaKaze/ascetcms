<?php
    require_once dirname(__FILE__).'/../functions/sdbcriteria.php';
    require_once dirname(__FILE__).'/../functions/sdbexpression.php';
    require_once dirname(__FILE__).'/../functions/ascetpdo.php';

    class SDBCriteriaTest extends PHPUnit_Framework_TestCase {
        function test__construct() {
            $property = new ReflectionProperty('SDBCriteria', '_prefix');
            $property->setAccessible(true);
            $prefixes = [];
            for ($i = 0; $i < 1000; $i++) {
                $cr = new SDBCriteria;
                $prefix = $property->getValue($cr);
                $this->assertNotContains($prefix, $prefixes);
                $prefixes[] = $prefix;
            }
        }

        function testIs_empty() {
            $cr = new SDBCriteria();
            $this->assertTrue($cr->is_empty());
            $cr->add_condition('true');
            $this->assertFalse($cr->is_empty());
        }

        function testAdd_condition() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);
            $cr = new SDBCriteria();
            $cr->add_condition('true');
            $value = $property->getValue($cr);
            $this->assertInternalType('array', $value);
            $this->assertEquals(1, count($value));
            $this->assertEquals('true', $value[0]);
            //
            $cr->add_condition('now()>`date`');
            $value = $property->getValue($cr);
            $this->assertEquals(2, count($value));
            $this->assertEquals('now()>`date`', $value[1]);
            //
            $cr->add_condition("('nyan'<>'pasu')");
            $value = $property->getValue($cr);
            $this->assertEquals(3, count($value));
            $this->assertEquals("('nyan'<>'pasu')", $value[2]);
            //
            $cr->add_condition('("nyan"<>"pasu")');
            $value = $property->getValue($cr);
            $this->assertEquals(4, count($value));
            $this->assertEquals('("nyan"<>"pasu")', $value[3]);
        }

        function testSql_like_sanation() {
            $value = ['nyan', "'pasu", '"pasu', '%foo%', 'bar_', 'nyan\\pasu'];
            $this->assertEquals('nyan', SDBCriteria::sql_like_sanation($value[0]));
            $this->assertEquals("'pasu", SDBCriteria::sql_like_sanation($value[1]));
            $this->assertEquals('"pasu', SDBCriteria::sql_like_sanation($value[2]));
            $this->assertEquals('\\%foo\\%', SDBCriteria::sql_like_sanation($value[3]));
            $this->assertEquals('bar\\_', SDBCriteria::sql_like_sanation($value[4]));
            $this->assertEquals('nyan\\\\pasu', SDBCriteria::sql_like_sanation($value[5]));
            $this->assertEquals(10.1, SDBCriteria::sql_like_sanation(10.1));
            $this->assertEquals(5, SDBCriteria::sql_like_sanation(5));
            $this->assertEquals(0, SDBCriteria::sql_like_sanation(0));
        }

        function testCompare() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);
            $cr = new SDBCriteria1();
            $cr->compare('key', ['nyan', 'pasu']);
            $this->assertEquals('key', $cr->field);
            $this->assertInternalType('array', $cr->value);
            $this->assertEquals(2, count($cr->value));
            $this->assertContains('nyan', $cr->value);
            $this->assertContains('pasu', $cr->value);
            $this->assertFalse($cr->not);
            $cr->compare('key', ['nyan', 'pasu'], false);
            $this->assertFalse($cr->not);
            $cr->compare('key', ['nyan', 'pasu'], true);
            $this->assertTrue($cr->not);

            foreach ([
                         ['value', false, 'value'],
                         ['value', true, 'value'],
                         ['\'value', false, '\'value'],
                         ['"value', false, '"value'],
                         ['%value%', false, '%value%'],
                         ['_value_', false, '_value_'],
                     ] as $set) {
                $cr = new SDBCriteria1();
                $cr->compare('key', $set[0], $set[1]);
                $value = $property->getValue($cr);
                $regexp = '_^\\s*key\\s*'.(!$set[1] ? '(=)' : '(<>|!=)').'\\s*:([a-z0-9]+)\\s*$_i';
                $this->assertRegExp($regexp, $value[0]);
                preg_match($regexp, $value[0], $a);
                $this->assertArrayHasKey($a[2], $cr->params);
                $this->assertEquals($set[2], $cr->params[$a[2]]);
            }

            $cr = new SDBCriteria1();
            $cr->compare('key', 'value');
            $cr->compare('foo', 'bar');
            $cr->compare('nyan', 'pasu', true);
            $value = $property->getValue($cr);
            //
            $regexp = '_^\\s*key\\s*(=)\\s*:([a-z0-9]+)\\s*$_i';
            $this->assertRegExp($regexp, $value[0]);
            preg_match($regexp, $value[0], $a);
            $this->assertArrayHasKey($a[2], $cr->params);
            $this->assertEquals('value', $cr->params[$a[2]]);
            //
            $regexp = '_^\\s*foo\\s*(=)\\s*:([a-z0-9]+)\\s*$_i';
            $this->assertRegExp($regexp, $value[1]);
            preg_match($regexp, $value[1], $a);
            $this->assertArrayHasKey($a[2], $cr->params);
            $this->assertEquals('bar', $cr->params[$a[2]]);
            //
            $regexp = '_^\\s*nyan\\s*(<>|!=)\\s*:([a-z0-9]+)\\s*$_i';
            $this->assertRegExp($regexp, $value[2]);
            preg_match($regexp, $value[2], $a);
            $this->assertArrayHasKey($a[2], $cr->params);
            $this->assertEquals('pasu', $cr->params[$a[2]]);
        }

        function testGenerate_hash() {
            for ($i = 0; $i < 100; $i++) {
                foreach ([1, 5, 10, 20, 30] as $len) {
                    $value = SDBCriteria::generate_hash($len);
                    $this->assertInternalType('string', $value);
                    $this->assertRegExp('_^[0-9a-zA-Z]{'.$len.','.$len.'}$_', $value);
                }
            }
        }

        function testSanify_raw_value() {
            // @hint Большая часть должна быть в sanify_lonely_raw_value
            $a = SDBCriteria::sanify_raw_value('nyan');
            $this->assertInternalType('string', $a);
            $this->assertEquals('"nyan"', $a);
            $a = SDBCriteria::sanify_raw_value(['nyan', 'pasu',]);
            $this->assertInternalType('array', $a);
            $this->assertEquals(2, count($a));
            $this->assertEquals('"nyan"', $a[0]);
            $this->assertEquals('"pasu"', $a[1]);
        }

        function testSanify_lonely_raw_value() {
            $reflection = new ReflectionMethod('SDBCriteria', 'sanify_lonely_raw_value');
            $reflection->setAccessible(true);
            foreach ([true, false,] as &$u) {
                foreach ([1, 0, 10.1, '1', 'true', '\'', null, true, false] as &$value) {
                    $return = $reflection->invoke(null, $value, $u);
                    if ($u) {
                        $this->assertInternalType('string', $return);
                    } else {
                        $this->assertContains(gettype($return), ['string', 'double', 'integer']);
                    }
                    switch (gettype($value)) {
                        case 'integer':
                        case 'double':
                            if ($u) {
                                $this->assertEquals(sprintf('"%s"', $value), $return);
                            } else {
                                $this->assertEquals($value, $return);
                            }
                            break;
                        case 'string':
                            $this->assertEquals(sprintf('"%s"', sad_safe_mysql($value)), $return);
                            break;
                        case 'null':
                            $this->assertEquals('null', strtolower($return));
                            break;
                        case 'boolean':
                            $this->assertEquals($value ? 'true' : 'false', strtolower($return));
                            break;
                    }
                }
            }

            foreach ([[], (object) []] as $value) {
                $u = false;
                try {
                    $reflection->invoke(null, $value, false);
                } catch (Exception $e) {
                    $u = true;
                }
                if (!$u) {
                    $this->fail('SDBCriteria::sanify_lonely_raw_value did not throw exception on malformed param');
                }
            }
        }

        function testAdd_between_condition() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);
            foreach ([true, false] as &$u) {
                foreach ([['nyan', 'pasu'], [1, 10]] as &$set) {
                    $cr = new SDBCriteria();
                    $cr->add_between_condition('key', $set[0], $set[1], $u);
                    $value = $property->getValue($cr);

                    $regexp = '_^\\s*key\\s+'.($u ? 'not\\s+' : '').'between\\s+:([a-z0-9]+)\\s+and\\s+:([a-z0-9]+)$_i';
                    $this->assertRegExp($regexp, $value[0]);
                    preg_match($regexp, $value[0], $a);
                    $this->assertEquals($set[0], $cr->params[$a[1]]);
                    $this->assertEquals($set[1], $cr->params[$a[2]]);
                }
            }
        }

        function testGet_full_sql_query_string() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);
            $cr = new SDBCriteria();
            $this->assertEquals('', $cr->get_full_sql_query_string());

            $cr->add_condition('nyan()');
            $this->assertEquals('nyan()', $cr->get_full_sql_query_string(false));
            $this->assertEquals('(nyan())', $cr->get_full_sql_query_string(true));

            $cr->add_condition('pasu()');
            $this->assertEquals('(nyan())and(pasu())', $cr->get_full_sql_query_string(false));
            $this->assertEquals('(nyan())and(pasu())', $cr->get_full_sql_query_string(true));
        }

        function testAdd_in_condition() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);

            $u = false;
            try {
                $cr = new SDBCriteria();
                $cr->add_in_condition('key', 'nyan', false);
            } catch (Exception $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SDBCriteria::add_in_condition did not throw exception on malformed param');
            }

            foreach ([false, true,] as &$u) {
                foreach ([['nyan'], ['foo', 'bar'], []] as $array) {
                    $key = SDBCriteria::generate_hash();
                    $cr = new SDBCriteria();
                    $cr->add_in_condition($key, $array, $u);
                    $value = $property->getValue($cr);
                    if (count($array) == 0) {
                        $regexp = '_^\\s*'.(!$u ? 'false' : 'true').'\\s*$_i';
                    } elseif (count($array) == 1) {
                        $regexp = '_^\\s*'.$key.'\\s*'.(!$u ? '(=)' : '(<>|!=)').'\\s*:([a-z0-9]+)\\s*$_i';
                    } else {
                        $regexp = '_^\\s*'.$key.'\\s+'.($u ? 'not\\s+' : '').'in\\s*\\((.+?)\\)\\s*$_i';
                    }
                    $this->assertRegExp($regexp, $value[0]);
                    preg_match($regexp, $value[0], $a);
                    if (count($array) == 1) {
                        $first_key = $a[2];
                        $this->assertArrayHasKey($first_key, $cr->params);
                        $this->assertEquals($array[0], $cr->params[$first_key]);
                    } elseif (count($array) > 1) {
                        $tabs = explode('","', substr($a[1], 1, -1));
                        foreach ($array as &$t) {
                            $this->assertContains($t, $tabs);
                        }
                    }

                    //
                    $cr->add_in_condition($key, ['pasu'], $u);
                    $value = $property->getValue($cr);
                    $regexp = '_^\\s*'.$key.'\\s*'.(!$u ? '(=)' : '(<>|!=)').'\\s*:([a-z0-9]+)\\s*$_i';
                    $this->assertRegExp($regexp, $value[1]);
                    preg_match($regexp, $value[1], $a);
                    $second_key = $a[2];
                    if (count($array) == 1) {
                        /** @noinspection PhpUndefinedVariableInspection */
                        $this->assertNotEquals($first_key, $second_key);
                    }
                    $this->assertArrayHasKey($second_key, $cr->params);
                    $this->assertEquals('pasu', $cr->params[$second_key]);
                }
            }
        }

        function testCompare_string() {
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);

            foreach ([false, true] as $partial) {
                foreach ([false, true] as $not) {
                    foreach ([false, true] as $empty_can_be_null) {
                        if ($empty_can_be_null and $partial) {
                            continue;
                        }
                        foreach ([0, 1, '', null, 'value'] as $value) {
                            $field = SDBCriteria::generate_hash();
                            $cr = new SDBCriteria();
                            $cr->compare_string($field, $value, $partial, $not, $empty_can_be_null);

                            $compare_sql = $property->getValue($cr);
                            $this->assertInternalType('array', $compare_sql);
                            $this->assertEquals(1, count($compare_sql));
                            $this->assertInternalType('array', $cr->params);

                            $is_empty = (($value === '') or is_null($value));
                            if ($partial and $is_empty) {
                                $this->assertEquals($not ? 'false' : 'true', $compare_sql[0]);
                                continue;
                            }
                            if ($partial or !$is_empty or !$empty_can_be_null) {
                                $this->assertEquals(1, count($cr->params));
                                $single_key = array_keys($cr->params)[0];
                                $real_value = $cr->params[$single_key];
                                $this->assertNotNull($real_value);
                                if (is_null($value)) {
                                    $this->assertEquals('', $real_value);
                                } else {
                                    $this->assertEquals($partial ? '%'.SDBCriteria::sql_like_sanation($value).'%' : $value,
                                        $real_value);
                                }
                            }

                            if ($partial) {
                                if ($not) {
                                    $regexp =
                                        "/^\\s*\\(([a-z0-9_]+)\\s+not\\s+like\\s+:([a-z0-9_]+)\\s*\\)\\s*or\\s*\\(\\s*([a-z0-9_]+)\\s+is\\s+null\\s*\\)\\s*$/i";
                                    $compared = [$field, null, $field];
                                } else {
                                    $regexp =
                                        "/^\\s*\\(([a-z0-9_]+)\\s+like\\s+:([a-z0-9_]+)\\s*\\)\\s*and\\s*\\(\\s*([a-z0-9_]+)\\s+is\\s+not\\s+null\\s*\\)\\s*$/i";
                                    $compared = [$field, null, $field];
                                }
                            } else {
                                if ($is_empty and $empty_can_be_null) {
                                    $regexp = $not
                                        ? "/^\\s*\\(\\s*([a-z0-9_]+)\\s*<>\\s*''\\s*\\)\\s*and\\s*\\(\\s*([a-z0-9_]+)\\s+is\\s+not\\s+null\\)\\s*$".
                                          "/i"
                                        : "/^\\s*\\(\\s*([a-z0-9_]+)\\s*=\\s*''\\)\\s*or\\s*\\(\\s*([a-z0-9_]+)\\s+is\\s+null\\s*\\)\\s*$".
                                          "/i";
                                    $compared = [$field, $field];
                                } else {
                                    $regexp = "/^\\s*([a-z0-9_]+)\\s*(=|<>|!=)\\s+:([a-z0-9_]+)\\s*$/i";
                                    $compared = [$field, $not ? '<>' : '=', null];
                                }
                            }

                            // regexp test
                            $this->assertRegExp($regexp, $compare_sql[0]);
                            preg_match($regexp, $compare_sql[0], $a);
                            $this->assertEquals(count($compared) + 1, count($a));
                            foreach ($compared as $id => $test_value) {
                                if ($test_value === null) {
                                    continue;
                                }
                                $this->assertEquals($test_value, $a[$id + 1]);
                            }
                        }
                    }
                }
            }
        }

        function testCompare_from_text() {
            $dictionary = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '1'), ['.']);
            $data = [
                // Просто текст
                (object) ['raw_value' => 'text', 'param' => 'text', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' text ', 'param' => 'text', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' text', 'param' => 'text', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1234 ', 'param' => '1234', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1 234 ', 'param' => '1234', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1234.1 ', 'param' => '1234.1', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1234,1 ', 'param' => '1234.1', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1 234.1 ', 'param' => '1234.1', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' 1 234,1 ', 'param' => '1234.1', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => '1234', 'param' => '1234', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => '1 234,1', 'param' => '1234.1', 'condition' => '_^{field}\\s*=\\s*:[a-z0-9]+$_i'],
                //
                (object) ['raw_value' => '> 1234,1', 'param' => '1234.1', 'condition' => '_^{field}\\s*>\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' > 1234,1', 'param' => '1234.1', 'condition' => '_^{field}\\s*>\\s*:[a-z0-9]+$_i'],
                (object) ['raw_value' => ' >1234,1', 'param' => '1234.1', 'condition' => '_^{field}\\s*>\\s*:[a-z0-9]+$_i'],
            ];
            $property = new ReflectionProperty('SDBCriteria', '_sql_compares');
            $property->setAccessible(true);
            foreach ($data as &$datum) {
                $criteria = new SDBCriteria();
                $field = '';
                $field_name_len = mt_rand(5, 10);
                for ($i = 0; $i < $field_name_len; $i++) {
                    $field .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                }
                $criteria->compare_from_text($field, $datum->raw_value, isset($datum->partial) ? $datum->partial : false);
                if (isset($datum->param)) {
                    $this->assertInternalType('array', $criteria->params);
                    $this->assertEquals(1, count($criteria->params));
                    $param_name = array_keys($criteria->params)[0];
                    $this->assertEquals($datum->param, $criteria->params[$param_name]);
                }
                $this->assertEquals(1, count($property->getValue($criteria)));
                $this->assertRegExp(str_replace('{field}', $field, $datum->condition), $property->getValue($criteria)[0]);
            }

            $criteria2 = new SDBCriteria();
            foreach (['<>', '<=', '>=', '<', '>', '='] as $id => $op) {
                $criteria = new SDBCriteria();
                $criteria->compare_from_text('field', $op.'1234');
                $this->assertEquals(1, count($property->getValue($criteria)));
                $this->assertRegExp('_^\\s*field\\s*'.$op.'\\s*:[a-z0-9]+$_i', $property->getValue($criteria)[0]);

                $criteria2->compare_from_text('field', $op.'1234');
                $this->assertEquals($id + 1, count($property->getValue($criteria2)));
            }
            foreach ([
                         ['!=', '<>'],
                         ['==', '='],
                         ['=>', '>='],
                         ['=<', '<='],
                     ] as &$set) {
                //
                $criteria = new SDBCriteria();
                $criteria->compare_from_text('field', $set[0].'1234');
                $this->assertEquals(1, count($property->getValue($criteria)));
                $this->assertRegExp('_^\\s*field\\s*'.$set[1].'\\s*:[a-z0-9]+$_i', $property->getValue($criteria)[0]);
            }
            // '>'
            $criteria = new SDBCriteria();
            $criteria->compare_from_text('field', '>');
            $this->assertEquals(1, count($property->getValue($criteria)));
            $this->assertRegExp('_^false$_i', $property->getValue($criteria)[0]);
            // ==''
            $criteria = new SDBCriteria();
            $criteria->compare_from_text('field', '');
            $this->assertEquals(0, count($property->getValue($criteria)));
            // like
            foreach ([
                         ['nyan', 'nyan'],
                         ['\'', '\''],
                         ['"', '"'],
                         // ['\0', '\0'], // @todo Вернуть, когда разберусь
                         ['nyan%', 'nyan\\%'],
                         ['ny_an', 'ny\\_an'],
                     ] as $set) {
                $criteria = new SDBCriteria();
                $criteria->compare_from_text('field', $set[0], true);
                $this->assertEquals(1, count($property->getValue($criteria)));
                $this->assertRegExp('_^\\s*\\(\\s*field\\s*like\\s*:[a-z0-9]+\\s*\\)\\s*and'.
                                    '\\s*\\(\\s*field\\s+is\\s+not\\s+null\\s*\\)\\s*$_i',
                    $property->getValue($criteria)[0]);

                $this->assertInternalType('array', $criteria->params);
                $this->assertEquals(1, count($criteria->params));
                $param_name = array_keys($criteria->params)[0];
                $this->assertEquals('%'.$set[1].'%', $criteria->params[$param_name]);
            }
        }

        function testSanify_double_value() {
            $method = new ReflectionMethod('SDBCriteria', 'sanify_double_value');
            $method->setAccessible(true);
            foreach ([
                         [' 1234 ', '1234'],
                         [' 1 234 ', '1234'],
                         [' 1234.1 ', '1234.1'],
                         [' 1234,1 ', '1234.1'],
                         [' 1 234.1 ', '1234.1'],
                         ['1 234,1', '1234.1'],
                         [' 1 234,1 ', '1234.1'],
                         [' 1 2 34.1 ', '1234.1'],
                         [' 12 34,1 ', '1234.1'],
                         [' 1234 ', '1234'],
                         [' 1234 ', '1234'],
                         ['1234', '1234'],
                         ['1.', '1'],
                         ['1.e', null],
                         ['1,.0', null],
                         ['11.e', null],
                         ['1,,0', null],
                         ['1234567890,,0', null],
                     ] as $set) {
                $value = !is_null($set[1]) ? $set[1] : $set[0];
                $this->assertEquals($value, $method->invoke(null, $set[0]));
            }
        }
    }

    class SDBCriteria1 extends SDBCriteria {
        public $field, $value, $not;

        /**
         * @param string  $field
         * @param array   $value
         * @param boolean $not
         */
        function add_in_condition($field, $value, $not = false) {
            $this->field = $field;
            $this->value = $value;
            $this->not = $not;
        }

        function clear() {
            $this->field = null;
            $this->value = null;
            $this->not = null;
        }
    }

?>