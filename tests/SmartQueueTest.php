<?php
    require_once dirname(__FILE__).'/../functions/smartmutex.php';
    require_once dirname(__FILE__).'/../functions/smartqueue.php';

    class SmartQueueTest extends PHPUnit_Framework_TestCase {
        private $_folder;

        function setUp() {
            parent::setUp();
            $this->_folder = sys_get_temp_dir().'/ascetcms_test_'.self::generate_hash();
            mkdir($this->_folder);
        }

        function tearDown() {
            system(sprintf('rm -rf "%s"', str_replace('"', '\\"', $this->_folder)));
            parent::tearDown();
        }

        function test__construct() {
            $u = false;
            try {
                new SmartQueueOverload((object) ['folder' => '/nyan/pasu']);
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue did not throw exception on malformed settings');
            }

            $queue = new SmartQueueOverload((object) ['folder' => '/nyan/pasu', 'name' => 'foo']);
            $reflection = new ReflectionProperty('SmartQueue', '_folder');
            $reflection->setAccessible(true);
            $this->assertEquals('/nyan/pasu', $reflection->getValue($queue));
            //
            $queue = new SmartQueueOverload((object) ['storage_type' => SmartQueue::StorageTemporary, 'name' => 'foo']);
            $folder = $reflection->getValue($queue);
            $queue = new SmartQueueOverload((object) ['storage_type' => SmartQueue::StoragePersistent, 'name' => 'foo']);
            $folder1 = $reflection->getValue($queue);
            $this->assertNotEquals($folder, $folder1);

            $u = false;
            try {
                new SmartQueueOverload((object) ['storage_type' => mt_rand(100, 200), 'name' => 'foo']);
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue did not throw exception on malformed storage_type');
            }
        }

        function testStandard_prefix_strategy() {
            $queue = new SmartQueueOverload((object) ['prefix' => 'nyan_', 'name' => 'foo', 'folder' => $this->_folder]);
            $reflection = new ReflectionProperty('SmartQueue', '_prefix');
            $reflection->setAccessible(true);
            $this->assertEquals('nyan_', $reflection->getValue($queue));
        }

        /**
         * @covers SmartQueue::push
         * @covers SmartQueue::save
         */
        function testPush() {
            $queue = new SmartQueueOverload((object) ['name' => 'foo', 'folder' => $this->_folder]);
            $reflection = new ReflectionProperty('SmartQueue', '_pushed_for_save');
            $reflection->setAccessible(true);
            $count1 = count($reflection->getValue($queue));
            $queue->push((object) ['nyan' => 'pasu', 'name' => 'foo', 'data' => mt_rand(0, 100)]);
            if ($queue->get_current_index_mutex() !== null) {
                $this->assertTrue($queue->get_current_index_mutex()->is_free());
            }

            $this->assertEquals($count1 + 1, count($reflection->getValue($queue)));
            $a = [];
            $r = mt_rand(1, 100);
            for ($i = 0; $i < $r; $i++) {
                $a[] = (object) ['name' => null, 'data' => mt_rand(0, 100)];
            }
            $queue->push($a);
            $this->assertEquals($count1 + 1 + $r, count($reflection->getValue($queue)));

            $queue->save();
            $this->assertTrue($queue->get_current_index_mutex()->is_free());
            $this->assertEquals(0, count($reflection->getValue($queue)));
            $queue->push((object) ['nyan' => 'pasu', 'name' => 'foo', 'data' => mt_rand(0, 100)]);
            $this->assertTrue($queue->get_current_index_mutex()->is_free());
            $this->assertEquals(1, count($reflection->getValue($queue)));
        }

        function testProduce_message() {
            foreach ([false, true] as $u) {
                if (file_exists($this->_folder)) {
                    system(sprintf('rm -rf "%s"', str_replace('"', '\\"', $this->_folder)));
                    mkdir($this->_folder);
                }
                $queue_name = self::generate_hash(20);
                if ($u) {
                    SmartQueueOverload::$producer_id = 0;
                }
                $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                if ($u) {
                    SmartQueueOverload::$producer_id = 1;
                }
                $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);

                //
                $queue1->produce_message('nyan', null);
                $this->assertTrue($queue1->get_current_index_mutex()->is_free());
                $ts1 = microtime(true);
                $obj = $queue2->consume_next_message(0);
                $ts2 = microtime(true);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertEquals('nyan', $obj->data);
                $this->assertNull($obj->name);
                $this->assertEquals('SmartQueueOverload', get_class($obj->queue));
                $this->assertGreaterThan($ts1, $obj->time_consumed);
                $this->assertLessThan($ts2, $obj->time_consumed);
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());

                //
                $queue1->produce_message('alpha', 'omega');
                $obj = $queue2->consume_next_message(0);
                $this->assertEquals('alpha', $obj->data);
                $this->assertEquals('omega', $obj->name);
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());
                //
                $queue1->produce_message('alpha', 'omegan');
                $queue1->produce_message('beta', 'omegan');
                $obj = $queue2->consume_next_message(0);
                $this->assertNotNull($obj);
                $obj = $queue2->consume_next_message(0);
                $this->assertNull($obj);
                //
                $queue1->push((object) [
                    'name' => 'omegan2',
                    'data' => 'alpha',
                    'time_created' => microtime(true),
                    'sort' => 0,
                    'is_read' => false,
                ]);
                $queue1->push((object) [
                    'name' => 'omegan2',
                    'data' => 'beta',
                    'time_created' => microtime(true),
                    'sort' => 0,
                    'is_read' => false,
                ]);
                $queue1->save();
                $obj = $queue2->consume_next_message(0);
                $this->assertNotNull($obj);
                $this->assertEquals('alpha', $obj->data);
                $obj = $queue2->consume_next_message(0);
                $this->assertNull($obj);
                //
                $queue1->produce_message('alpha', 'omega2');
                $queue1->produce_message('beta', 'omega2');
                $queue2->consume_next_message(0);
                $queue2->consume_next_message(0);
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());

                $a = [mt_rand(0, 1000000), mt_rand(0, 1000000),
                      mt_rand(0, 1000000), mt_rand(0, 1000000),
                      mt_rand(0, 1000000), mt_rand(0, 1000000),
                      mt_rand(0, 1000000), mt_rand(0, 1000000),];
                $queue1->produce_message($a[0], null);
                $queue1->produce_message($a[1], null, 3);
                $queue1->produce_message($a[2], null, 7);
                $queue1->produce_message($a[3], null, 5);
                $queue1->produce_message($a[4], null, 3);
                $queue1->produce_message($a[5], null, 5);
                $queue1->produce_message($a[6], null, 7);
                $queue1->produce_message($a[7], null);
                $b = [$a[1], $a[4], $a[0], $a[3], $a[5], $a[7], $a[2], $a[6],];
                foreach ($b as &$value) {
                    $obj = $queue2->consume_next_message(1);
                    $this->assertNotNull($obj);
                    $this->assertEquals($value, $obj->data);
                    $this->assertNull($obj->name);
                }
                $this->assertTrue($queue1->get_current_index_mutex()->is_free());
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $queue1->save();
                $this->assertNull($queue2->consume_next_message(0));
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());
            }
        }

        function testConsume_next_message() {
            foreach ([false, true] as $u) {
                // @hint Почти всё уже оттестировано в Produce message
                $queue_name = self::generate_hash(20);
                if ($u) {
                    SmartQueueOverload::$producer_id = 0;
                }
                $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                if ($u) {
                    SmartQueueOverload::$producer_id = 1;
                }
                $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                //
                $ts1 = microtime(true);
                $obj = $queue2->consume_next_message(5);
                $ts2 = microtime(true);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertNull($obj);
                $this->assertGreaterThanOrEqual(5, $ts2 - $ts1);
                $obj = $queue2->consume_next_message(0);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertNull($obj);
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());

                $queue1->produce_message('nyan', null);
                $this->assertTrue($queue1->get_current_index_mutex()->is_free());
                $queue2->consume_next_message(0);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());
                //
                $ts1 = microtime(true);
                $obj = $queue2->consume_next_message(5);
                $ts2 = microtime(true);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertNull($obj);
                $this->assertGreaterThanOrEqual(5, $ts2 - $ts1);
                $obj = $queue2->consume_next_message(0);
                $this->assertTrue($queue2->get_current_index_mutex()->is_free());
                $this->assertNull($obj);
                $this->assertFalse($queue1->lock_mutex_exists());
                $this->assertFalse($queue2->lock_mutex_exists());
            }
        }

        function testClear_consumed_keys() {
            $queue_name = self::generate_hash();
            $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $reflection = new ReflectionProperty('SmartQueue', '_consumed_keys');
            $reflection->setAccessible(true);
            $this->assertEquals(count($reflection->getValue($queue1)), 0);

            //
            $queue1->produce_message('nyan', null);
            $this->assertTrue($queue1->get_current_index_mutex()->is_free());
            $this->assertEquals(count($reflection->getValue($queue1)), 0);
            $queue2->consume_next_message(0);
            $this->assertTrue($queue2->get_current_index_mutex()->is_free());
            $this->assertGreaterThan(0, count($reflection->getValue($queue2)));
            $queue2->clear_consumed_keys();
            $this->assertEquals(count($reflection->getValue($queue2)), 0);
        }

        /**
         * @covers SmartQueue::get_mutex_for_thread
         * @covers SmartQueue::get_index_mutex
         */
        function testGet_mutex_for_thread() {
            $queue_name = self::generate_hash();
            $queue = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $reflection = new ReflectionMethod('SmartQueue', 'get_mutex_for_thread');
            $reflection->setAccessible(true);
            $values = [];
            for ($i = 0; $i < SmartQueue::SortCount; $i++) {
                /**
                 * @var SmartMutex $mutex
                 */
                $mutex = $reflection->invoke($queue, $i);
                $this->assertInternalType('object', $mutex);
                $this->assertEquals('SmartMutex', get_class($mutex));
                if (in_array($mutex->filename, $values)) {
                    $this->fail('SmartMutex gave the same files');
                }
                $values[] = $mutex->filename;
            }

            $reflection = new ReflectionMethod('SmartQueue', 'get_index_mutex');
            $reflection->setAccessible(true);
            $mutex = $reflection->invoke($queue);
            $this->assertInternalType('object', $mutex);
            $this->assertEquals('SmartMutex', get_class($mutex));
            if (in_array($mutex->filename, $values)) {
                $this->fail('SmartMutex gave the same files');
            }
        }

        function testGet_real_key_for_message() {
            $name = self::generate_hash();

            $obj1 = (object) ['name' => $name, 'time_created' => microtime(true) - 10];
            $obj2 = (object) ['name' => self::generate_hash(), 'time_created' => microtime(true) - 10];
            $obj3 = (object) ['name' => null, 'time_created' => microtime(true) - 20];
            $obj4 = (object) ['name' => null, 'time_created' => microtime(true) - 8];
            $this->assertEquals(SmartQueue::get_real_key_for_message($obj1), SmartQueue::get_real_key_for_message($obj1));
            $this->assertNotEquals(SmartQueue::get_real_key_for_message($obj1), SmartQueue::get_real_key_for_message($obj2));
            $this->assertNotEquals(SmartQueue::get_real_key_for_message($obj1), SmartQueue::get_real_key_for_message($obj3));
            $this->assertEquals(SmartQueue::get_real_key_for_message($obj3), SmartQueue::get_real_key_for_message($obj3));
            $this->assertNotEquals(SmartQueue::get_real_key_for_message($obj3), SmartQueue::get_real_key_for_message($obj4));
            $this->assertNotEquals(
                SmartQueue::get_real_key_for_message((object) ['name' => null, 'time_created' => microtime(true)]),
                SmartQueue::get_real_key_for_message((object) ['name' => null, 'time_created' => microtime(true)]));
        }

        static function generate_hash($key_length = 20) {
            $hashes = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            $hash = '';
            for ($i = ord('a'); $i <= ord('z'); $i++) {
                array_push($hashes, chr($i), strtoupper(chr($i)));
            }
            for ($i = 0; $i < $key_length; $i++) {
                $hash .= $hashes[mt_rand(0, count($hashes) - 1)];
            }

            return $hash;
        }

        function testSanify_event_object() {
            $obj = (object) ['data' => 'nyan', 'foo' => 'bar'];
            $ret = SmartQueue::sanify_event_object($obj);
            $this->assertArrayHasKey('name', (array) $ret);
            $this->assertNull($ret->name);
            $this->assertArrayHasKey('data', (array) $ret);
            /** @noinspection PhpParamsInspection */
            $this->assertArrayNotHasKey('name', (array) $obj);
            $this->assertEquals('bar', $obj->foo);
            $this->assertEquals('bar', $ret->foo);

            $u = false;
            try {
                SmartQueue::sanify_event_object((object) []);
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue::sanify_event_object did not throw exception on malformed event');
            }
        }

        function testGet_current_producer_thread_id() {
            $reflection = new ReflectionMethod('SmartQueue', 'get_current_producer_thread_id');
            $reflection->setAccessible(true);
            $ret1 = $reflection->invoke(null);
            $this->assertInternalType('integer', $ret1);
            for ($i = 0; $i < 10; $i++) {
                $ret2 = $reflection->invoke(null);
                $this->assertEquals($ret1, $ret2);
            }
        }

        function testInit_producer_mutex() {
            $queue_name = self::generate_hash(20);
            $queue = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $reflection = new ReflectionMethod('SmartQueue', 'init_producer_mutex');
            $reflection->setAccessible(true);
            $reflection->invoke($queue);

            $reflection1 = new ReflectionProperty('SmartQueue', '_producer_mutex');
            $reflection1->setAccessible(true);
            $this->assertInternalType('object', $queue->get_current_index_mutex());
            $this->assertEquals('SmartMutex', get_class($queue->get_current_index_mutex()));
            $this->assertInternalType('object', $reflection1->getValue($queue));
            $this->assertEquals('SmartMutex', get_class($reflection1->getValue($queue)));
        }

        function testGet_index_filename() {
            $reflection = new ReflectionMethod('SmartQueue', 'get_index_filename');
            $reflection->setAccessible(true);
            $prefix_property = new ReflectionProperty('SmartQueue', '_prefix');
            $prefix_property->setAccessible(true);
            $filenames = [];
            $new_prefix = mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z'));
            foreach ([false, true] as $u) {
                foreach (['nyan', 'pasu', 'foo', 'bar', 'нян', 'пасу', '日本語'] as $key) {
                    $queue = new SmartQueue((object) ['name' => $key, 'folder' => $this->_folder]);
                    if ($u) {
                        $prefix_property->setValue($queue, $new_prefix);
                    }
                    $filename = $reflection->invoke($queue);
                    $this->assertNotContains($filename, $filenames);
                    $filenames[] = $filename;
                }
            }
        }

        /**
         * @covers SmartQueue::index_data_save
         * @covers SmartQueue::write_full_data_to_file
         */
        function testIndex_data_save() {
            foreach ([
                         (object) ['method' => 'index_data_save', 'filename' => 'get_index_filename'],
                         (object) ['method' => 'write_full_data_to_file', 'filename' => 'get_producer_filename_for_thread'],
                     ] as $obj) {
                $reflection = new ReflectionMethod('SmartQueue', $obj->method);
                $reflection->setAccessible(true);
                $filename = tempnam(sys_get_temp_dir(), 'ascetcms_test_');
                $queue = new SmartQueue((object) ['name' => 'testname', 'folder' => $filename]);
                $u = false;
                try {
                    if ($obj->method == 'write_full_data_to_file') {
                        $reflection->invoke($queue, 0, null);
                    } else {
                        $reflection->invoke($queue);
                    }
                } catch (SmartQueueException $e) {
                    $u = true;
                }
                if (!$u) {
                    $this->fail('SmartQueue::'.$obj->method.' did not throw Exception on non folder');
                }
                unlink($filename);

                $folder = sprintf('%s/ascetcms_test_%s', sys_get_temp_dir(),
                    mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z')));
                $queue = new SmartQueue((object) ['name' => 'testname', 'folder' => $folder]);
                $u = false;
                try {
                    if ($obj->method == 'write_full_data_to_file') {
                        $reflection->invoke($queue, 0, null);
                    } else {
                        $reflection->invoke($queue);
                    }
                } catch (SmartQueueException $e) {
                    $u = true;
                }
                if (!$u) {
                    $this->fail('SmartQueue::'.$obj->method.' did not throw Exception on non existed folder');
                }

                //
                mkdir($folder);
                chmod($folder, 0);
                $u = false;
                try {
                    if ($obj->method == 'write_full_data_to_file') {
                        $reflection->invoke($queue, 0, null);
                    } else {
                        $reflection->invoke($queue);
                    }
                } catch (SmartQueueException $e) {
                    $u = true;
                }
                if (!$u) {
                    $this->fail('SmartQueue::'.$obj->method.' did not throw Exception on non writable folder');
                }
                //
                $reflection1 = new ReflectionMethod('SmartQueue', $obj->filename);
                $reflection1->setAccessible(true);
                chmod($folder, 7 << 6);
                if ($obj->filename == 'get_producer_filename_for_thread') {
                    $filename = $reflection1->invoke($queue, 0);
                } else {
                    $filename = $reflection1->invoke($queue);
                }
                touch($filename);
                chmod($filename, 0);
                $u = false;
                try {
                    if ($obj->method == 'write_full_data_to_file') {
                        $reflection->invoke($queue, 0, null);
                    } else {
                        $reflection->invoke($queue);
                    }
                } catch (SmartQueueException $e) {
                    $u = true;
                }
                if (!$u) {
                    $this->fail('SmartQueue::'.$obj->method.' did not throw Exception on non writable file');
                }
                chmod($filename, 6 << 6);
                unlink($filename);

                // @todo Покрыть file_put_contents===false
                system(sprintf('rm -rf "%s"', addslashes($folder)));
            }
        }

        var $set_from_closure;

        /**
         * @covers SmartQueue::start_listen
         * @covers SmartQueue::set_callback_closure
         */
        function testSet_callback_closure() {
            $queue_name = self::generate_hash(20);
            $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);

            $u = false;
            try {
                $queue2->start_listen(0);
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue::start_listen did not throw Exception on non set closure');
            }

            $queue2->set_callback_closure(function ($message) {
                $this->set_from_closure = $message;
            });
            $queue2->start_listen(0);
            $this->assertNull($this->set_from_closure);
            $this->assertFalse($queue2->lock_mutex_exists());

            $queue1->produce_message('nyan', null);
            $queue2->start_listen(0);
            $this->assertInternalType('object', $this->set_from_closure);
            $this->assertFalse($queue2->lock_mutex_exists());

            $ts1 = microtime(true);
            $queue2->start_listen(3);
            $ts2 = microtime(true);
            $this->assertGreaterThan(3, $ts2 - $ts1);
        }

        /**
         * @covers SmartQueue::consume_next_message
         */
        function testConsume_next_message_non_consistent() {
            $queue_name = self::generate_hash(20);
            SmartQueueOverload::$producer_id = null;
            $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue1->produce_message('data', 'foobar');
            $current_thread_id = SmartQueueOverload::get_current_producer_thread_id();
            $rp = new ReflectionMethod('SmartQueue', 'get_producer_filename_for_thread');
            $rp->setAccessible(true);
            $filename = $rp->invoke($queue1, $current_thread_id);
            unlink($filename);
            $obj = $queue2->consume_next_message(0);
            $this->assertNull($obj);
        }

        /**
         * @covers SmartQueue::set_exclusive_mode
         */
        function testExclusive_mode() {
            $queue_name = self::generate_hash(20);
            $rp = new ReflectionProperty('SmartQueue', '_index_mutex');
            $rp->setAccessible(true);

            $queue1 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue1->set_exclusive_mode(true);
            // Проверяем, что там init producer
            $this->assertNotNull($rp->getValue($queue1));
            // Проверяем, что монопольный режим ставит и снимает мьютекс
            $this->assertFalse($queue1->get_current_index_mutex()->is_free());
            $queue1->set_exclusive_mode(false);
            $this->assertTrue($queue1->get_current_index_mutex()->is_free());
            //
            $queue1->set_exclusive_mode(true);
            $queue1->produce_message('nyan', 'pasu');
            $this->assertFalse($queue1->get_current_index_mutex()->is_free());
            $queue1->set_exclusive_mode(false);
            $this->assertTrue($queue1->get_current_index_mutex()->is_free());
            //
            $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2->set_exclusive_mode(true);
            $this->assertFalse($queue2->get_current_index_mutex()->is_free());
            $this->assertNotNull($queue2->consume_next_message(0));
            $this->assertFalse($queue2->get_current_index_mutex()->is_free());
            $queue2->set_exclusive_mode(false);
            $this->assertTrue($queue2->get_current_index_mutex()->is_free());
        }

        private function produce_many_messages($count, $queue_name) {
            $queue1 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $u2 = (mt_rand(0, 1) === 0);

            /**
             * @var string[][]|integer[][]|null[][] $names
             */
            $names = [];
            for ($i = 0; $i < SmartQueue::SortCount; $i++) {
                $names[] = [];
            }
            $names_integers = [];
            $names_strings = [];
            for ($i = 0; $i < $count; $i++) {
                if (mt_rand(0, 2) === 0) {
                    $name = null;
                } elseif (mt_rand(0, 1) == 0) {
                    do {
                        $name = mt_rand(0, 100000);
                    } while (in_array($name, $names_integers));
                    $names_integers[] = $name;
                } else {
                    do {
                        $name = self::generate_hash(10);
                    } while (in_array($name, $names_strings));
                    $names_strings[] = $name;
                }
                $sort = mt_rand(0, SmartQueue::SortCount - 1);
                $names[$sort][] = $name;
                if ($u2) {
                    $queue1->produce_message(null, $name, $sort);
                } else {
                    $queue1->push($queue1::build_message(null, $name, $sort));
                }
            }
            if (!$u2) {
                $queue1->save();
            }
            $names_list = [];
            foreach ($names as &$data) {
                foreach ($data as &$datum) {
                    $names_list[] = $datum;
                }
            }

            return $names_list;
        }

        function testDelete_one_message() {
            $queue_name = self::generate_hash(20);
            $queue1 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue3 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue1->produce_message('nyan', 'pasu');
            $obj = $queue2->consume_next_message(0);
            $this->assertNotNull($obj);
            $queue2->delete_message($obj);
            //
            $obj = $queue3->consume_next_message(0);
            $this->assertNull($obj);
            unset($queue1, $queue2, $queue3, $queue_name, $obj);

            $hashes = [];
            for ($j = 0; ($j < 18 * 3) or (count($hashes) < 18); $j++) {
                $u1 = (mt_rand(0, 1) === 0);
                $u2 = (mt_rand(0, 1) === 0);
                if ($u2) {
                    $u3 = (mt_rand(0, 1) === 0);
                    $u4 = (mt_rand(0, 1) === 0);
                    $u5 = (mt_rand(0, 1) === 0);
                } else {
                    $u3 = null;
                    $u4 = null;
                    $u5 = null;
                }
                $hash = ($u1 ? 1 : 0) | ($u2 ? 2 : 0) | ($u3 ? 4 : 0) | ($u4 ? 8 : 0) | ($u5 ? 16 : 0);
                $hashes = array_unique(array_merge($hashes, [$hash]));
                $this->delete_one_message_big_iteration($u1, $u2, $u3, $u4, $u5);
            }
        }

        private function delete_one_message_big_iteration($u1, $u2, $u3, $u4, $u5) {
            $queue_name = self::generate_hash(20);
            $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue3 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);

            $count = mt_rand(20, 50);
            $names_list = $this->produce_many_messages($count, $queue_name);

            if ($u2) {
                // Тестируем update'ы
                $queue4 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                if ($u3) {
                    $queue4->set_exclusive_mode(true);
                }
                $temporary_data_list = [];
                for ($i = 0; $i < $count; $i++) {
                    $obj = $queue4->consume_next_message(0);
                    $this->assertEquals($count, $queue4->get_index_data_length());
                    $this->assertNotNull($obj);
                    if ($u5) {
                        $random = self::generate_hash(20);
                        $obj->data = $random;
                        $queue4->update_message($obj);
                        $temporary_data_list[] = $random;
                    } else {
                        $temporary_data_list[] = null;
                    }
                    unset($obj);
                }
                unset($queue4);

                $queue4 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                if ($u4) {
                    $queue4->set_exclusive_mode(true);
                }
                for ($i = 0; $i < $count; $i++) {
                    $obj = $queue4->consume_next_message(0);
                    $this->assertNotNull($obj);
                    if ($temporary_data_list[$i] === null) {
                        $this->assertNull($obj->data);
                    } else {
                        $this->assertEquals($temporary_data_list[$i], $obj->data);
                    }
                    unset($obj);
                }
                unset($queue4, $names_list4, $temporary_data_list);
            }

            $need_delete = [];
            for ($i = 0; $i < $count; $i++) {
                $obj = $queue2->consume_next_message(0);
                $this->assertNotNull($obj);
                if ($names_list[$i] === null) {
                    $this->assertNull($obj->name);
                } else {
                    if ($names_list[$i] != $obj->name) {
                        //var_dump($u1, $u2, $u3, $u4, $u5, $i, $count);
                    }
                    $this->assertEquals($names_list[$i], $obj->name);
                }
                if ($u1) {
                    $queue2->delete_message($obj);
                } else {
                    $need_delete[] = $obj;
                }
            }
            if (!$u1) {
                foreach ($need_delete as &$obj) {
                    $queue2->delete_message($obj);
                }
            }
            unset($need_delete);
            $this->assertFalse($queue2->lock_mutex_exists());
            //
            $obj = $queue3->consume_next_message(0);
            $this->assertNull($obj);
        }

        /**
         * @covers SmartQueue::delete_messages
         */
        function testDelete_many_messages() {
            $hashes = [];
            $hashes_non_unique = [];
            for ($j = 0; ($j < 8 * 4) or (count($hashes) < 8); $j++) {
                $queue_name = self::generate_hash(20);
                $queue2 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                $queue3 = new SmartQueueOverload((object) ['name' => $queue_name, 'folder' => $this->_folder]);
                $count = mt_rand(20, 50);
                $u1 = (mt_rand(0, 1) === 0);
                $u2 = (mt_rand(0, 1) === 0);
                $u3 = (mt_rand(0, 1) === 0);
                $hash = ($u1 ? 1 : 0) | ($u2 ? 2 : 0) | ($u3 ? 4 : 0);
                $hashes = array_unique(array_merge($hashes, [$hash]));
                $names_list = $this->produce_many_messages($count, $queue_name);
                $hashes_non_unique[] = [$u1, $u2, $u3, $count];
                $live_list = [];

                $deletes = [];
                for ($i = 0; $i < $count; $i++) {
                    $obj = $queue2->consume_next_message(0);
                    $this->assertEquals($count, $queue2->get_index_data_length());
                    $this->assertNotNull($obj);
                    if ($names_list[$i] === null) {
                        $this->assertNull($obj->name);
                    } else {
                        if ($names_list[$i] != $obj->name) {
                            //var_dump($u1, $u2, $u3, $j, $i, $count);
                        }
                        $this->assertEquals($names_list[$i], $obj->name);
                    }
                    if ($u2) {
                        $live_list[] = $obj->name;
                    } else {
                        $deletes[] = $obj;
                    }
                    unset($obj);
                }

                if ($u1) {
                    $queue2->set_exclusive_mode(true);
                }
                $queue2->delete_messages($deletes);
                unset($deletes, $i);
                if ($u1) {
                    $this->assertTrue($queue2->lock_mutex_exists());
                    $queue2->set_exclusive_mode(false);
                } else {
                    $this->assertFalse($queue2->lock_mutex_exists());
                }
                if ($u3) {
                    unset($queue2);
                }

                //
                foreach ($live_list as &$key) {
                    $obj = $queue3->consume_next_message(0);
                    $this->assertNotNull($obj);
                    if ($key === null) {
                        $this->assertNull($obj->name);
                    } else {
                        $this->assertEquals($key, $obj->name);
                    }
                }
                unset($obj, $live_list, $key, $queue2, $queue3, $count, $names_list, $queue_name, $u1, $consumed);
            }
        }

        /**
         * @covers SmartQueue::set_same_time_flag
         */
        function testConsumeAndProducerException() {
            $queue_name = self::generate_hash(20);
            $queue1 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue1->produce_message('nyan', 'pasu');
            $u = false;
            try {
                $queue1->consume_next_message(0);
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue::consume_next_message did\'n throw Exception on consuming & producing at the same time');
            }
            //
            $queue_name = self::generate_hash(20);
            $queue1 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $this->assertNull($queue1->consume_next_message(0));
            $u = false;
            try {
                $queue1->produce_message('nyan', 'pasu');
            } catch (SmartQueueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('SmartQueue::consume_next_message did\'n throw Exception on consuming & producing at the same time');
            }
        }

        function testAutoSort5() {
            $queue_name = self::generate_hash(20);
            $queue1 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $queue2 = new SmartQueue((object) ['name' => $queue_name, 'folder' => $this->_folder]);
            $rm = new ReflectionProperty('SmartQueue', '_pushed_for_save');
            $rm->setAccessible(true);
            $message = SmartQueue::build_message('nyan', 'pasu');
            unset($message->sort);
            $rm->setValue($queue1, [$message]);
            unset($message);
            $queue1->save();

            $message = $queue2->consume_next_message(0);
            $this->assertNotNull($message);
            $this->assertEquals(5, $message->sort);
        }
    }

    class SmartQueueOverload extends SmartQueue {
        /**
         * @var ReflectionProperty
         */
        private $_reflection;

        /**
         * @var integer|null
         */
        static $producer_id = null;

        function __construct($settings) {
            parent::__construct($settings);
            $this->_reflection = new ReflectionProperty('SmartQueue', '_index_mutex');
            $this->_reflection->setAccessible(true);
        }

        /**
         * Внутренний id треда
         *
         * @return integer
         */
        static function get_current_producer_thread_id() {
            if (self::$producer_id === null) {
                return posix_getpid() % self::ProducerThreadCount;
            } else {
                return self::$producer_id;
            }
        }

        /**
         * Паблик Морозов для текущего мьютекса, одного на всю заданную очередь сообщений
         *
         * @return SmartMutex
         */
        function get_current_index_mutex() {
            $rm = new ReflectionMethod('SmartQueue', 'init_producer_mutex');
            $rm->setAccessible(true);
            $rm->invoke($this);

            return $this->_reflection->getValue($this);
        }

        /**
         * @return boolean
         */
        function lock_mutex_exists() {
            $rm = new ReflectionMethod('SmartQueue', 'get_mutex_for_thread');
            $rm->setAccessible(true);
            for ($i = 0; $i < self::ProducerThreadCount; $i++) {
                /**
                 * @var SmartMutex $mutex
                 */
                $mutex = $rm->invoke($this, $i);
                if (!$mutex->is_free()) {
                    return true;
                }
            }

            $mutex = $this->_reflection->getValue($this);

            return (($mutex !== null) and !$mutex->is_free());
        }

        /**
         * @return integer
         */
        function get_index_data_length() {
            $rp = new ReflectionProperty('SmartQueue', '_index_data');
            $rp->setAccessible(true);
            $data = $rp->getValue($this);
            $count = 0;
            foreach ($data as &$datum) {
                $count += count($datum);
            }

            return $count;
        }
    }

?>