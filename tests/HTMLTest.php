<?php
    require_once dirname(__FILE__).'/../functions/helper/utf8.php';
    require_once dirname(__FILE__).'/../functions/html.php';

    class HTMLTest extends PHPUnit_Framework_TestCase {
        /**
         * @covers \AscetCMS\HTML::encode
         * @covers ::sad_safe_html
         */
        function testEncode() {
            foreach ([
                         ['a', 'a'],
                         ['абв', 'абв'],
                         ['&', '&amp;'],
                         ['&amp;', '&amp;amp;'],
                         ['<', '&lt;'],
                         ['>', '&gt;'],
                         ['http://example.com/', 'http://example.com/'],
                         ['<a href="http://example.com/"></a>', '&lt;a href=&quot;http://example.com/&quot;&gt;&lt;/a&gt;'],
                     ] as &$datum) {
                $this->assertEquals($datum[1], AscetCMS\HTML::encode($datum[0]));
                $this->assertEquals($datum[1], sad_safe_html($datum[0]));
            }
        }

        function testDecode() {
            foreach ([
                         ['a', 'a'],
                         ['абв', 'абв'],
                         ['&amp;', '&'],
                         ['&amp;amp;', '&amp;'],
                         ['&lt;', '<'],
                         ['&gt;', '>'],
                         ['&lt', '&lt'],
                         ['&gt', '&gt'],
                         ['http://example.com/', 'http://example.com/'],
                         ['&lt;a href=&quot;http://example.com/&quot;&gt;&lt;/a&gt;', '<a href="http://example.com/"></a>'],
                         ['&lsquo;lsquo;', '‘lsquo;'],
                         ['&rArr;', '⇒'],
                         ['&pound;', '£'],
                         ['&#39;', '\''],
                         ['&#181;', 'µ'],
                     ] as &$datum) {
                $value = AscetCMS\HTML::decode($datum[0]);
                $this->assertEquals($datum[1], $value);
            }

            foreach ([
                         ['&pound;', '£'],
                         ['&#39;', '&#39;'],
                         ['&#181;', '&#181;'],
                     ] as &$datum) {
                $value = AscetCMS\HTML::decode($datum[0], false);
                $this->assertEquals($datum[1], $value);
            }
        }

        private static function get_params_for_test() {
            return [
                (object) ['param' => ['key' => 'value', 'nyan'],
                          'regexp' => ['_\\s+key="value"(\\s*|$)_', '_\\s+nyan(\\s*|$)_']],
                (object) ['param' => ['key', 'nyan' => 'pasu'],
                          'regexp' => ['_\\s+key(\\s*|$)_', '_\\s+nyan="pasu"(\\s*|$)_']],
                (object) ['param' => ['key' => 'value', 'nyan' => 'pasu'],
                          'regexp' => ['_\\s+key="value"(\\s*|$)_', '_\\s+nyan="pasu"(\\s*|$)_']],
            ];
        }

        function testLink() {
            $this->assertRegExp('_<a\\s+href\\s*=\\s*[\'"]http://example\\.com/[\'"]\\s*>text</a>_',
                AscetCMS\HTML::link('http://example.com/', 'text'));
            /** @noinspection BadExpressionStatementJS */
            $this->assertNotRegExp('_<script>_', AscetCMS\HTML::link('http://example.com/', '<script>'));
            $this->assertNotRegExp('_<b>_', AscetCMS\HTML::link('http://example.com/', '<b>'));
            /** @noinspection BadExpressionStatementJS */
            $this->assertRegExp('_<script>_', AscetCMS\HTML::link('http://example.com/', '<script>', [], true));
            $this->assertRegExp('_<b>_', AscetCMS\HTML::link('http://example.com/', '<b>', [], true));
            foreach (self::get_params_for_test() as &$datum) {
                $value = AscetCMS\HTML::link('http://example.com/', 'example.com', $datum->param);
                if (!preg_match('_<a(.*?)>([^<]+?)</a>_sui', $value, $a)) {
                    $this->fail('link is not correct: '.$value);
                    continue;
                }
                foreach ($datum->regexp as &$regexp) {
                    $this->assertRegExp($regexp, $a[1]);
                }
            }
        }

        function testCompile_params() {
            $this->assertEquals('', trim(AscetCMS\HTML::compile_params([])));
            $this->assertEquals(' key="value"', AscetCMS\HTML::compile_params(['key' => 'value']));
            $this->assertEquals(' key="&lt;&gt;"', AscetCMS\HTML::compile_params(['key' => '<>']));
            $this->assertRegExp('_\\s+key\\s*_', AscetCMS\HTML::compile_params(['key']));
            foreach (self::get_params_for_test() as &$datum) {
                $value = AscetCMS\HTML::compile_params($datum->param);
                foreach ($datum->regexp as &$regexp) {
                    $this->assertRegExp($regexp, $value);
                }
            }
        }
    }

?>