<?php
    require_once dirname(__FILE__).'/../functions/curlwrapper.php';

    class CurlWrapperTest extends PHPUnit_Framework_TestCase {
        function test__construct() {
            $curl = new CurlWrapper();
            if (!is_resource($curl->ch)) {
                $this->fail('Curl ch is not a resource');
            }
            foreach (['ch', 'last_errno', 'buffer', 'header', 'additional_header', 'http_status', 'default_tries_count',
                      'default_value', 'last_encoding'] as $key) {
                try {
                    $curl->{$key};
                } catch (Error $e) {
                    $this->fail('Exception error on get property '.$key);
                } catch (Exception $e) {
                    $this->fail('Exception exception on get property '.$key);
                }
            }
        }

        function testSet_option() {
            $data_set = [CURLOPT_RETURNTRANSFER => true, CURLOPT_URL => 'http://example.com/'];

            foreach ($data_set as $key => &$value) {
                $curl = new CurlWrapper();
                $curl->set_option($key, $value);
                $property = (new ReflectionClass($curl))->getProperty('_options');
                $property->setAccessible(true);
                $this->assertEquals($value, $property->getValue($curl)[$key]);
                $this->assertEquals($value, $curl->get_option($key));
            }
            $curl = new CurlWrapper();
            $property = (new ReflectionClass($curl))->getProperty('_options');
            $property->setAccessible(true);
            foreach ($data_set as $key => &$value) {
                $curl->set_option($key, $value);
                $this->assertEquals($value, $property->getValue($curl)[$key]);
                $this->assertEquals($value, $curl->get_option($key));
            }
        }

        function testSet_timeout() {
            $curl = new CurlWrapper();
            foreach ([-1, 0, 1, 10, 3600] as $value) {
                $curl->set_timeout($value);
                $this->assertEquals($value, $curl->get_option(CURLOPT_TIMEOUT));
            }
        }

        function testSet_verify_server() {
            // @hint Тут специально тестируется не curl, а установка опций
            $curl = new CurlWrapper();
            $curl->set_verify_server(null);
            $this->assertFalse($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            $curl->set_verify_server();
            $this->assertFalse($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            $curl->set_verify_server(null);
            $this->assertFalse($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            $curl->set_verify_server('');
            $this->assertTrue($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_server();
            $this->assertFalse($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_server('');
            $this->assertTrue($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_server('/tmp/foo.crt');
            $this->assertTrue($curl->get_option(CURLOPT_SSL_VERIFYPEER));
            $this->assertEquals('/tmp/foo.crt', $curl->get_option(CURLOPT_CAINFO));
        }

        function testSet_verify_client() {
            $curl = new CurlWrapper();
            $curl->set_verify_client(null);
            $this->assertEquals(0, $curl->get_option(CURLOPT_SSL_VERIFYHOST));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_client();
            $this->assertEquals(0, $curl->get_option(CURLOPT_SSL_VERIFYHOST));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_client('/tmp/foo.p12');
            $this->assertEquals(2, $curl->get_option(CURLOPT_SSL_VERIFYHOST));
            $this->assertEquals('/tmp/foo.p12', $curl->get_option(CURLOPT_SSLCERT));
            $this->assertEquals('', $curl->get_option(CURLOPT_SSLCERTPASSWD));
            //
            $curl = new CurlWrapper();
            $curl->set_verify_client('/tmp/foo.p12', 'password');
            $this->assertEquals(2, $curl->get_option(CURLOPT_SSL_VERIFYHOST));
            $this->assertEquals('/tmp/foo.p12', $curl->get_option(CURLOPT_SSLCERT));
            $this->assertEquals('password', $curl->get_option(CURLOPT_SSLCERTPASSWD));
        }

        function testGet_default_port_for_scheme() {
            foreach (['http' => 80, 'https' => 443, 'nyan' => -1, 'pasu' => -1,] as $scheme => &$port) {
                $this->assertEquals($port, CurlWrapper::get_default_port_for_scheme($scheme));
            }
        }

        /**
         * @covers CurlWrapper::get_effective_url
         * @covers CurlWrapper::load_page
         */
        function testGet_effective_url() {
            foreach (['http://example.com/' => 'http://example.com/',
                      'http://ya.ru/' => 'https://ya.ru/',
                      'https://yandex.ru/' => 'https://yandex.ru/',
                      'http://joyreactor.cc/' => 'http://joyreactor.cc/',
                      'https://lurkmore.to/' => 'https://lurkmore.to/',] as $original_url => $new_url) {
                $curl = new CurlWrapper();
                $curl->load_page($original_url);
                $this->assertTrue($curl->is_success(), sprintf('Can not load %s', $original_url));
                $this->assertEquals($new_url, $curl->get_effective_url());
            }

            $curl = new CurlWrapper();
            $curl->load_page('http://joyreactor.cc/login', '');
            $this->assertTrue($curl->is_success(), 'Can not load http://joyreactor.cc/login');
            $this->assertEquals('http://joyreactor.cc/login', $curl->get_effective_url());
        }

        /**
         * @covers CurlWrapper::load_page
         */
        function testLoad_page() {
            $dictionary = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '1'), ['.']);
            $reflection = new ReflectionMethod('CurlWrapper', 'curl_exec');
            $reflection->setAccessible(true);
            // Проверяем несуществующие домены
            foreach (['http', 'https'] as &$scheme) {
                foreach ([false, true] as &$u) {
                    if ($u) {
                        $curl = new CurlWrapper();
                        $curl2 = new CurlWrapper();
                    }
                    for ($i = 0; $i < 10; $i++) {
                        if (!$u) {
                            $curl = new CurlWrapper();
                            $curl2 = new CurlWrapper();
                        }
                        $domain = '';
                        for ($j = 0; $j < mt_rand(5, 10); $j++) {
                            $domain .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                        }
                        $hash = '';
                        for ($j = 0; $j < mt_rand(10, 20); $j++) {
                            $hash .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                        }
                        /** @noinspection PhpUndefinedVariableInspection */
                        $curl->default_value = $hash;
                        /** @noinspection PhpUndefinedVariableInspection */
                        $this->assertEquals($hash, $curl->load_page(sprintf('%s://%s/', $scheme, $domain)));
                        $this->assertFalse($curl->is_success());
                        // low lvl test
                        /**
                         * @var CurlWrapper $curl2
                         */
                        $curl2->set_option(CURLOPT_URL, $domain);
                        $ret = $reflection->invoke($curl2);
                        $this->assertInternalType('string', $ret);
                        $this->assertEquals('', $ret);
                        $this->assertNotEquals($curl2->last_errno, 0);
                    }
                }
            }

            foreach ([false, true] as &$u) {
                foreach (['http://example.com', 'https://lurkmore.to/'] as &$url) {
                    $curl = new CurlWrapper();
                    $curl2 = new CurlWrapper();
                    if ($u) {
                        $curl->additional_header = 'nyanpasu';
                        $curl2->additional_header = 'nyanpasu';
                    }
                    $curl->load_page($url);
                    $this->assertTrue($curl->is_success(), sprintf('Can not load %s', $url));
                    $this->assertEquals('', $curl->additional_header);

                    // low lvl test
                    $curl2->set_option(CURLOPT_URL, $url);
                    $ret = $reflection->invoke($curl2);
                    $this->assertInternalType('string', $ret);
                    $this->assertNotEquals('', $ret);
                    $this->assertEquals($curl2->last_errno, 0);
                    $this->assertEquals($curl2->http_status, 200);
                }
            }
            foreach ([false, true] as &$u) {
                foreach (['http://google.ru', 'https://google.ru'] as &$url) {
                    $curl = new CurlWrapper();
                    $curl2 = new CurlWrapper();
                    if ($u) {
                        $curl->additional_header = 'nyanpasu';
                        $curl2->additional_header = 'nyanpasu';
                    }
                    $curl->load_page($url);
                    $this->assertTrue($curl->is_success(), sprintf('Can not load %s', $url));
                    $this->assertNotEquals('', $curl->additional_header);
                    $this->assertNotEquals('nyanpasu', $curl->additional_header);

                    // low lvl test
                    $curl2->set_option(CURLOPT_URL, $url);
                    $ret = $reflection->invoke($curl2);
                    $this->assertInternalType('string', $ret);
                    $this->assertNotEquals('', $ret);
                    $this->assertEquals($curl2->last_errno, 0);
                    $this->assertEquals($curl2->http_status, 200);
                }
            }

            // Покрываем кейс "загрузилось не с первого раза"
            $curl = new CurlWrapperNoCurl();
            $count = 1;
            $curl->_closure = function () use (&$count, $curl) {
                if ($count > 0) {
                    $count--;
                    $curl->last_errno = 7;
                    $curl->http_status = 0;

                    return '';
                } else {
                    $curl->last_errno = 0;
                    $curl->http_status = 200;

                    return "HTTP/1.0 200 OK\r\n\r\nNyanpasu";
                }
            };
            $this->assertEquals('', $curl->load_page('http://example.com/', null, 1));
            $this->assertFalse($curl->is_success());
            /** @noinspection PhpUnusedLocalVariableInspection */
            $count = 1;
            $this->assertNotEquals('', $curl->load_page('http://example.com/', null, 2));
            $this->assertTrue($curl->is_success());
            for ($i = 0; $i < 100; $i++) {
                /** @noinspection PhpUnusedLocalVariableInspection */
                $count = mt_rand(0, 4);
                $this->assertNotEquals('', $curl->load_page('http://example.com/', null, 5));
                $this->assertTrue($curl->is_success());
            }

            // Покрываем GZIP
            $curl = new CurlWrapperNoCurl();
            $curl->need_buf = sprintf("HTTP/1.0 200 OK\r\nContent-encoding: gzip\r\n\r\n%s",
                zlib_encode('nyanpasu', ZLIB_ENCODING_GZIP));
            $this->assertEquals('nyanpasu', $curl->load_page('http://example.com/'));
            $this->assertTrue($curl->is_success());
            $curl->need_buf = "HTTP/1.0 200 OK\r\nContent-encoding: gzip\r\n\r\nfoobar";
            $this->assertEquals('foobar', $curl->load_page('http://example.com/'));
            $this->assertTrue($curl->is_success());

            // additional_header, выставляется ли он через прокси
            for ($i = 0; $i < 30; $i++) {
                if (mt_rand(0, 4) == 0) {
                    $curl = new CurlWrapperNoCurl();
                }
                $curl->need_buf = '';
                $count = mt_rand(1, 5);
                $answer = '';
                $answer_len = mt_rand(5, 10);
                for ($j = 0; $j < $answer_len; $j++) {
                    $answer .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                }
                for ($j = 0; $j < $count; $j++) {
                    if (mt_rand(0, 1) == 0) {
                        $curl->need_buf .= "HTTP/1.0 100 Connect\r\n\r\n";
                    } else {
                        $curl->need_buf .= sprintf("HTTP/1.0 %d Redirect\r\nLocation: /nyan\r\n\r\n", mt_rand(300, 302));
                    }
                }
                $curl->need_buf .= "HTTP/1.0 200 OK\r\n\r\n".$answer;

                $this->assertEquals($answer, $curl->load_page('http://example.com/'));
                $this->assertTrue($curl->is_success());
                $this->assertNotEquals('', $curl->additional_header);
                $this->assertEquals($count, count(explode("\r\n\r\n", $curl->additional_header)));
            }

            // @todo Каким-нибудь образом покрыть прокси
        }

        function testCurl_exec() {
            $reflection = new ReflectionMethod('CurlWrapper', 'curl_exec');
            $reflection->setAccessible(true);
            $ch = new CurlWrapper();
            foreach (['http://example.com/', 'https://google.com/'] as $domain) {
                $ch->set_option(CURLOPT_URL, $domain);
                $ret = $reflection->invoke($ch);
                $this->assertInternalType('string', $ret);
                $this->assertNotEquals('', $ret);
                $this->assertEquals($ch->last_errno, 0);
                $this->assertEquals($ch->http_status, 200);
            }
            $dictionary = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '1'), ['.']);
            // Проверяем несуществующие домены
            foreach (['http', 'https'] as &$scheme) {
                foreach ([false, true] as &$u) {
                    if ($u) {
                        $curl = new CurlWrapper();
                    }
                    for ($i = 0; $i < 10; $i++) {
                        if (!$u) {
                            $curl = new CurlWrapper();
                        }
                        $domain = '';
                        for ($j = 0; $j < mt_rand(5, 10); $j++) {
                            $domain .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                        }
                        $hash = '';
                        for ($j = 0; $j < mt_rand(10, 20); $j++) {
                            $hash .= $dictionary[mt_rand(0, count($dictionary) - 1)];
                        }
                        /** @noinspection PhpUndefinedVariableInspection */
                        $curl->default_value = $hash;
                        /** @noinspection PhpUndefinedVariableInspection */
                        $this->assertEquals($hash, $curl->load_page(sprintf('%s://%s/', $scheme, $domain)));
                        $this->assertFalse($curl->is_success());
                    }
                }
            }
        }

        function testIs_success() {
            $curl = new CurlWrapper();
            $curl->last_errno = 0;
            $curl->http_status = 200;
            $this->assertTrue($curl->is_success());

            $curl = new CurlWrapper();
            $curl->last_errno = 0;
            $curl->http_status = 404;
            $this->assertFalse($curl->is_success());

            $curl = new CurlWrapper();
            $curl->last_errno = 0;
            $curl->http_status = 302;
            $this->assertFalse($curl->is_success());

            $curl = new CurlWrapper();
            $curl->last_errno = 0;
            $curl->http_status = 100;
            $this->assertFalse($curl->is_success());

            $curl = new CurlWrapper();
            $curl->last_errno = -1;
            $curl->http_status = 200;
            $this->assertFalse($curl->is_success());

            $curl = new CurlWrapper();
            $curl->last_errno = 1;
            $curl->http_status = 200;
            $this->assertFalse($curl->is_success());
        }

        function testUnparse_url() {
            $data_set = [
                ['params' => ['host' => 'example.com'],
                 'url' => 'http://example.com/'],
                ['params' => ['scheme' => 'http', 'host' => 'example.com'],
                 'url' => 'http://example.com/'],
                ['params' => ['scheme' => 'https', 'host' => 'example.com'],
                 'url' => 'https://example.com/'],
                ['params' => ['scheme' => 'http', 'host' => 'example.com', 'port' => 80],
                 'url' => 'http://example.com/'],
                ['params' => ['host' => 'example.com', 'port' => 80],
                 'url' => 'http://example.com/'],
                ['params' => ['scheme' => 'http', 'host' => 'example.com', 'port' => 8080],
                 'url' => 'http://example.com:8080/'],
                ['params' => ['scheme' => 'https', 'host' => 'example.com', 'port' => 443],
                 'url' => 'https://example.com/'],
                ['params' => ['host' => 'example.com', 'user' => 'nyan'],
                 'url' => 'http://nyan@example.com/'],
                ['params' => ['host' => 'example.com', 'user' => 'nyan', 'pass' => 'pasu'],
                 'url' => 'http://nyan:pasu@example.com/'],
                ['params' => ['host' => 'example.com', 'path' => ''],
                 'url' => 'http://example.com/'],
                ['params' => ['host' => 'example.com', 'path' => '/'],
                 'url' => 'http://example.com/'],
                ['params' => ['host' => 'example.com', 'path' => '/abc'],
                 'url' => 'http://example.com/abc'],
                ['params' => ['host' => 'example.com', 'query' => 'nyan=pasu'],
                 'url' => 'http://example.com/?nyan=pasu'],
                ['params' => ['host' => 'example.com', 'query' => 'nyan=pasu', 'fragment' => 'foobar'],
                 'url' => 'http://example.com/?nyan=pasu#foobar'],
                ['params' => ['host' => 'example.com', 'fragment' => 'foobar'],
                 'url' => 'http://example.com/#foobar'],
                ['params' => ['host' => 'example.com', 'fragment' => 'foobar', 'path' => ''],
                 'url' => 'http://example.com/#foobar'],
                ['params' => ['host' => 'example.com', 'fragment' => 'foobar', 'path' => '/'],
                 'url' => 'http://example.com/#foobar'],
            ];
            foreach ($data_set as &$datum) {
                $this->assertEquals($datum['url'], CurlWrapper::unparse_url($datum['params']));
            }
        }

    }


    class CurlWrapperNoCurl extends CurlWrapper {
        /**
         * @var Callable|null
         */
        var $_closure = null;

        var $need_buf = '';
        var $need_last_errno = 0;
        var $need_http_status = 200;

        /**
         * @return string
         */
        protected function curl_exec() {
            if ($this->_closure !== null) {
                return call_user_func($this->_closure);
            } else {
                $this->last_errno = $this->need_last_errno;
                if ($this->last_errno !== 0) {
                    return '';
                }

                $this->http_status = $this->need_http_status;

                return $this->need_buf;
            }
        }

    }

?>