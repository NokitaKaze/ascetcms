<?php
    require_once dirname(__FILE__).'/KVAbstractTest.php';
    require_once dirname(__FILE__).'/../functions/iascetkeyvaluestorage.php';
    require_once dirname(__FILE__).'/../functions/keyvaluestoragefile.php';

    class KeyValueStorageFileTest extends KVAbstractTest {
        private $_folder;

        function setUp() {
            parent::setUp();
            $this->_folder = sys_get_temp_dir().'/ascetcms_test_'.self::generate_hash();
            mkdir($this->_folder);
        }

        function tearDown() {
            chmod($this->_folder, 7 << 6);
            system(sprintf('rm -rf "%s"', str_replace('"', '\\"', $this->_folder)));
            parent::tearDown();
        }

        function test__construct() {
            $kv = new KeyValueStorageFile((object) ['storage_type' => KeyValueStorageFile::StorageTemporary,]);
            $folder1 = $kv->folder;
            $kv = new KeyValueStorageFile((object) ['storage_type' => KeyValueStorageFile::StoragePersistent,]);
            $this->assertNotEquals($folder1, $kv->folder);

            //
            $u = false;
            try {
                new KeyValueStorageFile((object) ['storage_type' => mt_rand(3, 10)]);
            } catch (Exception $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageFile did not throw exception on malformed settings');
            }
        }

        function testSet_value() {
            $this->set_value_sub(['multi' => false]);
            $this->set_value_sub(['multi' => false]);// @hint Это не ошибка, этот тест прогоняется дважды
            $this->set_value_sub(['multi' => true]);
        }

        /**
         * @param array $params
         *
         * @return object
         */
        protected function get_full_params($params) {
            return (object) array_merge(['folder' => $this->_folder], $params);
        }

        /**
         * @param array $params
         */
        function set_value_sub($params) {
            parent::set_value_sub($params);
            /**
             * @var KeyValueStorageFile $kv
             */
            $kv = $this->_last_kv;
            $reflection_lock = new ReflectionProperty('KeyValueStorageFile', '_locks');
            $reflection_lock->setAccessible(true);

            //
            touch($kv->get_filename('key').'.tmp');
            chmod($kv->get_filename('key').'.tmp', 0);
            $u = false;
            $exception_code1 = null;
            try {
                $kv->set_value('key', 'value');
            } catch (KeyValueException $e) {
                $u = true;
                /** @noinspection PhpUnusedLocalVariableInspection */
                $exception_code1 = $e->getCode();
                $this->assertLockNotAcquired($reflection_lock->getValue($kv), 'key');
            }
            if (!$u) {
                $this->fail('KeyValueStorageFile::set_value did not throw Exception on readonly temporary db filename');
            }
            @unlink($kv->get_filename('key').'.tmp');
            // @todo Покрыть тестами неполучившийся rename
            /*
            $kv = new KV2((object) ['folder' => $this->_folder]);
            $u = false;
            touch($kv->get_filename('key').'.tmp');
            chmod(dirname($kv->get_filename('key')), 0);
            try {
                $kv->set_value('key', 'value');
            } catch (KeyValueException $e) {
                $u = true;
                $this->assertNotEquals($exception_code1, $e->getCode());
                if (($reflection_lock->getValue($kv)!==null) and $reflection_lock->getValue($kv)->lock_acquired){
                    $this->fail('Lock has not been released on KeyValueException on KeyValueStorageFile::set_value');
                }
            }
            if (!$u) {
                $this->fail('KeyValueStorageFile::set_value did not throw Exception on readonly db filename');
            }
            chmod(dirname($kv->get_filename('key')), 7 << 6);
            @unlink($kv->get_filename('key').'.tmp');
            */
        }

        /**
         * @covers KeyValueStorageFile::get_filename
         * @covers KeyValueStorageFile::get_mutex_key_name
         * @covers KeyValueStorageFile::delete_value
         */
        function testGet_filename() {
            $files = [];
            $mutex_keys = [];
            foreach ([$this->_folder, '/dev/shm'] as $folder) {
                foreach (['', 'nya_'] as $prefix) {
                    foreach (['historia', 'christa'] as $key) {
                        foreach ([false, true] as $multi) {
                            $kv = new KeyValueStorageFile((object) [
                                'folder' => $folder,
                                'prefix' => $prefix,
                                'multi_folder' => $multi,
                            ]);
                            $filename = $kv->get_filename($key);
                            $this->assertNotContains($filename, $files);
                            $files[] = $filename;

                            //
                            $mutex = $kv->get_mutex_key_name($key);
                            $this->assertNotContains($mutex, $mutex_keys);
                            $mutex_keys[] = $mutex;

                            //
                            $kv1 = new KeyValueStorageFile((object) [
                                'folder' => $folder,
                                'prefix' => $prefix,
                                'multi_folder' => $multi,
                            ]);
                            $this->assertEquals($filename, $kv1->get_filename($key));
                            $this->assertEquals($mutex, $kv1->get_mutex_key_name($key));
                        }
                    }
                }
            }
        }

        function testGet_value_full_clear() {
            $kv = new KeyValueStorageFile((object) ['folder' => $this->_folder]);
            $key = mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z')).mt_rand(ord('a'), ord('z'));
            $filename = $kv->get_filename($key);

            $reflection = new ReflectionMethod('KeyValueStorageFile', 'get_value_full_clear');
            $reflection->setAccessible(true);
            $lock = new ReflectionProperty('KeyValueStorageFile', '_locks');
            $lock->setAccessible(true);
            $this->assertNull($reflection->invoke($kv, $key));
            $this->assertLockNotAcquired($lock->getValue($kv), $key);
            file_put_contents($filename, serialize('nyanpasu'), LOCK_EX);
            //
            chmod($filename, 0);
            $this->assertNull($reflection->invoke($kv, $key));
            $this->assertLockNotAcquired($lock->getValue($kv), $key);
            //
            chmod($filename, 6 << 6);
            $this->assertLockNotAcquired($lock->getValue($kv), $key);
            $this->assertEquals('nyanpasu', $reflection->invoke($kv, $key));
            //
            file_put_contents($filename, serialize(false), LOCK_EX);
            $this->assertLockNotAcquired($lock->getValue($kv), $key);
            $this->assertFalse($reflection->invoke($kv, $key));
            //
            file_put_contents($filename, 'nyanpasu', LOCK_EX);
            $this->assertNull($reflection->invoke($kv, $key));
            $this->assertLockNotAcquired($lock->getValue($kv), $key);

            unlink($filename);
        }

        function testMultiFolder() {
            $storage = new KeyValueStorageFile((object) ['multi_folder' => true, 'folder' => $this->_folder]);
            $storage1 = new KeyValueStorageFile((object) ['folder' => $this->_folder]);
            $this->assertNull($storage->get_value('key', null));
            foreach (['key', 'foobar', 'nyanpasu'] as $key) {
                $s1 = $storage->get_filename($key);
                $s2 = $storage1->get_filename($key);
                $this->assertInternalType('string', $s1);
                $this->assertInternalType('string', $s2);
                $this->assertEquals(strlen($s2) + 6, strlen($s1));
                $this->assertRegExp('_/[0-9a-f]{2,2}/[0-9a-f]{2,2}/_', substr($s1, strlen($this->_folder)));

                $value = self::generate_hash();
                $storage->set_value($key, $value);
                $this->assertEquals($value, $storage->get_value($key, null));
            }
        }

        function testCreate_path() {
            $reflection = new ReflectionMethod('KeyValueStorageFile', 'create_path');
            $reflection->setAccessible(true);

            $root_folder = sys_get_temp_dir().'/ascetcms_test_'.self::generate_hash();
            $key = self::generate_hash();
            $hash = hash('sha512', $key);

            $u = false;
            try {
                $reflection->invoke(null, $root_folder.'/nyanpasu', true, $key);
            } catch (KeyValueException $e) {
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageFile::create_path did not throw Exception on non existed root folder');
            }
            if (!mkdir($root_folder)) {
                throw new Exception('Can not create root folder '.$root_folder);
            }

            system(sprintf('rm -rf "%s"', addslashes($root_folder)));
            foreach ([false, true] as &$u1) {
                mkdir($root_folder);
                $folder = $root_folder.'/nyanpasu';
                if ($u1) {
                    mkdir($folder);
                }
                $reflection->invoke(null, $folder, true, $key);
                $this->assertFileExists($folder);

                $folder .= '/'.substr($hash, 0, 2);
                $this->assertFileExists($folder);
                $folder .= '/'.substr($hash, 2, 2);
                $this->assertFileExists($folder);
                system(sprintf('rm -rf "%s"', addslashes($root_folder)));
            }
        }
    }

?>