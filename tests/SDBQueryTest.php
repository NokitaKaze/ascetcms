<?php
    require_once dirname(__FILE__).'/../functions/ascetcmsengine.php';
    require_once dirname(__FILE__).'/../functions/sdbquery.php';

    class SDBQueryTest extends PHPUnit_Framework_TestCase {
        function testGet_query() {
            foreach ([null, '*', 'a', 'a.b,c', ['a.b', 'c']] as &$select) {
                foreach (['gau', 'gau as gaw', ['nyan as t', 'pasu as s']] as &$from) {
                    foreach ([false, [null, []], ['true', []], [['true', 'false'], []], [[], []]] as &$where_collect) {
                        foreach ([false, 'nyan', 'pasu desc'] as &$order) {
                            foreach ([false, null, 1, 10] as &$limit) {
                                foreach ([false, null, 1, 10] as &$offset) {
                                    foreach ([false, '', 'for update'] as &$last_query_block) {
                                        $query = new SDBQuery();
                                        $regexp = 'select\\s+';
                                        $regexp2 = null;
                                        if ($select !== false) {
                                            $query->set_select($select);
                                        }
                                        if ($select === false) {
                                            throw new Exception('Code flow exception');
                                        } elseif ($select == '') {
                                            $regexp .= '\\*\\s+';
                                        } elseif (is_array($select)) {
                                            $a = $select;
                                            foreach ($a as &$v) {
                                                $v = sad_safe_reg($v);
                                            }
                                            $regexp .= '\\s+'.implode('\\s*,\\s*', $a);
                                        } elseif (is_string($select)) {
                                            $regexp .= '\\s+'.sad_safe_reg($select);
                                        } else {
                                            throw new Exception('Code flow exception');
                                        }
                                        $query->set_from($from);
                                        if (is_array($from)) {
                                            $a = $from;
                                            foreach ($a as &$v) {
                                                $v = sad_safe_reg($v);
                                            }
                                            $regexp .= '\\s+from\\s+'.implode('\\s*,\\s*', $a);
                                        } elseif (is_string($from)) {
                                            $regexp .= '\\s+from\\s+'.sad_safe_reg($from);
                                        } else {
                                            throw new Exception('Code flow exception');
                                        }

                                        if ($where_collect !== false) {
                                            $query->set_where($where_collect[0], $where_collect[1]);
                                            if ($where_collect[0] != null) {
                                                $regexp .= '\\s+where\\s+(.+?)';
                                                // @todo добавить сюда скобочки
                                                if (is_array($where_collect[0])) {
                                                    $regexp2 = '\\(\\s*'.implode('\\s*\\)\\s*and\\s*\\(\\s*', $where_collect[0]).
                                                               '\\s*\\)';
                                                } else {
                                                    $regexp2 = $where_collect[0];
                                                }
                                            }
                                        }
                                        if ($order !== false) {
                                            $query->set_order($order);
                                            $regexp .= '\\s+order\\s+by\\s+'.sad_safe_reg($order);
                                        }
                                        if ($limit !== false) {
                                            $query->set_limit($limit);
                                        }
                                        if ($offset !== false) {
                                            $query->set_offset($offset);
                                        }
                                        if (($limit != null) or ($offset != 0)) {
                                            if ($offset == 0) {
                                                $regexp .= '\\s+limit\\s+('.$limit.'|0\\s*,\\s*'.$limit.')';
                                            } elseif (($limit === null) or ($limit === false)) {
                                                $regexp .= '\\s+limit\\s+'.$offset.'\\s*,\\s*[0-9]+';
                                            } else {
                                                $regexp .= '\\s+limit\\s+'.$offset.'\\s*,\\s*'.$limit;
                                            }
                                        }
                                        if ($last_query_block !== false) {
                                            $query->set_last_query_block($last_query_block);
                                            $regexp .= '\\s+'.sad_safe_reg($last_query_block);
                                        }
                                        list($string, $params) = $query->get_query();
                                        $regexp .= '\\s*;';
                                        do {
                                            $o = $regexp;
                                            $regexp = str_replace('\\s+\\s+', '\\s+', $regexp);
                                            $regexp = str_replace('\\s+\\s*', '\\s+', $regexp);
                                            $regexp = str_replace('\\s*\\s+', '\\s+', $regexp);
                                            $regexp = str_replace('\\s*\\s*', '\\s*', $regexp);
                                        } while ($o != $regexp);
                                        $full_regexp = '/^'.str_replace('\\s+;', '\\s*;', $regexp).'$/si';
                                        unset($o, $regexp);
                                        $this->assertRegExp($full_regexp, $string);
                                        if ($regexp2 !== null) {
                                            preg_match($full_regexp, $string, $a);
                                            unset($a[0]);
                                            $u = false;
                                            foreach ($a as $s) {
                                                if (preg_match('_'.$regexp2.'_si', $s)) {
                                                    $u = true;
                                                    break;
                                                }
                                            }
                                            if (!$u) {
                                                $this->fail('Where does not regexped');
                                            }
                                        }
                                        $this->assertInternalType('array', $params);
                                        if ($where_collect !== false) {
                                            $this->assertEquals(count($where_collect[1]), count($params));
                                            foreach ($where_collect[1] as $id => &$value) {
                                                $this->assertEquals($value, $params[$id]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        function testAdditional() {
            $this->assertNull(SDBQuery::get_select_block(false));
            $this->assertNull(SDBQuery::get_from_block(false));
            $this->assertNull(SDBQuery::get_where_block(false));
        }
    }

?>