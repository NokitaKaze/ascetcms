<?php
    require_once dirname(__FILE__).'/../functions/ascetcmsengine.php';

    class AscetCMSEngineTest extends PHPUnit_Framework_TestCase {
        function testGenerate_hash() {
            for ($i = 0; $i < 100; $i++) {
                $len = mt_rand(10, 200);
                $value = AscetCMSEngine::generate_hash($len);
                $this->assertInternalType('string', $value);
                $this->assertRegExp('_^[0-9a-zA-Z]{'.$len.','.$len.'}$_', $value);

                $value = AscetCMSEngine::generate_hash_trivial($len);
                $this->assertRegExp('_^[0-9a-zA-Z]{'.$len.','.$len.'}$_', $value);

                $value = AscetCMSEngine::generate_hash_openssl($len);
                $this->assertRegExp('_^[0-9a-zA-Z]{'.$len.','.$len.'}$_', $value);
            }
        }

        function testSad_safe_reg() {
            foreach ([
                         ['nyanpasu', 'nyanpasu'],
                         ['example.com', 'example\\.com'],
                         ['9-8', '9\\-8'],
                         ['[foo]bar', '\\[foo\\]bar'],
                         ['[0-8]bar', '\\[0\\-8\\]bar'],
                         ['{foobar}', '\\{foobar\\}'],
                         ['(foobar)', '\\(foobar\\)'],
                         ['foo * bar', 'foo \\* bar'],
                         ['foo? bar+', 'foo\\? bar\\+'],
                     ] as $v) {
                $this->assertEquals($v[1], sad_safe_reg($v[0]));
                preg_match('_'.sad_safe_reg($v[0]).'_', '');
            }
        }

        function testSad_safe_domain() {
            foreach ([
                         ['www.example.com', 'example.com'],
                         ['example.com', 'example.com'],
                         ['www.com', 'www.com'],
                         ['www.example.com:80', 'example.com'],
                         ['example.com:80', 'example.com'],
                         ['www.com:80', 'www.com'],
                         ['www.example.com:81', 'example.com'],
                         ['example.com:8080', 'example.com'],
                         ['www.com:443', 'www.com'],
                     ] as $v) {
                $this->assertEquals($v[1], sad_safe_domain($v[0]));
            }
        }
    }

?>