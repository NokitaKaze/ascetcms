<?php
    require_once dirname(__FILE__).'/../functions/smartmutex.php';

    class SmartMutexTest extends PHPUnit_Framework_TestCase {
        function test__construct() {
            $values = [];
            foreach (['nyan', 'pasu'] as &$name1) {
                foreach ([SmartMutex::SmartMutex_Domain,
                          SmartMutex::SmartMutex_Directory,
                          SmartMutex::SmartMutex_Server] as &$type1) {
                    foreach ([null, '/nyan/pasu', '/foo/bar'] as &$folder1) {
                        $mutex = new SmartMutex($name1, $type1, $folder1);
                        $this->assertNotContains($mutex->filename, $values);
                    }
                }
            }
            unset($values);
        }

        function testGetDomainString() {
            $server_original = $_SERVER;
            if (!isset($_SERVER['HTTP_HOST'])) {
                $_SERVER['HTTP_HOST'] = 'example.com';
                $this->assertEquals('example.com', SmartMutex::getDomainString());
            } else {
                $this->assertNotEquals('', SmartMutex::getDomainString());
            }
            if (!isset($_SERVER['HOSTNAME'])) {
                $_SERVER['HOSTNAME'] = 'example.com';
            }
            $this->assertNotEquals('', SmartMutex::getDomainString());
            $_SERVER['HTTP_HOST'] = 'example.com';
            foreach (['HTTP_HOST', 'HOSTNAME', 'SCRIPT_NAME'] as $id => $key) {
                $this->assertNotEquals('', SmartMutex::getDomainString());
                unset($_SERVER[$key]);
            }
            $this->assertNotNull(SmartMutex::getDomainString());
            $this->assertNotEquals('', SmartMutex::getDomainString());

            $_SERVER = $server_original;
        }

        function testGetDirectoryString() {
            $server_original = $_SERVER;
            $_SERVER['DOCUMENT_ROOT'] = '/dev/shm/nyanpasu';
            $this->assertEquals('/dev/shm/nyanpasu', SmartMutex::getDirectoryString());
            $_SERVER['DOCUMENT_ROOT'] = '';
            $this->assertNotEquals('', SmartMutex::getDirectoryString());
            $_SERVER['DOCUMENT_ROOT'] = '/dev/shm/nyanpasu';
            foreach (['DOCUMENT_ROOT', 'PWD', 'SCRIPT_NAME'] as $id => $key) {
                $this->assertNotEquals('', SmartMutex::getDirectoryString());
                unset($_SERVER[$key]);
            }
            $this->assertNotNull(SmartMutex::getDirectoryString());
            $this->assertNotEquals('', SmartMutex::getDomainString());

            $_SERVER = $server_original;
        }

        function testGet_lock() {
            do {
                $folder = sys_get_temp_dir().'/ascetcms_test_';
                for ($i = 0; $i < 10; $i++) {
                    $folder .= chr(mt_rand(ord('a'), ord('z')));
                }
            } while (file_exists($folder));
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);

            //
            $used_error_codes = [];
            $u = false;
            try {
                $mutex->get_lock(1);
            } catch (SmartMutexException $e) {
                $u = true;
                $used_error_codes[] = $e->getCode();
            }
            if (!$u) {
                $this->fail('SmartMutex did not throw exception on non existed folder');
            }
            //
            mkdir($folder);
            chmod($folder, 0);
            $u = false;
            try {
                $mutex->get_lock(1);
            } catch (SmartMutexException $e) {
                $u = true;
            }
            if (!$u) {
                @rmdir($folder);
                $this->fail('SmartMutex did not throw exception on non writable folder');
            }
            //
            chmod($folder, 7 << 6);
            touch($mutex->filename);
            chmod($mutex->filename, 0);
            $u = false;
            try {
                $mutex->get_lock(1);
            } catch (SmartMutexException $e) {
                $u = true;
                $this->assertNotContains($e->getCode(), $used_error_codes);
                $used_error_codes[] = $e->getCode();
            }
            if (!$u) {
                @unlink($mutex->filename);
                @rmdir($folder);
                $this->fail('SmartMutex did not throw exception on non writable file');
            }
            chmod($mutex->filename, 7 << 6);
            //
            $ts1 = microtime(true);
            $this->assertTrue($mutex->get_lock(0));
            $reflection = new ReflectionProperty($mutex, '_file_handler');
            $reflection->setAccessible(true);
            $this->assertNotFalse($reflection->getValue($mutex));
            $this->assertNotNull($reflection->getValue($mutex));
            $this->assertTrue($mutex->lock_acquired);
            $this->assertTrue($mutex->get_lock(0));
            $this->assertNotNull($mutex->lock_acquired_time);
            $this->assertLessThan(microtime(true), $mutex->lock_acquired_time);
            $this->assertGreaterThan($ts1, $mutex->lock_acquired_time);

            //
            $mutex1 = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            $this->assertFalse($mutex1->get_lock(0.01));
            $this->assertFalse($mutex1->lock_acquired);
            //
            $mutex1 = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            $this->assertFalse($mutex1->get_lock(2));

            //
            for ($i = 0; $i < 5; $i++) {
                $r = mt_rand(10, 50) / 10;
                $ts1 = microtime(true);
                $this->assertFalse($mutex1->get_lock($r));
                $ts2 = microtime(true);
                $this->assertGreaterThanOrEqual($r, $ts2 - $ts1);
                $this->assertLessThanOrEqual($r + 0.1, $ts2 - $ts1);
            }

            // @todo Проверить get_lock(-1), это невозможно в одном потоке

            @unlink($mutex->filename);
            @rmdir($folder);

            $filename = tempnam(sys_get_temp_dir(), 'ascetcms_mutex_test_');
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $filename);
            $u = false;
            try {
                $mutex->get_lock();
            } catch (SmartMutexException $e) {
                $u = true;
                $this->assertNotContains($e->getCode(), $used_error_codes);
                $used_error_codes[] = $e->getCode();
            }
            unlink($filename);
            if (!$u) {
                $this->fail('SmartMutex did not throw exception on non writable file');
            }
        }

        function testRelease_lock() {
            do {
                $folder = sys_get_temp_dir().'/ascetcms_test_';
                for ($i = 0; $i < 10; $i++) {
                    $folder .= chr(mt_rand(ord('a'), ord('z')));
                }
            } while (file_exists($folder));
            mkdir($folder);
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            $this->assertTrue($mutex->get_lock(0));

            $mutex->release_lock();
            $reflection = new ReflectionProperty($mutex, '_file_handler');
            $reflection->setAccessible(true);
            $this->assertFalse($mutex->lock_acquired);
            $this->assertFalse($reflection->getValue($mutex));
            $this->assertFileExists($mutex->filename);

            $mutex = new SmartMutex('pasu', SmartMutex::SmartMutex_Server, $folder);
            $this->assertTrue($mutex->get_lock(0));
            $this->assertTrue($mutex->get_lock(0));
            $mutex->delete_on_release = true;
            $mutex->release_lock();
            $this->assertFileNotExists($mutex->filename);

            @unlink($mutex->filename);
            @rmdir($folder);
        }

        function testIs_free() {
            do {
                $folder = sys_get_temp_dir().'/ascetcms_test_';
                for ($i = 0; $i < 10; $i++) {
                    $folder .= chr(mt_rand(ord('a'), ord('z')));
                }
            } while (file_exists($folder));
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            //
            $u = false;
            $used_error_codes = [];
            try {
                $mutex->is_free();
            } catch (SmartMutexException $e) {
                $u = true;
                $used_error_codes[] = $e->getCode();
            }
            if (!$u) {
                $this->fail('SmartMutex did not throw exception on non existed folder');
            }
            //
            mkdir($folder);
            chmod($folder, 0);
            $u = false;
            try {
                $mutex->is_free();
            } catch (SmartMutexException $e) {
                $u = true;
                $this->assertNotContains($e->getCode(), $used_error_codes);
                $used_error_codes[] = $e->getCode();
            }
            if (!$u) {
                @rmdir($folder);
                $this->fail('SmartMutex did not throw exception on non writable folder');
            }
            //
            chmod($folder, 7 << 6);
            touch($mutex->filename);
            chmod($mutex->filename, 0);
            $u = false;
            try {
                $mutex->is_free();
            } catch (SmartMutexException $e) {
                $u = true;
                $this->assertNotContains($e->getCode(), $used_error_codes);
                $used_error_codes[] = $e->getCode();
            }
            if (!$u) {
                @unlink($mutex->filename);
                @rmdir($folder);
                $this->fail('SmartMutex did not throw exception on non writable file');
            }
            chmod($mutex->filename, 7 << 6);

            //
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            $this->assertTrue($mutex->is_free());
            $this->assertFileExists($mutex->filename);
            $mutex->get_lock(0);
            $this->assertFalse($mutex->is_free());
            $mutex1 = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $folder);
            $this->assertFalse($mutex1->is_free());
            $mutex->release_lock();
            $this->assertTrue($mutex->is_free());
            unlink($mutex->filename);

            //
            @unlink($mutex->filename);
            @rmdir($folder);

            $filename = tempnam(sys_get_temp_dir(), 'ascetcms_mutex_test_');
            $mutex = new SmartMutex('nyan', SmartMutex::SmartMutex_Server, $filename);
            $u = false;
            try {
                $mutex->is_free();
            } catch (SmartMutexException $e) {
                $u = true;
                $this->assertNotContains($e->getCode(), $used_error_codes);
                $used_error_codes[] = $e->getCode();
            }
            unlink($filename);
            if (!$u) {
                $this->fail('SmartMutex did not throw exception on non writable file');
            }
        }
    }

?>