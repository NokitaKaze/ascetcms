<?php
    require_once dirname(__FILE__).'/KVAbstractTest.php';
    require_once dirname(__FILE__).'/../functions/iascetkeyvaluestorage.php';
    require_once dirname(__FILE__).'/../functions/keyvaluestorageredis.php';

    class KeyValueStorageRedisTest extends KVAbstractTest {
        private static $_test_server_post = null;

        private $_folder;

        function setUp() {
            parent::setUp();
            $this->_folder = sys_get_temp_dir().'/ascetcms_test_'.self::generate_hash();
            mkdir($this->_folder);
        }

        function tearDown() {
            chmod($this->_folder, 7 << 6);
            system(sprintf('rm -rf "%s"', str_replace('"', '\\"', $this->_folder)));
            parent::tearDown();
        }

        static function setUpBeforeClass() {
            parent::setUpBeforeClass();
            self::$_test_server_post = mt_rand(60000, 65500);
            exec(sprintf('php %s/TestRedisServer.php -p %d -w 300 >/dev/null 2>&1 &', __DIR__, self::$_test_server_post));
        }

        static function tearDownAfterClass() {
            self::get_redis()->redis_send("shutdown");
            parent::tearDownAfterClass();
        }

        private static function set_test_server_setting($settings) {
            file_put_contents(__DIR__.'/redis_server.dat', json_encode((object) $settings));
        }

        function testFolder() {
            foreach ([false, true] as $u) {
                foreach ([$this->_folder, null] as $folder) {
                    $this->set_value_sub_low_level(['folder' => $folder, 'multi_folder_mutex' => $u,]);
                }
            }
        }

        function testLazy_init() {
            $reflection = new ReflectionMethod('KeyValueStorageRedis', 'lazy_init');
            $reflection->setAccessible(true);
            //
            $kv = new KeyValueStorageRedis((object) ['port' => mt_rand(60000, 65534)]);
            $u = false;

            try {
                $reflection->invoke($kv);
            } catch (KeyValueException $e) {
                $code1 = $e->getCode();
                $u = true;
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis does not throw Exception on closed port');
            }
            //
            $kv = new KeyValueStorageRedis((object) ['database' => mt_rand(1000, 10000)]);
            $u = false;
            try {
                $reflection->invoke($kv);
            } catch (KeyValueException $e) {
                /** @noinspection PhpUndefinedVariableInspection */
                $this->assertNotEquals($code1, $e->getCode());
                $u = true;
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis does not throw Exception on malformed database');
            }
            //
            $kv = new KeyValueStorageRedis((object) ['database' => 1]);
            $reflection->invoke($kv);
        }

        function testRedis_send() {
            $kv = new KeyValueStorageRedis((object) []);
            $u = false;
            $reflection_socket = new ReflectionProperty('KeyValueStorageRedis', '_socket');
            $reflection_socket->setAccessible(true);
            $reflection = new ReflectionMethod('KeyValueStorageRedis', 'lazy_init');
            $reflection->setAccessible(true);
            $reflection->invoke($kv);
            $socket = $reflection_socket->getValue($kv);
            $this->assertNotNull($socket);
            @socket_shutdown($reflection_socket->getValue($kv));
            @socket_close($reflection_socket->getValue($kv));
            try {
                $kv->redis_send("info");
            } catch (KeyValueException $e) {
                $u = true;
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis::redis_send does not throw Exception on closed socket');
            }
        }

        function testRedis_recv() {
            $kv = new KeyValueStorageRedis((object) ['timeout' => 2]);
            $reflection = new ReflectionMethod('KeyValueStorageRedis', 'lazy_init');
            $reflection->setAccessible(true);
            $reflection->invoke($kv);
            $reflection2 = new ReflectionMethod('KeyValueStorageRedis', 'redis_recv');
            $reflection2->setAccessible(true);
            $reflection_settings = new ReflectionProperty('KeyValueStorageRedis', 'settings');
            $reflection_settings->setAccessible(true);
            $u = false;
            /**
             * @var double $ts1
             * @var double $ts2
             */
            $ts1 = microtime(true);
            try {
                $reflection2->invoke($kv);
            } catch (KeyValueException $e) {
                $ts2 = microtime(true);
                $code1 = $e->getCode();
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis::redis_recv does not throw Exception on timeout');
            }
            $this->assertGreaterThan(2, $ts2 - $ts1);
            $this->assertLessThan(2.2, $ts2 - $ts1);
            //
            $kv->redis_send('info');
            $reflection_socket = new ReflectionProperty('KeyValueStorageRedis', '_socket');
            $reflection_socket->setAccessible(true);
            socket_shutdown($reflection_socket->getValue($kv));
            socket_close($reflection_socket->getValue($kv));
            try {
                $reflection2->invoke($kv);
            } catch (KeyValueException $e) {
                $code2 = $e->getCode();
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis::redis_recv does not throw Exception on closed socket');
            }
            $this->assertGreaterThan(2, $ts2 - $ts1);
            $this->assertLessThan(2.2, $ts2 - $ts1);
            /** @noinspection PhpUndefinedVariableInspection */
            $this->assertNotEquals($code1, $code2);
            // Тестируем mode=0 с огромным чанком
            self::set_test_server_setting(['mode' => 'info_big_chunk']);
            $kv = self::get_redis();
            $kv->redis_send('info');
            $this->assertRegExp('_^Server:\r\n[a-zA-Z0-9]{1000,}(\r\n)?$_', $reflection2->invoke($kv, 0));
            //
            self::set_test_server_setting(['mode' => 'info_wait_before_crlf']);
            $kv = self::get_redis();
            $kv->redis_send('info');
            $this->assertNotFalse(strpos($reflection2->invoke($kv, 1), "\r\n"));
            //
            self::set_test_server_setting(['mode' => 'info_wait_before_crlf2']);
            $kv = self::get_redis();
            $kv->redis_send('info');
            $this->assertNotFalse(strpos($reflection2->invoke($kv, 1), "\r\n"));
            //
            $u = false;
            try {
                self::set_test_server_setting(['mode' => 'info_wait_before_crlf_timeout10']);
                $kv = self::get_redis();
                $kv->redis_send('info');
                $ts1 = microtime(true);
                $reflection_settings->setValue($kv, (object) ['timeout' => 3]);
                $reflection2->invoke($kv, 1);
            } catch (KeyValueException $e) {
                $ts2 = microtime(true);
                $this->assertRegExp('_timeout_i', $e->getMessage());
                $this->assertGreaterThan(3, $ts2 - $ts1);
                $this->assertLessThan(3 + 10, $ts2 - $ts1);
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis::redis_recv didn\'t throw Exception on waiting for crlf');
            }
        }

        function testSet_value() {
            $codes = [];
            $reflection = new ReflectionMethod('KeyValueStorageRedis', 'set_value_lowlevel');
            $reflection->setAccessible(true);
            $reflection_release = new ReflectionMethod('KeyValueStorageRedis', 'release_lock');
            $reflection_release->setAccessible(true);
            foreach (['multi_malformed', 'hmset_error', 'set_error2', 'expireat_malformed', 'expireat_malformed2',
                      'exec_malformed'] as &$mode) {
                self::set_test_server_setting(['mode' => $mode]);
                $kv = self::get_redis();
                $u = false;
                $last_code = null;
                try {
                    $reflection->invoke($kv, 'nyan', 'pasu', 60);
                } catch (KeyValueException $e) {
                    $last_code = $e->getCode();
                    $this->assertNotNull($last_code);
                    $this->assertGreaterThan(0, $last_code);
                    $this->assertNotContains($last_code, $codes);
                    $codes[] = $last_code;
                    $u = true;
                }
                if (!$u) {
                    $this->fail('KeyValueStorageRedis::set_value_lowlevel didn\'t throw Exception on mode '.$mode);
                }
                // Теперь через враппер
                $reflection_release->invoke($kv);
                $kv = self::get_redis();
                try {
                    $this->assertNull($kv->set_value('nyan', 'pasu', 60));
                } catch (KeyValueException $e) {
                    $this->assertEquals($last_code, $e->getCode());
                    $u = true;
                }
                if (!$u) {
                    $this->fail('KeyValueStorageRedis::set_value didn\'t throw Exception on mode '.$mode);
                }
            }
        }

        function testDelete_value() {
            self::set_test_server_setting(['mode' => '']);
            $kv = self::get_redis();
            $kv->delete_value('key');
            //
            self::set_test_server_setting(['mode' => 'del_error']);
            $kv = self::get_redis();
            $u = false;
            try {
                $kv->delete_value('key');
            } catch (KeyValueException $e) {
                $this->assertNotNull($e->getCode());
                $this->assertGreaterThan(0, $e->getCode());
                $u = true;
            }
            if (!$u) {
                $this->fail('KeyValueStorageRedis::delete_value didn\'t throw Exception on delete error');
            }
        }

        function testGeneral() {
            for ($i = 0; $i < 10; $i++) {// Зачем 100 раз? Потому что Redis, вот зачем
                $this->set_value_sub([]);
            }
        }

        /**
         * Время просрочки верное
         *
         * @return bool
         */
        static function time_expires_accurate() {
            return false;
        }

        /**
         * Время создания/обновление записи верное
         *
         * @return bool
         */
        static function time_create_accurate() {
            return false;
        }

        /**
         * @return KeyValueStorageRedis
         */
        private static function get_redis() {
            return new KeyValueStorageRedis((object) ['timeout' => 60, 'port' => self::$_test_server_post]);
        }

        function testGet_value_full_clear_lowlevel() {
            $reflection = new ReflectionMethod('KeyValueStorageRedis', 'get_value_full_clear_lowlevel');
            $reflection->setAccessible(true);
            $reflection2 = new ReflectionMethod('KeyValueStorageRedis', 'get_value_full_clear');
            $reflection2->setAccessible(true);
            $keys = ['malformed_hgetall', 'hgetall_malformed_123',];
            $codes = [];
            foreach ($keys as &$mode) {
                self::set_test_server_setting(['mode' => $mode]);
                $kv = self::get_redis();
                $u = false;
                try {
                    $reflection->invoke($kv, 'nyanpasu');
                } catch (KeyValueException $e) {
                    if (!preg_match('_timeout_i', $e->getMessage())) {
                        $codes[] = $e->getCode();
                        $u = true;
                    }
                    $this->assertNotNull($e->getCode());
                    $this->assertGreaterThan(0, $e->getCode());
                }
                if (!$u) {
                    $this->fail('KeyValueStorageRedis::get_value_full_clear_lowlevel didn\'t throw Exception on '.$mode);
                }
                $this->assertNull($reflection2->invoke($kv, 'nyanpasu'));
            }
            $this->assertGreaterThanOrEqual(2, count(array_unique($codes)));
            foreach (['hgetall_long_answer', 'hgetall_long_answer2'] as $mode) {
                self::set_test_server_setting(['mode' => $mode]);
                $kv = self::get_redis();
                $this->assertNotNull($reflection->invoke($kv, 'nyanpasu'));
            }
            //
            $kv = self::get_redis();
            self::set_test_server_setting(['mode' => 'empty_value',]);
            $reflection->invoke($kv, 'nyanpasu');// Здесь поведение не определено
            //
            $kv = self::get_redis();
            self::set_test_server_setting(['mode' => 'get_non64',]);
            $this->assertNull($reflection->invoke($kv, 'nyanpasu'));
            //
            $kv = self::get_redis();
            self::set_test_server_setting(['mode' => 'get_nongzip',]);
            $this->assertNull($reflection->invoke($kv, 'nyanpasu'));
            // @todo добавить  'loading'
            //
            $kv = self::get_redis();
            self::set_test_server_setting(['mode' => 'long_answer',]);
            $this->assertNull($reflection->invoke($kv, 'nyanpasu'));
            //
            $kv = self::get_redis();
            self::set_test_server_setting(['mode' => 'normal',]);
            $this->assertNotNull($reflection->invoke($kv, 'nyanpasu'));
        }

        function testGet_expires_time() {
            parent::testGet_expires_time();

            self::set_test_server_setting(['mode' => 'ttl_malformed']);
            $kv = self::get_redis();
            $this->assertNull($kv->get_expires_time('nyan'));
            self::set_test_server_setting(['mode' => 'ttl_minus']);
            $kv = self::get_redis();
            $this->assertNull($kv->get_expires_time('nyan'));
        }

        function testSet_expires_time() {
            $codes = [];
            foreach (['multi_malformed', 'expireat_malformed', 'expireat_malformed2', 'exec_malformed'] as &$mode) {
                self::set_test_server_setting(['mode' => $mode]);
                $kv = self::get_redis();
                $u = false;
                try {
                    $kv->set_expires_time('nyan', 60);
                } catch (KeyValueException $e) {
                    $this->assertNotContains($e->getCode(), $codes);
                    $codes[] = $e->getCode();
                    $this->assertNotNull($e->getCode());
                    $this->assertGreaterThan(0, $e->getCode());
                    $u = true;
                }
                if (!$u) {
                    $this->fail('KeyValueStorageRedis::set_expires_time didn\'t throw Exception on mode '.$mode);
                }
            }
            //
            self::set_test_server_setting(['mode' => 'exec_commit_set_expire']);
            $kv = self::get_redis();
            $kv->set_expires_time('nyan', 60);
            //
            self::set_test_server_setting(['mode' => 'exec_commit_set_expire_pause']);
            $kv = self::get_redis();
            $kv->set_expires_time('nyan', 60);
        }

        function testFormat_error() {
            $data = [
                [['nyan', ''], 'nyan'],
                [['nyan', '-ERR pasu'], 'nyan: pasu'],
                [['nyan', '-ERR  pasu'], 'nyan: pasu'],
                [['nyan', '-ERR pasu '], 'nyan: pasu'],
                [['nyan', '-ERR  pasu '], 'nyan: pasu'],
                [['nyan', ' -ERR  pasu '], 'nyan'],
                [['nyan', ' -ERR '], 'nyan'],
                [['nyan', '-PASU FOOBAR'], 'nyan: PASU FOOBAR'],
            ];
            foreach ($data as &$datum) {
                $ret = KeyValueStorageRedis::format_error($datum[0][0], $datum[0][1]);
                $this->assertEquals($ret, $datum[1]);
            }
        }
    }

?>